Corpse = fgui.container(Thing)

function Corpse:ctor()
	Thing.ctor(self)

	self.depth = -15

	self.avatar = Avatar.New(self)
	self.avatar.mainAction = AniConst.Die
	self.onClick:Set(self.OnTouch, self)

	self.packageSign = AniImage.New()
	self.packageSign:SetSource("effect/bag")

	self.headBar = HeadBar.New()
	self.autoRemoveCounter = Countdown.New(self.Destroy, self)
end

function Corpse:OnCreate()
	self.avatar:Create()
	self.avatar.eventDispatcher:Add(self.OnAvatarEvent, self)
	self.avatar:PlayFixedFrame(-1)

	self.onHeatBeat:Add(self.OnUpdate, self)
	
	self.data.is_corpse = true
	if self.data.items~=nil 
		and (self.data.killer==Player.me.name 
		or self.data.belong~=nil and self.data.belong==Player.data.team_id) then
		self.data.clickable = true

		self.packageSign:SetXY(0, -10)
		self.packageSign:Load()
		self:AddChild(self.packageSign)
	else
		self.data.clickable = false
	end

	if self.data.auto_remove then
		self.autoRemoveCounter:Start(self.data.auto_remove)
	else
		self.autoRemoveCounter:Reset()
	end

	if self.data.fct==nil then
		self.avatar:SetSource("npc/"..GetIdFromRid(self.data.org_rid))
	else
		self.avatar:SetSource('user/yf_5_'..self.data.fct..self.data.sex)
		self.avatar:SetWeapon('user/wq_5_'..self.data.fct..self.data.sex)
	end
end

function Corpse:OnDestroy()
	if not World.cleaningUp then
		World.RemoveThing(self)
	end

	self.packageSign:Unload()
	if self.packageSign.parent~=nil then
		self.packageSign.parent:RemoveChild(self.packageSign)
	end

	self.headBar:Destroy()
	self.avatar:Destroy()
end

function Corpse:SetDirection(value)
	if self.direction ~= value then
		self.direction = value
		self.avatar:SetDirection(value)
		self:AdjustLayout()
	end
end

function Corpse:OnAvatarEvent(type)
	if type==WEvent.AvatarReady then
		self.outline:CopyFrom(self.avatar.outline)
		self:AdjustLayout()
	end
end

function Corpse:AdjustLayout()
	local yy = -self.avatar.bodyHeights[self.direction]
	self.headBar.y = yy - 10

	if self.packageSign.parent~=nil then
		local rect = self.avatar.hitTestRects[self.direction]
		self.packageSign:SetXY(rect.x + rect.width/2,
			rect.y + rect.height/2 - 10)
	end
end

function Corpse:OnUpdate()
	self.packageSign:Draw(false)
	self.packageSign:NextFrame()
	self.autoRemoveCounter:Run()
end

function Corpse:OnTouch(context)
	if not self.data.clickable then return end

	context:StopPropagation()
	Player.ClickObject(self)
end
