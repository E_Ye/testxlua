LiveObjectPool = {}

function LiveObjectPool.GetInstance(t)
	local col = LiveObjectPool[t]
	if col==nil then
		col = {}
		LiveObjectPool[t] = col
	end
	local ret
	local len = #col
	if len>0 then
		ret = col[len]
		table.remove(col, len)
	else
		ret = t.New()
	end

	return ret
end

function LiveObjectPool.Returns(obj)
	local mt = tolua.getpeer(obj)
	if mt==nil then
		error("Returning invalid liveobject to pool")
		return
	end
	
	mt = getmetatable(mt)
	local col = LiveObjectPool[mt]
	if col==nil then return end

	table.insert(col, obj)
end