CritterRef = class()

function CritterRef:ctor()
	self._updateTime = 0
end

function CritterRef:SetIfNullOrExpired(target)
	if self._critter ~= target then
		local now = Time.time
		if self._critter == nil or now - self._updateTime > 3 then
			self._critter = target
			self._updateTime = now
			if self._critter~=nil then
				self.critterVer = self._critter.version
			else
				self.critterVer = 0
			end
		end
	end
end

CritterRef.get.critter = function(self)
	if self._critter~=nil then
		if self.critterVer ~= self._critter.version or self._critter.destroyed then
			self._critter = nil
			self.critterVer = 0
		end
	end
	return self._critter
end

CritterRef.set.critter = function(self, value)
	if self._critter ~= value then
		self._critter = value
		if self._critter ~= nil then
			self.critterVer = self._critter.version
		else
			self.critterVer = 0
		end
		self._updateTime = Time.time
	end
end