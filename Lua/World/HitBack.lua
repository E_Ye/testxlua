HitBack = class()

function HitBack:ctor(critter, tx, ty)
	self.critter = critter
	self.target = Vector2.New(tx, ty)
	self.waitingAttack = 2
	self.destroyed = false
	critter:StartFastMoving(self)
end

HitBack.get.finalPoint = function(self)
	return self.target
end

function HitBack:Play()
	self.waitingAttack = nil
	self.critter:SetDirection(UtilsDir.GetDirectionOfPath(self.target, self.critter.pathPosition, false, true))
	self.critter:PlayActionLoop(AniConst.Attacked)

	local pt = self.critter.env.pathInfo:GetRealPosition(self.target)
	self.tween = TweenUtils.TweenVector2(self.critter.xy, pt, 0.3, self.TweenUpdate, self.TweenComplete, self)
	TweenUtils.SetEase(self.tween, Ease.OutQuad)
end

function HitBack:TweenUpdate(pt)
	if self.critter==nil then return end
	self.critter:SetXY(pt.x, pt.y)
	if self.critter==World.player then
		World.NotifyCameraPositionChanged()
	end
end

function HitBack:TweenComplete()
	self.tween = nil
	local c = self.critter
	if c==nil then return end
	
	self:Destroy()
	c:TerminateFastMoving(true)
end

function HitBack:Run()
	if self.waitingAttack then
		self.waitingAttack = self.waitingAttack-Time.deltaTime
		if self.waitingAttack<0 then
			local c = self.critter
			self:Destroy()
			c:TerminateFastMoving(true)
		end
	end
end

function HitBack:Destroy()
	if self.destroyed then return end

	self.destroyed = true
	if self.tween then self.tween:Kill(false) end
	if not self.critter.destroyed then
		if not self.waitingAttack then
			if self.critter.avatar.action~=AniConst.Die or not self.critter.data.dead then
				self.critter:PlayActionLoop(-1)
			end
		end
		
		self.critter:SetPathPosition(self.target.x, self.target.y)
		self.critter.env.depthChanged = true
	end
	self.critter = nil
end