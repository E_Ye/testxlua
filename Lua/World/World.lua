require "World/WorldConsts"
require "World/Thing"
require "World/Critter"
require "World/Pet"
require "World/Mount"
require "World/FaBao"
require "World/SceneExit"
require "World/Player"
require "World/Corpse"
require "World/CommandHandlers"
require "World/Env"
require "World/TileMap"
require "World/TerminalSign"
require "World/SkillEffect"
require "World/SkillCD"
require "World/SkillPlayer"
require "World/EmitManager"
require "World/Bot"
require "World/HitBack"

local _firstIn = true
local _newPos = Vector2.New()
local _joystickStartCounter = 0
local _joystickMode = false
local _terminalSign
local _moving = false
local _toAdjustViewPos = false
local _toAdjustAnimated = true
local _shaking = 0
local _shakingTime = 0
local _shakeStartTime = 0
local _shakeOffsets = {-5,5,5,5,5,-5,-5,5,-5,-5,5,5,-3,-3,3,3,3,-3,-3,3}
local _toUpdateInView = false
local _player
local _camera
local _mapSource
local _counter1 = 0
local _mapCache = {}

World = static_class()

World.cfg_walkSpeedBase = 0.25 -- 像素/每毫秒
World.cfg_numOfSendingPoints = 3 --每次发送的走路坐标个数
World.cfg_fightingDuration = 15
World.cfg_hpBarVisibleDuration = 10
World.cfg_MapMoveStartOffsetX = 80
World.cfg_MapMoveStartOffsetY = 50
World.cfg_MapMoveSteps = 5
World.cfg_joystickStartDelay = 1.5
World.cfg_viewActionDist = 7
World.cfg_showTopBlankArea = false
World.cfg_depthSortInterval = 0.3
World.cfg_fullDepthSortInterval = 5

local function ResetThingsCollection()
	if World.thingsByType==nil then
		World.thingsByType = {}
	end
	for i=ThingType.User,ThingType.End do
		World.thingsByType[i] = {}
	end
	World.things = {}
end

function World.Create()
	ResetThingsCollection()
	World.root = Container.New()
	World.root.gameObject.name = "World"

	TileMap.Create()
	TileMap.view.onClick:Add(World.OnMapTouch)
	TileMap.view.onTouchBegin:Add(World.OnMapTouchBegin)
	World.root:AddChild(TileMap.view)

	World.envSprite = Container.New()
	World.envSprite.gameObject.name = "EnvSprite"
	World.root:AddChild(World.envSprite)

	World.envContainer = Container.New()
	World.envContainer.gameObject.name = "EnvContainer"
	World.envContainer.onClick:Add(World.OnMapTouch)
	World.envContainer.onTouchBegin:Add(World.OnMapTouchBegin)
	World.envSprite:AddChild(World.envContainer)

	World.mainEnv = Env.New()
	World.mainEnv.gameObject.name = "MainEnv"
	World.envContainer:AddChild(World.mainEnv)

	_terminalSign = TerminalSign.New()
	World.joystickDir = 0

	Stage.inst:AddChildAt(World.root, 0)
	Stage.inst.onTouchEnd:Add(World.OnStageTouchEnd)

	local scale = Stage.inst.stageHeight / 720
	World.width = math.ceil(Stage.inst.stageWidth / scale)
	World.height = 720
	
	TileMap.SetCanvasSize(World.width, World.height)
	World.envContainer.size = Vector2.New(World.width, World.height)
	World.viewRect = Rect.New()
	World.viewRect.width = World.width + 100
	World.viewRect.height = World.height + 100
	World.root:SetScale(scale, scale)

	UpdateBeat:Add(World.OnUpdate)

	InitCommandHandlers()
	SkillCD.Init()
	Bot.Init()
end

function World.EnterScene(data)
	NetClient.Pause()
	G_DeadWin:Hide()
	G_MapWin:Hide()

	IconManager.inst:Cleanup()
	SoundManager.inst:Cleanup()
	AnimationManager.inst:Cleanup()
	AssetManager.inst:Cleanup()
	--ScriptUtils.ClearMemory()

	World.sceneReady = false
	World.sceneData = data
	World.sceneDef = Defs.all.scene[data.eid]
    print("Enter scene:"..data.eid)

 	GRoot.inst:ShowModalWait()

    if _firstIn then
    	_firstIn = false
    	AssetManager.inst:LoadAsset("ui", "main")
        AssetManager.inst:LoadAsset("ui", "headbar")
    end

    _mapSource = _mapCache[World.sceneDef.img]
    if not _mapSource then
    	AssetManager.inst:LoadAsset("map", World.sceneDef.img, FairyGame.LoadAssetCallback(World.OnMapLoaded))
    else
    	World.mainEnv.pathInfo:DecodeAsync(_mapSource:get_Item("pathData"))
    end

	local col
	col = World.sceneData.users
	for _,d in ipairs(col) do
		AnimationManager.inst:LoadAnimation("user", "u"..d.fct..d.sex)
	end
	AnimationManager.inst:LoadAnimation("effect", "arrow")

	coroutine.start(World.WaitForResources)
end

function World.OnMapLoaded(assetPath, fileName, data)
	_mapSource = data
	_mapCache[World.sceneDef.img] = _mapSource
	World.mainEnv.pathInfo:DecodeAsync(_mapSource:get_Item("pathData"))
end

function World.WaitForResources()
	while World.mainEnv.pathInfo.asyncDecoding or not AssetManager.inst.isDone do
		coroutine.step()
	end

	fgui.add_timer(0.001, 1, World.EnterScene2)
end

function World.EnterScene2()
	EmitManager.Create()
	World.Reset()

	TileMap.InitMap(World.mainEnv.pathInfo.mapWidth, World.mainEnv.pathInfo.mapHeight, _mapSource:get_Item("pieces"))
	World.thumbMap = _mapSource:get_Item("thumb")

	_moving = false
	_toAdjustViewPos = false
	_toAdjustAnimated = true
	_shaking = 0
	_shakingTime = 0
	World.sceneReady = true
	World.sceneId = World.sceneData.eid

	TileMap.view:SetXY(0,0)
	World.envSprite:SetXY(TileMap.displayOffsetX, TileMap.displayOffsetY)
	World.SetViewPos(-World.envContainer.x, -World.envContainer.y, false)
    World.UpdateThings()
    
	if RoleUI.visible then
		RoleUI.Hide()
	end

	if not MainUI.visible then
		MainUI.Show()
	end
	MainUI.OnNewScene()
	
	TalkWin.Create()
	G_NPCInfoWin:Init()
	G_MapWin:OnNewScene()

	World.PlayEnvSound(World.sceneId)
	SoundManager.inst.bgSoundOn = tonumber(Player.prefs.sound_off)~=1
	SoundManager.inst.soundOn = tonumber(Player.prefs.sound_off2)~=1

	NetClient.Resume()
	GRoot.inst:CloseModalWait()

	_mapSource = nil
	_counter1 = 0
	World.OnUpdate()

	NetClient.Send(NetKeys.QCMD_READY)
end

function World.Close()
	World.Reset()

	MainUI.Hide()
	GRoot.inst:CloseAllWindows()
	SoundManager.inst:SetEnvSound(nil)
end

function World.PlayEnvSound(eid)
	local def = Defs.GetDef("scene", eid)
	if not string.isNilOrEmpty(def.sound_yq) then
		local pos = string.find(def.sound_yq, '%.')
		local str = string.sub(def.sound_yq, 1, pos-1)
		SoundManager.inst:SetEnvSound(str)
	elseif def.zid~=nil and def.zid~=eid then
		World.PlayEnvSound(def.zid)
	else
		SoundManager.inst:SetEnvSound(nil)
	end
end

function World.UpdateThings()
	local col

	col = World.sceneData.users
	for _,d in ipairs(col) do
		if d.self then
			World.AddCritter(d)
		end
	end

	for _,d in ipairs(col) do
		if not d.self then
			World.AddCritter(d)
		end
	end

	col = World.sceneData.robots
	if col~=nil then
		for _,d in ipairs(col) do
			World.AddCritter(d)
		end
	end

	col = World.sceneData.exits
	if col~=nil then
		for _,d in ipairs(col) do
			World.AddExit(d)
		end
	end
end

local g_index = 0
function World.AddThing(t, id, defId, thingType, typedIndex, posX, posY, data, env, owner)
	local thing = World.things[id]
	if thing~=nil then
		thing:Destroy()
	end

	thing = LiveObjectPool.GetInstance(t)
	if not string.isNilOrEmpty(defId) then
		thing.defId = defId
	else
		thing.defId = GetIdFromRid(id)
	end
	thing.thingType = thingType
	if typedIndex < 0 then
		thing.typedIndex = g_index+1
		g_index = g_index +1
	else
		thing.typedIndex = typedIndex
	end
	thing.name = id
	thing.owner = owner
	thing:Create(data)
	if env==nil then
		thing.env = World.mainEnv
	else
		thing.env = env
	end
	thing:SetPathPosition(posX, posY)
	thing:SetScale(1,1)
	World.things[thing.name] = thing
	World.thingsByType[thing.thingType][thing.typedIndex] = thing
	thing:ProcessFlags()
	return thing
end

function World.AddCritter(aData)
	local defId
	local rid
	local thingType
	if aData.rid~=nil then
		rid = aData.rid
		defId = GetIdFromRid(rid)
		thingType = ThingType.NPC
	else
		rid = aData.id
		defId = 'u'..aData.fct..aData.sex
		thingType = ThingType.User
	end
	local critter = World.AddThing(Critter, rid, defId, thingType, -1,
		aData.x, aData.y, aData, World.mainEnv)

	if aData.self then
		Player.Attach(critter)
	end
end

function World.AddExit(aData)
	if aData.go_target==nil then
		aData.go_target = aData.id
	end
	local exit = World.AddThing(SceneExit, aData.id, aData.id, ThingType.Exit, -1,
		aData.x, aData.y, aData, World.mainEnv)
	exit.to_eid = aData.to_eid
	return exit
end

function World.AddCorpse(aData)
	local corpse = World.AddThing(Corpse, aData.rid, aData.rid, ThingType.Corpse, -1, 
		aData.x, aData.y, aData, World.mainEnv)
	local thing = World.GetThing(aData.org_rid)
	if thing~=nil then
		if thing.inView then
			thing.data.corpse = corpse
			corpse:SetDisplay(false)
			thing:Die(true)
		else
			thing:Destroy()
		end
	end
	return corpse
end

World.get.player = function()
	return _player
end

World.set.player = function(value)
	if _player == _camera then
		World.camera = nil
	end

	_player = value
	if _player ~= nil then
		World.camera = _player
	end
end

function World.IsPlayer(value)
	return value == _player
end

World.get.camera = function()
	return _camera
end

World.set.camera = function(value)
	_camera = value
	if _camera ~= nil then	
		_toUpdateInView = true
		World.AdjustViewPos(false)
		World.NotifyCameraPositionChanged()
	end
end

World.get.cleaningUp = function()
	return _cleaningUp
end

World.get.moving = function()
	return _moving
end

function World.RemoveThing(thing)
	thing:Destroy()
	if thing.thingType ~= 0 then
		World.thingsByType[thing.thingType][thing.typedIndex] = nil
	end
	World.things[thing.name] = nil
	if thing.parent ~= nil then
		thing.parent:RemoveChild(thing)
	end
end

function World.RemoveThingByIndex(thingType, index)
	local thing = World.thingsByType[thingType][index]
	if thing ~= nil then
		World.RemoveThing(thing)
	end
end

function World.RemoveThingsOnEnv(env)
	local arr = {}
	for k,v in pairs(World.things) do
		table.insert(arr, k)
	end
	for _,k in ipairs(arr) do
		local thing = World.things[k]
		if thing.env == env then
			thing:Destroy()
		end
	end
end

function World.GetThing(rid)
	return World.things[rid]
end

function World.GetThingByIndex(thingType, index)
	return World.thingsByType[thingType][index]
end

function World.FindThingByDefId(defId)
	for k,v in pairs(World.things) do
		if v.defId == defId then
			return v
		end
	end
end

function World.FindThingByTypeAndDefId(thingType, defId)
	for _,thing in pairs(World.things) do
		if thing.thingType == thingType and thing.defId == defId then
			return thing
		end
	end
end

function World.FindNearestThingById(id)
	local min = 1000000000
	local result
	for _,thing in pairs(World.things) do	
		if GetIdFromRid(thing.name)==id and not thing.data.dead then
			local dist =  thing.distToCamera
			if dist<min then
				min = dist
				result = v
			end
		end
	end
	
	return result
end

function World.FindNearestCritterById(id) 
	local min = 1000000000
	local result
	local col = World.thingsByType[ThingType.NPC]
	local npc
	for _,thing in pairs(col) do
		if not thing.data.dead and (id=="*" or GetIdFromRid(thing.name)==id) then
			local dist =  thing.distToCamera
			if dist<min then
				min = dist
				if not thing.data.warrior then --优先寻找NPC，而不是挑战刷出的怪物
					npc = thing
				end
				result = thing
			elseif npc==nil or not thing.data.warrior then
				npc = thing
			end
		end
	end
	
	if npc~=nil then
		return npc
	else
		return result
	end
end

function World.FindExitTo(eid)
	local col = World.thingsByType[ThingType.Exit]
	for _,thing in pairs(col) do	
		if thing.to_eid == eid then
			return thing
		end
	end
end

function World.FindExitOn(ex, ey)
	local col = World.thingsByType[ThingType.Exit]
	for _,thing in pairs(col) do		
		if thing.outline:Contains(ex - thing.x, ey - thing.y) then			
			return thing
		end
	end
end

function World.Reset()
	_cleaningUp = true

	World.DisposeThings()
	ResetThingsCollection()
	World.mainEnv:ClearThings()

	if _terminalSign.parent ~= nil then
		_terminalSign.parent:RemoveChild(_terminalSign)
	end

	_camera = nil
	_player = nil
	_cleaningUp = false
	World.sceneReady = false
end

function World.DisposeThings()
	local arr = {}
	for k,v in pairs(World.things) do
		table.insert(arr, k)
	end
	for _,k in ipairs(arr) do
		local thing = World.things[k]
		thing:Destroy()
	end
end

function World.ShowTerminalSign(px, py)
	local pt = _player.env.pathInfo:GetRealPosition(Vector2.New(px, py))
	_terminalSign.xy = pt
	if _player.env.pathInfo:IsAlpha(Vector2.New(px, py)) then
		_terminalSign.alpha = 0.6
	else
		_terminalSign.alpha = 1
	end
	_player.env:AddChild(_terminalSign)
end

function World.HideTerminalSign()
	if _terminalSign.parent ~= nil then
		_terminalSign.parent:RemoveChild(_terminalSign)
	end
end

function World.UpdateThingsViewState()
	_toUpdateInView = true
end

function World.Shake(time)
	time = time or 0
	_shakingTime = time
	_shaking = 1
	_shakeStartTime = Timers.time
end

function World.SetViewPos(newX, newY, animation, isCenter)
	if isCenter then	
		newX = newX - World.width / 2
		newY = newY - World.height / 2
	end

	if newX + World.width > World.mainEnv.pathInfo.mapWidth then
		newX = math.floor(World.mainEnv.pathInfo.mapWidth - World.width)
	end
	if newX < 0 then
		newX = 0
	end

	if newY + World.height > World.mainEnv.pathInfo.mapHeight then
		newY = math.floor(World.mainEnv.pathInfo.mapHeight - World.height)
	end
	if newY < 0 then
		newY = 0
	end
	World.viewRect.x = newX - 50
	World.viewRect.y = newY - 50

	if not animation then	
		World.envContainer:SetXY (-newX, -newY)
		_moving = false
		_toUpdateInView = true
		TileMap.SetViewPos(newX, newY)
	else	
		_newPos.x = -newX
		_newPos.y = -newY
		_moving = true
	end
	if World.sceneId ~= nil and World.sceneReady then
		World.NotifyMapMoved()
	end
end

function World.AdjustViewPos(animation)
	if animation==nil then animation=true end
	_toAdjustViewPos = true
	if _toAdjustAnimated then
		_toAdjustAnimated = animation
	end
end

function World.OnUpdate()
	if not World.sceneReady then return end --这个return很重要

	if _toAdjustViewPos and _camera ~= nil then	
		local ani = _toAdjustAnimated
		_toAdjustViewPos = false
		_toAdjustAnimated = true

		local cameraX = _camera.x
		local cameraY = _camera.y
		local borderX = 0
		local borderY = 0
		borderX = World.cfg_MapMoveStartOffsetX
		borderY = World.cfg_MapMoveStartOffsetY

		local newX, newY
		if not ani then		
			newX = cameraX - World.width / 2
			newY = cameraY - World.height / 2
		else		
			local centerX, centerY
			centerX = -World.envContainer.x + World.width / 2
			centerY = -World.envContainer.y + World.height / 2

			local flag = 0
			if math.abs(centerX - cameraX) > borderX then			
				if centerX > cameraX then
					centerX = cameraX + borderX
				elseif centerX < cameraX then
					centerX = cameraX - borderX
				end
			else
				flag = flag+1
			end

			if math.abs(centerY - cameraY) > borderY then			
				if centerY > cameraY then
					centerY = cameraY + borderY
				elseif centerY < cameraY then
					centerY = cameraY - borderY
				end
			else
				flag = flag+1
			end

			if flag == 2 then			
				_toUpdateInView = true
				World.NotifyMapMoved()
				return
			end

			newX = centerX - World.width / 2
			newY = centerY - World.height / 2
		end

		World.SetViewPos(newX, newY, ani)
	end

	_counter1 = _counter1 + Time.deltaTime
	if _toUpdateInView and _counter1>0.3 then
		_counter1 = 0
	--update all thing inViewState	
		_toUpdateInView = false
		for _,v in pairs(World.things) do		
			v:UpdateInViewState()
		end
	end

	if _joystickStartCounter > 0 then	
		_joystickStartCounter = _joystickStartCounter-Time.deltaTime
		if _joystickStartCounter <= 0 then
			_joystickMode = true
		end
	end

	if _joystickMode then
		World.HandleJoystick()
	end

	if _moving then	
		local deltaX = _newPos.x - World.envContainer.x
		local deltaY = _newPos.y - World.envContainer.y
		local xEnd = math.abs(deltaX)<1
		local yEnd = math.abs(deltaY)<1
		local nx
		local ny
		if xEnd then
			nx = _newPos.x
		elseif deltaX > 0 then
			nx = World.envContainer.x + math.ceil(deltaX / World.cfg_MapMoveSteps)
		elseif deltaX < 0 then
			nx = World.envContainer.x + math.floor(deltaX / World.cfg_MapMoveSteps)
		end
		if yEnd then
			ny = _newPos.y
		elseif deltaY > 0 then
			ny = World.envContainer.y + math.ceil(deltaY / World.cfg_MapMoveSteps)
		elseif deltaY < 0 then
			ny = World.envContainer.y + math.floor(deltaY / World.cfg_MapMoveSteps)
		end
		World.envContainer:SetXY(nx, ny)
		TileMap.SetViewPos(-nx, -ny)
		if xEnd and yEnd then
			_moving = false
		end
		_toUpdateInView = true
	end

	if _shaking ~= 0 then	
		local ax = 0
		local ay = 0
		if _shaking ~= 0 then		
			ax = _shakeOffsets[_shaking - 1][0]
			ay = _shakeOffsets[_shaking - 1][1]
			_shaking = _shaking+1
			if _shaking == #_shakeOffsets then			
				if _shakingTime > 0 and (Timers.time - _shakeStartTime) < _shakingTime * 1000 then				
					_shaking = 1
				else				
					_shaking = 0
				end
			end

			TileMap.view:SetXY(ax, ay)
		end
	end

	World.mainEnv:OnUpdate()
	_terminalSign:OnUpdate()
end

function World.OnMapTouch(context)
	if not World.sceneReady then return end

	local env
	if _player ~= nil then
		env = _player.env
	else
		env = World.mainEnv
	end
	local pt = Vector2.New(context.inputEvent.x, context.inputEvent.y)
	pt = World.envSprite:GlobalToLocal(pt)
	local xx = pt.x - World.envContainer.x
	local yy = pt.y - World.envContainer.y
	pt = env.pathInfo:GetPathPosition(Vector2.New(xx, yy))
	Player.ClickPoint(pt, true)
end

function World.OnMapTouchBegin()
	if not World.sceneReady then return end

	World.joystickDir = 0
	_joystickStartCounter = World.cfg_joystickStartDelay
end

function World.OnStageTouchEnd()
	_joystickStartCounter = 0
	if _joystickMode then	
		_joystickMode = false
		World.joystickDir = 0
	end
end

function World.HandleJoystick()
	if _player == nil then return end
	local deg = UtilsDir.GetAngle(Vector2.New(_player.screenX, _player.screenY), Stage.inst.touchPosition)
	local dir = UtilsDir.GetDirOfDeg(deg)

	World.joystickDir = dir
	if _player ~= nil then		
		_player:ClearPendingWalkPath()
		if not _player.walking then
			_player:SetDirection(World.joystickDir)
		end
		local pt = _player.env.pathInfo:GetForwardPoint(_player.pathPosition, World.joystickDir)
		if pt ~= _player.pathPosition then
			Player.ClickPoint(pt, false)
		end
	end
end

function World.UpdateSize()
	TileMap.SetCanvasSize(World.width, World.height)
	World.envContainer.hitArea = Rect.New(0, 0, World.width, World.height)

	World.viewRect.width = World.width + 100
	World.viewRect.height = World.height + 100
	World.envSprite.x = TileMap.displayOffsetX
	World.envSprite.y = TileMap.displayOffsetY
	if World.sceneReady then	
		if _camera ~= nil then
			World:AdjustViewPos(false)
		else
			World:SetViewPos(-World.envContainer.x, -World.envContainer.y, false)
		end
	end
end

function World.NotifyMapMoved()
	G_MapWin:NotifyMapMoved()
end

function World.NotifyCameraPositionChanged()
end