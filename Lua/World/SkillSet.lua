SkillSet = class()

function SkillSet:ctor()
	self.pointer = 0
end

function SkillSet:SetOwner(owner)
	self.owner = owner
	self.basicSkill = Defs.basicSkills[owner.data.fct]
	self:LoadHotkeySkills()
end

function SkillSet:LoadHotkeySkills()
	if self.skills==nil or #self.skills>0 then
		self.skills={}
	end

	local main_list = self.owner.data.main_list
	if main_list ~= nil and #main_list > 0 then
	   	for _,v in ipairs(main_list) do
	   		local skill = Defs.GetDef("skill", GetIdFromRid(v.rid or v.id), true)
	       	if skill~=nil then
	       		table.insert(self.skills, skill)
	       	end
	   	end
	end
end

function SkillSet:Add(skill)
	table.insert(self.skills, skill)
end

function SkillSet:GetNext(minRange)
	if not Bot.active then
		return self.basicSkill
	end

	if #self.skills>0 then
		if self.pointer<0 then
			self.pointer = #self.skills
		end

		local i = self.pointer
		while true do
			i = i + 1
			if i>=#self.skills then
				i = 0
			end

			if self:Validate(self.skills[i+1], minRange) then
				self.pointer = i
				return self.skills[i+1]
			end
			if i==self.pointer then
				break
			end
		end
	end
	
	return self.basicSkill
end

function SkillSet:Validate(skill, minRange)
	if not SkillCD.CanUse(skill.id) then
		return false
	end

	if skill.is_learn then
		return false
	end

	if minRange~=nil and skill.attack_distance and skill.attack_distance<minRange then
		return false
	end

	if skill.lastFailedTime and skill.lastFailedTime~=0 and Time.unscaledTime-skill.lastFailedTime<60 then
		return false
	end

	return true
end