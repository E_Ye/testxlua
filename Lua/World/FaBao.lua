require "World/LiveObject"
require "World/Avatar"

FaBao = fgui.container(LiveObject)

local get = tolua.initget(FaBao)
local set = tolua.initset(FaBao)  

function FaBao:ctor()
	LiveObject.ctor(self)

	self.touchable = false
	self.avatar = Avatar.New(self)
end

function FaBao:OnCreate()
	self.owner.eventDispatcher:Add(self.OnOwnerEvent, self)
	self.onHeatBeat = self.owner.onHeatBeat  --心跳与宿主保持一致

	self.defId = GetIdFromRid(self.data.rid)

	self.avatar:Create()
	self.avatar.active = self.owner.inView
	self.avatar:SetSource("magic/"..self.defId)
	self.avatar.eventDispatcher:Add(self.OnAvatarEvent, self)

	self:SetXY(55, -self.owner.bodyHeight+10)
	self.owner:AddEffect(self, "front")
end

function FaBao:OnDestroy()
	if self.parent~=nil then
		self.parent:RemoveChild(self)
	end

	self.avatar:Destroy()
	self.owner.eventDispatcher:Remove(self.OnOwnerEvent, self)

	self.owner = fgui.null
end

function FaBao:OnOwnerEvent(type)
	if type==WEvent.DirectionChanged then
		self.avatar:SetDirection(self.owner.direction)
	end

	if type==WEvent.DirectionChanged or type==WEvent.EnterView or type==WEvent.LeaveView then
		self.eventDispatcher(type)
	end
end

function FaBao:OnAvatarEvent(type)
	if type==WEvent.ActionFire then
		self.eventDispatcher(WEvent.ActionFire)
	end
end

function FaBao:Attack(target, skillId, attackData)
	if not self.owner.inView then
		return false
	end

	self:PlayActionOnce(AniDef.Attack, true, false)
	return true
end

get.bodyHeight = function(self)
	return self.avatar.bodyHeights[5]
end

get.firePoint = function(self)
	return self.avatar.firePoint
end

get.envX = function(self)
	return self.owner.x+self.x
end

get.envY = function(self)
	return self.owner.y-self.owner.distFromGround+self.y
end