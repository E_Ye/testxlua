require "Common/Flags"
require "Common/Countdown"

require "World/CritterRef"
require "World/WalkSupport"
require "World/HeadBar"
require "World/Shadow"

Critter = fgui.container(Thing)

local get = tolua.initget(Critter)
local set = tolua.initset(Critter)

function Critter:ctor()
	Thing.ctor(self)

	self.gameObject.name = "Critter"
	self.home = World.mainEnv.cachedTransform

	--主要容器
	self.sprite = Container.New()
	self.sprite.onClick:Add(self.OnTouch, self)
	self:AddChild(self.sprite)

	--纸娃娃
	self.avatar = Avatar.New(self, self.sprite)
	--脚下阴影
	self.shadow = Shadow.New()
	--头顶名字和图标等
	self.headBar = HeadBar.New(self)
	self.sprite:AddChild(self.headBar)

	--定身标志
	self.dingShen = Flags.New()
	--是否锁定变身（即不允许变身）
	self.shellLock = Flags.New()
	--升高的高度（例如坐骑升起，或者跳跃）
	self.liftingHeight = Flags.New()
	--敌人，如果被攻击，就会设置这个
	self.enemy = CritterRef.New()
	--快速移动处理，例如击退
	self.fastMoving = fgui.null
	--脱离战斗状态倒计时
	self.fightingCounter = Countdown.New(self.LeaveFightingMode, self)
	--头顶说话倒计时
	self.speakCounter = Countdown.New(self.SpeakSomething, self)
	--头顶说话结束倒计时
	self.speakOverCounter = Countdown.New(self.SpeakOver, self)
	--在一定时间内设置当前的敌人
	self.enemyCounter = Countdown.New(self.enemy.SetIfNullOrExpired, self.enemy)
	--在一定时间内显示血条
	self.hpBarVisibleCounter = Countdown.New(self.headBar.ShowHpBar, self.headBar, false)
	--死亡过程
	self.dyingCounter = Countdown.New(self.Destroy, self)

	--走路支持
	self.walkSupport = WalkSupport.New(self)
	self.walkingPath = self.walkSupport.walkingPath

	--坐骑对象
	self.mount = fgui.null
	--宠物对象
	self.pet = fgui.null
	--法宝对象
	self.fabao = fgui.null
end

function Critter:OnCreate()
	self.onHeatBeat:Add(self.OnUpdate, self)

	self.shellLock:Clear()
	self.dingShen:Clear()
	self.liftingHeight:Clear()

	self.outline:Set(-17,-85,34,80)

	self.fightingCounter:Reset()
	self.speakCounter:Reset()
	self.speakOverCounter:Reset()
	self.enemyCounter:Reset()
	self.hpBarVisibleCounter:Reset()
	self.dyingCounter:Reset()

	self.walking = false
	self.walkingLock = false
	self.shellChanged = false
	self.notifyFireFrame = false
	self.iconSrc = ''
	self.aniSrc = ''

	self.nextPlayAction = -1
	self.adjustWalkActionBySpeed = true
	self.direction = ScriptUtils.RandomInt(4, 7)
	
	self.standActionCode = AniConst.Stand
	self.walkActionCode = AniConst.Walk
	self.attackActionCode = AniConst.Attack
	self.loopingActionCode = -1
	self.playingOnceAction = false

	self.walkSupport:Create()
	self.walkSupport.supportCancelWalkingSteps = self.thingType==ThingType.User
	self.moving = false
	self.savedWalkTarget = fgui.null
	self.movingTarget = fgui.null
	self.actionType = 0
	self.actionDistance = 0

	self.hurted = false
	self.enemyGroupAttacking = false
	self.skillRange = 4	

	self.alpha = 1
	self.visible = true

	self.shadow:Create()
	self.headBar:Create()

	self.sprite.alpha = 1
	self.sprite.visible = true
	self.sprite:SetXY(0, 0)
	self.headBar.y = -120

	self.avatar:Create()
	self.avatar:SetAction(self.standActionCode)
	self.avatar:SetDirection(self.direction)
	self.avatar.eventDispatcher:Add(self.OnAvatarEvent, self)
end

function Critter:ProcessFlags()
	self:UpdateAvatar()
	self:UpdateDisplayName()

	self.data.warrior = self.thingType==ThingType.NPC and self.data.kill
	self.data.clickable = not self.data.self and not self.data.self_pet

	if self.data.task then
		self.headBar:SetIcon("task"..self.data.task)
	end

	if self.data.team then
		self.headBar:SetIcon("team")
	end

	if self.data.mount then
		self:RideOnMount(self.data.mount)
	end

	if self.data.pet then
		self:CallOutPet(self.data.pet)
	end

	if self.data.magic then
		self:CallOutFaBao(self.data.magic)
	end
end

function Critter:OnDestroy()
	if self.fastMoving then
		self:TerminateFastMoving()
	end

	--if self.walkingPath.length > 0 then
		--self.pathPosition = self.walkingPath.last
	--end
	self.walkSupport:Destroy()

	self.eventDispatcher(WEvent.Destroy)
	self.headBar:Destroy()
	self.shadow:Destroy()
	self.avatar:Destroy()

	if not World.cleaningUp then
		self:SetMovingTarget(nil)
		World.RemoveThing(self)
	end

	--注销可能对其他对象的引用
	self.enemy.critter = nil
	self.movingTarget = fgui.null

	self:JumpDownMount()
	self:CallBackPet()
	self:CallBackFaBao()

	if not World.cleaningUp then
		local corpse = self.data.corpse
		if corpse then
			corpse:SetPathPosition(self.pathPosition.x, self.pathPosition.y)
			corpse:SetDirection(UtilsDir.GetDiagonalDirection(self.direction))
			corpse:SetDisplay(true)
			self.data.corpse = nil

			if not self.data.killer then
				local killer_id = corpse.data.killer
				local killer = World.GetThing(killer_id)
				if killer~=nil then
					self.data.killer = killer.data.name
				end
			end
		end

		if self.data.self then
			G_DeadWin:Open(self.data.killer)
		end
	end
end

get.isPlayer = function(self)
	return World.player == self
end

--@private
function Critter:UpdateAvatar()
	if self.thingType==ThingType.NPC then
		self.iconSrc = Defs.GetString("robot", self.defId, "src_id", self.defId)
	elseif self.thingType==ThingType.Pet then
		self.iconSrc = Defs.GetString("pet", self.defId, "src_id", self.defId)
	elseif self.thingType==ThingType.User then
		self.iconSrc = "ui://Sucai/u"..self.data.fct..self.data.sex
	else
		self.iconSrc = self.defId
	end

	if not string.isNilOrEmpty(self.data.src_id) then
		self.iconSrc = self.data.src_id
	end

	local c = string.sub(self.iconSrc, 1, 1)
	if c=="u" then
		self.aniSrc = 'user/u'..self.data.fct..self.data.sex
		self.avatar:SetClothing('user/yf_1_'..self.data.fct..self.data.sex)
		self.avatar:SetWeapon('user/wq_1_'..self.data.fct..self.data.sex)
	elseif c=="p" then
		self.aniSrc = "pet/"..self.iconSrc
	else
		self.aniSrc = "npc/"..self.iconSrc
	end

	self.avatar:SetSource(self.aniSrc)
end

--@private
function Critter:UpdateDisplayName()
	local name

	if self.thingType==ThingType.NPC and self.data.name==nil then
		self.data.name = Defs.GetString("robot", self.defId, "name")
	end

	if self.thingType==ThingType.Pet then
		name =self.data.name.."\n"..self.owner.data.name..'的宠物'
	else
		name = self.data.name
	end

	if self.data.self then
		self.headBar:SetTitle('[color=#ffff00]'..name..'[/color]')
	elseif self.thingType==ThingType.NPC then
		if self.data.warrior then
			self.headBar:SetTitle('')
		else
			self.headBar:SetTitle('[color=#80ff4d]'..name..'[/color]')
		end
	else
		self.headBar:SetTitle('[color=#2bf5f5]'..name..'[/color]')
	end
end

function Critter:SpeakSomething()
	local word
	if self.fightingCounter.time ~= 0 then
		word = Defs.GetNPCSpeakWord(self.defId, true)
		if self.inView and not string.isNilOrEmpty(word) then
			self.headBar:Speak(word)
			self.speakOverCounter:Start(3)
			self.speakCounter:Start(10+ScriptUtils.RandomInt(0,5))
		end
	else
		word = Defs.GetNPCSpeakWord(self.defId, false)
		if self.inView and not string.isNilOrEmpty(word) then
			self.headBar:Speak(word)
			self.speakOverCounter:Start(6)
			self.speakCounter:Start(40+ScriptUtils.RandomInt(0,30))
		end
	end
end

function Critter:SpeakOver()
	self.headBar:Speak(nil)
end

get.idle = function(self)
	return not self.moving and not self.movingTarget and not self.fastMoving
end

------------------------------event from avatar---------------------------
--@private
function Critter:OnAvatarEvent(type)
	if type==WEvent.ActionEnd then
		if self.data.dead and self.dyingMode==3 then
			self:Destroy()
		elseif self.nextPlayAction ~= -1 then		
			self.playingOnceAction = true
			self.avatar:SetAction(self.nextPlayAction)
			self.notifyFireFrame = self.nextNotifyFireFrame
			self.nextPlayAction = -1
		else
			self.walkingLock = false
			self.playingOnceAction = false
			if self.loopingActionCode ~= -1 then
				self.avatar:SetAction(self.loopingActionCode)
			elseif self.walking then
				self.avatar:SetAction(self.walkActionCode)
			else
				self.avatar:SetAction(self.standActionCode)
			end
		end
	elseif type==WEvent.ActionFire then
		self.eventDispatcher(WEvent.ActionFire)	
	elseif type==WEvent.AvatarReady then
		self.outline:CopyFrom(self.avatar.outline)
		self.outline:Multiply(self.sprite.scaleX)
		self:AdjustLayout()
		self.eventDispatcher(WEvent.OutlineChanged)
	end
end

------------------------------Animation Support-------------------------------
function Critter:SetDirection(value)
	if self.direction ~= value then
		self.direction = value
		self.avatar:SetDirection(value)
		self:AdjustLayout()
		self.eventDispatcher(WEvent.DirectionChanged)
	end
end

function Critter:SetShell(value)
	if value=='' then value=nil end
	self.data.shell = value
	self.shellChanged = true
end

function Critter:LockShell(requestor)
	self.shellLock:Set(requestor)
	if self.data.shell then
		self.shellChanged = true
	end
end

function Critter:UnlockShell(requestor)
	self.shellLock:Unset(requestor)
	if self.data.shell then
		self.shellChanged = true
	end
end

--@private
function Critter:UpdateShell()
	self.shellChanged = false
	if self.data.shell and self.shellLock.isTrue then
		self.avatar:SetSource(self.data.shell)
	else
		self.avatar:SetSource(self.aniSrc)
	end
	if self.avatar.action == AniConst.Prepare then
		self.avatar:SetAction(AniConst.Stand)
	end
end

function Critter:SetStandActionCode(value)
	if self.avatar.action == self.standActionCode then
		self.avatar:SetAction(value)
	end
	self.standActionCode = value
end

function Critter:SetWalkActionCode(value)
	self.walkActionCode = value
end

function Critter:PlayActionLoop(action)
	local f = self.notifyFireFrame and action == -1
	if action == -1 then
		if self.walking then
			action = self.walkActionCode
		elseif self.fightingCounter.time ~= 0 and action == AniConst.Stand then
			action = AniConst.Prepare
		else
			action = self.standActionCode
		end

		self.loopingActionCode = -1
	else
		self.loopingActionCode = action
	end
	if not f then
		self.avatar:SetAction(action)
	end
	self.avatar:CancelFrameLock()
	self.walkingLock = false
end

function Critter:PlayActionOnce(action, notifyFireFrame, overrides)
	if notifyFireFrame==nil then notifyFireFrame = false end
	if overrides==nil then overrides = true end
	if self.playingOnceAction and not overrides then
		self.nextPlayAction = action
		self.nextNotifyFireFrame = notifyFireFrame
		return
	end
	self.walkingLock = true
	self.playingOnceAction = true
	if not self.notifyFireFrame then
		self.notifyFireFrame = notifyFireFrame
	end
	self.avatar:SetAction(action)
	self.avatar:CancelFrameLock()
	self.avatar.notifyFireFrame = self.notifyFireFrame
end

function Critter:CancelWalkingLock()
	self.walkingLock = false
end

function Critter:PlayFixedFrame(action, index)
	self.avatar:SetAction(action)
	self.avatar:PlayFixedFrame(index)
	self.walkingLock = true
end

---------------------------Position Support------------------------	
function Critter:SetPathPosition(px, py, updateXY)
	if updateXY==nil then updateXY = true end
	if updateXY then
		self.walkSupport.walkingStepLeft = false
	end
	if self.fastMoving then
		self:TerminateFastMoving()
	end
	Thing.SetPathPosition(self, px, py, updateXY)
end

function Critter:SetRealPosition(x, y)
	Thing.SetRealPosition(self, x, y)
	self.shadow:SetXY(self.x, self.y)
end

get.serverPosition = function(self)
	if self.fastMoving then
		return self.fastMoving.finalPoint
	elseif self.walkingPath.length == 0 then
		return self.pathPosition
	else
		return self.walkingPath.last
	end
end

---------------------------Outline Support------------------------
get.bodyHeight = function(self)
	return self.avatar.bodyHeights[5]
end

get.distFromGround = function(self)
	return self.liftingHeight.value
end


get.firePoint = function(self)
	return self.avatar.firePoint
end

function Critter:SetLiftingHeight(requestor, value)
	self.liftingHeight:Set(requestor, value)
	self.sprite.y = -self.liftingHeight.value
end

function Critter:UnsetLiftingHeight(requestor)
	self.liftingHeight:Unset(requestor)
	self.sprite.y = -self.liftingHeight.value
end

--@private
function Critter:AdjustLayout()
	local yy = -self.avatar.bodyHeights[self.direction]
	self.headBar.y = yy - 10
end

-----------------------------Move Support---------------------------
get.walkEnding = function(self)
	if self.fastMoving then
		return self.fastMoving.finalPoint
	elseif self.walkSupport.supportCancelWalkingSteps or self.walkingPath.length == 0 then
		return self.pathPosition
	else
		return self.walkingPath.last
	end
end

--移动到指定目标，terminal终点坐标，showTerminator是否显示终点指示符
function Critter:MoveTo(terminal, showTerminator)
	if showTerminator==nil then showTerminator = true end
	if self.dingShen.isTrue or self.data.dead then return end

	if self.walkSupport:MoveTo(self.walkEnding, terminal) then
		if not self.timer.running then self.timer:Start() end
		self.moving = true
		if World.player == self then
			if showTerminator and not self.movingTarget then
				World.ShowTerminalSign(terminal.x, terminal.y)
			else
				World.HideTerminalSign()
			end
		end
		self:OnMovingStart()
	else
		if World.player == self then		
			World.HideTerminalSign()
		end
	end
end

--移动到指定对象，target对象，actionType到达后操作类型
function Critter:MoveToObject(target, actionType)
	if actionType==nil then actionType=ActionType.Auto end
	if self.dingShen.isTrue then return end

	if actionType == ActionType.Auto then	
		if target.thingType==ThingType.NPC or target.thingType==ThingType.User then
			if Player.CanAttack(target) then
				actionType = ActionType.Kill
			elseif Player.CanCure(target) then
				actionType = ActionType.Cure
			else
				actionType = ActionType.View
			end
		else
			actionType = ActionType.View
		end
	end
	self.actionType = actionType

	if actionType == ActionType.Kill or actionType == ActionType.Cure then
		self.actionDistance = self.skillRange
	elseif actionType == ActionType.Follow then
		self.actionDistance = 5
	else --if(actionType==ActionType.VIEW)
		self.actionDistance = World.cfg_viewActionDist
	end

	if World.player == self then
		World.HideTerminalSign()
	end

	if self.movingTarget == target then
		self:ShouldReachTarget(false)
	else
		self:SetMovingTarget(target)
		self:ShouldReachTarget(true)
	end
end

function Critter:MoveToMovingTarget()
	if not self.movingTarget then return end
	self:MoveTo(self.movingTarget.serverPosition)
end

function Critter:SetMovingTarget(target)
	if target ~= self.movingTarget then
		if self.movingTarget then
			self.movingTarget.eventDispatcher:Remove(self.OnMovingTargetChanged, self)
		end
		if target==nil then target = fgui.null end
		self.movingTarget = target
		if self.movingTarget then
			self.movingTarget.eventDispatcher:Add(self.OnMovingTargetChanged, self)
		end
	end
end

function Critter:GetDistanceToTarget(target, useServerPosition)
	if useServerPosition==nil then useServerPosition = true end
	if useServerPosition then
		return self.env.pathInfo:GetPathDistance(self.serverPosition, target.serverPosition)
	else
		return self.env.pathInfo.GetPathDistance(self.pathPosition, target.pathPosition)
	end
end

--@private
function Critter:OnMovingTargetChanged(type)
	if type==WEvent.PathPosChanged then
		if self.actionType == ActionType.Kill or self.actionType == ActionType.Cure then	
			self:ShouldReachTarget(true)
		end
	elseif type==WEvent.Die or type==WEvent.Destroy then
		self:SetMovingTarget(nil)

		if self.actionType == ActionType.Follow then
			self:MoveTo(sender.serverPosition)
		else
			self:CancelWalking(true)
		end
	end
end

--@private
function Critter:ShouldReachTarget(failedThenMove)
	local src = self.pathPosition
	local dest = self.movingTarget.serverPosition

	if src==dest or self.env.pathInfo:IsSkillReachable(src, dest, self.actionDistance) then
		self:SetDirection(self.env.pathInfo:GetDirectionOfPath(self.pathPosition, dest, false))
		self:OnReachTarget()
		return true
	end

	if failedThenMove then	
		self:MoveTo(dest)
	end

	return false
end

--可覆盖：到达目标时的处理
function Critter:OnReachTarget()
	if World.player == self then
		Player.OnReachTarget()
	elseif self.actionType ~= ActionType.Follow then 
		self:SetMovingTarget(nil)
	end
end

--可覆盖：移动开始时的处理
function Critter:OnMovingStart()
	if World.player == self then
		Player.OnMovingStart()
	end
end

--可覆盖：移动结束时的处理
function Critter:OnMovingEnd()
	if World.player == self then
		Player.OnMovingEnd()
	end
end

--由WalkSupport调用
function Critter:OnMovingStep(pt)
	if self.movingTarget then
		local lastToWalkPoint = self.walkSupport.lastToWalkPoint
		if lastToWalkPoint==nil then
			return false
		end

		if self.env.pathInfo:IsSkillReachable(pt, lastToWalkPoint, self.actionDistance) then		
			self.walkSupport:CancelWalking()
			return false
		end
	end
	return true
end

--由WalkSupport调用
function Critter:OnMovingToBlock(pt)
	self:SetMovingTarget(nil)
	return false
end

-----------------------------Walk Support----------------------------

function Critter:AddWalkSteps(path)
	if #path == 0 then return end

	if not self.timer.running then self.timer:Start() end
	self.walkSupport:AddWalkSteps(path)
end

function Critter:ClearWalkSteps(resolveTerminal)
	if resolveTerminal==nil then resolveTerminal = true end
	self.walkSupport:ClearWalkSteps(resolveTerminal)
end

function Critter:SetWalkSpeedRatio(val)
	self.walkSupport:SetWalkSpeedRatio(val)
	self.avatar:SetWalkSpeedRatio(val)
end

function Critter:StartFastMoving(provider)
	self:SaveWalkTarget()
	if self.fastMoving then
		self:TerminateFastMoving()
	end
	if provider==nil then provider=fgui.null end
	self.fastMoving = provider
	if self.movingTarget then
		self:ShouldReachTarget(false)
	end
end

function Critter:TerminateFastMoving(callInside)
	if not self.fastMoving then return end

	if not callInside then	
		local f = self.fastMoving
		self.fastMoving = fgui.null
		f:Destroy()

		self:ContinueWalkTarget()
	else
		self.fastMoving = fgui.null
	end
end

function Critter:CancelWalking(allowRestSteps)
	self.walkSupport:CancelWalking(allowRestSteps)
	self.moving = false
	self:SetMovingTarget(nil)
	if World.player == self then
		World.HideTerminalSign()
	end
end

function Critter:ClearPendingWalkPath()
	self.walkSupport:CancelWalking()
end

--跳跃或击退开始前需要保存原来的移动目标
function Critter:SaveWalkTarget()
	local lastToWalkPoint = self.walkSupport.lastToWalkPoint
	if lastToWalkPoint then	
		self.savedWalkTarget = lastToWalkPoint
		self.walkSupport:CancelWalking()
		self.savedWalkTargetEnv = self.env
		self.moving = false
	end
end

--跳跃或击退后继续原来的移动目标
function Critter:ContinueWalkTarget()
	if self.savedWalkTarget then 
		local dest = self.savedWalkTarget
		if  self.savedWalkTargetEnv ~= self.env then		
			dest = self.savedWalkTargetEnv:LocalToMainPathPosition(dest)
			dest = self.env:MainToLocalPathPosition(dest)
		end
		self.savedWalkTarget = fgui.null
		self.savedWalkTargetEnv = fgui.null
		if self.movingTarget then
			self:ShouldReachTarget(true)
		else
			self:MoveTo(dest)
			if not self.moving then
				self:OnMovingEnd()
			end
		end
	end
end

--服务端通知移动失败时的处理
function Critter:FixErrorPosition(px, py, dontTryRecover)
	if not dontTryRecover and
		(self.fastMoving and self.fastMoving.finalPoint.x == px and self.fastMoving.finalPoint.y == py
			or px == serverPosition.x and py == serverPosition.y) then
		return
	end

	local target = self.walkSupport.lastToWalkPoint

	self:CancelWalking()
	self.savedWalkTarget = fgui.null

	self:SetPathPosition(px, py)
	if not dontTryRecover and target then
		self:MoveTo(target)
	end
end

------------------------------View Support------------------------------------------
function Critter:OnEnterView()
	if self.data.warrior then
		self.speakCounter:Start(5+ScriptUtils.RandomInt(0, 60))
	else
		self.speakCounter:Start(2+ScriptUtils.RandomInt(0, 2))
	end

	Thing.OnEnterView(self)
end

function Critter:OnLeaveView()
	local toFire = false
	if self.notifyFireFrame then
		self.notifyFireFrame = false
		toFire = true
	end
	if self.nextPlayAction ~= -1 then
		self.nextPlayAction = -1
		if self.notifyFireFrame then		
			self.notifyFireFrame = false
			toFire = true
		end
	end
	if toFire then
		self.eventDispatcher(WEvent.AttackFire)
	end

	Thing.OnLeaveView(self)
end

function Critter:AddToEnv()
	if self.parent == nil then
		self.env:AddChild(self)
		self.env.depthChanged = true
		World.mainEnv:AddToBottomLayer(self.shadow, 3)
	end
end

function Critter:RemoveFromEnv()
	if self.parent ~= nil then
		self.parent:RemoveChild(self)

		if self.shadow.parent ~= nil then
			self.shadow.parent:RemoveChild(self.shadow)
		end
	end
end

function Critter:ShiftTo(targetEnv, dest)
	if self.env == targetEnv then
		if dest ~= nil then
			self:SetPathPosition(dest.x, dest.y)
		end
		return
	end

	if self.fastMoving then
		self:TerminateFastMoving()
	end

	self:CancelWalking()

	if dest then
		self:SetPathPosition(dest.x, dest.y)
	end
end

----------------------------Fight Support------------------------------------

function Critter:EnterFightingMode()
	self.fightingCounter:Start(World.cfg_fightingDuration)

	if self.loopingActionCode == -1 and self.standActionCode == AniConst.Stand and not self.avatar.isFrameLock then
		self.avatar:SetAction(AniConst.Prepare)
	end
end

function Critter:LeaveFightingMode()
	if self.standActionCode == AniConst.Prepare then				
		self.standActionCode = AniConst.Stand
		if self.avatar.action == AniConst.Prepare then
			self.avatar:SetAction(self.standActionCode)
		end
	end
	self.fightingCounter:Reset()
end

function Critter:SetSkillRange(value)
	if value==nil then value=0 end
	self.skillRange = value
	if self.actionType == ActionType.Kill then
		self.actionDistance = value
	end
end

function Critter:Attack(target, skillId, attackData)
	if not self.inView then
		return false
	end

	self:EnterFightingMode()
	if target then
		local pt = Vector2.New(target.envX, target.envY)
		if self.pathPosition~=pt then
			self:SetDirection(UtilsDir.GetDirectionOfPoints(self.xy, pt))
		end
	end

	local skill = Defs.GetDef("skill", skillId)
	if skill.deplete_attr~=nil then
		local val = self.data[skill.deplete_attr] or 0
		val = val - attackData.dep
		if val<0 then val = 0 end
		self:SetAttrValue(skill.deplete_attr, val)
	end

	if not skill.is_magic then
		local bj = ScriptUtils.BitAnd(attackData.xg or 0, 1)
		if bj and self.attackActionCode==AniConst.Attack and self.avatar.animation:GetAction(AniConst.Attack_BaoJi)~=nil then
			self:PlayActionOnce(AniConst.Attack_BaoJi, true, false)
		else
			self:PlayActionOnce(self.attackActionCode, true, false)
		end
		return true
	else
		return false
	end	
end

function Critter:Attacked(src, enemy, mainTarget, skillId, attackData, myData)
	if self.fastMoving then
		self.fastMoving:Play(skillId, attackData)
	end
	if World.player == self or enemy and enemy.isPlayer then
		self.headBar:ShowHpBar(true)
		self.hpBarVisibleCounter:Start(World.cfg_hpBarVisibleDuration)
	end
	if self.fightingCounter.time ~= 0 and (self.speakCounter.time==0 or self.speakCounter.time > 5) then
		self.speakCounter:Start(ScriptUtils.RandomInt(0, 5))
	end

	self.hurted = false
	local hurt_attr = Defs.GetString("skill", skillId, "hurt_attr")
	if myData ~= nil then
		if myData.hurt>=0 then
			self.hurted = true
			local t
			if self.data.self then t = 0 else t = 1 end
			EmitManager.Emit(self, t, myData.hurt, attackData.xg, myData.xg)
		else --加血
			EmitManager.Emit(self, t, myData.hurt)
		end

		if hurt_attr~=nil then self:SetAttrValue(hurt_attr, myData.cur) end

		if ScriptUtils.BitAnd(myData.xg or 0, 1) then
			self.data.killer = enemy.data.name
			self:Die(false)
		end
	end

	if self.hurted then
		if src ~= nil then
			self:SetEnemy(enemy, mainTarget ~= self)
		end

		local a = self.avatar.action
		if self.loopingActionCode == -1 and not self.avatar.isFrameLock
			and a ~= AniConst.Attacked
			and (a == AniConst.Stand or a == AniConst.Prepare) then
			self.avatar:SetAction(AniConst.Attacked)
		end
	end
end

function Critter:SetAttrValue(key, value)
	self.data[key] = value
	if key=="hp" or key=="max_hp" then
		self.headBar:UpdateHpBar()

		if World.player==self then
			MainUI.UpdatePlayerStatus_hp()
		elseif World.player.pet==self then
			MainUI.UpdatePetStatus_hp()
		else
			MainUI.UpdateCritterStatus(self)
		end
	elseif key=="mp" or key=="max_mp" then
		if World.player==self then
			MainUI.UpdatePlayerStatus_mp()
		elseif World.player.pet==self then
			MainUI.UpdatePetStatus_mp()
		else
			MainUI.UpdateCritterStatus(self)
		end
	elseif key=="task" then
		if self.data.task~=nil and self.data.task~=0 then
			self.headBar:SetIcon("task"..self.data.task)
		else
			self.headBar:SetIcon(nil)
		end
	elseif key=="team" then
		if value then
			self.headBar:SetIcon("team")
		else
			self.headBar:SetIcon(nil)
		end
	elseif key=="shell" then
		self:SetShell(value)
	end
end

function Critter:SetEnemy(critter, groupAttacking)
	if not groupAttacking or self.enemyGroupAttacking or self.enemy.critter == nil then
		self.enemy:SetIfNullOrExpired(critter)
		if critter ~= nil then
			self.enemyCounter:Start(3)
		else
			self.enemyCounter:Reset()
		end
		self.enemyGroupAttacking = groupAttacking
	end
end

function Critter:GetEnemy(ignoreGroupAttacking)
	local ret = self.enemy.critter
	if ret ~= nil or not ignoreGroupAttacking and self.enemyGroupAttacking then
		if self.movingTarget and self.actionType == ActionType.Kill then
			ret = self.movingTarget
		end
	end
	return ret
end

--delay表示是不明不白死亡(true)，还是被人打死的(false)。如果是被人打死的，播放死亡动作
function Critter:Die(delay)
	self.data.dead = true

	if not self.timer.running then
		self:Destroy()
		return
	end

	if delay then self.dyingMode = 2 else self.dyingMode = 1 end
	if self.dyingCounter.time~=0 then return end

	self.walkSupport:CancelWalking()
	self.dyingCounter:Start(4)
	self.eventDispatcher(WEvent.Die)
end

get.playingAttack = function(self)
	return self.avatar.action == self.attackActionCode or self.avatar.action == AniConst.Attack_BaoJi
end

---------------------------Effect Support----------------------------------

function Critter:PlayStatusEffect(statusId, effectId, duration)
	if effectId==nil or string.len(effectId)==0 then
		effectId = statusId
	end
	local ge = self.sprite:GetChild(statusId)
	if ge ~= nil then
		ge.Destroy()
	end
	ge = SkillEffect.GetInstance(effectId)
	ge.name = statusId
	ge:PlayIn(self, duration)
	return ge
end

function Critter:TerminateStatusEffect(statusId)
	local ge = self:GetStatusEffect(statusId)
	if ge ~= nil then
		ge:Destroy()
	end
end

function Critter:HasStatusEffect(statusId)
	return self:GetStatusEffect(statusId) ~= nil
end

function Critter:GetStatusEffect(statusId)
	local ge = self.sprite:GetChild(statusId)
	if ge == nil then
		ge = self:GetChild(statusId)
	end
	return ge
end

function Critter:AddEffect(effect, position)
	if position=="front" then
		--保持最后一个为Label
		self.sprite:AddChildAt(effect, self.sprite.numChildren - 1)
	elseif position== "back" then
		local i = self.sprite:GetChildIndex(self.avatar.animation)
		if i > 0 then
			self.sprite:AddChildAt(effect, i - 1)
		else
			self.sprite:AddChildAt(effect, 0)
		end
	elseif position=="bottom" then
		self:AddChildAt(effect, 0)
	end
end

-------------------------------Mount support--------------------------------
function Critter:RideOnMount(mountData)
	if self.fastMoving then
		self:TerminateFastMoving()
	end

	self.mount = LiveObjectPool.GetInstance(Mount)
	self.mount.owner = self
	self.mount:Create(mountData)
end

function Critter:JumpDownMount()
	if not self.mount then return end

	if self.fastMoving then
		self:TerminateFastMoving()
	end

	self.mount:Destroy()
	self.mount = fgui.null
end

----------------------------Pet support---------------------------------------
function Critter:CallOutPet(petData)
	if self.pet then
		self:CallBackPet()
	end
	if self.data.self then
		petData.self_pet = true
	end
	petData.is_pet = true
	self.pet = World.AddThing(Pet, petData.rid, GetIdFromRid(petData.rid), ThingType.Pet, -1,
		self.pathPosition.x, self.pathPosition.y, petData, World.mainEnv, self)
	self.pet:StandBy()	
end
	
function Critter:CallBackPet()
	if not self.pet then return end
	self.pet:Destroy()
	self.pet = fgui.null
end

function Critter:NotifyPetDestroy(pet)
	if self.pet==pet then
		if self.pet.data.self_pet then
			MainUI.ShowPetStatus()
		end
		self.pet = fgui.null
	end
end

----------------------------FaBao support---------------------------------------
function Critter:CallOutFaBao(fabaoData)
	if self.fabao then
		self:CallBackFaBao()
	end

	self.fabao = LiveObjectPool.GetInstance(FaBao)
	self.fabao.owner = self
	self.fabao:Create(fabaoData)
end
	
function Critter:CallBackFaBao()
	if not self.fabao then return end
	self.fabao:Destroy()
	self.fabao = fgui.null
end

----------------------------frame script--------------------------------------

function Critter:OnUpdate()
	if self.fastMoving then
		self.fastMoving:Run()
	elseif self.data.dead then
		if self.dyingMode==1 then
			self.avatar:CancelFrameLock()
			self:PlayActionOnce(AniConst.Die)
			self.dyingMode = 3
		elseif self.dyingMode==2 then
			self.dyingCounter:Run()
		end
		return
	end

	if self.inView then
		if self.shellChanged and (self.avatar.action == self.standActionCode or self.avatar.action == self.walkActionCode) then
			self:UpdateShell()
		end

		if self.fightingCounter.time~=0 then self.fightingCounter:Run() end
		if self.speakCounter.time~=0 then self.speakCounter:Run() end
		if self.speakOverCounter.time~=0 then self.speakOverCounter:Run() end
		if self.enemyCounter.time~=0 then self.enemyCounter:Run() end
		if self.hpBarVisibleCounter.time~=0 then self.hpBarVisibleCounter:Run() end
	end

	if not self.fastMoving and not self.walkingLock and not self.dingShen.isTrue then
		local walking = self.walkSupport:Run()
		if walking ~= self.walking then
			self.walking = walking
			if self.walking then			
				self.avatar:SetAction(self.walkActionCode)
				self.loopingActionCode = -1
			elseif self.loopingActionCode == -1 and not self.avatar.isFrameLock then			
				self.avatar:SetAction(self.standActionCode)
			end
		end

		if self.moving then		
			if not self.walking then			
				self.moving = false
				if World.player == self then				
					World.HideTerminalSign()
				end

				if self.movingTarget then
					self:OnReachTarget()
				else
					self:OnMovingEnd()
				end
			end
		end
	else
		self.walking = false
	end
end

----------------------------Event Handlers-----------------------------------
function Critter:OnTouch(context)
	if not self.data.clickable then return end

	context:StopPropagation()
	Player.ClickObject(self)
end

