Avatar = class()

function Avatar:ctor(owner, container)
	self.owner = owner
	self.fixedScale = 1
	self.mainAction = 0
	self.outline = Rect.New()
	self.bodyHeights = {}
	self.hitTestRects = {}
	for i=1,8 do
		self.hitTestRects[i] = Rect.New()
	end

	self.container = container or owner
	self.container.opaque = true
	self.container.touchChildren = false

	self.playSettings = PlaySettings.New()
	self.animation = AniImage.New(self.playSettings)
	self.animation.home = self.container.cachedTransform
	self.clothingAnimation = AniImage.New(self.playSettings)
	self.clothingAnimation.home = self.container.cachedTransform
	self.weaponAnimation = AniImage.New(self.playSettings)
	self.weaponAnimation.home = self.container.cachedTransform

	self.container:AddChild(self.animation)
	self.eventDispatcher = event("WEvent")
end
		
function Avatar:Create()
	self.playSettings:Reset()

	self.ready = false
	self.outline:Set(0,0,0,0)
	self.ridingHeight = 0
	
	self.frameLock = false
	self.frameLockIndex = 0
	self.walkSpeedRatio = 1
	self.action = self.mainAction
	self.notifyFireFrame = false

	for i=1,8 do
		self.bodyHeights[i] = 120
		self.hitTestRects[i]:Set(0,0,0,0)
	end

	self.owner.eventDispatcher:Add(self.OnOwnerStatusChanged, self)
	self.owner.onHeatBeat:Add(self.OnUpdate, self)
end

function Avatar:Destroy()
	self.animation:Unload()
	self.clothingAnimation:Unload()
	self.weaponAnimation:Unload()

	if self.clothingAnimation.parent~=nil then
		self.container:RemoveChild(self.clothingAnimation)
	end

	if self.weaponAnimation.parent~=nil then
		self.container:RemoveChild(self.weaponAnimation)
	end

	self.eventDispatcher:Clear()
end

function Avatar:Clear()
	self.animation:Clear()
end

function Avatar:SetSource(value)
	self.animation:SetSource(value)
	if self.active then
		self.animation:Load()
		self.ready = false
	end

	if self.animation.def.assetPath=='user' then
		self.fixedScale = 1.4
	else
		self.fixedScale = 1.2
	end

	self.clothingAnimation:SetScale(self.fixedScale, self.fixedScale)
	self.animation:SetScale(self.fixedScale, self.fixedScale)
	self.weaponAnimation:SetScale(self.fixedScale, self.fixedScale)
end

function Avatar:SetClothing(value)
	if not string.isNilOrEmpty(value) then
		if self.clothingAnimation.parent==nil then
			local i = self.container:GetChildIndex(self.animation) + 1
			self.container:AddChildAt(self.clothingAnimation, i)
		end

		self.clothingAnimation:SetSource(value)
		if self.active then
			self.clothingAnimation:Load()
		end
	else
		self.clothingAnimation:Unload()
	end
end

function Avatar:SetWeapon(value)
	if not string.isNilOrEmpty(value) then
		if self.weaponAnimation.parent==nil then
			local i = self.container:GetChildIndex(self.clothingAnimation) + 1
			self.container:AddChildAt(self.weaponAnimation, i)
		end

		self.weaponAnimation:SetSource(value)
		if self.active then
			self.weaponAnimation:Load()
		end
	else
		self.weaponAnimation:Unload()
	end
end

function Avatar:CancelFrameLock()
	self.frameLock = false
end

Avatar.get.direction = function(self)
	return self.playSettings.direction
end

function Avatar:SetDirection(value)
	if self.playSettings.direction ~= value then
		self.playSettings.direction = value
		self.forceDraw = true

		self.container.contentRect = self.hitTestRects[self.playSettings.direction].nativeRect
	end
end

function Avatar:SetAction(value)
	if self.action ~= value then
		self.action = value
		self:UpdateAction()
		self.playSettings:Rewind()
	end
end

function Avatar:UpdateAction()
	if self.ready and self.animation:GetAction(self.action) == nil then
		self.playSettings.action = self.mainAction
	else
		self.playSettings.action = self.action
	end
	if self.playSettings.action == AniConst.Walk then
		self.playSettings.speedRatio = self.walkSpeedRatio
	else
		self.playSettings.speedRatio = 1
	end
end

Avatar.get.hasFireFrame = function(self)
	local ad = self.animation:GetAction(self.action)
	if ad~=nil and ad.fireFrame > 0 then
		return true
	else
		return false
	end
end

Avatar.get.firePoint = function(self)
	local dd = self.animation.drawingDir
	if dd~=nil then
		if dd.firePoint.x ~= 0  or dd.firePoint.y ~= 0 then
			return Vector2.New(dd.firePoint.x * self.fixedScale, dd.firePoint.y * self.fixedScale)
		else
			return Vector2.New(0, -math.floor(dd.owner.boundsRect.height * self.fixedScale / 2))
		end
	else
		return Vector2.New(0, -self.bodyHeights[self.playSettings.direction] / 2)
	end
end

function Avatar:SetWalkSpeedRatio(value)
	self.walkSpeedRatio = value
	if self.playSettings.action == AniConst.Walk then
		self.playSettings.speedRatio = self.walkSpeedRatio
	end
end

function Avatar:Rewind()
	self.playSettings.Rewind()
end

function Avatar:PlayFixedFrame(frame)
	self.frameLock = true
	self.frameLockIndex = frame
	if frame >= 0 then
		self.playSettings.curFrame = frame
	else
		local ad = self.animation:GetAction(self.action)
		if ad ~= nil then
			if ad.frameCount + frame >= 0 then
				self.playSettings.curFrame = ad.frameCount + frame
			else
				self.playSettings.curFrame = ad.frameCount - 1
			end
		else
			self.playSettings.curFrame = 0
		end
	end
end

function Avatar:OnOwnerStatusChanged(type)
	if type==WEvent.EnterView then
		self.active = true
		self.animation:Load()
		self.clothingAnimation:Load()
		self.weaponAnimation:Load()
	elseif type==WEvent.LeaveView then
		self.active = false
	end
end

function Avatar:OnUpdate()
	if not self.ready and self.animation.def.ready then
		self.ready = true
		self:UpdateAction()
		if self.frameLock then
			self:PlayFixedFrame(self.frameLockIndex)
		end

		local mainAction = self.animation:GetAction(self.mainAction)
		if mainAction~=nil then
			self.ridingHeight = math.floor(mainAction.ridingPoint * self.fixedScale)

			local fh
			local dd
			local fd
			for i = 1,8 do
				fh = 0
				dd = mainAction.dirList[i]
				if mainAction==AniConst.Die then
					fd = dd.frameList[mainAction.frameCount-1]
				else
					fd = dd.frameList[0]
				end
				fh = fd.rect.y
				self.bodyHeights[i] = math.floor(-fh * self.fixedScale)

				local hitTestRect = self.hitTestRects[i]
				hitTestRect:CopyFrom(dd.hitTestRect)
				hitTestRect:Multiply(self.fixedScale)
			end
		else
			self.outline:CopyFrom(self.animation.def.unionRect)
		end
		self.container.contentRect = self.hitTestRects[self.playSettings.direction].nativeRect

		self.outline:CopyFrom(mainAction.boundsRect)
		self.outline:Multiply(self.fixedScale)
		self.eventDispatcher(WEvent.AvatarReady)
	end

	if self.ready then
		if self.notifyFireFrame then
			local ad = self.animation:GetAction(self.action)
			if ad ~= nil and (self.playSettings.curFrame >= ad.fireFrame or self.playSettings.curFrame == ad.frameCount - 1) then
				self.notifyFireFrame = false
				self.eventDispatcher(WEvent.ActionFire)
			end
		end

		if self.active then
			self.animation:Draw(self.forceDraw)
			self.clothingAnimation:Draw(self.forceDraw)
			self.weaponAnimation:Draw(self.forceDraw)
			self.forceDraw = false
		end

		if not self.frameLock then
			self.animation:NextFrame()
			if self.playSettings.reachEnding then
				self.eventDispatcher(WEvent.ActionEnd)
			end
		end
	end
end