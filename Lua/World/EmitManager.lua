require "World/EmitComponent"

EmitManager = {}

local pool = list()
local _created = false

function EmitManager.Create()
	if _created then return end
	_created = true

	EmitManager.view = GComponent.New()
	EmitManager.view.gameObjectName = 'EmitManager'
	GRoot.inst:AddChild(EmitManager.view)
end

function EmitManager.Emit(owner, type, hurt, xg1, xg2)
	local ec
	if pool.length > 0 then
		ec = pool:pop()
	else
		ec = EmitComponent.New()
	end
	ec:SetHurt(owner, type, hurt, xg1, xg2)
end

function EmitManager.ReturnComponent(com)
	pool:push(com)
end