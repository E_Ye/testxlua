Mount = fgui.container(LiveObject)

local get = tolua.initget(Mount)
local set = tolua.initset(Mount)  

local SHAKING_SEQUENCE =  {0,1,2,3,2,1,0,-1,-2,-3,-2,-1}

function Mount:ctor()
	LiveObject.ctor(self)

	self.touchable = false
	self.avatar = Avatar.New(self)
end

function Mount:OnCreate()
	self.shaking = false
	self.flyHeight = 0
	self.ridingAction = AniConst.Stand
	self.hasWing = false
	self.standActionCode = AniConst.Stand
	self.connected = true
	self.ridingFixedHeight = 0
	self.shakingIndex = 0
	self.liftingHeight = 0

	self:SetXY(0, 0)

	self.ownerPlaySettings = self.owner.avatar.playSettings
	self.owner:AddEffect(self, "back")
	self.owner.shadow.visible = false

	self.owner.eventDispatcher:Add(self.OnOwnerEvent, self)
	self.onHeatBeat = self.owner.onHeatBeat --心跳与宿主保持一致
	self.onHeatBeat:Add(self.OnUpdate, self)

	self.avatar:Create()
	self.avatar:SetAction(self.standActionCode)
	self.avatar.eventDispatcher:Add(self.OnAvatarEvent, self)
	self.defId = GetIdFromRid(self.data.rid)
	local mountDef = Defs.GetDef("mount", self.defId)
	if self.data.speed~=nil then
		self.owner:SetWalkSpeedRatio(self.data.speed)
	elseif mountDef.speed~=nil then
		self.owner:SetWalkSpeedRatio(mountDef.speed)
	else
		self.owner:SetWalkSpeedRatio(1.3)
	end
	self.avatar.active = self.owner.inView
	self.avatar:SetSource("mount/"..self.defId)
end

function Mount:OnDestroy()
	if self.parent~=nil then
		self.parent:RemoveChild(self)
	end

	self.avatar:Destroy()
	self.owner.eventDispatcher:Remove(self.OnOwnerEvent, self)
	self.onHeatBeat:Remove(self.OnUpdate, self)

	if not self.owner.destroyed then
		self.owner:SetWalkSpeedRatio(1)
		self.owner.shadow.visible = true
	end

	if not World.cleaningUp then
		self.owner:SetStandActionCode(AniConst.Stand)
		self.owner:SetWalkActionCode(AniConst.Walk)
		self.owner:SetLiftingHeight(self, 0)
	end

	self.owner = nil
end

function Mount:SetHasWing(value)
	self.hasWing = value
	self.shaking = self.hasWing
end

function Mount:PlayActionLoop(action)
	if action == -1 then
		action = AniConst.Stand
	end
	if self.avatar.action ~= action then
		self.avatar.action = action
	end

	self.standActionCode = action
end

get.direction = function(self)
	return self.avatar.direction
end

function Mount:SetDirection(value)
	self.avatar:SetDirection(value)
	self.eventDispatcher(WEvent.DirectionChanged)
end

function Mount:OnOwnerEvent(type)
	if type==WEvent.DirectionChanged then
		if not self.connected then return end
		self:SetDirection(self.owner.direction)
	end

	if type==WEvent.EnterView or type==WEvent.LeaveView then
		self.eventDispatcher(type)
	end
end

function Mount:OnAvatarEvent(type)
	if type==WEvent.AvatarReady then
		if self.ridingAction == AniConst.RideStand then
			self.owner.standActionCode = AniConst.RideStand
			self.owner.walkActionCode = AniConst.RideWalk
		else
			self.owner.standActionCode = self.ridingAction
			self.owner.walkActionCode = self.ridingAction
		end
		self:SetDirection(self.owner.direction)
		self.liftingHeight = -self.avatar.ridingHeight
		if self.liftingHeight~=0 then
			self.liftingHeight = self.liftingHeight - self.ridingFixedHeight
		end
		self.liftingHeight = self.liftingHeight + self.flyHeight
		self.owner:SetLiftingHeight(self, self.liftingHeight)
		self:SetXY(0, -self.avatar.ridingHeight)
	end
end

function Mount:OnUpdate()
	if self.shaking and self.connected then
		if not self.owner.fastMoving and self.parent ~= nil then
			if self.shakingIndex%8==0 then
				self.owner:SetLiftingHeight(self, self.liftingHeight + self.shakingSequence[((self.shakingIndex/8)%SHAKING_SEQUENCE)+1])
			end
			self.shakingIndex = self.shakingIndex+1
		end
	end

	if self.owner.walking and self.connected then
		if self.avatar.action ~= AniConst.Walk then
			self.avatar:SetAction(AniConst.Walk)
		end

		if self.ownerPlaySettings.action == AniConst.RideWalk then
			self.ownerPlaySettings.curFrame = self.avatar.playSettings.curFrame
			self.ownerPlaySettings.curFrameDelay = self.avatar.playSettings.curFrameDelay
		end
	else
		if self.avatar.action ~= self.standActionCode then
			self.avatar:SetAction(self.standActionCode)
		end
	end
end

function Mount:AddEffect(effect, position)
	if position=="front" then
		--保持最后一个为Label
		self:AddChild(effect)
	elseif position=="back" then
		self:AddChildAt(effect, 0)
	elseif position=="bottom" then
		self:AddChildAt(effect, 0)
	end
end

function Mount:Disconnect()
	if not self.connected then return end

	self.connected = false
	self.parent:RemoveChild(self)
	self.owner:SetLiftingHeight(self, 0)
	self.owner.shadow.visible = true
end

function Mount:Connect()
	if self.connected then return end

	self.connected = true
	self.owner:AddEffect(self, "back")
	self.owner:SetLiftingHeight(self,self.liftingHeight)
	self.shakingIndex = 0
	self.owner.shadow.visible = false
end