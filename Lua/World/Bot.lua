Bot = {}

local _targets = {}
local _waitRefresh = 0
local _killCount = {}
local _totalKillCount = 0
local _stopAfterClear = false
local _envId
local _envLayer
local _fbEnvList = {}
local _fbEnvIndex = 0
local _autoFb = false
local _killAll = false
local _timer

Bot.routingBack = false
Bot.active = false

function Bot.Init()
	_timer = Timer.New(Bot.OnTimer, 1, -1)
	NetClient.ListenRet(NetKeys.QCMD_HANGING_START, Bot.OnHangingStart)
	NetClient.ListenRet(NetKeys.QCMD_HANGING_STOP, Bot.OnHanginStop)
end

function Bot.Start(scheme, type, _cacheInfo)
	_targets = scheme.robots

	_waitRefresh = 0
	_totalKillCount = 0
	_stopAfterClear = scheme.stop_after_clear
	Bot.routingBack = false
	_killAll = false;
	_autoFb = type==1
	if #_killCount>0 then _killCount = {} end
	for i,_ in ipairs(_targets) do
		_killCount[i] = 0
	end

	GRoot.inst:ShowModalWait()
	NetClient.Send(NetKeys.QCMD_HANGING_START, _cacheInfo.type, _cacheInfo.time)
end

function Bot.OnHangingStart(cmd, data)
	GRoot.inst:CloseModalWait()
	PromptResult(data)
	if data.code~=0 then return end

	_timer:Start()
	G_GuaJiWin:Hide()
	MainUI.ShowGuaJiSign(true)
	_envId = World.sceneData.eid
	_envLayer = World.sceneData.layer
	
	_fbEnvList = nil
	_fbEnvIndex = -1

	local list = Defs.GuaJiAutoFb
	if list then
		local ff = _envId..","
		for _,v in ipairs(list) do
			local t = string.find(v, ff)
			if t~=nil then
				_fbEnvList = string.split(v, ",")
				_fbEnvIndex = table.indexOf(_fbEnvList, envId)
			end
		end
	end
	Bot.active = true
end

function Bot.OnHanginStop(cmd, data)
	PromptResult(data)
end

function Bot.AbortQuery()
	--Game.inst.confirm(Resource.g.gua_ji_stop_query, 
	--	onQuitConfirm);
	Bot.Stop()
end

function Bot.OnQuitConfirm(button)
	if button==0 then
		Bot.Stop()
	end
end

function Bot.Abort(reason)
	if not Bot.active then return end
		
	Bot.Stop()
	if reason then
		
	end
end

function Bot.Stop()
	NetClient.Send(NetKeys.QCMD_HANGING_STOP)
	Bot.active = false
	_timer:Stop()
	MainUI.ShowGuaJiSign(false)
	AutoRouting.Cancel()
end

function Bot.OnTimer()
	if not World.sceneReady then return end
		
	if _envId~=World.sceneData.eid then
		if not Bot.routingBack then
			local path = Defs.FindMapPath(World.sceneData.eid, _envId)
			if not path then --没有路径返回
				Abort("无法返回原场景")
				return
			end
			Bot.routingBack = true
		end
		--努力尝试返回原场景
		if not Player.me.moving then
			AutoRouting.Start(_envId)
			return
		end
	elseif _envLayer and _envLayer~=World.sceneData.layer then
		if not Bot.routingBack then
			Bot.routingBack = true
		end
		if not Player.me.moving then
			AutoRouting.Start(_envId, {exit=_envId.."@".._envLayer})
			return
		end
	elseif Bot.routingBack then
		Bot.routingBack = false
		_waitRefresh = 0
	end

	if not Player.idle() then return end

	local target
	if _totalKillCount~=0 and _totalKillCount%30==0 then --每杀了n个后检讨，找杀的数量最少的怪杀
		target = Bot.FindNearestKillableInMap(true)
	else
		target = Bot.FindNearestKillableInSight()
		if target==nil then
			target = Bot.FindNearestKillableInMap(false)
		end
	end
	
	if target~=nil then
		local index = table.indexOf(_targets, target.defId)
		_killCount[index] = _killCount[index]+1
		_totalKillCount = _totalKillCount+1
		if _totalKillCount>1000 then --清零
			for i=1,#_kllCount do
				_kllCount[i] = 0
			end
		end
		target.data.lastFailedTime = nil
		Player.ClickObject(target)
	else
		if _waitRefresh>0 then
			_waitRefresh = _waitRefresh-1
			if _waitRefresh==3 then
				Bot.ClearCritterFailedStatus()
			elseif _waitRefresh==0 then
				if _autoFb then --副本里自动进入下一层
					if Bot.FindNextFbEnv() then
						_killAll = true
						_targets = {}
						for i=1,#_kllCount do
							_kllCount[i] = 0
						end
						return;
					end
				end

				if _stopAfterClear then
					Bot.Abort("所有怪物已经清除")
				end
			end
		else	
			_waitRefresh = 3
		end
	end
end

function Bot.FindNextFbEnv()
	if _envId=="s212" then --炼狱塔
		if _envLayer<180 then
			_envLayer = _envLayer+1
			return true
		else
			return false
		end
	end
	
	if _fbEnvList~=nil then
		_fbEnvIndex = _fbEnvIndex+1
		if _fbEnvIndex<#_fbEnvList then
			_envId = _fbEnvList[_fbEnvIndex+1]
			return true
		end
	end
	return false
end

function Bot.ClearCritterFailedStatus()
	for _,thing in pairs(World.things) do
		if thing.thingType==ThingType.NPC then
			thing.data.lastFailedTime = nil
		end
	end
end

function Bot.FindNearestKillableInMap(useKillCount)
	local min = 100000000
	local result
	local possResult
	for _,critter in pairs(World.things) do
		if critter.thingType==ThingType.NPC
			and critter.data.kill 
			and not string.endswith(critter.data.name, "【云游】") then
			local index = table.indexOf(_targets, critter.defId)
			if _killAll and index~=nil then
				index = #_targets
				table.insert(_targets, critter.defId)
			end
			if index~=nil then
				possResult = critter
				if not critter.data.lastFailedTime 
					or Time.unscaledTime-critter.data.lastFailedTime>60 then
					local dist =  math.pow(critter.x-Player.me.x,2)+math.pow(critter.y-Player.me.y,2)
					if useKillCount then
						dist = dist + _killCount[index]*1000000 --杀的次数越少，调整后的距离越近
					end
					if dist<min then
						min = dist
						result = critter
					end
				end
			end
		end
	end
	if result==nil then
		result = possResult
	end
	return result
end

function Bot.FindNearestKillableInSight()
	local min = 100000000
	local result
	local cnt = World.mainEnv.numChildren
	for i=0,cnt-1 do
		local critter = World.mainEnv:GetChildAt(i)
		if LiveObject.instanceof(critter, Critter)
			and critter.data.kill 
			and not string.endswith(critter.data.name, "【云游】") then
			local index = table.indexOf(_targets, critter.defId)
			if _killAll and index~=nil then
				index = #_targets
				table.insert(_targets, critter.defId)
			end
			if index~=nil then
				if not critter.data.lastFailedTime 
					or Time.unscaledTime-critter.data.lastFailedTime>60 then
					local dist = math.pow(critter.x-Player.me.x,2)+math.pow(critter.y-Player.me.y,2)
					if dist<min then
						min = dist
						result = critter
					end
				end
			end
		end
	end
	return result
end