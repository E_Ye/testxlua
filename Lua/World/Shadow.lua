Shadow = fgui.container()

local _shadowTexture

function Shadow:ctor()
	self.touchable = false
	
	self.gameObject.name = "Shadow"
	self.home = World.mainEnv.bottomLayer.cachedTransform

	self.image = Image.New()
	self.image:SetXY(-32,-16)

	if _shadowTexture==nil then
		_shadowTexture = NTexture.New(UnityEngine.Resources.Load("shadow"))
	end
	self.image.texture = _shadowTexture
end

function Shadow:Create()
	if self.image.parent==nil then
		self:AddChild(self.image)
	end
	self.visible = true
end

function Shadow:Destroy()
	if self.parent~=nil then
		self.parent:RemoveChild(self)
	end
end