HeadBar = fgui.container()

function HeadBar:ctor(owner)
	self.owner = owner
	self.touchable = false
	self.view = UIPackage.CreateObject("HeadBar", "HUD")
	self:AddChild(self.view.displayObject)

	self.titleObject = self.view:GetChild("name")
	self.signObject = self.view:GetChild("sign")
	self.speakPanel = self.view:GetChild("speakPanel")
	self.bloodBar = self.view:GetChild("bloodBar")
end

function HeadBar:Create()
	self.speakPanel.visible = false
	self.bloodBar.visible = false
end

function HeadBar:Destroy()
	self.signObject.url = nil
end

function HeadBar:SetIcon(value)
	if value then
		self.signObject.url = "ui://HeadBar/"..value
	else
		self.signObject.url = nil
	end
end

function HeadBar:SetTitle(value)
	 self.titleObject.text = value
end

function HeadBar:ShowHpBar(val)
	--self.bloodBar.visible = val
end

function HeadBar:UpdateHpBar()
	--self.bloodBar.max = self.owner.data.max_hp
	--self.bloodBar.value = self.owner.data.hp
end

function HeadBar:Speak(msg)
	if msg then
		self.speakPanel.visible = true
		self.speakPanel.text = msg
	else
		self.speakPanel.visible = false
	end
end