EmitComponent = fgui.gcomponent()

local OFFSET_ADDITION = 2.2
local JITTER_FACTOR = 80

function EmitComponent:ctor()
	self.touchable = false
	self.owner = CritterRef.New()

	self.symbolLoader = GLoader.New()
	self.symbolLoader.autoSize = true
	self:AddChild(self.symbolLoader)

	self.numberText = GTextField.New()
	self:AddChild(self.numberText)

	self.critterPos = Vector2.New()
end

function EmitComponent:SetHurt(owner, type, hurt, xg1, xg2)
	self.owner.critter = owner
	local tf = self.numberText.textFormat
	if hurt>0 then
		if type == 0 then
			tf.font = "ui://HeadBar/number1"
		else
			tf.font = "ui://HeadBar/number2"
		end
	else
		tf.font = "ui://HeadBar/number3"
	end

	self.numberText.textFormat = tf

	if hurt>0 then
		self.numberText.text = hurt
	else
		self.numberText.text = '+'..hurt
	end

	local bj = ScriptUtils.BitAnd(xg1 or 0, 1)
	if bj then
		self.symbolLoader.url = "ui://HeadBar/critical"
	else
		self.symbolLoader.url = nil
	end

	self:UpdateLayout()

	self.alpha = 1
	self:UpdatePosition(Vector2.zero)
	local rnd = ScriptUtils.RandomInsideUnitCircle() * JITTER_FACTOR
	local toX = math.floor(rnd.x) * 2
	local toY = math.floor(rnd.y) * 2

	EmitManager.view:AddChild(self)
	TweenUtils.TweenVector2(Vector2.zero, Vector2.New(toX,toY), 1, self.UpdatePosition, self.OnCompleted, self)
	self:TweenFade(0, 0.5):SetDelay(0.5)
end

function EmitComponent:UpdateLayout()
	self:SetSize(self.symbolLoader.width + self.numberText.width, math.max(self.symbolLoader.height, self.numberText.height))
	local xx
	if self.symbolLoader.width > 0 then
		xx = self.symbolLoader.width + 2
	else
		xx = 0
	end
	self.numberText:SetXY(xx, (self.height - self.numberText.height) / 2)
	self.symbolLoader.y = (self.height - self.symbolLoader.height) / 2
end

function EmitComponent:UpdatePosition(pos)
	local critter = self.owner.critter
	if critter~=nil then
		self.critterPos = Vector2.New(critter.screenX, critter.screenY - critter.bodyHeight)
	end

	self:SetXY((self.critterPos.x + pos.x) / GRoot.contentScaleFactor - self.width / 2, (self.critterPos.y + pos.y) / GRoot.contentScaleFactor)
end

function EmitComponent:OnCompleted()
	self.owner.critter = nil
	EmitManager.view:RemoveChild(self)
	EmitManager.ReturnComponent(self)
end

function EmitComponent:Cancel()
	self.owner.critter = nil
	if self.parent ~= nil then
		DOTween.Kill(self)
		EmitManager.view:RemoveChild(self)
	end
	EmitManager.ReturnComponent(self)
end