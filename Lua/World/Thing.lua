require "World/LiveObject"
require "World/CritterRef"
require "World/Avatar"

Thing = fgui.container(LiveObject)
Thing.is_thing = true

local get = tolua.initget(Thing)
local set = tolua.initset(Thing)  

function Thing:ctor()
	LiveObject.ctor(self)
	self.touchable = true

	--物体的范围，用于判断是否可见
	self.outline = Rect.New()
	--物体的格子坐标
	self.pathPosition = Vector2.New()

	--如果被遮挡是否自动变为半透明
	self.autoAlpha = true

	--物体方向
	self.direction = 1

	--用于深度排序
	self.depth = 0

	self.onHeatBeat = event("onHeatBeat")

	--心跳定时器，会自动在第一次进入屏幕时启动，但不会停止。派发onUpdate事件。
	self.timer = Timer.New(function() self.onHeatBeat() end, 0, -1)
end

function Thing:Create(data)
	self.inView = false
	self.display = true
	self.env = fgui.null
	self.distToCamera = 0

	LiveObject.Create(self, data)
end

function Thing:Destroy()
	self.timer:Stop()

	LiveObject.Destroy(self)
	self.onHeatBeat:Clear()
end

function Thing:ProcessFlags()
end

function Thing:SetPathPosition(px, py, updateXY)
	if updateXY==nil then updateXY=true end
	self.pathPosition.x = px
	self.pathPosition.y = py
	if self.autoAlpha then
		self.alpha = self.env.pathInfo:IsAlpha(self.pathPosition) and 0.8 or 1
	end
	if updateXY then
		local pt =  self.env.pathInfo:GetRealPosition(self.pathPosition)
		self:SetRealPosition(pt.x, pt.y)
	end

	self:UpdateInViewState()
	if self.parent ~= nil then
		self.env.depthChanged = true
	end

	if World.camera == self then
		World.NotifyCameraPositionChanged()
	end
	self.eventDispatcher(WEvent.PathPosChanged)
end

--设置物体是否可见
function Thing:SetDisplay(value)
	self.display = value
	if self.display then
		if self.inView then
			self:AddToEnv()
		end
	else
		self:RemoveFromEnv()
	end
end

--在多场景应用中，设计物体在哪个场景
function Thing:SetEnv(value)
	if self.env ~= value then	
		self.env = value

		self:RemoveFromEnv()
		if self.display and self.inView then
			self:AddToEnv()
		end

		self.eventDispatcher(WEvent.EnvChanged)
	end
end

function Thing:SetRealPosition(x, y)
	self:SetXY(x, y)
	if World.camera == self then
		World.AdjustViewPos()
	end
	self.eventDispatcher(WEvent.RealPosChanged)
end

function Thing:RefreshPosition()
	self:SetPathPosition(self.pathPosition.x, self.pathPosition.y)
end

function Thing:SetDirection(value)
	if self.direction ~= value then
		self.direction = value
		self.eventDispatcher(WEvent.DirectionChanged)
	end
end

function Thing:UpdateInViewState()
	local rect = World.viewRect
	local xx = self.x
	local yy = self.y

	local camera = World.camera
	local val = false
	if camera == self then
		val = true
		self.distToCamera = 0
	else
		val = xx + self.outline.xMax > rect.x and xx + self.outline.x < rect.xMax
			and yy + self.outline.yMax > rect.y and yy + self.outline.y < rect.yMax
		if camera ~= nil then
			self.distToCamera = math.sqrt(math.pow(xx - camera.x, 2) + math.pow(yy - camera.y, 2))
		end
	end

	if val then
		if self.display then
			self:AddToEnv()
		end

		if self.inView ~= val then
			self.inView = true
			if not self.timer.running then self.timer:Start() end
			self:OnEnterView()
		end
	else
		self:RemoveFromEnv()

		if self.inView ~= val then		
			self.inView = false
			self:OnLeaveView()
		end
	end
end

function Thing:AddToEnv()
	if self.parent == nil then
		self.env:AddChild(self)
		self.env.depthChanged = true
	end
end

function Thing:RemoveFromEnv()
	local p = self.parent
	if p ~= nil then
		p:RemoveChild(self)
	end
end

function Thing:OnEnterView()
	self.eventDispatcher(WEvent.EnterView)
end

function Thing:OnLeaveView()
	self.eventDispatcher(WEvent.LeaveView)
end

get.envX = function(self)
	return self.x
end

get.envY = function(self)
	return self.y - self.distFromGround
end

--获得对象在屏幕上的坐标
get.screenX = function(self)
	 return (World.envContainer.parent.x + World.envContainer.x + self.x) * World.root.scaleX
end

get.screenY = function(self)
	 return (World.envContainer.parent.y + World.envContainer.y + self.y) * World.root.scaleY
end

--物体离地面高度
get.distFromGround = function(self)
	return 0
end

get.serverPosition = function(self)
	return self.pathPosition
end

--深度排序值
get.sortWeight = function(self)
	local ret = self.y * 1000 + self.depth * 10000000
	if self.data.stickingObject then
		ret = ret + self.data.stickingDepthHint
	end
	return ret
end

get.isCamera = function(self)
	 return World.camera == self
end

---------跟随支持--------------
function Thing:Stick(target, depthHint)
	if self.data.stickingObject ~= target then
		if self.data.stickingObject then		
			self.data.stickingObject.eventDispatcher:Remove(self.OnStickingObjectChanged, self)
		end
		self.data.stickingObject = target
		if self.data.stickingObject then
			self.data.stickingDepthHint = depthHint or 0
			self.data.stickingObject.eventDispatcher:Add(self.OnStickingObjectChanged, self)
			self:OnStickingObjectChanged(WEvent.PathPosChanged)
			self:OnStickingObjectChanged(WEvent.RealPosChanged)
			if self == World.camera then
				World.AdjustViewPos(false)
			end
		end
	end
end

function Thing:OnStickingObjectChanged(type)
	if type==WEvent.PathPosChanged then
		local pt = self.data.stickingObject.pathPosition
		if self.env ~= self.data.stickingObject.env then
			pt = self.data.stickingObject.env:LocalToMainPathPosition(pt)
			pt = self.env:MainToLocalPathPosition(pt)
		end
		self:SetPathPosition(pt.x, pt.y, false)
	elseif type==WEvent.RealPosChanged then
		if self.env ~= self.data.stickingObject.env then	
			local pt = self.data.stickingObject.env:LocalToMainRealPosition(self.data.stickingObject.xy)
			pt = self.env:MainToLocalRealPosition(pt)
			self:SetRealPosition(pt.x, pt.y)
		else
			self:SetRealPosition(self.data.stickingObject.x, self.data.stickingObject.y)
		end
	elseif type==WEvent.DirectionChanged then
		self.direction = self.data.stickingObject.direction
	elseif type==WEvent.Destroy then
		self.data.stickingObject = fgui.null
	end
end