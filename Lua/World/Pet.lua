Pet = fgui.container(Critter)

local STAND_POSITION = {{2,-1}, {2,0}, {0,2}, {-1,2}, {1,-2}, {2,0}, {2,0}, {2,1}}
local MAX_DISTANCE = 6

Pet.STATUS_PEACE = 1
Pet.STATUS_FIGHT = 2

function Pet:ctor()
	Critter.ctor(self)

	self.pendingPath = ArrayList()
	self.standDelay = 0
	self.distRatio = 3
end

function Pet:StandBy()
	self.pendingPath:Clear()
	self:SetDirection(self.owner.direction)
	self:ClearWalkSteps()
	local px = self.owner.pathPosition.x+STAND_POSITION[self.direction][1]*self.distRatio
	local py = self.owner.pathPosition.y+STAND_POSITION[self.direction][2]*self.distRatio
	if self.env.pathInfo:IsBlock(Vector2.New(px, py)) then
		px = self.owner.pathPosition.x
		py = self.owner.pathPosition.y
	end
	self:SetPathPosition(px, py)
end

function Pet:ClearWalkSteps()
	Critter.ClearWalkSteps(self)
	
	self.pendingPath:Clear()
end

function Pet:Die(delay)
	delay = delay or false
	self.data.corpse = { rid=self.name, org_rid=self.name, auto_remove=5 }

	Critter.Die(self, delay)
end

function Pet:OnCreate()
	Critter.OnCreate(self)
	
	self.pendingPath:Clear()
	self.standDelay = 0
	self.owner.eventDispatcher:Add(self.OnOwnerEvent, self)
end

function Pet:OnDestroy()
	self.owner.eventDispatcher:Remove(self.OnOwnerEvent, self)
	self.owner = nil

	Critter.OnDestroy(self)
end

function Pet:OnUpdate()
	Critter.OnUpdate(self)

	if self.pendingPath.Count>0 then
		self.standDelay = 24
		if self.walkingPath.length==0 then
			self.walkingPath:Push(self.pendingPath:get_Item(0))
			self.pendingPath:RemoveAt(0)
		end
	elseif self.standDelay>0 then
		self.standDelay = self.standDelay-1
		if self.standDelay==0 then
			self:SetDirection(self.owner.direction)
		end
	end
end

function Pet:OnOwnerEvent(type)
	if type==WEvent.PathPosChanged then
		if not self.inView or not self.owner.inView or self.pendingPath.Count>20 then
			self:StandBy()
			return
		end

		local origin = self.walkEnding
		local terminal = Vector2.New()
		terminal.x = self.owner.pathPosition.x+STAND_POSITION[self.owner.direction][1]*self.distRatio
		terminal.y = self.owner.pathPosition.y+STAND_POSITION[self.owner.direction][2]*self.distRatio
		self.env.pathInfo:GetNearest(origin, terminal)
		self.env.pathInfo:FindPath(origin, terminal, self.pendingPath)
	end
end