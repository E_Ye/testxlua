WalkSupport = class()

--走动计算支持
function WalkSupport:ctor(owner)
	self.owner = owner

    self.walkingPath = Array()
    self.requestPath = Array()
    self.toWalkPath = ArrayList()
    self.walkingStepLeft = false
    self.walkSpeed = 0
	self.walkSpeedRatio = 1
	self.fixedWalkSpeedRatio = 1
	self.walkingStepLeft = Vector2.New()
	self.optimizePathIndex = -1
	self.pathCancelled = false
	self.xRemain = 0
	self.yRemain = 0
    self.supportOptimizePath = true
	self.supportCancelWalkingSteps = false
end

function WalkSupport:Create()
	self.walkingPath.length = 0
	self.toWalkPath:Clear()
	self.walkingStepLeft = false
	self.walkSpeedRatio = 1
	self.fixedWalkSpeedRatio = 1
	self.walkSpeed = World.cfg_walkSpeedBase
end

function WalkSupport:Destroy()
end

function WalkSupport:AddWalkSteps(path)
	if #path == 0 then return end

	local i
	if self.supportCancelWalkingSteps and self.walkingPath.length > 0 then
	--检查是否取消了原来的路径 
		local pt0 = path[1]
		local ptLast = self.walkingPath.last
		local nearby = math.abs(pt0.x - ptLast.x) <= 1 and math.abs(pt0.y - ptLast.y) <= 1
		if not nearby then
			local inOldPath = 0
			for i=1,self.walkingPath.length do
				if pt0==self.walkingPath[i] then
					inOldPath = i
					break
				end
			end
			self.walkingPath.length = inOldPath
		end
	end

	self.walkingPath:PushTable(path)
end

function WalkSupport:ClearWalkSteps(resolveTerminal)
	if resolveTerminal==nil then resolveTerminal=true end

	if self.walkingPath.length>0 then
		if resolveTerminal then
			self.owner:SetPathPosition(math.floor(self.walkingPath.last.x), math.floor(self.walkingPath.last.y))
		end
		self.walkingPath.length = 0
		self.walkingStepLeft = false
	end

	if self.walkingStepLeft then
		self.walkingStepLeft = false
		self.owner:SetRealPosition(self._walkingStepTarget.x, self._walkingStepTarget.y)
	end
end

function WalkSupport:CancelWalking(allowRestSteps)
	if allowRestSteps==nil then allowRestSteps=true end
	self.toWalkPath:Clear()
	if not allowRestSteps then
		self:ClearWalkSteps()
	end
end

WalkSupport.get.hasWalkSteps = function(self)
	return self.walkingPath.length > 0 or self.walkingStepLeft
end

WalkSupport.get.lastToWalkPoint = function(self)
	if self.toWalkPath.Count>0 then
		return self.toWalkPath:get_Item(self.toWalkPath.Count-1)
	end
end

function WalkSupport:MoveTo(origin, terminal)
	self.toWalkPath:Clear()

	if self.owner.env.pathInfo:FindPath(origin, terminal, self.toWalkPath) then
		self.optimizePathIndex = 0
		if self.supportCancelWalkingSteps and self.walkingPath.length>0 then
			self.pathCancelled = true
			self.walkingPath.length = 0
		end
		return true
	else
		return false
	end
end

function WalkSupport:SetWalkSpeedRatio(val)
	self.walkSpeedRatio = val * self.fixedWalkSpeedRatio
	self.walkSpeed = World.cfg_walkSpeedBase * self.walkSpeedRatio
end

function WalkSupport:Run()
	if self.walkingPath.length <= 2 then
		if self.toWalkPath.Count>0 then
			if self.supportOptimizePath then
				self:TryOptimizePath()
			end

			local i = 0
			self.requestPath.length = 0

			local blockEncouter = false
			local pt
			while i < World.cfg_numOfSendingPoints and self.toWalkPath.Count>0 do
				pt = self.toWalkPath:get_Item(0)
				if self.owner.env.pathInfo:IsBlock(pt) then
					blockEncouter = true
					break
				end

				self.toWalkPath:RemoveAt(0)
				self.requestPath:Push(pt)
				self.walkingPath:Push(pt)
				i = i+1
				self.optimizePathIndex = self.optimizePathIndex-1
				if not self.owner:OnMovingStep(pt) then
					break
				end
			end

			if self.requestPath.length>0 then
				if self.pathCancelled then
					self.pathCancelled = false
					self.requestPath:Unshift(self.owner.pathPosition)
				end

				if self.owner.data.self then
					NetClient.Send(NetKeys.QCMD_MOVE, self.requestPath)
				end
			end

			if blockEncouter then
			--尝试跳过障碍
				if not self.owner:OnMovingToBlock(pt) then
					self.toWalkPath:Clear()
				end
			end
		end
	end

	if self.walkingPath.length > 0 or self.walkingStepLeft then
		self:Step()
		return true
	else
		return false
	end
end

function WalkSupport:Step()
	if not self.walkingStepLeft then self:NextStep(0) end
	if not self.walkingStepLeft then return end

	local pathInfo = self.owner.env.pathInfo
	local distToWalk = self.walkSpeed * Time.deltaTime * 1000

	local weight = pathInfo.cellXYWeight[self.owner.direction]
	local dx_ = self.xRemain + distToWalk * weight[0]
	local dy_ = self.yRemain + distToWalk * weight[1]
	local dx
	local dy
	if dx_>0 then dx = math.floor(dx_) else dx = math.ceil(dx_) end
	if dy_>0 then dy = math.floor(dy_) else dy = math.ceil(dy_) end
	self.xRemain = dx_ - dx
	self.yRemain = dy_ - dy
	local tx = self.owner.x + dx
	local ty = self.owner.y + dy

	if dx > 0 and tx >= self._walkingStepTarget.x or dx < 0 and tx <= self._walkingStepTarget.x then
		dx = tx - math.floor(self._walkingStepTarget.x)
		tx = -1
	end

	if dy > 0 and ty >= self._walkingStepTarget.y or dy < 0 and ty <= self._walkingStepTarget.y then
		dy = ty - math.floor(self._walkingStepTarget.y)
		ty = -1
	end

	if tx == -1 or ty == -1 then
		self.owner:SetRealPosition(self._walkingStepTarget.x, self._walkingStepTarget.y)

		distToWalk = math.sqrt(math.pow(dx, 2) + math.pow(dy, 2))
		if self.walkingPath.length>0 then
			self:NextStep(distToWalk)
		else
			self.walkingStepLeft = false

			if self.owner.isCamera then
				if World.joystickDir ~= 0 then
					self:HandleJoystick(self.owner.pathPosition, World.joystickDir)
					if self.walkingPath.length>0 then
						self:NextStep(distToWalk)
					end
				end
			end

			--trace("setXY1 " + this.x + "," + this.y)
		end
	else
		self.owner:SetRealPosition(tx, ty)
		--trace("setXY2 " + this.x + "," + this.y)
	end
end

function WalkSupport:NextStep(distToWalk)
	local nextPt = self.owner.pathPosition
	local pathInfo = self.owner.env.pathInfo

	while self.walkingPath.length>0 do
		local pt = self.walkingPath:Shift()
		local nearby = math.abs(pt.x - nextPt.x) <= 1 and math.abs(pt.y - nextPt.y) <= 1
		if nearby then
			local nextDir = pathInfo:GetDirectionOfPath(nextPt, pt, true)
			nextPt = pt
			if distToWalk > 0 then
				local weight = pathInfo.cellXYWeight[nextDir]
				local dx_ = distToWalk * weight[0]
				local dy_ = distToWalk * weight[1]
				local dx
				local dy
				if dx_>0 then dx = math.floor(dx_) else dx = math.ceil(dx_) end
				if dy_>0 then dy = math.floor(dy_) else dy = math.ceil(dy_) end
				local tx = math.floor(self._walkingStepTarget.x) + dx
				local ty = math.floor(self._walkingStepTarget.y) + dy

				self._walkingStepTarget = pathInfo:GetRealPosition(nextPt)

				if dx > 0 and tx >= self._walkingStepTarget.x or dx < 0 and tx <= self._walkingStepTarget.x then
					dx = tx - math.floor(self._walkingStepTarget.x)
					tx = -1
				end

				if dy > 0 and ty >= self._walkingStepTarget.y or dy < 0 and ty <= self._walkingStepTarget.y then
					dy = ty - math.floor(self._walkingStepTarget.y)
					ty = -1
				end

				if tx == -1 or ty == -1 then
					distToWalk = math.sqrt(math.pow(dx, 2) + math.pow(dy, 2))

					if self.walkingPath.length==0 then
						if self.owner.isCamera then
							if World.joystickDir ~= 0 then
								self:HandleJoystick(self.owner.pathPosition, World.joystickDir)
							end
						end

						if self.walkingPath.length==0 then
							self.walkingStepLeft = false
							self.owner:SetDirection(nextDir)
							self.owner:SetPathPosition(math.floor(nextPt.x), math.floor(nextPt.y))
							self.xRemain = 0
							self.yRemain = 0
							break
						end
					end
				else
					self.walkingStepLeft = true
					self.owner:SetDirection(nextDir)
					self.owner:SetPathPosition(math.floor(nextPt.x), math.floor(nextPt.y), false)
					self.owner:SetRealPosition(tx, ty)
					self.xRemain = 0
					self.yRemain = 0
					--trace("aaa " + distToWalk + "-" + iOwner.x + "," + iOwner.y + "," + iWalkingStepTarget)
					break
				end
			else
				self.walkingStepLeft = true
				self.owner:SetDirection(nextDir)
				self._walkingStepTarget = pathInfo:GetRealPosition(nextPt)
				self.owner:SetPathPosition(math.floor(nextPt.x), math.floor(nextPt.y), false)
				self.xRemain = 0
				self.yRemain = 0
				--trace("bbb " + iOwner.x + "," + iOwner.y + "," + iWalkingStepTarget)
				break
			end
		else
			--发生跳格
			self.owner:SetPathPosition(math.floor(pt.x), math.floor(pt.y))
			self.walkingStepLeft = false
			self.xRemain = 0
			self.yRemain = 0
			break
		end
	end
end

local _tempPath = ArrayList()
function WalkSupport:TryOptimizePath()
	if self.optimizePathIndex > 50 then return end

	if self.optimizePathIndex < 0 then
		self.optimizePathIndex = 0
	end

	local pt1 = self.toWalkPath:get_Item(0)
	local checkPos = math.min(self.optimizePathIndex + 100, self.toWalkPath.Count)
	local i
	local pathInfo = self.owner.env.pathInfo
	_tempPath:Clear()
	for i = checkPos-1,self.optimizePathIndex+3,-1 do
	--每两格检查一下就好了
		local pt2 = self.toWalkPath:get_Item(i)
		if pathInfo:IsReachable(pt1, pt2) then
		--直线可到达，重新寻径
			pathInfo:FindPath(pt1, pt2, _tempPath)
			self.optimizePathIndex = _tempPath.Count
			self.toWalkPath:RemoveRange(0, i + 1)
			self.toWalkPath:InsertRange(0, _tempPath)
			self.toWalkPath:Insert(0, pt1)
			--trace("optimize path " + i + "," +  iOptimizePathIndex)
			break
		end
	end
end

function WalkSupport:HandleJoystick(pt, dir)
	local pt2 = self.owner.env.pathInfo:GetForwardPoint(pt, dir)
	if pt2 ~= nil then
		self.walkingPath:Push(pt2)

		if self.owner.data.self then
			NetClient.Send(NetKeys.QCMD_MOVE, self.walkingPath)
		end
	end
end