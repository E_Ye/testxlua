SkillEffectDef = {
	TYPE_FIRE = 0,
	TYPE_MOVE_TO_TARGET = 1,
	TYPE_MOVE_TO_ALL_TARGETS = 2,
	TYPE_DIRECT_TO_TARGET = 3,
	TYPE_DIRECT_TO_ALL_TARGETS = 4,
	TYPE_WAVE = 5,
	TYPE_HIT_ALL_TARGETS = 6,
	TYPE_HIT = 7,
	TYPE_STATUS = 8,
	TYPE_POP_TEXT = 9,
}

function SkillEffectDef.Parse(def)
	local list1 = {}
	local list2 = {}
	local list3 = {}
	local list4 = {}

	if type(def)=="string" then
		local array = string.split(def, ',')
		for _,sss in ipairs(array) do
			local str = string.trim(sss)
			local c = string.sub(str, 1, 1)
			local m = {id=sss}
			if c=='@' then
				m["src"] = string.sub(str,2)
				m["type"] = SkillEffectDef.TYPE_FIRE
				table.insert(list1, m)
			elseif c=='<' then
				m["src"] = string.sub(str, 2)
				m["type"] = SkillEffectDef.TYPE_MOVE_TO_TARGET
				table.insert(list2, m)
			elseif c=='%' then
				m["src"] = string.sub(str, 2)
				m["type"] = SkillEffectDef.TYPE_POP_TEXT
				table.insert(list3, m)
			else
				m["src"] = str
				m["type"] = SkillEffectDef.TYPE_HIT_ALL_TARGETS
				table.insert(list3, m)
			end
		end
	else
		for _,m in ipairs(def) do
			local t = m["type"]
			if (t == SkillEffectDef.TYPE_HIT or t == SkillEffectDef.TYPE_HIT_ALL_TARGETS) then
				table.insert(list3,m)
			elseif (t == SkillEffectDef.TYPE_FIRE) then
				table.insert(list1,m)
			elseif (t == SkillEffectDef.TYPE_STATUS) then
				table.insert(list4,m)
			else
				table.insert(list2,m)
			end
		end
	end

	return { list1, list2, list3, list4 }
end