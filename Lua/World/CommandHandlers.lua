local function OnAddUser(cmd, data)
	if data.eid~= World.sceneData.eid then return end

	World.AddCritter(data)
end

local function OnRemoveUser(cmd, data)
	local thing = World.GetThing(data)
	if thing~=nil then
		World.RemoveThing(thing)
	end
end

local function OnMoveUser(cmd, data)
	local critter  = World.GetThing(data.target)
	if critter==nil then return end

	local len = #data.tracks
	if data.type==1 or len==1 and not critter.inView then
		critter:ClearWalkSteps(true)
		critter:SetPathPosition(data.tracks[len].x, data.tracks[len].y, true)
	elseif data.type==3 then
		HitBack.New(critter, data.tracks[len].x, data.tracks[len].y)
	elseif data.type==2 then
		--蚩尤跳
	else
		if not critter.data.self then
			critter:AddWalkSteps(data.tracks)
		end
	end
end

local function OnAddNPC(cmd, data)
	if data.eid~= World.sceneData.eid then return end

	World.AddCritter(data)
end

local function OnMoveNPC(cmd, data)
	local critter  = World.GetThing(data.target)
	if critter==nil then return end

	local len = #data.tracks
	if data.type==2 or len==1 and not critter.inView then
		critter:ClearWalkSteps(true)
		critter:SetPathPosition(data.tracks[len].x, data.tracks[len].y, true)
	elseif data.type==0 then
		critter:SetWalkSpeedRatio(0.6)
		critter:AddWalkSteps(data.tracks)
	elseif data.type==1 then
		critter:SetWalkSpeedRatio(1)
		critter:AddWalkSteps(data.tracks)
	elseif data.type==3 then --htiback
		HitBack.New(critter, data.tracks[len].x, data.tracks[len].y)
	else
	end
end

local function OnRemoveNPC(cmd, data)
	local thing = World.GetThing(data)
	if thing~=nil then
		World.RemoveThing(thing)
	end
end

local function OnAddItem(cmd, data)
	if data.eid~= World.sceneData.eid then return end

	if data.is_corpse then
		local corpse = World.AddCorpse(data)
	else
	--[[				var pi:PickableItem = PickableItem(Pool.getInstance(PickableItem));
			pi.create(item);
			pi.name = item.rid;
			pi.setPathPosition(item.pos_x, item.pos_y);
			iThings[pi.name] = pi;]]
	end
end

local function OnRemoveItem(cmd, data)
	local thing = World.GetThing(data)
	if thing~=nil then
		World.RemoveThing(thing)
	end
end

local function OnAddExit(cmd, data)
	if data.eid~= World.sceneData.eid then return end

	World.AddExit(data)
end

local function OnRemoveExit(cmd, data)
	local thing = World.GetThing(data)
	if thing~=nil then
		World.RemoveThing(thing)
	end
end

local function BuildAffectedList(data)
    data.affectedList = {}
    local index = 1
    if data.us ~= nil then
        for _k, _v in pairs(data.us) do
            data.affectedList[index] = _v
            data.affectedList[index].target = World.GetThing(_v.id)
            index = index+1
        end
    end
    if data.ups ~= nil then
        for _k, _v in pairs(data.ups) do
            data.affectedList[index] = _v
            data.affectedList[index].target = World.GetThing(_v.id)
            index = index+1
        end
    end
    if data.ns ~= nil then
        for _k, _v in pairs(data.ns) do
            data.affectedList[index] = _v
            data.affectedList[index].target = World.GetThing(_v.rid)
            index = index+1
        end
    end
    if data.nps ~= nil then
        for _k, _v in pairs(data.nps) do
            data.affectedList[index] = _v
            data.affectedList[index].target = World.GetThing(_v.rid)
            index = index+1
        end
    end
end

local function OnAttackPrepare(cmd, data)
end

local function OnAttackFire(cmd, data)
	local skill = Defs.all.skill[data.kid]
	if skill==nil then
		skill = Defs.all.skill["k1"]
	end
	
	data.src = World.GetThing(data.src)
	if data.src==nil then return end
	if data.src_type==5 then
		data.src = data.src.fabao
		if data.src==nil then return end
	end
	if data.src==Player.me then
		Player.HandleAttackResult({code=0, kid=data.kid})
	end

    if data.type ~= 5 then
        data.target = World.GetThing(data.target)
    else
        data.target = Vector2.New(data.posx, data.posy)
    end
    if data.target==nil then return end
    
	BuildAffectedList(data)
	local sp = SkillPlayer.GetInstance()
	sp:Prepare(data.src, data.target, skill, data, data.affectedList)		
	sp:Play()
end

local function OnAttack(cmd, data)
	local skill = Defs.all.skill[data.kid]
	if skill==nil then
		skill = Defs.all.skill["k6"]
	end

	data.src = World.GetThing(data.src)
	if data.src==nil then return end
	if data.src_type==5 then
		data.src = data.src.fabao
		if data.src==nil then return end
	end

	if data.type ~= 5 then
	    data.target = World.GetThing(data.target)
	else
	    data.target = Vector2.New(data.posx, data.posy)
	end
	if data.target==nil then return end
	
	BuildAffectedList(data)

	local sp = SkillPlayer.GetInstance()
	sp:Prepare(data.src, data.target, skill, data, data.affectedList)
	
	-- fire 是否直接攻击
	if data.fire ~= nil and data.fire~=0 then
		if data.src==Player.me then
			Player.HandleAttackResult({code=0, kid=data.kid})
		end
		sp:Play()
	else
		sp:PlayHitsOnly()
	end
end

local function OnAlert(cmd, data)
	if data.type==3 then
		MainUI.ShowBroadcastMessage(data.msg)
	else
	   	ChatPanel.AddAlertMsg(data.msg)
    end
end

local function OnChangeAttrs(cmd, data)
	local target
	if string.len(data.id)==0 then
		target = Player.me
		if data.type==2 then
			target = target.pet
		end
	else
		target = World.GetThing(data.id)
	end
			
	if target==nil then return end

	for k,v in pairs(data.attrs) do
		if k=="max_hp" then
			target:SetAttrValue("max_hp", v)
		elseif k=="hp" then
			target:SetAttrValue("hp", v)
		elseif k=="max_mp" then
			target:SetAttrValue("max_mp", v)
		elseif k=="mp" then
			target:SetAttrValue("mp", v)
		elseif k=="pk" then
			target:SetAttrValue("pk", v)
		elseif k=="status" then
			target:SetAttrValue("status", v)
		elseif k=="lvl" then
			target:SetAttrValue("lvl", v)
		elseif k=="shell" then
			if v~=0 then
				target:SetAttrValue("shell", "r"..v)
			else
				target:SetAttrValue("shell", nil)
			end
		end
	end
end

local function OnTaskStatus(cmd, data)
	for k,v in pairs(data) do
		local target = World.GetThing(k)
		if target~=nil then
			target:SetAttrValue('task', v)
		end		
	end
end

local function OnFinishTask(cmd, data)
	if TalkWin.isShowing then
		TalkWin.openTaskOnClose = data
	else
		G_TaskWin:Open(data)
	end
end

local function OnSyncRole(cmd, data)
	local d = Player.data
	d.hp = data.hp
	d.max_hp = data.max_hp
	d.mp = data.mp
	d.max_mp = data.max_mp
	d.lvl = data.lvl
	d.exp = data.exp
	d.uexp = data.uexp
	d.fly = data.fly
	d.money = data.money
	MainUI.UpdatePlayerStatus()
	
	if Player.me.pet and data.pet then
		d = Player.me.pet.data
		d.hp = data.pet.hp
		d.max_hp = data.pet.max_hp
		d.mp = data.pet.mp
		d.max_mp = data.pet.max_mp
		d.lvl = data.pet.lvl
		d.exp = data.pet.exp
		d.uexp = data.pet.uexp
		MainUI.UpdatePetStatus()
	end
	
	if data.tsm then
		for rid,td in pairs(data.tsm) do
			local critter = World.GetThing(rid)
			if critter~=nil then
				critter:SetAttrValue("task", td)
			end
		end
	end
end

local function OnMoveError(cmd, data)
	print('fix path to '..data.x..','..data.y)

	Player.me:FixErrorPosition(data.x, data.y, true)
end

local function OnGoError(cmd, data)
	if data.code~=0 then
		World.sceneReady = true
		GRoot.inst:CloseModalWait()
		AlertWin.Open(data.msg)
	end
end

local function OnAttackResult(cmd, data)
	local msg = data.msg
	if msg~=nil and string.len(msg)>0 and string.sub(msg, 1, 1)=='#' then
		local pos =  string.find(msg, '#', 2)
		data.kid = string.sub(msg,2,pos-1)
		msg = string.sub(msg, pos+1)
		pos = string.find(msg, '#')
		if pos~=nil then
			data.seq = string.sub(msg, 1, pos-1)
			msg = string.sub(msg, pos+1)
		end
		data.msg = msg
	end
	
	Player.HandleAttackResult(data)
end

local function OnRideOn(cmd, data)
	local user = World.GetThing(data.id)
	if user==nil then return end

	user:RideOnMount(data.mount)
end

local function OnJumpDown(cmd, data)
	local user = World.GetThing(data)
	if user==nil then return end

	user:JumpDownMount()
end

local function OnCallOutPet(cmd, data)
	local user = World.GetThing(data.id)
	if user==nil then return end

	user:CallOutPet(data.pet)
end

local function OnCallBackPet(cmd, data)
	local user = World.GetThing(data)
	if user==nil then return end

	user:CallBackPet()
end

local function OnCallOutMagic(cmd, data)
	local user = World.GetThing(data.id)
	if user==nil then return end

	user:CallOutFaBao(data.magic)
end

local function OnCallBackMagic(cmd, data)
	local user = World.GetThing(data)
	if user==nil then return end

	user:CallBackFaBao()
end

local function OnTaskTracts(cmd, data)
	MainUI.UpdateTasks(data)
end

local function OnTeamChange(cmd, data)
	local user = World.GetThing(data.uid)
	if user==nil then return end

    local type = data.type
    if type == 0 or type == 1 then
    	user:SetAttrValue("team", data.tid)
    elseif type == 2 or type == 3 or type == 4 then
      	user:SetAttrValue("team", nil)
    end
end

local function OnUseItem(cmd, data)
    local user = World.GetThing(data.uid)
    if user==nil then return end

    local d = Player.data
    if data.use_attr == 1 then
        d.hp = data.cur_value
    elseif data.use_attr == 2 then
        d.mp = data.cur_value
    elseif data.use_attr == 71 then
        -- 元神生命是什么鬼
    end
    if data.uid == Player.data.id then
        MainUI.UpdatePlayerStatus()
    end
end

function InitCommandHandlers()
	NetClient.Listen(NetKeys.ACMD_SYNC_ROLE, OnSyncRole)
	NetClient.Listen(NetKeys.ACMD_ALERT, OnAlert)
	NetClient.Listen(NetKeys.ACMD_ALERT_EXT, OnAlert)

	NetClient.Listen(NetKeys.ACMD_ADD_USER, OnAddUser)
	NetClient.Listen(NetKeys.ACMD_MOVE_USER, OnMoveUser)
	NetClient.Listen(NetKeys.ACMD_REMOVE_USER, OnRemoveUser)
	NetClient.Listen(NetKeys.ACMD_ADD_ROBOT, OnAddNPC)
	NetClient.Listen(NetKeys.ACMD_MOVE_ROBOT, OnMoveNPC)
	NetClient.Listen(NetKeys.ACMD_REMOVE_ROBOT, OnRemoveNPC)
	NetClient.Listen(NetKeys.ACMD_ADD_ITEM, OnAddItem)
	NetClient.Listen(NetKeys.ACMD_REMOVE_ITEM, OnRemoveItem)
	NetClient.Listen(NetKeys.ACMD_ADD_EXIT, OnAddExit)
	NetClient.Listen(NetKeys.ACMD_REMOVE_EXIT, OnRemoveExit)

	NetClient.Listen(NetKeys.ACMD_ATTACK_PREPARE, OnAttackPrepare)
	NetClient.Listen(NetKeys.ACMD_ATTACK_FIRE, OnAttackFire)
	NetClient.Listen(NetKeys.ACMD_ATTACK, OnAttack)


	NetClient.Listen(NetKeys.ACMD_CHANGE_ATTRS, OnChangeAttrs)
	NetClient.Listen(NetKeys.ACMD_OBJECT_TASK_STATUS, OnTaskStatus)
	NetClient.Listen(NetKeys.ACMD_FINISH_TASK, OnFinishTask)
	NetClient.Listen(NetKeys.ACMD_TASK_TRACKS, OnTaskTracts)

	NetClient.Listen(NetKeys.ACMD_CALL_OUT_MOUNT, OnRideOn)
	NetClient.Listen(NetKeys.ACMD_CALL_BACK_MOUNT, OnJumpDown)
	NetClient.Listen(NetKeys.ACMD_CALL_OUT_PET, OnCallOutPet)
	NetClient.Listen(NetKeys.ACMD_CALL_BACK_PET, OnCallBackPet)
	NetClient.Listen(NetKeys.ACMD_CALL_OUT_MAGIC, OnCallOutMagic)
	NetClient.Listen(NetKeys.ACMD_CALL_BACK_MAGIC, OnCallBackMagic)

	NetClient.Listen(NetKeys.ACMD_TEAM_CHANGE, OnTeamChange)
	NetClient.Listen(NetKeys.ACMD_USE_ITEM, OnUseItem)
	
	NetClient.ListenRet(NetKeys.QCMD_MOVE, OnMoveError)
	NetClient.ListenRet(NetKeys.QCMD_GO, OnGoError)
	NetClient.ListenRet(NetKeys.QCMD_ATTACK, OnAttackResult)
end