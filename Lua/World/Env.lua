Env = fgui.container()

function Env:ctor()
	self.bottomLayer = Container.New()
	self.bottomLayer.gameObject.name = "BottomLayer"
	self.bottomLayer.touchable = false
	self:AddChild(self.bottomLayer)

	self.depthChanged = false
	self.pathInfo = PathInfo.New()
	self.counter1 = 0
	self.counter2 = 0
	self.sortingObjects = {}
	self.sortingWeights = {}
end

--depth: 0~3 0-scene_effect, 1-floating map,  3-shadow
function Env:AddToBottomLayer(child, depth)
	if depth == 0 then
		self:AddChildAt(child, 0)
	elseif depth == 1 then
		local i = self:GetChildIndex(self.bottomLayer)
		self:AddChildAt(child, i)	
	elseif depth == 2 then
		self.bottomLayer:AddChildAt(child, 0)
	else
		self.bottomLayer:AddChild(child)
	end
end

function Env:ClearThings()
	self:RemoveChildren()	
	self.bottomLayer:RemoveChildren()
	self:AddChild(self.bottomLayer)
end

function Env:OnUpdate()
	self.counter1 = self.counter1 - Time.deltaTime
	self.counter2 = self.counter2 - Time.deltaTime

	if self.counter1>0 then	
		return
	end
	self.counter1 = World.cfg_depthSortInterval

	local sortAll
	if self.counter2<=0 then
		sortAll = true
		self.counter2 = World.cfg_fullDepthSortInterval
	end

	if self.depthChanged or sortAll then	
		self.depthChanged = false

		local scnt = 0
		local cnt = self.numChildren
		local sw
		for i = 0,cnt-1 do	
			local obj = self:GetChildAt(i)
			if LiveObject.instanceof(obj, Thing) then
				if sortAll or obj.distToCamera < 300 then
					sw = obj.sortWeight
					if sw ~= 0 then
						scnt = scnt+1
						self.sortingWeights[scnt] = sw + i * 10
						self.sortingObjects[scnt] = obj					
					end
				end
			elseif LiveObject.instanceof(obj, TerminalSign) then
				scnt = scnt+1		
				self.sortingWeights[scnt] = obj.y * 1000 + i * 10
				self.sortingObjects[scnt] = obj
			end
		end
		--print("scnt="+scnt)

		for i = 1,scnt do		
			for j = i,scnt do		
				local c1 = self.sortingObjects[i]
				local c2 = self.sortingObjects[j]
				if self.sortingWeights[i] > self.sortingWeights[j] then				
					self.sortingObjects[i] = c2
					self.sortingObjects[j] = c1
					self:SwapChildren(c1, c2)
				end
			end
		end
	end
end

