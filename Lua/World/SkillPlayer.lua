SkillPlayer = class()

SkillPlayer.Pool = list()

function SkillPlayer.GetInstance(id)
	if SkillPlayer.Pool.length>0 then
		return SkillPlayer.Pool:pop()
	else
		return SkillPlayer.New()
	end
end

function SkillPlayer:ctor()
	self.enemy = CritterRef.New()
end

function SkillPlayer:Prepare(src, target, skill, attackData, affectedList)
	self.disposed = false
	self.src = src
	self.target = target

	self.src.eventDispatcher:Add(self.OnSrcEvent, self)
	self.target.eventDispatcher:Add(self.OnTargetEvent, self)

	self.attackData = attackData
	self.affectedList = affectedList
	self.skill = skill
	self.skillId = skill.id
	self.allEffects = SkillEffectDef[self.skillId]
	if self.allEffects==nil then self.allEffects = {} end
	
	if LiveObject.instanceof(self.src, FaBao) then
		self.masterThing = self.src.owner
	else
		self.masterThing = self.src
	end

	self.enemy.critter = self.masterThing
	self.playerAttacker = self.masterThing==World.player
	self.playerAttacked = false
	self.waitingActionFire = false
end

function SkillPlayer:Play()
	if self.src==nil then
		self:AttackFly()
		return
	end
	self.waitingActionFire = self.src:Attack(self.target, self.skillId, self.attackData)
	local effects = self.allEffects[1]
	if effects~=nil and #effects>0 and self.masterThing.inView and (self.masterThing==self.src) then	
		local se
		for _,ef in ipairs(effects) do
			se = SkillEffect.GetInstance(ef.src)
			ge:Apply(ef)
			ge.PlayIn(self.src, SkillEffect.DURATION_ONCE)
		end
		if se.hasFireFrame then
			self.waitingActionFire = false
			se.onHit:Add(self.FireEffectCallback, self)
		elseif not self.waitingActionFire then
			self:AttackFly()
		end
	elseif not self.waitingActionFire then
		self:AttackFly()
	end
end

function SkillPlayer:PlayHitsOnly()
	self:AttackHit(self.affectedList)
end

function SkillPlayer:OnSrcEvent(type)
	if type==WEvent.Destroy then
		self.src = nil
		if self.waitingActionFire then
			self:FireCallback()
		end
	elseif type==WEvent.ActionFire then
		if self.waitingActionFire then
			self:FireCallback()
		end
	end
end

function SkillPlayer:OnTargetEvent(type)
	if type==WEvent.Destroy then
		self.target = nil
	end
end

function SkillPlayer:FireCallback()
	self.waitingActionFire = false
	self:AttackFly()
end

function SkillPlayer:FireEffectCallback(ge)
	self:AttackFly(ge)
end

function SkillPlayer:AttackFly(fromEffect)
	if self.src==nil then	
		self:AttackHit(self.affectedList)
		return
	end

	local effects = self.allEffects[2]
	if effects == nil  then
		self:AttackHit(self.affectedList)
		return
	end

	local firePoint
	if fromEffect~=nil then
		firePoint = fromEffect.firePoint
	else
		firePoint = self.src.firePoint
	end

	local ge
	local handled = false
	local hasAffectedList = self.affectedList~=nil and #self.affectedList > 0
	for _,ef in ipairs(effects) do	
		if (ef.type == 2 or ef.type == 4) and hasAffectedList then
		 --//多体飞行
			local f = handled
			for i,m in ipairs(self.affectedList) do
				local target = m.target
				if target == nil then				
					m.ok = true
				else
					ge = SkillEffect.GetInstance(ef.src)
					ge:Apply(ef)
					ge:PlayArrow(self.src.envX,
						self.src.envY,
						firePoint,
						target.envX,
						target.envY - target.bodyHeight / 2,
						ef.type == 4)
					ge.name = i
					if not f then				
						ge.onHit:Add(self.ArrowEffectCallback, self)
						handled = true
					end
				end
			end
		elseif ef.type == 5 then
		 --//扩散型冲击波
			ge = SkillEffect.GetInstance(ef.src)
			ge:Apply(ef)
			ge:Play(self.src.envX, self.src.envY, self.src.bodyHeight, self.masterThing)
			if not handled then			
				ge.onFrame:Add(self.AttackWave, self)
				ge.onHit:Add(self.AttacWaveHit, self)
				handled = true
			end
		else
		 --//单体飞行
			if self.target==nil then			
				if not handled then				
					self:AttackHit(self.affectedList)
					return
				end
			else
				ge = SkillEffect.GetInstance(ef.src)
				ge:Apply(ef)
				ge:PlayArrow(self.src.envX,
					self.src.envY,
					firePoint,
					self.target.envX,
					self.target.envY - self.target.bodyHeight / 2,
					ef.type == 3)
				ge.name = "*"
				if not handled then				
					ge.onHit:Add(self.ArrowEffectCallback, self)
					handled = true
				end
			end
		end
	end

	if not handled then
		self:AttackHit(self.affectedList, self)
	end
end

function SkillPlayer:ArrowEffectCallback(ge)
	if ge.name=="*" then
		self:AttackHit(self.affectedList)
	else
		self:AttackHit({ self.affectedList[tonumber(ge.name)] })
	end
end

function SkillPlayer:AttackWave(he)
	if self.affectedList == nil or #self.affectedList==0 then
		return
	end

	local tempList = {}
	for _,m in ipairs(self.affectedList) do	
		if not m.ok then
			local target = m.target
			if target == nil then			
				m.ok = true
			else 
				if he:HitTestEnv(target.x, target.y) then
					table.insert(tempList, m)
				end
			end
		end
	end

	if #tempList > 0 then
		self:AttackHit(tempList)
	end

	if self.disposed then	
		he.onFrame:Clear()
		he.onHit:Clear()
	end
end

function SkillPlayer:AttacWaveHit(ge)
	self:AttackHit(self.affectedList)
	ge.onFrame:Clear()
end

function SkillPlayer:AttackHit(targets)
	local handled = 0
	local effects = self.allEffects[3]

	if effects~=nil and targets==self.affectedList and #targets==0 and self.target~=nil then --如果没有伤害列表，可能是魔法技能，在目标身上播特效

		for _,ef in ipairs(effects) do
			local ge = SkillEffect.GetInstance(ef.src)
			ge:Apply(ef)
			ge:Play(self.target.envX,
				self.target.envY,
				self.target.bodyHeight,
				self.target)
		end
		self:Destroy()
		return
	end

	if targets == nil or #targets == 0 then	
		self:Destroy()
		return
	end

	local enemy = self.enemy.critter
	if enemy == nil then
	   self:Destroy()
       return
    end

	if effects==nil then
		effects = SkillEffectDef["*"][3]
	end

	for _,m in ipairs(targets) do
		if not m.ok then
			m.ok = true
			local target = m.target
			if target~=nil then
				handled = handled+1
				target:Attacked(self.src, enemy, target, self.skillId, self.attackData, m)
				if target.hurted and World.IsPlayer(target) then
					self.playerAttacked = true
				end

				for _,ef in ipairs(effects) do
					if ef.type == 6 or target == self.target then
						local ge = SkillEffect.GetInstance(ef.src)
						ge:Apply(ef)
						ge:Play(target.envX,
							target.envY,
							target.bodyHeight,
							target)
					end
				end
			end
		end
	end

	if handled > 0 then
		SoundManager.inst:Play("hit")
	end

	if targets ~= self.affectedList then	
		for _,m in pairs(self.affectedList) do		
			if not m.ok then
				return
			end
		end
	end

	self:Destroy()
end

function SkillPlayer:Destroy()
	if self.disposed then return end

	self.disposed = true
	if self.src~=nil then	
		self.src.eventDispatcher:Remove(self.OnSrcEvent, self)
		self.src = nil
	end
	if self.target~=nil then	
		self.target.eventDispatcher:Remove(self.OnTargetEvent, self)
		self.target = nil
	end

	SkillPlayer.Pool:push(self)
end