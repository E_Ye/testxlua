SceneExit = fgui.container(Thing)

function SceneExit:ctor()
	Thing.ctor(self)

	self.gameObject.name = "SceneExit"
	self.home = World.mainEnv.bottomLayer.cachedTransform
	self.outline:Set(-40, -30, 80, 60)
	self.touchable = false

	self.animation = Avatar.New(self)
end

function SceneExit:AddToEnv()
	if self.parent==nil then
		self.env:AddToBottomLayer(self, 2)
	end
end

function SceneExit:OnCreate()
	self.animation:Create()
	self.animation:SetSource("effect/transfer")
end

function SceneExit:OnDestroy()
	if not World.cleaningUp then
		World.RemoveThing(self)
	end

	self.animation:Destroy()
end