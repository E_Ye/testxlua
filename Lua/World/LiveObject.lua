require "World/LiveObjectPool"

LiveObject = fgui.container()

local g_ver = 1

function LiveObject.instanceof(obj, type)
	local mt = tolua.getpeer(obj)
	if mt~=nil then
		mt = getmetatable(mt)
		if mt then
			if mt==type then
				return true
			end

			mt = getmetatable(mt)
			if mt==type then
				return true
			end
		end
	end
end

function LiveObject:ctor()
	self.destroyed = false
	self.ref = 0
	self.version = 0

	--所有物体状态改变都会派发这个事件，事件参数为状态常量，参见WorldConst.WEvent
	self.eventDispatcher = event("WEvent")
end

function LiveObject:Create(data)
	self.data = data
	self.destroyed = false
	self.version = g_ver
	g_ver = g_ver + 1
	self.ref = 0

	self:OnCreate()
end

function LiveObject:Destroy()
	if self.destroyed then return end

	self.destroyed = true
	self.version = 0

	self:OnDestroy()
	self.eventDispatcher:Clear()
	
	if self.ref==0 then
		LiveObjectPool.Returns(self)
	end
end

function LiveObject:OnCreate()
end

function LiveObject:OnDestroy()
end

function LiveObject:AddRef()
	self.ref = self.ref + 1
end

function LiveObject:ReleaseRef()
	if self.ref>0 then
		self.ref  = self.ref - 1
		if self.ref == 0 and self.destroyed then  
			LiveObjectPool.Returns(self)
		end
	end
end