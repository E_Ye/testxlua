require "World/AutoRouting"
require "World/SkillSet"

local ATTACK_INTERVAL = 1

Player = static_class()

Player.attackTarget = CritterRef.New()
Player.lockedTarget = CritterRef.New()
Player.skillSet = SkillSet.New()
Player.data = {}
Player.prefs = {}

function Player.Attach(user)
	World.player = user
	Player.me = user
	Player.data = user.data
	Player.prefs = World.sceneData.psn
	if Player.prefs==nil then
		Player.prefs = {}
	end
	Player.attackTarget.critter = nil
	Player.attackSkill = nil
	Player.sendingAttack = 0
	Player.lastAttack = 0
	Player.attackFailTimes = 0
	Player.attackPaused = 0
	Player.skillSet:SetOwner(user)
	Player.me.onHeatBeat:Add(Player.AttackTimer)
end

function Player.Detach()
end

function Player.ClickObject(target)
	if Player.me==nil then return end
	if target==Player.me then return end
	
	Player.CancelTargets()
	
	if target.thingType==ThingType.Corpse then
		G_CorpseWin:Open(target)
	elseif target.thingType==ThingType.Exit then
		AutoRouting.Cancel()
		Player.LockTarget(nil)

		Player.me:MoveTo(target.pathPosition)
	else
		AutoRouting.Cancel()
		Player.LockTarget(target)
		
		local actionType
		if target.data.warrior then
			actionType = ActionType.Kill
		elseif(Player.CanCure(target)) then
			actionType = ActionType.Cure
		else
			actionType = ActionType.View
		end
		
		if actionType==ActionType.Kill or actionType==ActionType.Cure then
			if Player.attackSkill==nil then
				Player.attackSkill = Player.skillSet:GetNext()
			end
			Player.me:SetSkillRange(Player.attackSkill.attack_distance)
			Player.me:MoveToObject(target, actionType)
		elseif target.thingType==ThingType.NPC then 
			Player.me:MoveToObject(target, actionType)
		else
			MainUI.ShowCritterStatus(target)
		end
	end
end

function Player.ClickPoint(pt, byTouch)
	if Player.me==nil then return end

	if Bot.active then
		Bot.AbortQuery()
		return
	end

	Player.me:LeaveFightingMode()
	Player.CancelTargets()
	AutoRouting.Cancel()

	Player.me:MoveTo(pt, byTouch)
end

function Player.OnMovingStart()
end

function Player.OnMovingEnd()
	local exit = World.FindExitOn(Player.me.x, Player.me.y)
	if exit~=nil then
		Player.Go(exit.name)
		return
	end

	AutoRouting.OnReachPoint(Player.me.pathPosition)
end

function Player.UseSkill(kid, force)
	local skill = Defs.all.skill[kid]
	if skill==nil then return false end
	
	force = force or false
	if force and not SkillCD.IsReady(kid) then
		return false
	end
	
	Player.attackSkill = skill
	Player.me:SetSkillRange(skill.attack_distance)
	local lockedTarget = Player.lockedTarget.critter
	if lockedTarget~=nil and skill.is_attack and Player.CanAttack(lockedTarget) then
		Player.ClickObject(lockedTarget)
	end

	return true
end

function Player.CanAttack(target)
	if target.thingType==ThingType.NPC then
		return target.data.kill
	elseif target.thingType==ThingType.Pet then
		return target.data.status==2 and Player.canAttack(target.owner)
	else
		if target.data.team_id and target.data.team_id==Player.me.data.team_id then --same team
			return false
		end

		return target.data.pk and Player.me.data.pk
	end
end

function Player.CanCure(target)
end

function Player.OnReachTarget()
	local actionType = Player.me.actionType
	if actionType==ActionType.Follow then
		return
	end

	local target = Player.me.movingTarget
	AutoRouting.OnReachObject(target)
	Player.LockTarget(target)

	if actionType==ActionType.Kill or actionType==ActionType.Cure then
		Player.attackTarget.critter = target
		Player.attackPaused = 0
		Player.AttackTimer()
	elseif actionType==ActionType.View then
		Player.me:LeaveFightingMode()
		Player.me:SetMovingTarget(nil)

		if target.thingType==ThingType.Corpse then
			if target.data.items then

			end
		elseif target.thingType==ThingType.NPC then
			NetClient.Send(NetKeys.QCMD_VIEW_NPC, target.name)
		end
	end
end

function Player.Go(exitId)
	World.sceneReady = false
	Player.CancelTargets()
	GRoot.inst:ShowModalWait()
	NetClient.Send(NetKeys.QCMD_GO, exitId)
end

function Player.Fly(mapId)
	World.sceneReady = false
	Player.CancelTargets()
	GRoot.inst:ShowModalWait()
	NetClient.Send(NetKeys.QCMD_FLY, mapId)
end

function Player.AttackTimer()
	if not NetClient.connected then return end

	local lockedTarget = Player.lockedTarget.critter
	if lockedTarget~=nil and (lockedTarget.distToCamera>2500 or lockedTarget.data.dead) then
		Player.LockTarget(nil)
	end

	if Player.sendingAttack>0 then
		Player.sendingAttack = Player.sendingAttack-Time.deltaTime
	end

	if Player.attackPaused>0 then
		Player.attackPaused = Player.attackPaused-Time.deltaTime
	end

	if Player.attackPaused>0 or Player.sendingAttack>0
		or Player.me.dingShen.isTrue or Player.me.playingAttack then
		return
	end

	local t = Time.unscaledTime
	if t-Player.lastAttack<ATTACK_INTERVAL then
		return
	end

	local target = Player.attackTarget.critter
	if target~=nil then
		if target.data.dead then
			Player.CancelTargets()
			return
		end
	else
		if Player.attackSkill==nil or Player.attackSkill.target_type~=2 then
			return
		end
	end

	local skill
	if Player.attackSkill~=nil and SkillCD.CanUse(Player.attackSkill.id) then
		skill = Player.attackSkill
		if skill.is_learn then
			--只有勾魂类技能可以连续使用，挂机除外
			if Bot.active then
				Player.attackSkill = nil
			end
		else 
			Player.attackSkill = nil
		end
	else
		skill = Player.skillSet:GetNext()
	end

	if skill.target_type==2 then
		target = Player.me
	else
		if target==nil then return end

		if skill.attack_distance and Player.me:GetDistanceToTarget(target, true)>skill.attack_distance then
			Player.me:SetSkillRange(skill.attack_distance)
			Player.attackFailTimes = Player.attackFailTimes + 1
			Player.me:MoveTo(target.serverPosition, false)
			if Player.me.moving then
				Player.attackPaused = 5
			end
			return
		end
	end
	Player.lastAttack = t
	NetClient.Send(NetKeys.QCMD_ATTACK, 3, target.name, skill.id, 0)
	Player.sendingAttack = 2
end

function Player.CancelTargets()
	Player.attackTarget.critter = nil
	Player.me:SetEnemy(nil, false)
	Player.sendingAttack = 0
	Player.me:CancelWalking(true)
	World:HideTerminalSign()
end

function Player.HandleAttackResult(data)
	local ret = data.code
	local msg = data.msg
	local kid = data.kid

	local skill = Defs.all.skill[kid]
	if skill==nil then return end

	Player.attackPaused = 0
	if ret==0 then
		SkillCD.CoolDown(kid)
	end

	Player.sendingAttack = 0
	if ret==0 then
		Player.lastAttack = Time.unscaledTime-0.05
		Player.attackFailTimes = 0
		return
	end

	if ret==4 then --too fast
		Player.lastAttack = Time.unscaledTime - ATTACK_INTERVAL
	elseif ret==211 or ret==212 then --太远，或有障碍
		Player.attackFailTimes = Player.attackFailTimes + 1
		if Player.attackFailTimes<6 then
			Player.lastAttack = Time.unscaledTime - ATTACK_INTERVAL
		else
			Player.lastAttack = Time.unscaledTime
		end

		if Player.attackFailTimes<15 and Player.me.movingTarget~=nil then
			if Player.attackSkill==nil then
				Player.attackSkill = skill
				Player.me:SetSkillRange(skill.attack_distance)
			end
			Player.me:MoveToMovingTarget()
			if Player.me.moving then
				Player.attackPaused = 5
			end
		else
			Player.attackFailTimes = 0
			Player.CancelTargets()
			ChatPanel.AddAlertMsg(msg)
		end
	elseif ret==201 or ret==202 or ret==203 or ret==204 or ret==207 or ret>=221 then --由于技能的原因造成的失败
		ChatPanel.AddAlertMsg(msg)

		if ret==202 then --尚未冷却完毕
			local remain = tonumber(GetBetweenBrakets(msg))
			if remain~=nil and remain~=0 then
				SkillCD.CoolDown(skill.id, remain)
			end
		elseif Bot.active then
			skill.lastFailedTime = Time.unscaledTime
		end

		if not skill.is_attack or ret~=201 and ret~=202 then
			Player.CancelTargets()
		end
		Player.lastAttack = Time.unscaledTime - ATTACK_INTERVAL
	else
		if ret~=2 and ret~=6 and ret~=209 then
			ChatPanel.AddAlertMsg(msg)
		end

		if Bot.active then
			local critter = Player.attackTarget.critter
			if critter then --记录错误状态，避免重复选择这个怪物攻击
				critter.data.lastFailedTime = Time.unscaledTime
			end
		end

		Player.CancelTargets()
	end
end

function Player.LockTarget(target)
	local old = Player.lockedTarget.critter
	if old~=nil then
		if old~=target then
			old:TerminateStatusEffect("selected")

			if target~=nil then
				Player.lockedTarget.critter = target
				target:PlayStatusEffect("selected", nil, 0)
			else
				Player.lockedTarget.critter = nil
			end
		end
	elseif target~=nil then
		Player.lockedTarget.critter = target
		target:PlayStatusEffect("selected", nil, 0)
	else
		Player.lockedTarget.critter = nil
	end

	if target~=nil and (target.thingType==ThingType.User or target.data.warrior) then
		MainUI.ShowCritterStatus(target)
	end
end

function Player.LockTargetIfNull(target)
	if Player.lockedTarget.critter==nil then
		Player.LockTarget(target)
	end
end

function Player.idle()
	return Player.me.idle and Player.attackTarget.critter==nil
		and Player.sendingAttack==0 and not Player.data.ding_shen
end