TerminalSign = fgui.container()

function TerminalSign:ctor()
	self.touchable = false
	self.gameObject.name = 'TerminalSign'
	self.home = World.mainEnv.cachedTransform
	
	self.animation = AniImage.New()
	self:AddChild(self.animation)
	self.animation:SetSource("effect/terminal")
	self.animation:Load()
end

function TerminalSign:OnUpdate()
	self.animation:Draw(false)
	self.animation:NextFrame()
end