AutoRouting = {}

function AutoRouting.Start(eid, target, actionType, isTask)
	if eid==nil then
		eid = World.sceneId
	end
	if actionType==nil then
		actionType = ActionType.Auto
	end

	AutoRouting.target = { eid=eid, actionType=actionType}
	if target~=nil then
		if target.pos~=nil then
			AutoRouting.target.pos = target.pos
		elseif target.x~=nil then
	        AutoRouting.target.pos = Vector2.New(target.x, target.y)
	    end
	    if target.nid~=nil then
	    	AutoRouting.target.rid = target.nid
	    end
	    if target.exit~=nil then
	    	AutoRouting.target.exit = target.exit
	    end
	end
	AutoRouting.route = {}
	AutoRouting.started = true
	AutoRouting.parsed = false
	AutoRouting.isTask = isTask
	AutoRouting.timer:Start()
	AutoRouting.OnTimer()
end

function AutoRouting.Cancel()
	AutoRouting.started = false
	AutoRouting.target = nil
	AutoRouting.timer:Stop()
	MainUI.ShowXunLuSign(false)
end

function AutoRouting.Pause()
	if not AutoRouting.started then return end
	AutoRouting.timer:Stop()
end

function AutoRouting.Continue()
	if not AutoRouting.started then return end

	if AutoRouting.parsed then
		AutoRouting.target = nil
	end

	AutoRouting.timer:Start()
	AutoRouting.OnTimer()
end

function AutoRouting.OnReachPoint(pt)
	if AutoRouting.target~=nil and AutoRouting.target.pos~=nil and pt==AutoRouting.target.pos then
		AutoRouting.target.pos = nil
		return true
	else
		return false
	end
end

function AutoRouting.OnReachObject(target)
	if AutoRouting.target~=nil and AutoRouting.target.rid==target.name then
		AutoRouting.target = nil
		return true
	else
		return false
	end
end

function AutoRouting.OnTimer()
	if not World.sceneReady then return end

	local eid = GetIdFromRid(World.sceneId)
	if not AutoRouting.parsed then
		local path = Defs.FindMapPath(eid, AutoRouting.target.eid)
		if path~=nil then
			local k = eid
			for i=1,#path do
				table.insert(AutoRouting.route, {eid=k, exit=path[i]})
				k = path[i]
			end
			table.insert(AutoRouting.route, AutoRouting.target)
			AutoRouting.parsed = true
			AutoRouting.target = nil
			Player.CancelTargets()
			MainUI.ShowXunLuSign(true)
		else
			AutoRouting.Cancel()
			return
		end
	end

	if not Player.me.idle then return end

	if AutoRouting.target==nil then
		local len = #AutoRouting.route
		while len>0 do
			local n = AutoRouting.route[1]
			table.remove(AutoRouting.route, 1)
			len = len-1
			if n.eid==eid then
				AutoRouting.target = n
				break
			end
		end
	end

	local p = AutoRouting.target
	if p==nil then
		AutoRouting.Cancel()
		return
	end

	if p.exit~=nil then
		if p.exit==eid then
			AutoRouting.target = nil
			AutoRouting.OnTimer()
			return
		end

		local exit = World.FindExitTo(p.exit)
		if exit~=nil then
			Player.me:MoveTo(exit.pathPosition, true)
			--//这里不直接设置iTarget为空的原因是为了防止玩家在移动到目标的过程意外停止了，还可以继续自动寻路。以下同
			return
		end
	end

	if p.rid~=nil then
		local thing = World.GetThing(p.rid)
		if thing==nil then
			if string.sub(p.rid, 1,1)=='i' then
				thing = World.FindNearestThingById(GetIdFromRid(p.rid))
			else
				thing = World.FindNearestCritterById(GetIdFromRid(p.rid))
			end
		end

		if thing~=nil then
			p.pos = nil
			p.rid = thing.name
			Player.me:MoveToObject(thing, p.actionType)
			return
		end
	end

	if p.pos~=nil then
		Player.me:MoveTo(p.pos, true)
	end

	AutoRouting.Cancel()
end

AutoRouting.timer = Timer.New(AutoRouting.OnTimer, 0.8, -1)