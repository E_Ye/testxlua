TileMap = {}

local _mapWidth = 0
local _mapHeight = 0
local _canvasWidth = 0
local _canvasHeight = 0
local _originCanvasWidth = 0
local _originCanvasHeight = 0
local _scale = 1
local _viewPosX = 0
local _viewPosY = 0
local _pieces = {}
local _images = {}
local _container

local _pieceCountH = 0
local _pieceCountV = 0
local _bitmapsCountH = 0
local _bitmapsCountV = 0

local PIECE_WIDTH = 512
local PIECE_HEIGHT = 512

function TileMap.Create()
	TileMap.view = Container.New()
	TileMap.view.gameObject.name = "TileMap"
	TileMap.view.opaque = true
	_container = Container.New()
	TileMap.view:AddChild(_container)
end

function TileMap.SetCanvasSize(w, h)
	if _originCanvasWidth == w and _originCanvasHeight == h then return end

	_originCanvasWidth = w
	_originCanvasHeight = h
	_canvasWidth = math.floor(w / _scale)
	_canvasHeight = math.floor(h / _scale)
	TileMap.view.size = Vector2.New(w, h)
	TileMap.AdjustLayout()
end

function TileMap.AdjustLayout()
	if _canvasWidth == 0 or _canvasHeight == 0 or _mapWidth == 0 then return end

	local image
	local cnt = #_images
	for i=1,cnt do
		image = _images[i]
		image.texture = nil
		_container:RemoveChild(image)
	end

	_bitmapsCountH = math.ceil(_canvasWidth / PIECE_WIDTH) + 1
	_bitmapsCountV = math.ceil(_canvasHeight / PIECE_HEIGHT) + 1

	cnt = _bitmapsCountV * _bitmapsCountH
	for i=#_images+1,cnt do
		image = Image.New()
		_images[i] = image
	end

	for i=1,cnt do
		image = _images[i]
		_container:AddChild(image)
		image:SetXY(math.floor((i-1) % _bitmapsCountH) * PIECE_WIDTH, math.floor((i-1) / _bitmapsCountH) * PIECE_HEIGHT)
	end

	if _mapWidth < _canvasWidth then
		TileMap.displayOffsetX = math.floor((_canvasWidth - _mapWidth) / 2 * _scale)
	else
		TileMap.displayOffsetX = 0
	end

	if _mapHeight < _canvasHeight then
		TileMap.displayOffsetY = math.floor((_canvasHeight - _mapHeight) / 2 * _scale)
	else
		TileMap.displayOffsetY = 0
	end

	_container.size = Vector2.New(_canvasWidth, _canvasHeight)
	TileMap.SetViewPos(_viewPosX,_viewPosY)
end

function TileMap.Scale(val)
	_scale = val
	_container.scaleX = _scale
	_container.scaleY = _scale
	_canvasWidth = math.floor(_originCanvasWidth / _scale);
	_canvasHeight = math.floor(_originCanvasHeight / _scale);
	TileMap.AdjustLayout()
end

function TileMap.Clear()
	for i=1,#_images do
		image = _images[i]
		image.texture = nil
	end
end

function TileMap.InitMap(mapWidth, mapHeight, pieces)
	_mapWidth = mapWidth
	_mapHeight = mapHeight
	_pieces = pieces
	_pieceCountH = math.ceil(mapWidth / PIECE_WIDTH)
	_pieceCountV = math.ceil(mapHeight / PIECE_HEIGHT)
	_viewPosX = 0
	_viewPosY = 0
	TileMap.AdjustLayout()
end

function TileMap.SetViewPos(x, y)
	_viewPosX = x
	_viewPosY = y
	local pieceX = math.floor(_viewPosX / PIECE_WIDTH)
	local pieceY = math.floor(_viewPosY / PIECE_HEIGHT)
	_container:SetXY(TileMap.displayOffsetX - (_viewPosX % PIECE_WIDTH) * _scale, TileMap.displayOffsetY - (_viewPosY % PIECE_HEIGHT) * _scale)

	local k = 1
	local piece
	local index
	for i = 0,_bitmapsCountV-1 do
		for j = 0,_bitmapsCountH-1 do
			local image = _images[k]
			k = k+1
			piece = nil
			if pieceY+i<_pieceCountV and pieceX+j<_pieceCountH then
				index = (pieceY+i) * _pieceCountH + pieceX + j
				if _pieces~=nil and index < _pieces.Length then
					piece = _pieces[index]
				end
			end
				
			if image.texture ~= piece then
				image.texture = piece
			end
		end
	end
end