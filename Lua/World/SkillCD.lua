SkillCD = {}
SkillCD.YaoCDTime = 2
SkillCD.i_Yao = "i_player_yp"
SkillCD.i_PetYao = "i_pet_yp"

local _coolings = {}
local _lastTime

function SkillCD.Init()
	SkillCD.timer = Timer.New(SkillCD.OnTimer, 0.042, -1, false)
	SkillCD.timer:Start()
	_lastTime = Time.unscaledTime
end

function SkillCD.CoolDown(iid, forceTime)
	local t
	if forceTime==nil then forceTime=0 end
	if string.sub(iid, 1, 1)=="k" then
		if forceTime==0 then
			t = Defs.GetInt("skill", iid, "cool_time")
		else
			t = forceTime
		end
		if t>0 then
			local da = Defs.GetString("skill", iid, "deplete_item")
			if not string.isNilOrEmpty(da) then --同时冷却消耗的物品
				_coolings[da] = { elapsed=0, time=t }
				MainUI.NotifyCooling(da, 0)
			end
			_coolings[iid] = { elapsed=0, time=t }
			MainUI.NotifyCooling(iid, 0)
		end
	else
		if forceTime==0 then
			t = Defs.GetInt("item", iid, "cool_time")
		else
			t = forceTime
		end
		if t>0 then
			_coolings[iid] = { elapsed=0, time=t }
			MainUI.NotifyCooling(iid, 0)
		end
	end
end

function SkillCD.CoolDownYao(toPet)
	local iid
	if toPet then
		iid = i_PetYao
	else
		iid = i_Yao
	end
	_coolings[iid] = { elapsed=0, time=YaoCDTime }
	MainUI.NotifyCooling(iid, 0)
end

function SkillCD.GetCdProgress(iid)
	local d = _coolings[iid]
	if d then
		return d.elapsed/d.time*100
	else
		return 0
	end
end

function SkillCD.GetYaoCdProgress(toPet)
	local d
	if toPet then
		d = _coolings[i_PetYao]
	else
		d = _coolings[i_Yao]
	end
	if d then
		return d.elapsed/d.time*100
	else
		return 0
	end
end

function SkillCD.CancelCooldown(iid)
	if _coolings[iid] then
		_coolings[iid] = nil
		MainUI.NotifyCooling(iid, 100)
	end
end

function SkillCD.CancelYaoCd()
	if _coolings[i_Yao] then
		_coolings[i_Yao] = nil
		MainUI.NotifyCooling(i_Yao, 100)
	end
	
	if _coolings[i_PetYao] then
		_coolings[i_PetYao] = nil
		MainUI.NotifyCooling(i_PetYao, 100)
	end
end

function SkillCD.CanUse(iid)
	return _coolings[iid]==nil
end

function SkillCD.CanUseYao(toPet)
	if toPet then
		return _coolings[i_PetYao]==nil
	else
		return _coolings[i_Yao]==nil
	end
end

function SkillCD.OnTimer()
	local elapsed = Time.unscaledTime - _lastTime
	_lastTime = Time.unscaledTime

	for k,v in pairs(_coolings) do
		v.elapsed = v.elapsed + elapsed
		if v.elapsed>=v.time then
			_coolings[k] = nil
		end
		local progress = math.ceil(v.elapsed/v.time*100)
		if v.progress~=progress then
			MainUI.NotifyCooling(k, progress)
			v.progress = progress
		end
	end
end