require "Common/LineGenerator"

SkillEffect = fgui.container()
local get = tolua.initget(SkillEffect)
local set = tolua.initset(SkillEffect)

SkillEffect.DURATION_LOOP = 0
SkillEffect.DURATION_ONCE = -1

local cfg_defaultFlyStep = 5
local cfg_effectPath = "effect/"

SkillEffect.Pool = {}

function SkillEffect.GetInstance(id)
	local arr = SkillEffect.Pool[id]
	if arr==nil then
		arr = list()
		SkillEffect.Pool[id] = arr
	end
	if arr.length>0 then
		return arr:pop()
	else
		return SkillEffect.New(id)
	end
end

function SkillEffect:ctor(id)
	self.effectId = id

	self.touchable = false
	self.destroyed = false
	self.forceDraw = false

	self.playSettings = PlaySettings.New()
	self.animation = AniImage.New(self.playSettings)
	self.animation:SetSource(cfg_effectPath..self.effectId)
	self:AddChild(self.animation)

	self.lineGenerator = LineGenerator.New()
	self.startPoint = Vector2.New()
	self.endPoint = Vector2.New()	
	self.offset = Vector2.New()
	self.colorFilter = fgui.null
	self.actionData = fgui.null

	self.onHit = event("onHit")

	self.timer = Timer.New(function() self:OnUpdate() end, 0, -1)
end

function SkillEffect:Apply(data)
	self.rotation = data.rotation or 0
	self.offset.x = data.x or 0
	self.offset.y = data.y or 0
	self:SetScale((data.scale_x or 1)*1.4, (data.scale_y or 1)*1.4)
	self.step = data.step or cfg_defaultFlyStep
	if data.color then
		if not self.colorFilter then
			self.colorFilter = ColorFilter.New()
		end
		self.animation.filter = self.colorFilter
		self.colorFilter:AdjustBrightness(data.color[1]/100)
		self.colorFilter:AdjustContrast(data.color[2]/100)
		self.colorFilter:AdjustSaturation(data.color[3]/100)
		self.colorFilter:AdjustHue(data.color[4]/180)
	else
		self.animation.filter = nil
	end
end

function SkillEffect:Reset()
	self.destroyed = false

	self.animation:Load()

	if not self.actionData then
		local ad = self.animation.def.actionList[AniConst.Stand]
		if ad~=nil then self.actionData=ad end
	end

	self.alpha = 1
	self.visible = true
	self.flyType = 0
	self.loop = false

	self.playSettings:Reset()
	self.animation:Clear()

	self.owner = fgui.null
	self.setFlags = fgui.null
end

function SkillEffect:Play(x, y, bodyHeight, refObject)
	self:Reset()

	self.playY = y
	self:SetXY(x + self.offset.x, y + self.offset.y)
	local childIndex = -1
	local env = World.mainEnv
	if refObject ~= nil then	
		if refObject.parent == env then
			childIndex = env:GetChildIndex(refObject)
		end
		if refObject.direction~=nil then
			self.playSettings.direction = refObject.direction
		end
	end

	if self.actionData then
		local ep = self.actionData.effectPosition
		if ep == 1 or ep == 5 or ep == 8 then
			self.y = self.y - bodyHeight / 2 - 5
		elseif ep == 2 or ep == 6 or ep == 9 then
			self.y= self.y - bodyHeight
		end

		if ep == 10 then
			env:AddChildAt(self, 0)
		elseif ep == 3 then
		--后面
			if childIndex ~= -1 then
				env:AddChildAt(self, childIndex)
			else
				env:AddChild(self)
			end
		else
		 --前面
			if childIndex ~= -1 then
				env:AddChildAt(self, childIndex + 1)
			else
				env:AddChild(self)
			end
		end
	else
	end

	self.timer:Start()
end

function SkillEffect:PlayIn(owner, duration, setFlags)
	self:Reset()

	self.owner = owner
	self.playSettings.direction = owner.direction
	self.playY = 0
	self.xy = self.offset
	self.owner:AddEffect(self, "front")
	self.inFront = true
	if self.actionData then	
		self.visible = true
		self:AdjustPosition()
		self:AdjustDepth()
	else
		self.visible = false
	end
	if setFlags then	
		self.setFlags = setFlags
		self.setFlags:Set(self)
	end
	self:UpdateDuration(duration)

	owner.eventDispatcher:Add(self.OnOwnerEvent, self)
	owner.onHeatBeat:Add(self.OnUpdate, self)
end

function SkillEffect:PlayArrow(fromX, fromY, firePoint, toX, toY, directly)
	self:Reset()

	self.startPoint.x = fromX + firePoint.x
	self.startPoint.y = fromY + firePoint.y
	self.endPoint.x = toX
	self.endPoint.y = toY

	local deg = UtilsDir.GetAngle(self.startPoint, self.endPoint)
	if not directly then
		self.flyType = 1
	else
		self.flyType = 2
	end

	if self.actionData and self.actionData.originDirCount == 1 then
		if self.actionData then
			self.rotation = deg
			if deg > 0 and deg < 180 then
				self.endPoint.x = self.endPoint.x - self.width / 2
			elseif deg < 0 and deg > -180 then
				self.endPoint.x = self.endPoint.x + self.width / 2
			end
		end
	else	
		self.playSettings.direction = UtilsDir.GetDirOfDeg(deg)
		local deg2 = UtilsDir.GetDegOfDir(self.playSettings.direction)
		if deg2 > 180 then
			deg2 = deg2 - 360
		end
		if math.abs(deg2 - deg) > 10 then
			self.rotation = deg - deg2
		else
			self.rotation = 0
		end
	end

	self.xy = self.startPoint
	if self.flyType == 1 then	
		self.lineGenerator:Start(self.startPoint, self.endPoint)
		self.loop = true
	end

	if self.actionData then
		if self.actionData.effectPosition == 10 then
			World.mainEnv:AddChildAt(self, 0)
		else
			World.mainEnv:AddChild(self)
		end
	end

	self.timer:Start()
end

function SkillEffect:UpdateDuration(duration)
	if duration > 0 then	
		fgui.add_timer(duration/1000, 1, self.Destroy, self)
		self.loop = true
	elseif duration==nil or duration == 0 then	
		self.loop = true
	else
	 --play once
		self.loop = false
	end
end

function SkillEffect:SetDirection(value)
	if self.playSettings.direction~=value then
		self.playSettings.direction = value
		self.forceDraw = true
		
		if self.actionData and self.actionData.effectPosition>3 and self.actionData.effectPosition<10 then
			self:AdjustDepth()
		end
	end
end

function SkillEffect:SetAction(value)
	if self.playSettings.action~=value then
		self.playSettings.action = value
		self.playSettings:Rewind()
	end
end
		
function SkillEffect:HitTestEnv(x, y)
	if not self.actionData then	return false end
	
	x = x - self.x
	y = y - self.y
	local dirData = self.actionData.dirList[self.playSettings.direction]
	local rect = dirData.frameList[self.playSettings.curFrame].rect
	if dirData.flip then
		return x>-rect.right and x<-rect.x and y>rect.y and y<rect.bottom
	else
		return x>rect.x and x<rect.right and y>rect.y and y<rect.bottom
	end
end

get.hasFireFrame = function(self)
	return self.actionData and self.actionData.fireFrame>0
end

get.firePoint = function(self)
	if self.actionData then
		return self.actionData.dirList[self.playSettings.direction].firePoint
	else
		return Vector2.New(0,0)
	end
end

function SkillEffect:AdjustPosition()
	local ep = self.actionData.effectPosition
	if ep == 1 or ep == 5 or ep == 8 then
		self.y = self.playY - self.owner.bodyHeight / 2
	elseif ep == 2 or ep == 6 or ep == 9 then
		self.y = self.playY - self.owner.bodyHeight
	end
end

function SkillEffect:AdjustDepth()
	local ep = self.actionData.effectPosition
	if ep == 3 then
	--身体底座
		if self.inFront then		
			self.owner:AddEffect(self, "back")
			self.inFront = false
		end
	elseif ep == 10 then
	 --坐标底座
		if self.inFront then		
			self.owner:AddEffect(self, "bottom")
			self.inFront = false
		end
	elseif ep < 4 then
	 --总在最前
		if not self.inFront then		
			self.owner:AddEffect(self, "front")
			self.inFront = true
		end
	elseif ep < 7 then
	 --正面
		if UtilsDir.IsUpwardDirection(self.owner.direction) then
		--背面方向，特效在底层
			if self.inFront then			
				self.owner:AddEffect(self, "back")
				self.inFront = false
			end
		else
			if not self.inFront then			
				self.owner:AddEffect(self, "front")
				self.inFront = true
			end
		end
	else
	 --背面
		if not UtilsDir.IsUpwardDirection(self.owner.direction) then
		  --正面方向，特效在底层
			if self.inFront then			
				self.owner:AddEffect(self, "back")
				self.inFront = false
			end
		else		
			if not self.inFront then			
				self.owner:AddEffect(self, "front")
				self.inFront = true
			end
		end
	end
end

function SkillEffect:OnUpdate()
	if not self.actionData then
		local ad = self.animation.def.actionList[AniConst.Stand]
		if ad~=nil then 
			self.actionData = ad
			if self.owner then
				self.visible = true
				self:AdjustPosition()
				self:AdjustDepth()
			end
		end
	end

	if self.flyType == 1 then	
		if self.lineGenerator.hasNext then		
			self.xy = self.lineGenerator:Next()
			self.lineGenerator:Skip(self.step)
		else
			if self.onHit:Count()>0 then
				self.onHit(self)
				self.onHit:Clear()
			end
			self:Destroy()
			return
		end
	elseif self.flyType == 2 then	
		if self.onHit:Count()>0 then		
			if self.animation.isFireFrame then			
				self.onHit(self)
				self.onHit:Clear()
			elseif self:HitTestEnv(self.endPoint.x, self.endPoint.y) then			
				self.onHit(self)
				self.onHit:Clear()
			end
		end
	else
		if self.onHit:Count()>0 and self.animation.isFireFrame then		
			self.onHit(self)
			self.onHit:Clear()
		end
	end

	self.animation:Draw(self.forceDraw)
	self.animation:NextFrame()
	self.forceDraw = false
	if self.playSettings.reachEnding and not self.loop then
		self:Destroy()
	end
end

function SkillEffect:OnOwnerEvent(type)
	if type==WEvent.DirectionChanged then
		self:SetDirection(self.owner.direction)
	elseif type==WEvent.OutlineChanged then
		if self.actionData then
			self:AdjustPosition()
		end
	elseif type==WEvent.LeaveView then
		if self.onHit:Count()>0 then		
			self.onHit(self)
			self.onHit:Clear()
		end
	elseif type==WEvent.Destroy then
		if self.onHit:Count()>0 then		
			self.onHit(self)
			self.onHit:Clear()
		end
		self.owner = fgui.null
		self:Destroy()
	end
end

function SkillEffect:Destroy()
	if self.destroyed then return end
	self.destroyed = true

	self.onHit:Clear()
	if self.setFlags then	
		self.setFlags:Unset(self)
		self.setFlags = fgui.null
	end

	if self.parent ~= nil then
		self.parent:RemoveChild(self)
	end
	self.timer:Stop()
	fgui.remove_timer(self.Destroy, self)

	if self.owner then	
		self.owner.eventDispatcher:Remove(self.OnOwnerEvent, self)
		self.owner.onHeatBeat:Remove(self.OnUpdate, self)
		self.owner = fgui.null
	end
	SkillEffect.Pool[self.effectId]:push(self)
end