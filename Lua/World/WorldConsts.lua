ActionType = {
	Auto = 0,
	View = 1,
	Kill = 2,
	Follow = 3,
	Cure = 5
}

ThingType = {
	User = 1,
	Pet = 2,
	Mount = 3,
	NPC = 4,
	Scene = 5,
	Item = 6,
	Corpse = 7,
	Exit = 10,
	OP = 11,

	End = 12
}

AniConst = {
	Stand = 0,
	Walk = 1,
	Attack = 2,
	Attacked = 3,
	Die = 4,
	RideStand = 5,
	RideWalk = 6,
	Prepare = 7,
	Sitted = 8,
	Attack_BaoJi = 9,
	Run = 22
}

WEvent = {
	PathPosChanged = 1,
	RealPosChanged = 2,
	DirectionChanged = 3,
	OutlineChanged = 4,
	EnvChanged = 5,
	EnterView = 6,
	LeaveView = 7,
	Die = 9,

	AvatarReady = 20,
	ActionEnd = 21,
	ActionFire = 22,

	EffectHit = 30,

	Destroy = 999
}
