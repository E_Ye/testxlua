require "MainUI/Voice"

ChatPanel = {}

local COLORS = {"#d2d2d2", "#FFFFCC", "#FFFFCC", "#d2d2d2", "#d2d2d2", 
		"#FFFF00", "#FFFFCC", "#d2d2d2"}

local _panel
local _list
local _speakBtn
local _currentChannel

-- local lockPanel = false

function ChatPanel.OnCreate()
	_panel = MainUI.view:GetChild("chatPanel")
	_list = _panel:GetChild("list")
	-- _list.onClick:Add(function() lockPanel = not lockPanel end)
	_list.onClickItem:Add(function() G_ChatWin:Open() end)

	_speakBtn = _panel:GetChild("btnSpeak")
	_speakBtn.onTouchBegin:Add(ChatPanel.StartRecord)
	_speakBtn.onTouchEnd:Add(ChatPanel.StopRecord)

	_panel:GetChild("btnFriends").onClick:Add(function() G_ChatWin:OpenFriends() end)

	NetClient.Listen(NetKeys.ACMD_CHAT_MSG, ChatPanel.OnMsg)
end

function ChatPanel.DisableSpeak()
	_speakBtn.grayed = true
	_speakBtn.touchable = false
end

function ChatPanel.EnableSpeak()
	_speakBtn.grayed = false
	_speakBtn.touchable = true
end

function ChatPanel.StartRecord()
	ChatWin.StartRecord()
end

function ChatPanel.StopRecord()
	ChatWin.StopRecord()
end

function ChatPanel.OnMsg(cmd, data)
	if data.fid==Player.data.id then
		data.self = true
	end

	local msg = EncodeHTML(data.msg)
	if data.is_gm or data.type==5 or data.ubb_enabled then
		data.msg = ScriptUtils.ParseUBB(msg)
	end

	ChatPanel.AddMsg(data)
	G_ChatWin:AddMsg(data)
end

function ChatPanel.AddMsg(data)
	if _list.numChildren>10 then
		_list:RemoveChildrenToPool(0, _list.numChildren-5)
	end

	local item = _list:AddItemFromPool()

	local arr = {}

	table.insert(arr, "<font color='")
	if data.is_gm then
		table.insert(arr, "#FF66FF")
	elseif data.is_svip then
		table.insert(arr, "#F397B1")
	else
		table.insert(arr, COLORS[data.type])
	end

	table.insert(arr, "'>[")
	table.insert(arr, Defs.ChatChannelNames[data.type + 1])
	table.insert(arr, "]")

	if not string.isNilOrEmpty(data.fname) then
		table.insert(arr, "<font color='#b2b2b2'>")
		table.insert(arr, EncodeHTML(data.fname))
		table.insert(arr, "</font>说")
	end

	table.insert(arr, "：")
	if data.message ~= nil then
	    local second = math.floor(data.message.Duration / 1000)
		table.insert(arr,  "<img src='ui://Sucai/voice'/>     <font color='#aaaaaa'>"..second.."''</font>")
	else
		table.insert(arr, data.msg)
	end
	table.insert(arr, '</font>')
	item.text = table.concat(arr, "")
	-- if not lockPanel then
	_list.scrollPane:ScrollBottom()
	-- end
end

function ChatPanel.AddAlertMsg(msg)
	ChatPanel.AddMsg({type=5, msg=msg})
end

function ChatPanel.ShowModalWait()
	_panel:GetChild("n46").visible = true
end

function ChatPanel.CloseModalWait()
	_panel:GetChild("n46").visible = false
end
