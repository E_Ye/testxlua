require "MainUI/ChatPanel"
require "Windows/bangpai/ClanMgr"

MainUI = {
    --view
}

local _userPanel
local _petPanel
local _critterPanel
local _skillPanel
local _taskList
local _hotkeys
local _displayTarget
local _guaJiZhong
local _xunLuZhong
local _broadcast
local _broadcastMsgs = list()

function MainUI.Create()
    MainUI.view = UIPackage.CreateObject("Main", "MainUI")
    MainUI.view:SetSize(GRoot.inst.width, GRoot.inst.height)
    MainUI.view:AddRelation(GRoot.inst, RelationType.Size)
    MainUI.view.opaque = false
    MainUI.view.fairyBatching = true

    _guaJiZhong = MainUI.view:GetChild("gua_ji_zhong")
    _guaJiZhong.visible = false

    _xunLuZhong = MainUI.view:GetChild("xun_lu_zhong")
    _xunLuZhong.visible = false

    _broadcast = MainUI.view:GetChild("broadcast")
    _broadcast.visible = false

    _userPanel = MainUI.view:GetChild("userPanel")
    _userPanel:GetChild("head").onClick:Add(function() G_MyInfoWin:Open(Player.data.id) end)
    _userPanel:GetChild("btnSheZhi").onClick:Add(function() G_SheZhiWin:Show() end)

    _petPanel = MainUI.view:GetChild("petPanel")
    _petPanel.visible = false

    _critterPanel = MainUI.view:GetChild("critterPanel")
    _critterPanel.visible = false
    _critterPanel.onClick:Add(function() G_PlayerInfoQueryWin:Open(_displayTarget.critter.data, 1) end)
    _displayTarget = CritterRef.New()

    _skillPanel = MainUI.view:GetChild("skillPanel")
    _hotkeys = {}
    for i = 0, 11 do
        local hotkey = _skillPanel:GetChild("g"..i)
        _hotkeys[i] = hotkey
        hotkey.onClick:Add(MainUI.OnClickHotkey)
        local lpg = LongPressGesture.New(hotkey)
        lpg.once = true
        lpg.trigger = 0.6
        lpg.onAction:Add(MainUI.OnLongPressHotKey)
    end
    MainUI.UpdateHotkeys(Player.data)

	_taskList = MainUI.view:GetChild("taskList")
	_taskList:RemoveChildrenToPool()
	_taskList.onClickItem:Add(MainUI.OnClickTaskItem)

    MainUI.SetupButton("btnZuoJi", G_MountWin)
    MainUI.SetupButton("btnChongWu", G_PetWin)
    MainUI.SetupButton("btnFaBao", G_FaBaoWin)
    MainUI.SetupButton("btnGuaJi", G_GuaJiWin)
    MainUI.SetupButton("btnMap", G_MapWin)
    MainUI.SetupButton("btnTask", G_TaskWin)
    MainUI.SetupButton("btnXiLian", G_XiLianChiWin)
    MainUI.view:GetChild("btnBeiBao").onClick:Add(function() G_BagWin:Open(0) end)
    MainUI.view:GetChild("btnTeam").onClick:Add(function() G_TeamWin:Open(Player.data.id) end)

    MainUI.SetupButton("btnPaiHang", G_PaiHangBangWin)
    MainUI.SetupButton("btnFuLi", G_FuLiWin)
    MainUI.SetupButton("btnXiLian", G_XiLianChiWin)
    MainUI.SetupButton("btnShangCheng", G_MallWin)
    MainUI.SetupButton("btnLine", G_FenXianWin)
    MainUI.view:GetChild("btnBangPai").onClick:Add(MainUI.OnClickBangPai)

    Voice.OnCreate(UIPackage.CreateObject("Main", "Recording"))
    ChatPanel.OnCreate()

    NetClient.Listen(NetKeys.ACMD_ACTIVE, MainUI.OnActive)
    NetClient.Listen(NetKeys.ACMD_MAIN_LIST, MainUI.OnHotkeyList)
    
    NetClient.ListenRet(NetKeys.QCMD_USE_ITEM, MainUI.UseItemQCMDHandler)
end

function MainUI.OnActive(cmd, data)
    local progressbar = MainUI.view:GetChild("signal").asProgress
    local ping = (UnityEngine.Time.realtimeSinceStartup - data.client_time)
    if ping < 0 then
        ping = 0
    end
    if ping > progressbar.max then
        ping = progressbar.max
    end
    progressbar.value = progressbar.max - ping
end

function MainUI.CellDrag(context)
    if G_MainListWin ~= nil then
        G_MainListWin:OnClickSkillItem(context)
    end
end

function MainUI.SetupButton(btnName, uiInst)
    MainUI.view:GetChild(btnName).onClick:Add(MainUI.onBtnClick, uiInst)
end

function MainUI.onBtnClick(uiInst)
    if (uiInst.isShowing == false) then
        uiInst:Show()
    else
        uiInst:Hide()
    end
end

function MainUI.Show()
    if MainUI.view==nil then
        MainUI.Create()
        Voice.OnShow()
    end

    GRoot.inst:AddChildAt(MainUI.view, 0)    
    
    MainUI.UpdatePlayerStatus()
    MainUI.ShowPetStatus(Player.data.pet)
    MainUI.UpdateBattery()
    
    fgui.add_timer(3, 0, MainUI.UpdateBattery)
    MainUI.UpdateDateTime()
    fgui.add_timer(1, 0, MainUI.UpdateDateTime)
    NetClient.KeepActive()
end

function MainUI.Hide()
    fgui.remove_timer(MainUI.UpdateBattery)
    fgui.remove_timer(MainUI.UpdateDateTime)
    GRoot.inst:RemoveChild(MainUI.view)
    Voice.OnHide()
end

function MainUI.OnNewScene()
    MainUI.view:GetChild("mapName").text = World.sceneData.name
    MainUI.UpdateLineInfo()
end

function MainUI.UpdateBattery()
    MainUI.view:GetChild("battery").value = FairyGame.BatteryInfo.BatteryLevel()
end

function MainUI.UpdateDateTime()
    MainUI.view:GetChild("txtTime").text = System.DateTime.Now:ToString("HH:mm")
end

function MainUI.UpdateLineInfo()
    if Player.data.line_na ~= nil then
        MainUI.view:GetChild("btnLine"):GetChild("n2").text = Player.data.line_na
    end
end

function MainUI.UpdateTasks(tasks)
    if _taskList==nil then return end
	_taskList:RemoveChildrenToPool()
	for i,task in ipairs(tasks.tss) do
		local item = _taskList:AddItemFromPool()
		item.text = MainUI.GetTaskDesc(task)
		item.data = task
	end
end

function MainUI.GetTaskDesc(task)
	local str = task.name
	str = str.."["..task.status.."]"
	return str
end

function MainUI.OnClickTaskItem(context)
	local task = context.data.data
	local target = task.targets[1]
	if #task.targets > 1 and task.ready ~= nil and task.ready == false then
	   target = task.targets[2]
    end
    
    if target==nil then
        G_TaskWin:Open(task.id)
        return
    end

    if Bot.active then Bot.Stop() end
    AutoRouting.Start(target.eid, target, ActionType.Auto, true)
end

--点击快捷键，触发技能
function MainUI.OnClickHotkey(context)
    local cell = context.sender
    local index = cell.parent:GetChildIndex(cell)
    if G_MainListWin.isShowing then
         G_MainListWin:Open(index)
         return
    end

    if cell.data == nil then
        return
    end
    if index == 0 or index == 1 then
        NetClient.Send(NetKeys.QCMD_USE_ITEM, 0, '', cell.data.rid, 0)
    else
        Player.UseSkill(cell.data.id)
    end
end

--长按快捷键，设置技能
function MainUI.OnLongPressHotKey(context)
    local cell = context.sender.host
    local index = cell.parent:GetChildIndex(cell)
    G_MainListWin:Open(index)
end

function MainUI.OnHotkeyList(cmd, data)
    MainUI.UpdateHotkeys(data)
    if G_MainListWin ~= nil and G_MainListWin.isShowing then
        G_MainListWin:ReadyToUpdate()
    end
end

function MainUI.UpdateHotkeys(data)
    local main_list = data.main_list
    local d
    for i = 0, 11 do
        d = nil
        if main_list then
            for _, v in ipairs(main_list) do
                if v~=nil and v.pos==i then
                    d = v
                    break
                end
            end
        end

        if d~=nil then
            _hotkeys[i]:SetItem(d)
        else
            _hotkeys[i]:Clear()
        end
    end

    Player.data.main_list = main_list
    Player.skillSet:LoadHotkeySkills()
end

function MainUI.NotifyCooling(iid, progress)
    if iid==SkillCD.i_Yao then
        _hotkeys[0]:SetCD(progress)
        _hotkeys[1]:SetCD(progress)
    else
        for i = 0, 11 do
            local hotkey = _hotkeys[i]
            if hotkey.data~=nil and hotkey.data.id==iid then
                hotkey:SetCD(progress)
            end
        end
    end
end

function MainUI.UpdatePlayerStatus()
    _userPanel:GetChild("head").icon = "ui://Sucai/u"..Player.data.fct..Player.data.sex
    _userPanel:GetChild("txtLevel").text = Player.data.lvl

    MainUI.UpdatePlayerStatus_hp()
    MainUI.UpdatePlayerStatus_mp()
    MainUI.UpdatePlayerStatus_exp()
end

function MainUI.UpdatePlayerStatus_hp()
    _userPanel:GetChild("hpBar").value = Player.data.hp or 0
    _userPanel:GetChild("hpBar").max = Player.data.max_hp or 0
end

function MainUI.UpdatePlayerStatus_mp()
    _userPanel:GetChild("mpBar").value = Player.data.mp or 0
    _userPanel:GetChild("mpBar").max = Player.data.max_mp or 0
end

function MainUI.UpdatePlayerStatus_exp()
    if Player.data.exp ~= nil then
        _userPanel:GetChild("expBar").value = Player.data.exp or 0
    else
        _userPanel:GetChild("expBar").value = 0
    end
    _userPanel:GetChild("expBar").max = Player.data.uexp or 0
end

function MainUI.ShowPetStatus()
    if not Player.me.pet then
        _petPanel.visible = false
        return
    end
    
    _petPanel.visible = true
    local data = Player.me.pet.data
    _petPanel:GetChild("head").icon = Player.me.pet.iconSrc
    _petPanel:GetChild("txtLevel").text = data.lvl
    MainUI.UpdatePetStatus()
end

function MainUI.HidePetStatus()
    _petPanel.visible = false
end

function MainUI.UpdatePetStatus()
    MainUI.UpdatePetStatus_hp()
    MainUI.UpdatePetStatus_mp()
end

function MainUI.UpdatePetStatus_hp()
     local data = Player.me.pet.data
    _petPanel:GetChild("hpBar").value = data.hp or 0
    _petPanel:GetChild("hpBar").max = data.max_hp or 0
end

function MainUI.UpdatePetStatus_mp()
     local data = Player.me.pet.data
    _petPanel:GetChild("mpBar").value = data.mp or 0
    _petPanel:GetChild("mpBar").max = data.max_mp or 0
end

function MainUI.ShowCritterStatus(target)
    if target == nil or target==World.player then return end
    
    _displayTarget.critter = target
    _critterPanel.visible = true
    _critterPanel:GetChild("head").icon = target.iconSrc
    _critterPanel:GetChild("n21").text = target.data.name
    _critterPanel:GetChild("txtLevel").text = target.data.lvl
    MainUI.UpdateCritterStatus(target)

    fgui.add_timer(2, 0, MainUI.CheckCritterStatus)
end

function MainUI.CheckCritterStatus()
    if _displayTarget.critter==nil then
        MainUI.HideCritterStatus()
    end
end

function MainUI.HideCritterStatus()
    _displayTarget.critter = nil
    _critterPanel.visible = false
    fgui.remove_timer(MainUI.CheckCritterStatus)
end

function MainUI.UpdateCritterStatus(target)
    if target~=_displayTarget.critter then return end

    _critterPanel:GetChild("hpBar").value = target.data.hp or 0
    _critterPanel:GetChild("hpBar").max = target.data.max_hp or 0
    _critterPanel:GetChild("mpBar").value = target.data.mp or 0
    _critterPanel:GetChild("mpBar").max = target.data.max_mp or 0
end

function MainUI.UseItemQCMDHandler(cmd, data)
    if data.items ~= nil and data.items.is_main_list then
        NetClient.Send(NetKeys.QCMD_MAIN_LIST_VIEW)
    end
    
    if data.code~=0 then
        AlertWin.Open(data.msg)
    else
        if data.msg then  
            ChatPanel.AddAlertMsg(data.msg) 
        end
    end
end

function MainUI.OnClickBangPai()
    -- G_BangPaiListWin:Show()
    ClanMgr:ShowClanWindow()
end

function MainUI.ShowGuaJiSign(value)
    _guaJiZhong.visible = value
end

function MainUI.ShowXunLuSign(value)
    _xunLuZhong.visible = value
end

function MainUI.ShowBroadcastMessage(msg)
    if _broadcast.visible then
        _broadcastMsgs:push(msg) 
        return
    end

    MainUI.PlayBroadcastMessage(msg)
end

function MainUI.PlayBroadcastMessage(msg)
    _broadcast.visible = true
    _broadcast.title = msg
    local titleObject =  _broadcast:GetChild("title")
    titleObject.x = _broadcast.width
    local tween = titleObject:TweenMoveX(_broadcast.width-titleObject.width, titleObject.width/150)
    TweenUtils.SetEase(tween, Ease.Linear)
    TweenUtils.OnComplete(tween, MainUI.BroadcastComplete)
end

local _flag = false
function MainUI.BroadcastComplete()
    if _broadcastMsgs.length==0 then
        if _flag then
            _flag = false
            _broadcast.visible = false
        else
            _flag = true
            fgui.add_timer(1, 1, MainUI.BroadcastComplete)
        end
    else
        MainUI.PlayBroadcastMessage(_broadcastMsgs:shift())
    end
end