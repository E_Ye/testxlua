local json = require "cjson"
Voice={}

local _playingVoiceItem
local _waitForPlayVoiceItem
local _recordingObj
local _voiceLength
local _currentChannel
local _ext
local _toUser

function Voice.OnCreate(recordingObj)
	_recordingObj = recordingObj
	GameLink.OnLogin = GameLink.OnLogin + Voice.OnGLLogin
	GameLink.OnJoinChannel = GameLink.OnJoinChannel + Voice.OnGLJoinChannel
	GameLink.OnStartRecord = GameLink.OnStartRecord + Voice.OnStartRecord
	GameLink.OnRecording = GameLink.OnRecording + Voice.OnRecording
	GameLink.OnStopRecord = GameLink.OnStopRecord + Voice.OnStopRecord
	GameLink.OnPlayMessageStart = GameLink.OnPlayMessageStart + Voice.OnPlayMessageStart
	GameLink.OnPlayingMessage = GameLink.OnPlayingMessage + Voice.OnPlayingMessage
	GameLink.OnPlayMessageStop = GameLink.OnPlayMessageStop + Voice.OnPlayMessageStop
	GameLink.OnUploadVoice = GameLink.OnUploadVoice + Voice.OnUploadVoice
	GameLink.OnSendMessage = GameLink.OnSendMessage + Voice.OnSendMessage
	GameLink.OnReceiveMessage = GameLink.OnReceiveMessage + Voice.OnReceiveMessage
end

function Voice.OnShow()
	ChatPanel.DisableSpeak()
	G_ChatWin:DisableSpeak()
	--注册聊天账号
	GameLink.Login(Player.me.name)
end

function Voice.OnHide()
	print("登出")
	GameLink.StopRecord()
	GameLink.StopPlay()
	if _currentChannel ~= nil then
		Voice.LeaveChannel()
	end
	GameLink.Logout()
	_playingVoiceItem = nil
	_waitForPlayVoiceItem = nil
end

function Voice.OnGLLogin(target, error)
	if error.Code ~= GLErrorCode.NoError then
		print("登录聊天账户失败:"..error.Code:ToString())
		return
	end
	GameLink.SetProfile(Player.data.name,"","{\"lvl\":"..Player.data.lvl..",\"fct\":"..Player.data.fct..",\"sex\":"..Player.data.sex.."}")
	Voice.JoinChannel("世界")
end

function Voice.LeaveChannel()
	if _currentChannel ~= nil then
		GameLink.LeaveChannel(_currentChannel)
		ChatPanel.DisableSpeak()
		G_ChatWin:DisableSpeak()
		_currentChannel = nil
	end
end

function Voice.JoinChannel(channelName)
	--GameLink.StopRecord()
	--GameLink.StopPlay()
	Voice.LeaveChannel()
	GameLink.JoinChannel(channelName)
end

function Voice.OnGLJoinChannel(channel, error)
	ChatWin.OnJoinChannel()
	if error.Code ~= GLErrorCode.NoError then
		print("进入频道失败:"..error.Code:ToString())
		return
	end
	_currentChannel = channel
	ChatPanel.EnableSpeak()
	G_ChatWin:EnableSpeak()
end

function Voice.SetItemPlayingStatus(playing)
	local img = ScriptUtils.GetHtmlImage(_playingVoiceItem:GetChild("msg"), 0)
	if playing then
		img.url = "ui://Sucai/voice_playing"
	else
		img.url = "ui://Sucai/voice"
	end
end

function Voice.OnPlayMessageStart(message, error)
	if error.Code ~= GLErrorCode.NoError then
		print("语音播放失败:"..error.Code:ToString())
		Voice.SetItemPlayingStatus(false)
		_playingVoiceItem = nil
		return
	end
end

function Voice.OnPlayingMessage(message, duration)
end

function Voice.OnPlayMessageStop(message)
	Voice.SetItemPlayingStatus(false)
	_playingVoiceItem = nil

	if _waitForPlayVoiceItem ~= nil then
		local v = _waitForPlayVoiceItem
		_waitForPlayVoiceItem = nil
		Voice.PlayVoice(v)
	end
end

function Voice.PlayItem(item)
	if _playingVoiceItem ~= nil then --如果有音频正在播放
		if _playingVoiceItem ~= item then --如果将要播放的音频不是当前播放音频,则记录下将要播放的音频
			_waitForPlayVoiceItem = item
		end
		--停止播放
		GameLink.StopPlay()
	else --没有正在播放的音频
		Voice.PlayVoice(item)
	end
end

function Voice.PlayVoice(item)
	_playingVoiceItem = item
	Voice.SetItemPlayingStatus(true)
	--播放音频信息
	GameLink.PlayVoice(item.data.message)
end

function Voice.StartRecord()
	GameLink.StartRecord()
end

function Voice.StopRecord( ext, toUser )
	_ext = ext
	_toUser = toUser
	GameLink.StopRecord()
end

function Voice.OnStartRecord(error)
	if error.Code ~= GLErrorCode.NoError then
		print("开始录音失败:"..error.Code:ToString())
		return
	end
    GRoot.inst:AddChild(_recordingObj)
    _recordingObj:SetSize(GRoot.inst.width, GRoot.inst.height)
    _recordingObj:AddRelation(GRoot.inst, RelationType.Size)
    fgui.add_timer(0.5, 0, Voice.OnRecordState)
end

function Voice.OnStopRecord(file, voiceContent, duration, error)
    fgui.remove_timer(Voice.OnRecordState)
    _recordingObj:GetChild("vol").value = 0
    _recordingObj:GetChild("time").value = 0
    GRoot.inst:RemoveChild(_recordingObj)
	if error.Code ~= GLErrorCode.NoError then
		print("结束录音失败:"..error.Code:ToString())
		return
	end
	_voiceLength = duration
	--上传音频
	GameLink.UploadVoice(file)
end

function Voice.OnRecordState()
	local state = GameLink.GetRecordingState()
    _recordingObj:GetChild("vol").value = state.volume * 100
    _recordingObj:GetChild("time").value = state.duration / 1000
end

function Voice.OnUploadVoice(file, url, error)
	if error.Code ~= GLErrorCode.NoError then
		print("音频上传失败:"..error.Code:ToString())
		return
	end
	--发送消息
	local target
	if _toUser ~= nil and _toUser ~= "" then
		target = GLTarget.New(GLTargetType.User, _toUser)
		_toUser = nil
	else
		target = _currentChannel
	end
	GameLink.SendVoice(target, url, _voiceLength, _ext, "")
	_ext = ""
end

function Voice.OnSendMessage(message, error)
	if error.Code ~= GLErrorCode.NoError then
		print("消息发送失败:"..error.Code:ToString())
		return
	end
	Voice.HandleMessage(message)
end

function Voice.OnReceiveMessage(message)
	Voice.HandleMessage(message)
end

function Voice.HandleMessage(message)
	if message.Type == GLMessageType.Voice then
		local data = {}
		if message.Receiver.Type == GLTargetType.Channel then
			data.type = tonumber(message.Receiver.ExtraInfo)
		else
			data.type = 0
		end
		local messageExtraText = json.decode(message.ExtraText)
		local senderExtraText = json.decode(message.Sender.ExtraInfo)
		data.type = messageExtraText.channel
		data.fid = message.Sender.Account
		data.fname = message.Sender.Nickname
		data.tid = message.Receiver.Account
		data.flvl = senderExtraText.lvl
		data.ffct = senderExtraText.fct
		data.fsex = senderExtraText.sex
		data.message = message
		ChatPanel.AddMsg(data)
		G_ChatWin:AddMsg(data)
	end
end