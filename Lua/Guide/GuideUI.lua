GuideUI = {}

GuideDirection = {
    Left = 0,
    Right = 1,
    Up = 2,
    Down = 3
}

local _winLeft
local _winRight
local _topMask
local _bottomMask
local _finger
local _arrow
local _callback
local _enableJoystick

function GuideUI.Create()
    _winLeft = UIPackage.CreateObject("Guide", "指引消息窗口左")
    _winRight = UIPackage.CreateObject("Guide", "指引消息窗口右")
    _topMask = UIPackage.CreateObject("Guide", "指引挡板")
    _bottomMask = UIPackage.CreateObject("Guide", "指引挡板")
    _finger = UIPackage.CreateObject("Guide", "指引手指")
    _arrow = UIPackage.CreateObject("Guide", "指引箭头")
    _arrow:SetPivot(_arrow.width, _arrow.height / 2)

    _winLeft.touchable = false
    _winRight.touchable = false
    _finger.touchable = false
    _arrow.touchable = false
    _bottomMask:GetChild("n0").alpha = 0.3

    _topMask:SetSize(GRoot.inst.width, GRoot.inst.height)
    _topMask.sortingOrder = SortingOrder.Guide
    _winLeft.sortingOrder = SortingOrder.Guide + 1
    _winRight.sortingOrder = SortingOrder.Guide + 2
    _finger.sortingOrder = SortingOrder.Guide + 3
    _arrow.sortingOrder = SortingOrder.Guide + 4

    Listen(_topMask.onClick, GuideUI.OnClick)
    Listen(_topMask.onTouchBegin, GuideUI.onTouchBegin)
end

function GuideUI.CloseAll()
    if _topMask.parent ~= nil then
        GRoot.inst:RemoveChild(_topMask)
    end
    if _bottomMask.parent ~= nil then
        GRoot.inst:RemoveChild(_bottomMask)
    end
    if _winLeft.parent ~= nil then
        GRoot.inst:RemoveChild(_winLeft)
    end
    if _winRight.parent ~= nil then
        GRoot.inst:RemoveChild(_winRight)
    end
    if _finger.parent ~= nil then
        GRoot.inst:RemoveChild(_finger)
    end
    if _arrow.parent ~= nil then
        GRoot.inst:RemoveChild(_arrow)
    end
    _callback = nil
    _enableJoystick = false
end

function GuideUI.IsShowing()
    return _topMask.parent ~= nil
end

function GuideUI.MakeSceneDark()
    if _bottomMask.parent == nil then
        GRoot.inst:AddChildAt(_bottomMask, 0)
    end
end

function GuideUI.EnableJoystick()
    _enableJoystick = true
    _finger:GetTransition("t0"):Play()
end

function GuideUI.ShowMsg(msg, winPos, girlPos, callback)
    if _topMask.parent == nil then
        GRoot.inst:AddChild(_topMask)
        GuideUI.MakeSceneDark()
    end

    if callback ~= nil then
        _callback = callback
    end
    if _winLeft.parent ~= nil then
        GRoot.inst:RemoveChild(_winLeft)
    end
    if _winRight.parent ~= nil then
        GRoot.inst:RemoveChild(_winRight)
    end

    local win
    if girlPos == GuideDirection.Left or girlPos == GuideDirection.Up then
        win = _winLeft
    else
        win = _winRight
    end
    GRoot.inst:AddChild(win)
    win:GetChild("msg").text = msg
    local origPos = Vector2.New()
    local targetPos = Vector2.New()
    if winPos==GuideDirection.Left or winPos==GuideDirection.Up then
        targetPos.x = 100
        targetPos.y = GRoot.inst.height / 2 - win.height / 2
        origPos.x = -win.width
        origPos.y = targetPos.y
    else
        targetPos.x = GRoot.inst.width - win.width - 100
        targetPos.y = GRoot.inst.height / 2 - win.height / 2
        origPos.x = GRoot.inst.width + win.width
        origPos.y = targetPos.y
    end
    win.xy = origPos
    local tween = win:TweenMove(targetPos, 0.5, true)
    TweenUtils.SetEase(tween, Ease.OutQuad)
end

function GuideUI.ShowFinger(pt, callback)
    if _topMask.parent == nil then
        GRoot.inst:AddChild(_topMask)
        GuideUI.MakeSceneDark()
    end

    if callback ~= nil then
        _callback = callback
    end
    _finger.xy = pt
    GRoot.inst:AddChild(_finger)
end

function GuideUI.ShowArrow(pt, dir, callback)
    if _topMask.parent == nil then
        GRoot.inst:AddChild(_topMask)
        GuideUI.MakeSceneDark()
    end

    if callback ~= nil then
        _callback = callback
    end
    _arrow.x = pt.x - _arrow.width
    _arrow.y = pt.y - _arrow.height / 2
    if dir==GuideDirection.Left then
        _arrow.rotation = 180
    elseif dir==GuideDirection.Up then
        _arrow.rotation = -90
    elseif dir==GuideDirection.Right then
         _arrow.rotation = 0
    elseif dir==GuideDirection.Down then
         _arrow.rotation = 90
    end
    GRoot.inst:AddChild(_arrow)
end

function GuideUI.OnClick()
    if _enableJoystick then return end

    local cb = _callback
    GuideUI.CloseAll()

    if cb ~= nil then
        cb()
    end
end

function GuideUI.onTouchBegin(context)
    if _enableJoystick then
        JoystickModule.instance:Trigger(context)

        if not ExistsTimer(GuideUI.__timeout) then
            AddTimer(2, 1, GuideUI.__timeout)
        end
    end
end

function GuideUI.__timeout()
    _enableJoystick = false
    _finger:GetTransition("t0"):Stop()
    GuideUI.OnClick()
end