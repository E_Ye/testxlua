require 'Guide/GuideUI'

GuideCenter = {}
local GUIDE_ENTER_FB = false
local GUIDE_USE_ITEM = false
local GUIDE_UPGRADE_SKILL = false
local GUIDE_CANG_BAO_GE = false
local GUIDE_SHEN_QI = false
local GUIDE_FORGE = false
local GUIDE_SELL_ITEM = false
local GUIDE_BUY_BEI_DONG_SKILL = false
local GUIDE_YUN_GONG = false

local autoClickNext = false
local canClickNext = false

--进入场景
function GuideCenter.OnEnterScene()
    local sid = Game.env.scene.sid
    if sid == "s761" then --新手关
        GUIDE_CONTINUE = true

        local btn = MainUI.view:GetChild("joystick")
        local guidePos = btn:LocalToGlobal(Vector2.New(btn.width / 2, btn.height / 2))
        GuideUI.ShowFinger(guidePos)
        GuideUI.EnableJoystick()
        GuideUI.ShowMsg("触摸方向键移动，可控制角色移动方向", GuideDirection.Left, GuideDirection.Right,
            function()
                local btn = MainUI.battleView:GetChild("attackBtn")
                local guidePos = btn:LocalToGlobal(Vector2.New(btn.width / 2, btn.height / 2))
                GuideUI.ShowFinger(guidePos)
                GuideUI.ShowMsg("普通攻击，最多可进行四段连击", GuideDirection.Right, GuideDirection.Left,
                    function()                    
                        SkillModule.TriggerAttack()
                    end)
            end)
    end
end

--NPC对话开始
function GuideCenter.NotifyTalkBegin()
    if TalkWin.diaId=="s2_b" then --第一个对话弹出
        GuideUI.ShowMsg("大侠，点击屏幕任意一处可快速跳过剧情对话哦~", GuideDirection.Right, GuideDirection.Right,
            function()            
                TalkWin.OnClick()

                local btn = MainUI.cityView:GetChild("taskPanel")
                local guidePos = btn:LocalToGlobal(Vector2.New(btn.width / 2, btn.height / 2))
                GuideUI.ShowFinger(guidePos)
                GuideUI.ShowMsg("点击这里继续任务", GuideDirection.Right, GuideDirection.Left,
                    function()                    
                        TaskModule.OnClickTask()
                    end)
            end)
        GuideUI.ShowFinger(Vector2.New(GRoot.inst.width * 3 / 4, GRoot.inst.height - 60))
    end
end

--NPC对话结束
function GuideCenter.NotifyTalkOver()
    if TalkWin.diaId=="s762_b" then --进入第一关后的对话
        local btn = MainUI.battleView:GetChild("skill0")
        local guidePos = btn:LocalToGlobal(Vector2.New(btn.width / 2, btn.height / 2))
        GuideUI.ShowFinger(guidePos)
        GuideUI.ShowMsg("战斗过程中使用技能可加大角色的输出哦", GuideDirection.Right, GuideDirection.Left,
            function()
                SkillModule.TriggerSkill()
            end)
    end
end

--任务状态更新
function GuideCenter.OnTaskUpdate()
    local taskData = TaskModule.taskData

    printf("Received task %s,%s", taskData.id, taskData.status)

    if taskData.id=="t9" and taskData.status==1 then
        local btn = MainUI.cityView:GetChild("taskPanel")
        local guidePos = btn:LocalToGlobal(Vector2.New(btn.width / 2, btn.height / 2))
        GuideUI.ShowFinger(guidePos)
        GuideUI.ShowMsg("点击这里继续任务", GuideDirection.Right, GuideDirection.Left,
            function()
                GUIDE_ENTER_FB = true
                TaskModule.OnClickTask()
            end)
    end

    canClickNext = true
    GuideCenter.ContinueTaskFlow()
end

function GuideCenter.ContinueTaskFlow()
    if autoClickNext and canClickNext then
        if not GuideUI.isShowing then
            autoClickNext = false
            canClickNext = false
            TaskModule.OnClickTask()
        end
    end
end

function GuideCenter.OnTaskAccepted()
    autoClickNext = true
    canClickNext = false

    local tid = TaskModule.taskData.id
    if tid=="t97" then
        MainUI.ShowBottomButtons(true)
        GuideUI.ShowArrow(MainBtnModule.GetBtnPos("skillBtn"), GuideDirection.Down)
        GuideUI.ShowMsg("点击技能", GuideDirection.Right, GuideDirection.Right,
            function()
                GUIDE_UPGRADE_SKILL = true
                G_SkillWin:Show()
            end)
    elseif tid=="t106" then
        MainUI.ShowBottomButtons(true)
        GuideUI.ShowArrow(MainBtnModule.GetBtnPos("cangbaogeBtn", GuideDirection.Up), GuideDirection.Up)
        GuideUI.ShowMsg("点击藏宝阁", GuideDirection.Right, GuideDirection.Right,
            function()
                GUIDE_BUY_BEI_DONG_SKILL = true
                G_CangJingGeWin:Show()
            end)
    end 
end

function GuideCenter.OnTaskCompleted()
    autoClickNext = true
    canClickNext = false

    local tid = TaskModule.taskData.id
    if tid=="t93" then
        MainUI.ShowBottomButtons(true)
        GuideUI.ShowArrow(MainBtnModule.GetBtnPos("bagBtn"), GuideDirection.Down)
        GuideUI.ShowMsg("点击背包", GuideDirection.Left, GuideDirection.Left,
            function()            
                GUIDE_USE_ITEM = true
                G_BagWin:Show()
            end)
    elseif tid=="t95" then
        MainUI.ShowBottomButtons(true)
        GuideUI.ShowArrow(MainBtnModule.GetBtnPos("cangbaogeBtn", GuideDirection.Up), GuideDirection.Up)
        GuideUI.ShowMsg("点击藏宝阁", GuideDirection.Right, GuideDirection.Right,
            function()
                GUIDE_CANG_BAO_GE = true
                G_CangJingGeWin:Show()
            end)
    elseif tid=="t101" then
        MainUI.ShowBottomButtons(true)
        GuideUI.ShowArrow(MainBtnModule.GetBtnPos("shenqiBtn"), GuideDirection.Down)
        GuideUI.ShowMsg("点击神器", GuideDirection.Right, GuideDirection.Right,
            function()
                GUIDE_SHEN_QI = true
                G_ShenQiWin:Show()
            end)
    elseif tid=="t102" then
        MainUI.ShowBottomButtons(true)
        GuideUI.ShowArrow(MainBtnModule.GetBtnPos("qianghuaBtn"), GuideDirection.Down)
        GuideUI.ShowMsg("点击强化", GuideDirection.Right, GuideDirection.Right,
            function()
                GUIDE_FORGE = true
                G_ForgeWin:Show()
            end)
    elseif tid=="t103" then
        MainUI.ShowBottomButtons(true)
        GuideUI.ShowArrow(MainBtnModule.GetBtnPos("bagBtn"), GuideDirection.Down)
        GuideUI.ShowMsg("点击背包", GuideDirection.Right, GuideDirection.Right,
            function()
                GUIDE_SELL_ITEM = true
                G_BagWin:Show()
            end)
    end
end

function GuideCenter.OnTaskWin()
    if TaskModule.taskData.id=='t9' and TaskModule.taskData.status==0 then
        GuideUI.ShowMsg("点击任意位置接受任务~", GuideDirection.Right, GuideDirection.Right,
            function()
                G_TaskWin:OnClick()
            end)
    end
end

--指示单人出战的指引
function GuideCenter.OnFuBenWin()
    if not GUIDE_ENTER_FB then  return end
    GUIDE_ENTER_FB = false

    local btn = G_FuBenXiangQingWin.contentPane:GetChild("btn_single")
    local guidePos = btn:LocalToGlobal(Vector2.New(btn.width / 2, btn.height / 2))
    GuideUI.ShowFinger(guidePos)
    GuideUI.ShowMsg("点击进入任务场景", GuideDirection.Left, GuideDirection.Left,
        function()
            G_FuBenXiangQingWin:OnSingleFun()
        end)
end

--指示使用装备的指引
function GuideCenter.OnBagWin()
    if GUIDE_USE_ITEM then
        local btn = G_BagWin.contentPane:GetChild("list_items"):GetChildAt(0)
        local guidePos = btn:LocalToGlobal(Vector2.New(btn.width / 2, btn.height))
        GuideUI.ShowArrow(guidePos, GuideDirection.Up)
        GuideUI.ShowMsg("点击需要装备的道具", GuideDirection.Right, GuideDirection.Right,
            function()
               btn:OnClick()
            end)
    elseif GUIDE_SELL_ITEM then
        local btn = G_BagWin.contentPane:GetChild("list_items"):GetChildAt(0)
        local guidePos = btn:LocalToGlobal(Vector2.New(btn.width / 2, btn.height))
        GuideUI.ShowArrow(guidePos, GuideDirection.Up)
        GuideUI.ShowMsg("点击需要出售的道具", GuideDirection.Right, GuideDirection.Right,
            function()        
               btn:OnClick()
            end)
    end
end

--指示使用装备的指引
function GuideCenter.OnItemInfoWin()
    if GUIDE_USE_ITEM then
        GUIDE_USE_ITEM = false

        local btn = G_ItemInfoWin.contentPane:GetChild("btn0")
        local guidePos = btn:LocalToGlobal(Vector2.New(btn.width * 3 / 4, btn.height / 2))
        GuideUI.ShowFinger(guidePos)
        GuideUI.ShowMsg("点击装备按钮", GuideDirection.Left, GuideDirection.Left,
            function()        
                G_ItemInfoWin:OnEquipBtn0()

                GuideUI.ShowFinger(Vector2.New(GRoot.inst.width - 80, 30))
                GuideUI.ShowMsg("装备成功！点击返回", GuideDirection.Right, GuideDirection.Left,
                    function()
                        G_BagWin:Hide()
                        GuideCenter.ContinueTaskFlow()
                    end)
            end)
    elseif GUIDE_SELL_ITEM then
        GUIDE_SELL_ITEM = false

        local btn = G_ItemInfoWin.contentPane:GetChild("btn1")
        local guidePos = btn:LocalToGlobal(Vector2.New(btn.width * 3 / 4, btn.height / 2))
        GuideUI.ShowFinger(guidePos)
        GuideUI.ShowMsg("点击出售按钮", GuideDirection.Left, GuideDirection.Left,
            function()     
                --720822# 能否让出售装备的指引只是提示，不真的出售装备   
                --G_ItemInfoWin:OnEquipBtn1(true)

                GuideUI.ShowFinger(Vector2.New(GRoot.inst.width - 80, 30))
                GuideUI.ShowMsg("点击返回", GuideDirection.Right, GuideDirection.Left,
                    function()                
                        G_BagWin:Hide()
                        GuideCenter.ContinueTaskFlow()
                    end)
            end)  
    end
end

function GuideCenter.OnCangBaoGeWin()
    if GUIDE_CANG_BAO_GE then
        GUIDE_CANG_BAO_GE = false

        G_CangJingGeWin.contentPane:GetController("c1").selectedIndex = 0
        local btn = G_CangJingGeWin.contentPane:GetChild("txt_sw")
        local guidePos = btn:LocalToGlobal(Vector2.New(btn.width/2, btn.height))
        GuideUI.ShowArrow(guidePos, GuideDirection.Up)
        GuideUI.ShowMsg("玩家通关获得的声望可用来购买技能和宝物", GuideDirection.Right, GuideDirection.Right,
            function() 
                local btn2 = G_CangJingGeWin.contentPane:GetChild("tab0"):GetChild("list"):GetChildAt(0):GetChild("btn")
                local guidePos = btn2:LocalToGlobal(Vector2.New(btn2.width / 2, btn2.height / 2))
                GuideUI.ShowFinger(guidePos)
                GuideUI.ShowMsg("大侠，让我们先购买一个技能吧", GuideDirection.Left, GuideDirection.Left,
                    function()
                        GuideCenter.SimulateClick(btn2)

                        local btn3 = G_ConfirmWin.contentPane:GetChild("ok")
                        local guidePos = btn3:LocalToGlobal(Vector2.New(btn3.width / 2, btn3.height / 2))
                        GuideUI.ShowFinger(guidePos)
                        GuideUI.ShowMsg("点击确定", GuideDirection.Left, GuideDirection.Left,
                            function()
                                GuideCenter.SimulateClick(btn3)

                                GuideUI.ShowFinger(Vector2.New(GRoot.inst.width - 80, 30))
                                GuideUI.ShowMsg("兑换成功！点击返回", GuideDirection.Right, GuideDirection.Left,
                                    function()
                                        G_CangJingGeWin:Hide()
                                        GuideCenter.ContinueTaskFlow()
                                    end)
                        end)
                    end)
            end)
    elseif GUIDE_BUY_BEI_DONG_SKILL then
        GUIDE_BUY_BEI_DONG_SKILL = false

        G_CangJingGeWin.contentPane:GetController("c1").selectedIndex = 0
        local list = G_CangJingGeWin.contentPane:GetChild("tab0"):GetChild("list")
        local last = list.numChildren-1
        for i=0,last do
            local item = list:GetChildAt(i)
            if item:GetChild("name").text=="御气诀" then
                list.scrollPane:ScrollToView(item, false)
                local btn = item:GetChild("btn")
                btn.visible = true
                local guidePos = btn:LocalToGlobal(Vector2.New(btn.width / 2, btn.height / 2))
                GuideUI.ShowFinger(guidePos)
                GuideUI.ShowMsg("兑换被动技能", GuideDirection.Left, GuideDirection.Left,
                    function()
                        GuideCenter.SimulateClick(btn)

                        local btn3 = G_ConfirmWin.contentPane:GetChild("ok")
                        local guidePos = btn3:LocalToGlobal(Vector2.New(btn3.width / 2, btn3.height / 2))
                        GuideUI.ShowFinger(guidePos)
                        GuideUI.ShowMsg("点击确定", GuideDirection.Left, GuideDirection.Left,
                            function()
                                GuideCenter.SimulateClick(btn3)

                                GuideUI.ShowFinger(Vector2.New(GRoot.inst.width - 80, 30))
                                GuideUI.ShowMsg("兑换成功！点击返回", GuideDirection.Right, GuideDirection.Left,
                                    function()                         
                                        G_CangJingGeWin:Hide()
                                        MainUI.ShowBottomButtons(true)
                                        GuideUI.ShowArrow(MainBtnModule.GetBtnPos("skillBtn"), GuideDirection.Down)
                                        GuideUI.ShowMsg("点击技能", GuideDirection.Right, GuideDirection.Right,
                                            function()
                                                GUIDE_YUN_GONG = true
                                                G_SkillWin:Show()
                                            end)
                                    end)
                        end)
                    end)
                break
            end
        end
    end
end


function GuideCenter.OnSkillWin()
    if GUIDE_UPGRADE_SKILL then
        GUIDE_UPGRADE_SKILL = false

        G_SkillWin.contentPane:GetController("c1").selectedIndex = 0
        local btn = G_SkillWin.contentPane:GetChild("zhaoshi"):GetChild("btn_upgrade")
        local guidePos = btn:LocalToGlobal(Vector2.New(btn.width / 2, btn.height / 2))
        GuideUI.ShowArrow(guidePos, GuideDirection.Down)
        GuideUI.ShowMsg("升级技能可以使你变得更强！", GuideDirection.Left, GuideDirection.Left,
            function()                   
                GuideCenter.SimulateClick(btn)

                GuideUI.ShowFinger(Vector2.New(GRoot.inst.width - 80, 30))
                GuideUI.ShowMsg("升级成功！点击返回", GuideDirection.Right, GuideDirection.Left,
                    function()                        
                        G_SkillWin:Hide()
                        GuideCenter.ContinueTaskFlow()
                    end)
            end)
    elseif GUIDE_YUN_GONG then
        GUIDE_YUN_GONG = false

        G_SkillWin.contentPane:GetController("c1").selectedIndex = 1
        local btn = G_SkillWin.contentPane:GetChild("xinfa"):GetChild("list_useSkills"):GetChildAt(0)
        local guidePos = btn:LocalToGlobal(Vector2.New(btn.width / 2, btn.height / 2))
        GuideUI.ShowArrow(guidePos, GuideDirection.Left)
        GuideUI.ShowMsg("选择被动技能御气诀", GuideDirection.Right, GuideDirection.Right,
            function()                   
                GuideCenter.SimulateClick(btn)

                local btn2 = G_SkillWin.contentPane:GetChild("xinfa"):GetChild("btn_rungong")
                local guidePos2 = btn2:LocalToGlobal(Vector2.New(btn2.width / 2, btn2.height / 2))
                GuideUI.ShowArrow(guidePos2, GuideDirection.Up)
                GuideUI.ShowMsg("运功后，角色可以在战斗中触发回血效果", GuideDirection.Left, GuideDirection.Left,
                    function()                   
                        GuideCenter.SimulateClick(btn2)

                        GuideUI.ShowFinger(Vector2.New(GRoot.inst.width - 80, 30))
                        GuideUI.ShowMsg("运功成功！点击返回", GuideDirection.Right, GuideDirection.Left,
                            function()                        
                                G_SkillWin:Hide()
                                GuideCenter.ContinueTaskFlow()
                            end)
                    end)
            end) 
    end
end

function GuideCenter.OnShenQiWin()
    if not GUIDE_SHEN_QI then return end
    GUIDE_SHEN_QI = false

    local btn = G_ShenQiWin.contentPane:GetChild("forgeBtn")
    local guidePos = btn:LocalToGlobal(Vector2.New(btn.width / 2, btn.height / 2))
    GuideUI.ShowArrow(guidePos, GuideDirection.Down)
    GuideUI.ShowMsg("点击修复，可使神器重现江湖", GuideDirection.Left, GuideDirection.Left,
        function()                   
            GuideCenter.SimulateClick(btn)

            GuideUI.ShowFinger(Vector2.New(GRoot.inst.width - 80, 30))
            GuideUI.ShowMsg("修复成功！点击返回", GuideDirection.Right, GuideDirection.Left,
                function()                        
                    G_ShenQiWin:Hide()
                    GuideCenter.ContinueTaskFlow()
                end)

            --[[local btn2 = G_ShenQiWin.contentPane:GetChild("useBtn")
            local guidePos2 = btn2:LocalToGlobal(Vector2.New(btn2.width / 2, btn2.height / 2))

            GuideUI.ShowArrow(guidePos2, GuideDirection.Down)
            GuideUI.ShowMsg("点击使用该神器可提高角色的战斗力", GuideDirection.Left, GuideDirection.Left,
                 function()                   
                     GuideCenter.SimulateClick(btn2)

                     GuideUI.ShowFinger(Vector2.New(GRoot.inst.width - 80, 30))
                     GuideUI.ShowMsg("使用成功！点击返回", GuideDirection.Right, GuideDirection.Left,
                         function()                        
                             G_ShenQiWin:Hide()
                             GuideCenter.ContinueTaskFlow()
                         end)
                 end)]]
        end)
end

function GuideCenter.OnForgeWin()
    if not GUIDE_FORGE then return end
    GUIDE_FORGE = false

    local btn = G_ForgeWin.contentPane:GetChild("qianghuaUI"):GetChild("eList"):GetChild("e0")
    local guidePos = btn:LocalToGlobal(Vector2.New(btn.width, btn.height / 2))
    GuideUI.ShowArrow(guidePos, GuideDirection.Left)
    GuideUI.ShowMsg("选择您要强化的装备", GuideDirection.Right, GuideDirection.Right,
        function()                   
            GuideCenter.SimulateClick(btn)

            local btn2 = G_ForgeWin.contentPane:GetChild("qianghuaUI"):GetChild("btn1")
            local guidePos2 = btn2:LocalToGlobal(Vector2.New(btn2.width / 2, btn2.height / 2))

            GuideUI.ShowArrow(guidePos2, GuideDirection.Down)
            GuideUI.ShowMsg("强化装备可以使你变得更强！(1/3)", GuideDirection.Right, GuideDirection.Right,
                function()                   
                    GuideCenter.SimulateClick(btn2)

                    GuideUI.ShowArrow(guidePos2, GuideDirection.Down)
                    GuideUI.ShowMsg("强化装备可以使你变得更强！(2/3)", GuideDirection.Right, GuideDirection.Right,
                        function()
                            GuideCenter.SimulateClick(btn2)

                            GuideUI.ShowArrow(guidePos2, GuideDirection.Down)
                            GuideUI.ShowMsg("强化装备可以使你变得更强！(3/3)", GuideDirection.Right, GuideDirection.Right,
                                function()

                                    GuideCenter.SimulateClick(btn2)
                                    GuideUI.ShowFinger(Vector2.New(GRoot.inst.width - 80, 30))
                                    GuideUI.ShowMsg("使用成功！点击返回", GuideDirection.Right, GuideDirection.Left,
                                        function()                        
                                            G_ForgeWin:Hide()
                                            GuideCenter.ContinueTaskFlow()
                                        end)
                                end)
                        end)
                end)
        end)
end

function GuideCenter.SimulateClick(obj)
    obj.onClick:BubbleCall(InputEvent())
end