EquipBuildWin = fgui.window_class(WindowBase)

local _pane
local _list
local _c1
local _c2

local _listData
local _data
local _op
local _npcRid
local _type

function EquipBuildWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common" , "装备打造")
end

function EquipBuildWin:DoInit()
    self:Center()
    
    _pane = self.contentPane
    _c1=_pane:GetController("c1")
    _c2=_pane:GetController("c2")
    _list = _pane:GetChild("n2")
    _list.onClickItem:Add(self.OnWQItemClick, self)
    
    for _i = 1, 13 do
        _pane:GetChildInGroup(_pane:GetChild("n75"), _i.."").onClick:Add(self.OnAddClick, self)
    end
    
    NetClient.Listen(NetKeys.ACMD_BUILD_ITEMS, self.OnWeaponsView, self)
    NetClient.Listen(NetKeys.ACMD_BUILD_ITEM_VIEW, self.OnWeaponView, self)
end

function EquipBuildWin:Open(type)
    _type = type
    self:Show()
end

function EquipBuildWin:OnShown()
    _c1.selectedIndex = 0
    self:RefreshList()
end

function EquipBuildWin:RefreshList()
    NetClient.Send(NetKeys.QCMD_BUILD_ITEMS, _type)
    self:ShowModalWait()
end

function EquipBuildWin:OnWeaponsView(cmd, data)
    if self.isShowing then
        self:CloseModalWait()
        _listData = data
        self:ResetWeaponListInfo(_listData)
    end
end

function EquipBuildWin:ResetWeaponListInfo(data)
    
    _list:RemoveChildrenToPool()
    if _listData.items ~= nil then
        for _i, _item in ipairs(_listData.items) do
            local btn = _list:AddItemFromPool()
            local id = GetIdFromRid(_item.rid)
            btn:GetChild("icon").icon = Defs.GetIconSrc(id)
            btn:GetChild("title").text = _item.name
            btn:GetChild("lvl").text = _item.lvl
            btn.data = _item.rid
        end
    end
    _pane:GetChild("n4"):Clear()
end

function EquipBuildWin:Refresh()
    if _listData ~= nil and #_listData.items > _list.selectedIndex and _list.selectedIndex ~= -1 then
        self:doWeaponView(_listData.items[_list.selectedIndex+1].rid)
    end
end

function EquipBuildWin:OnWQItemClick(context)
    self:doWeaponView(context.data.data)
end

function EquipBuildWin:doWeaponView(rid)
    NetClient.Send(NetKeys.QCMD_BUILD_ITEM_VIEW, _type, rid)
    self:ShowModalWait()
end

function EquipBuildWin:OnWeaponView(cmd, data)
    if self.isShowing then
        self:CloseModalWait()
        _c1.selectedIndex = 1
        _data = data
        self:ResetWeaponInfo()
    end
end

function EquipBuildWin:ResetWeaponInfo()
    local wd = {}
    wd.rid = _data.rid
    _pane:GetChild("n4"):SetItem(wd)
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n52").text = _data.name
    _pane:GetChildInGroup(_pane:GetChild("n75"), "v1").text = _data.value
    _pane:GetChildInGroup(_pane:GetChild("n75"), "v2").text = _data.ca_j
    _pane:GetChildInGroup(_pane:GetChild("n75"), "v3").text = _data.ca_m
    _pane:GetChildInGroup(_pane:GetChild("n75"), "v4").text = _data.ca_s
    _pane:GetChildInGroup(_pane:GetChild("n75"), "v5").text = _data.ca_h
    _pane:GetChildInGroup(_pane:GetChild("n75"), "v6").text = _data.ca_t
    _pane:GetChildInGroup(_pane:GetChild("n75"), "v7").text = _data.cz
    _pane:GetChildInGroup(_pane:GetChild("n75"), "v8").text = _data.j
    _pane:GetChildInGroup(_pane:GetChild("n75"), "v9").text = _data.m
    _pane:GetChildInGroup(_pane:GetChild("n75"), "v10").text = _data.s
    _pane:GetChildInGroup(_pane:GetChild("n75"), "v11").text = _data.h
    _pane:GetChildInGroup(_pane:GetChild("n75"), "v12").text = _data.t
    _pane:GetChildInGroup(_pane:GetChild("n75"), "v13").text = _data.embed_num
    
    _c2.selectedIndex = _type
end

function EquipBuildWin:OnAddClick(context)
    --NetClient.Send(NetKeys.QCMD_BUILD_EQUIP, _data.rid, tonumber(context.sender.name), n)
    --self:ShowModalWait()
    G_DaZaoTiShenWin.data = _data
    _data.type = _type -- 0- 防具 1- 兵器
    _data.type2 = tonumber(context.sender.name)
    _data.v = _pane:GetChildInGroup(_pane:GetChild("n75"), "v".._data.type2).text
    G_DaZaoTiShenWin:Popup()
end
