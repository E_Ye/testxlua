require "Common/SortingOrder"
require "Windows/WindowBase"

require "Windows/ItemCell"
require "Windows/InputCountForm"
require "Windows/SkillCell"

require "Windows/AlertWin"
require "Windows/ConfirmWin"
require "Windows/BagWin"
require "Windows/DeadWin"
require "Windows/TalkWin"
require "Windows/SheZhiWin"
require "Windows/MapWin"
require "Windows/NPCInfoWin"
require "Windows/TaskWin"
require "Windows/NPCShopWin"
require "Windows/MountWin"
require "Windows/PetWin"
require "Windows/MyInfoWin"
require "Windows/FaBaoWin"
require "Windows/EquipBuildWin"
require "Windows/EquipRongLianWin"
require "Windows/EquipXiangQianWin"
require "Windows/WeaponUpgradeWin"
require "Windows/WeaponUpgradeShenBingWin"
require "Windows/JiuLongLuWin"
require "Windows/LianYaoHuWin"
require "Windows/MountDuanLianWin"
require "Windows/FaBaoLianHuaWin"
require "Windows/GuaJiWin"
require "Windows/ChatAndFriends/ChatWin"
require "Windows/TeamWin"
require "Windows/CorpseWin"

require "Windows/BuyWin"
require "Windows/HangOnItemWin"

require "Windows/MainListWin"

require "Windows/GuaJiLvlWin"
require "Windows/GuaJiGuiZeWin"
require "Windows/ItemInfoCmpWin"
require "Windows/ItemInfoWin"
require "Windows/SaleWin"
require "Windows/ShuXingDianWin"
require "Windows/JiuLongLuWin"
require "Windows/PetListWin"
require "Windows/MountDuanLianWin"
require "Windows/MountListWin"
require "Windows/FaBaoLianHuaWin"
require "Windows/LianYaoHuPetListWin"
require "Windows/LianYaoHuWin"
require "Windows/XiangQianWin"
require "Windows/DaZaoTiShenWin"
require "Windows/GongXianWin"
require "Windows/PlayerInfoQueryWin"
require "Windows/OthersPetWin"
require "Windows/HangOnItemInfoWin"
require "Windows/HangOnBagWin"
require "Windows/GiveItemWin"
require "Windows/XiLianChiWin"

require "Windows/XiLianChiWin"
require "Windows/FuLiWin"
require "Windows/MallWin"
require "Windows/PaiHangBangWin"
require "Windows/FenXianWin"

require "Windows/bangpai/BangPaiListWin"
require "Windows/bangpai/BangPaiWin"
require "Windows/bangpai/ClanPopMenuWin"
require "Windows/bangpai/AddBranchWin"
require "Windows/bangpai/BonusWin"
require "Windows/bangpai/ModifyClanWin"
require "Windows/bangpai/ClanWarsItemWin"
require "Windows/bangpai/ClanWarsInfoWin"

require "Windows/DiaoYuWin"
require "Windows/TouHuWin"
require "Windows/GotItemAlertWin"
require "Windows/BuyWinShangCheng"
require "Windows/UseItemReturnWin"


G_AlertWin = AlertWin.New()
G_ConfirmWin = ConfirmWin.New()

--购买NPC物品
G_BuyWin = BuyWin.New()
--背包
G_BagWin = BagWin.New()
--挂售
G_HangOnItemWin = HangOnItemWin.New()
--死亡提示
G_DeadWin = DeadWin.New()
--地图
G_MapWin = MapWin.New() 
--坐骑
G_MountWin = MountWin.New()
--NPC信息
G_NPCInfoWin = NPCInfoWin.New() 
--NPC商店
G_NPCShopWin = NPCShopWin.New() 
--宠物
G_PetWin = PetWin.New()
--设置
G_SheZhiWin = SheZhiWin.New() 
--任务
G_TaskWin = TaskWin.New() 
--角色
G_MyInfoWin = MyInfoWin.New()
--法宝
G_FaBaoWin = FaBaoWin.New()
--装备打造
G_EquipBuildWin = EquipBuildWin.New()
--装备熔炼
G_EquipRongLianWin = EquipRongLianWin.New()
--装备镶嵌
G_EquipXiangQianWin = EquipXiangQianWin.New()
--兵器升级
G_WeaponUpgradeWin = WeaponUpgradeWin.New()
--升级神兵
G_WeaponUpgradeShenBingWin = WeaponUpgradeShenBingWin.New()
--九龙炉
G_JiuLongLuWin = JiuLongLuWin.New()
--炼妖壶
G_LianYaoHuWin = LianYaoHuWin.New()
--坐骑锻炼炉
G_MountDuanLianWin = MountDuanLianWin.New()
--法宝炼化炉
G_FaBaoLianHuaWin = FaBaoLianHuaWin.New()
-- 挂机
G_GuaJiWin = GuaJiWin.New()
--聊天
G_ChatWin = ChatWin.New()
--组队
G_TeamWin = TeamWin.New()
--掉落物品
G_CorpseWin = CorpseWin.New()
--技能物品选择列表
G_MainListWin = MainListWin.New()
--捡装备等级弹出框
G_GuaJiLvlWin = GuaJiLvlWin.New()
--挂机规则
G_GuaJiGuiZeWin = GuaJiGuiZeWin.New()
--物品信息
G_ItemInfoWin = ItemInfoCmpWin.New()
--出售
G_SaleWin = SaleWin.New()
--属性点
G_ShuXingDianWin = ShuXingDianWin.New()
--九龙炉宠物列表
G_PetListWin = PetListWin.New()
--坐骑查询列表
G_MountListWin = MountListWin.New()
--炼妖壶
G_LianYaoHuWin = LianYaoHuWin.New()
--炼妖壶宠物列表
G_LianYaoHuPetListWin = LianYaoHuPetListWin.New()
--镶嵌
G_XiangQianWin = XiangQianWin.New()
--打造提升
G_DaZaoTiShenWin = DaZaoTiShenWin.New()
--贡献
G_GongXianWin = GongXianWin.New()
--查询用户信息
G_PlayerInfoQueryWin = PlayerInfoQueryWin.New()
--查看其它玩家的宠物
G_OthersPetWin = OthersPetWin.New()
--挂售物品信息
G_HangOnItemInfoWin = HangOnItemInfoWin:New()
--挂售背包
G_HangOnBagWin = HangOnBagWin:New()
--赠送
G_GiveItemWin = GiveItemWin.New()
--洗炼池
G_XiLianChiWin = XiLianChiWin.New()
--福利
G_FuLiWin = FuLiWin.New()
--商城
G_MallWin = MallWin.New()
--排行榜
G_PaiHangBangWin = PaiHangBangWin.New()
--分线
G_FenXianWin = FenXianWin.New()
-- 帮派
G_BangPaiListWin = BangPaiListWin.New() --未加入帮派
G_BangPaiWin = BangPaiWin.New() 		--已加入帮派
G_AddBranchWin = AddBranchWin.New() 	--增设分堂
G_BonusWin = BonusWin.New()			--发放奖励	
G_ModifyClanWin = ModifyClanWin.New()	--修改资料
G_ClanWarsItemWin = ClanWarsItemWin.New() --保护物品详细窗口
G_ClanWarsInfoWin = ClanWarsInfoWin.New() --战场详细
--钓鱼
G_DiaoYuWin = DiaoYuWin.New()
--投壶
G_TouHuWin = TouHuWin.New()
--获得物品界面
G_GotItemAlertWin = GotItemAlertWin.New()
--购买商城物品
G_BuyWinShangCheng = BuyWinShangCheng.New()
--使用物品后获得物品弹窗
G_UseItemReturnWin = UseItemReturnWin.New()


function AlertWin.Open(msg, callback)
    G_AlertWin:InternalOpen(msg, callback)
end

function ConfirmWin.Open(msg, leftCallback, rightCallback)
    G_ConfirmWin:InternalOpen(msg, leftCallback, rightCallback)
end

function GongXianWin.Open(msg, title, leftCallback, rightCallback)
    G_GongXianWin:InternalOpen(msg, title, leftCallback, rightCallback)
end