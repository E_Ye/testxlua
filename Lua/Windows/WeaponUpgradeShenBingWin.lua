WeaponUpgradeShenBingWin = fgui.window_class(WindowBase)

local _pane
local _list
local _c1

local _listData
local _data
local _op
local _npcRid

function WeaponUpgradeShenBingWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common" , "升级神兵")
end

function WeaponUpgradeShenBingWin:DoInit()
    self:Center()
    
    _pane = self.contentPane
    _c1=_pane:GetController("c1")
    _list = _pane:GetChild("n2")
    _list.onClickItem:Add(self.OnWQItemClick, self)
    
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n65").onClick:Add(self.OnUpgradeClick, self)
    
    NetClient.Listen(NetKeys.ACMD_UPGRADE_WEAPONS, self.OnWeaponsView, self)
    NetClient.Listen(NetKeys.ACMD_UPGRADE_WEAPON_VIEW, self.OnWeaponView, self)
    
    NetClient.ListenRet(NetKeys.QCMD_UPGRADE_SHENBING, self.DefaultQCMDHandler, self)
end

function WeaponUpgradeShenBingWin:Open(op, npcRid)
    _op = op
    _npcRid = npcRid
    self:Show()
end

function WeaponUpgradeShenBingWin:OnShown()
    _c1.selectedIndex = 0
    self:RefreshList()
end

function WeaponUpgradeShenBingWin:RefreshList()
    NetClient.Send(NetKeys.QCMD_UPGRADE_WEAPONS, 1, "")
    self:ShowModalWait()
end

function WeaponUpgradeShenBingWin:OnWeaponsView(cmd, data)
    if self.isShowing then
        self:CloseModalWait()
        _listData = data
        self:ResetWeaponListInfo(_listData)
    end
end

function WeaponUpgradeShenBingWin:ResetWeaponListInfo(data)
    _list:RemoveChildrenToPool()
    if _listData.items ~= nil then
        for _i, _item in ipairs(_listData.items) do
            local btn = _list:AddItemFromPool()
            local id = GetIdFromRid(_item.rid)
            btn:GetChild("icon").icon = Defs.GetIconSrc(id)
            btn:GetChild("title").text = _item.name
            btn:GetChild("lvl").text = _item.lvl
            btn.data = _item.rid
        end
    end
    _pane:GetChild("n4"):Clear()
end

function WeaponUpgradeShenBingWin:Refresh()
    if _listData ~= nil and #_listData.items > _list.selectedIndex and _list.selectedIndex ~= -1 then
        self:doWeaponView(_listData.items[_list.selectedIndex+1].rid)
    end
end

function WeaponUpgradeShenBingWin:OnWQItemClick(context)
    self:doWeaponView(context.data.data)
end

function WeaponUpgradeShenBingWin:doWeaponView(rid)
    NetClient.Send(NetKeys.QCMD_UPGRADE_WEAPON_VIEW, 1, rid)
    self:ShowModalWait()
end

function WeaponUpgradeShenBingWin:OnWeaponView(cmd, data)
    if self.isShowing then
        self:CloseModalWait()
        _c1.selectedIndex = 1
        _data = data
        self:ResetWeaponInfo()
    end
end

function WeaponUpgradeShenBingWin:ResetWeaponInfo()
    local wd = {}
    wd.rid = _data.rid
    _pane:GetChild("n4"):SetItem(wd)
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n56").text = _data.name
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n87").text = _data.name
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n88").text = _data.name2
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n66").text = _data.damage
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n67").text = _data.cz_damage
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n84").text = _data.embed_num
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n68").text = _data.need_money
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n79").text = _data.need_items
    local lvl = _data.lvl == nil and 0 or _data.lvl
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n82").text = "Lv."..lvl
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n85"):SetItem(wd)
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n86"):SetItem(wd)
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n89").value = _data.shenbing_p
end

function WeaponUpgradeShenBingWin:OnUpgradeClick(context)
    NetClient.Send(NetKeys.QCMD_UPGRADE_SHENBING, _npcRid, _op, _data.rid)
    self:ShowModalWait()
end
