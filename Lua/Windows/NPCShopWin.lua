NPCShopWin = fgui.window_class(WindowBase)

local _itemList
local _listData
local _NPCRid

function NPCShopWin:ctor()
	WindowBase.ctor(self)

	self.animation = {"move_left", "move_right"}
	self:SetContentSource("Common","NPC商店")
end

function NPCShopWin:DoInit()
	self:SetXY(GRoot.inst.width-self.width-50, 50)

	_itemList=self.contentPane:GetChild("list")
	_itemList.itemRenderer = function(index, obj) self.RenderListItem(self,index, obj) end
	_itemList.numItems = 24
	_itemList.onClickItem:Add(self.OnClickCell, self)

	NetClient.ListenRet(NetKeys.QCMD_BUY_ITEM_FROM_NPC, self.DefaultQCMDHandler, self)
end

function NPCShopWin:Open(NPCRid, listData)
	_NPCRid = NPCRid
	_listData = listData
	self:Show()
end

function NPCShopWin:OnShown()
	_itemList.numItems = 24
end

function NPCShopWin:OnHide()
	G_BuyWin:Hide()
end

function NPCShopWin:RenderListItem(index, obj)
	obj:SetItem(_listData[index+1])
	obj.owner = self
end

function NPCShopWin:OnClickCell(context)
	local item = context.data
	if item == nil or item.data == nil then return end
	
	if not G_BuyWin.isShowing then
		G_BuyWin:Open(self)
	end
	G_BuyWin:SetItem(item.data)
end

function NPCShopWin:DoBuy(rid, count)
	self:ShowModalWait()
	NetClient.Send(NetKeys.QCMD_BUY_ITEM_FROM_NPC, _NPCRid, rid, count)
end
