DiaoYuWin = fgui.window_class(WindowBase)

local _typeTab
local _clipCom
local _clip

function DiaoYuWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Huodong","钓鱼")
end

function DiaoYuWin:DoInit()
    self:Center()
    
    _typeTab=self.contentPane:GetController("type")
    _typeTab.onChanged:Add(self.OnTypeChanged, self)
    _clipCom = self.contentPane:GetChild("n17")
    _clipCom.onClick:Add(self.OnClickYuGan, self)
    _clip = _clipCom:GetChild("n17")
    _clip:SetPlaySettings(0, -1, 1, 0)
    _clip.onPlayEnd:Add(self.DiaoYuEnd, self)
    
    NetClient.Listen(NetKeys.ACMD_DIAOYU_VIEW, self.OnView, self)
    NetClient.ListenRet(NetKeys.QCMD_DO_DIAOYU, self.DoDiaoYuHandler, self)
    NetClient.Listen(NetKeys.ACMD_DO_DIAOYU, self.OnDiaoYuReturn, self)
end

function DiaoYuWin:OnTypeChanged()
    self:Refresh()
end

function DiaoYuWin:OnShow()
    self:Refresh()
end

function DiaoYuWin:Refresh()
    local index = _typeTab.selectedIndex
    if index == -1 then
        index = 0
    end
    NetClient.Send(NetKeys.QCMD_DIAOYU_VIEW, _typeTab.selectedIndex+1)
    self:ShowModalWait()
end

function DiaoYuWin:OnView(cmd, data)
    self:CloseModalWait()
    local count = data.count == nil and 0 or data.count
    local curCount = data.curCount == nil and 0 or data.curCount
    self.contentPane:GetChild("n10").text = count.."/"..curCount
end

function DiaoYuWin:OnClickYuGan(context)
    _clip.playing = true
    _clip:SetPlaySettings(0, -1, 1, 0)
end

function DiaoYuWin:DiaoYuEnd(context)
    NetClient.Send(NetKeys.QCMD_DO_DIAOYU, _typeTab.selectedIndex+1)
    self:ShowModalWait()
end

function DiaoYuWin:DoDiaoYuHandler(cmd, data)
    if self.isShowing then
        self:CloseModalWait()
        _clip.playing = false
        if data.code~=0 then
            AlertWin.Open(data.msg)
        else
            self:Refresh()
--            G_GotItemAlertWin:PopupOpen(data)
        end
    end
end

function DiaoYuWin:OnDiaoYuReturn(cmd, data)
    if self.isShowing then
        self:CloseModalWait()
        G_UseItemReturnWin:Open(data)
    end
end
