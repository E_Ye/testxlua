XiangQianWin = fgui.window_class(WindowBase)

local _pane
local _list
local _rid
local _type -- 0-自己，1-宠物，2-坐骑，3-法宝，4-兵神，5-精灵
local _target
local _is_weapon

local _data

function XiangQianWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common" , "装备镶嵌")
end

function XiangQianWin:DoInit()
    self:Center()
    
    _pane = self.contentPane
    _is_weapon=_pane:GetController("is_weapon")
    _list = _pane:GetChild("n2")
    _list.onClickItem:Add(self.OnXQItemClick, self)
    
    _pane:GetChildInGroup(_pane:GetChild("switch"), "n53").onClick:Add(self.OnClickLeft, self)
    _pane:GetChildInGroup(_pane:GetChild("switch"), "n54").onClick:Add(self.OnClickRight, self)
    
    NetClient.Listen(NetKeys.ACMD_EMBED_VIEW, self.OnEmbedView, self)
    
    NetClient.ListenRet(NetKeys.QCMD_EMBED_ON, self.DefaultQCMDHandler, self)
end

function XiangQianWin:Open(type, target, rid)
    _type = type
    _target = target
    _rid = rid
    self:Show()
end

function XiangQianWin:OnShown()
    local def = Defs.GetDef("item", GetIdFromRid(_rid))
    if string.startswith(def.type, "bq")  then
        _is_weapon.selectedIndex = 1
    else
        _is_weapon.selectedIndex = 0
    end
    self:Refresh()
end

function XiangQianWin:Refresh()
    NetClient.Send(NetKeys.QCMD_EMBED_VIEW, _type, _target, _rid)
    self:ShowModalWait()
end

function XiangQianWin:OnEmbedView(cmd, data)
    self:CloseModalWait()
    _data = data
    self:ResetEmbedInfo(_data)
end

function XiangQianWin:ResetEmbedInfo(data)
    _list:RemoveChildrenToPool()
    if _data.e_items ~= nil then
        for _i, _item in ipairs(_data.e_items) do
            local btn = _list:AddItemFromPool()
            local id = GetIdFromRid(_item.rid)
            btn:GetChild("icon").icon = Defs.GetIconSrc(id)
            local def = Defs.GetDef("item", id)
            btn:GetChild("title").text = def.name
            btn:GetChild("lvl").text = _item.lvl
            btn:GetChild("recovery").text = _item.recovery
            btn.data = _item.rid
        end
    end

    local ed = {}
    ed.rid = data.rid
    _pane:GetChild("n4"):SetItem(ed)
    _pane:GetChild("n57").text = data.name
    
    local ed_items = _data.ed_items
    for i=59, 67 do
        if ed_items ~= nil and ed_items[i-58] ~= nil then
            _pane:GetChild("n"..i):SetItem(ed_items[i-58])
        else
            _pane:GetChild("n"..i):Clear()
        end
    end
end

function XiangQianWin:OnXQItemClick(context)
    NetClient.Send(NetKeys.QCMD_EMBED_ON, _type, _target, _rid, context.data.data)
    self:ShowModalWait()
end

function XiangQianWin:OnClickLeft(context)
    local rid = _rid
    _rid = self:GetNextRid(1)
    if rid ~= _rid then
        self:Refresh()
    end
end

function XiangQianWin:OnClickRight(context)
    local rid = _rid
    _rid = self:GetNextRid(0)
    if rid ~= _rid then
        self:Refresh()
    end
end

-- type 0- 右  1- 左
function XiangQianWin:GetNextRid(type)
    local items = _data.items
    if items == nil then
        return _rid
    end
    
    local len = #items
    for _i, _item in ipairs(items) do
        if _rid == _item then
            if type == 0 then
                if _i == len then
                    return _rid
                else
                    return items[_i+1]
                end
            else
                if _i == 1 then
                    return _rid
                else
                    return items[_i-1]
                end
            end
        end
    end
    
    return _rid
end