GiveItemWin = fgui.window_class(WindowBase)

local _panel
local _data
local c1
local _uid

function GiveItemWin:ctor()
    WindowBase.ctor(self)

    self:SetContentSource("Common","赠送")
end

function GiveItemWin:DoInit()
    --self:Center()
    
    _panel = self.contentPane
    _panel:GetChild("btnMax").onClick:Add(self.OnClickMax, self)
    _panel:GetChild("btnOk").onClick:Add(self.OnClickGive, self)
    c1 = _panel:GetController("c1")
    
    NetClient.ListenRet(NetKeys.QCMD_GIVE_ITEM, self.DefaultQCMDHandlerAndHide, self)
end

function GiveItemWin:Open(uid)
    _uid = uid
    self:Show()
end

function GiveItemWin:OnShow()
    c1.selectedIndex = 0
end

function GiveItemWin:OnSelectItem(data)
    if self.isShowing then
        self:ResetItemInfo(data)
    end
end

function GiveItemWin:ResetItemInfo(data)
    if data == nil then
        return
    end
    _data = data
    c1.selectedIndex = 1
    _panel:GetChild("cell"):SetItem(data)
    _panel:GetChildInGroup(_panel:GetChild("n21"), "n18").text = data.def.name
    _panel:GetChildInGroup(_panel:GetChild("n21"), "ownCount").text = data.count == nil and 1 or data.count
    
    _panel:GetChild("input"):SetRegionAndStep(1, 0, data.count)
    _panel:GetChild("input"):ResetMax()
end

function GiveItemWin:OnClickMax(context)
    _panel:GetChild("input"):ResetMax()
end

function GiveItemWin:OnClickGive()
    local count = tonumber(_panel:GetChild("input").value)
    if count == nil or count == 0 then
        return
    end
    NetClient.Send(NetKeys.QCMD_GIVE_ITEM, _uid, _data.rid, count)
    self:ShowModalWait()
end

function GiveItemWin:DefaultQCMDHandlerAndHide(cmd, data)
    if self.isShowing then
        self:CloseModalWait()
        if data.code~=0 then
            AlertWin.Open(data.msg)
        else
            self:Hide()
            if G_BagWin.isShowing then  G_BagWin:Hide() end
        end
    end
end