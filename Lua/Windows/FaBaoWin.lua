FaBaoWin = fgui.window_class(WindowBase)

local _list
local _tab
local _shuxing
local _sxC1
local _skills
local _skillList

local _listData
local _info
local _skillData

function FaBaoWin:ctor()
	WindowBase.ctor(self)

	self.asyncCreate = true
	self:SetContentSource("Common","法宝")
end

function FaBaoWin:DoInit()
	self:Center()
	
	_list = self.contentPane:GetChild("list")
    _list:RemoveChildrenToPool()
    _list.onClickItem:Add(self.OnListItemChanged, self)
    
    _tab = self.contentPane:GetController("tab")
    _tab.onChanged:Add(self.OnTabChanged, self)
    
    _shuxing = self.contentPane:GetChild("n22")
    _sxC1 = _shuxing:GetController("c1")
    _shuxing:GetChildInGroup(_shuxing:GetChild("n50"), "btnOn").onClick:Add(self.OnClickCall, self)
    -- 16-法宝攻击资质，17-法宝防御资质，18-法宝生命资质，19-法宝法力资质
    _shuxing:GetChildInGroup(_shuxing:GetChild("n50"), "n38").onClick:Add(self.OnClickZZ, self)
    _shuxing:GetChildInGroup(_shuxing:GetChild("n50"), "n38").data = 18
    _shuxing:GetChildInGroup(_shuxing:GetChild("n50"), "n39").onClick:Add(self.OnClickZZ, self)
    _shuxing:GetChildInGroup(_shuxing:GetChild("n50"), "n39").data = 19
    _shuxing:GetChildInGroup(_shuxing:GetChild("n50"), "n45").onClick:Add(self.OnClickZZ, self)
    _shuxing:GetChildInGroup(_shuxing:GetChild("n50"), "n45").data = 16
    _shuxing:GetChildInGroup(_shuxing:GetChild("n50"), "n46").onClick:Add(self.OnClickZZ, self)
    _shuxing:GetChildInGroup(_shuxing:GetChild("n50"), "n46").data = 17
    _shuxing:GetChildInGroup(_shuxing:GetChild("n53"), "btnOn").onClick:Add(self.OnClicklhl, self)
    
    _skills = self.contentPane:GetChild("n33")
    _skills:GetChild("n25").onClickItem:Add(self.OnSkillChanged, self)
    _skills:GetChild("n62").onClick:Add(self.OnClickLearnSkill, self)
    _skills:GetChild("n65").onClick:Add(self.OnClickSkillLvlUp, self)
    _skillList = _skills:GetChild("n25")
    
    NetClient.Listen(NetKeys.ACMD_MY_MAGICS, self.OnServer_MyMagics, self)
    NetClient.Listen(NetKeys.ACMD_VIEW_MAGIC, self.OnServer_ViewMagic, self)
    NetClient.Listen(NetKeys.ACMD_CALL_OUT_MAGIC, self.OnServer_CallMagic, self)
    NetClient.Listen(NetKeys.ACMD_CALL_BACK_MAGIC, self.OnServer_CallMagic, self)
    
    NetClient.ListenRet(NetKeys.QCMD_VIEW_MAGIC, self.DefaultQCMDHandler, self)
    NetClient.ListenRet(NetKeys.QCMD_ADD_PET_ZZ, self.DefaultQCMDHandler, self)
    NetClient.ListenRet(NetKeys.QCMD_LEARN_SKILL, self.DefaultQCMDHandler, self)
    NetClient.ListenRet(NetKeys.QCMD_SKILL_LVLUP, self.DefaultQCMDHandler, self)
end

function FaBaoWin:OnShown()
    self:RefreshList()
end

function FaBaoWin:RefreshList()
    if self.isShowing then
        NetClient.Send(NetKeys.QCMD_MY_PETS, 2)
        self:ShowModalWait()
    end
end

function FaBaoWin:OnServer_MyMagics(cmd, data)
    self:CloseModalWait()
        
    local lsi = _list.selectedIndex
    _list:RemoveChildrenToPool()
    _listData = data.list
    for _i,obj in ipairs(_listData) do
        local btn = _list:AddItemFromPool()
        btn.title = obj.name
        btn:GetChild("lvl").text = obj.lvl
        btn.icon = GetIdFromRid(obj.rid)
        btn:GetChild("n12").visible = obj.status == 1 and true or false
        if obj.not_got then
            btn.grayed = true
        else
            btn.grayed = false
        end
    end
    
    if lsi==-1 or lsi>=#_listData then
        _list.selectedIndex = 0
    elseif lsi >= 0 and lsi < #_listData then
       _list.selectedIndex = lsi
    end
        
    self:OnListItemChanged(nil)
end

function FaBaoWin:OnListItemChanged(context)
    _info = nil
    self:Refresh()
end

function FaBaoWin:Refresh()
    if self.isShowing then
        if _listData ~= nil and #_listData >= (_list.selectedIndex+1) then
            local index = _list.selectedIndex+1
            if index == 0 then
                index = 1
            end
            NetClient.Send(NetKeys.QCMD_VIEW_MAGIC, _listData[index].rid, "")
            self:ShowModalWait()
        end
    end
end

function FaBaoWin:OnTabChanged(context)
    local si = _tab.selectedIndex
    if si == 0 then
        self:ResetShuXing()
    else
        self:ResetSkills()
    end
end

-- tab 属性
function FaBaoWin:OnServer_ViewMagic(cmd, data)
    self:CloseModalWait()
    if _listData==nil then return end

    _info = data
    self:OnTabChanged(nil)
end

function FaBaoWin:ResetShuXing()
    if _info == nil then
        return
    end
    
    if _info.not_got then
        self:ResetShuXing2()
        return
    end
    
    _sxC1.selectedIndex = 0
    local iid = GetIdFromRid(_info.rid)
    local group = _shuxing:GetChild("n50")
    _shuxing:GetChild("n48").text = _info.name.." ".._info.lvl.."级"
    _shuxing:GetChildInGroup(group, "n16").value  = _info.exp
    _shuxing:GetChildInGroup(group, "n16").max = _info.upgrade_exp
    _shuxing:GetChild("n20").text = _info.hp_zz
    _shuxing:GetChild("n22").text = _info.mp_zz
    _shuxing:GetChild("n30").text = _info.attack_zz
    _shuxing:GetChild("n26").text = _info.recovery_zz
    _shuxing:GetChild("n32").text = _info.mp_attack_zz

    _shuxing:GetChild("image").icon = iid.."_b"
    
    local hp_zz = _info.hp_zz_item
    _shuxing:GetChild("n57").text = hp_zz <= 0 and "" or ("(+"..hp_zz..")")
    local mp_zz = _info.mp_zz_item
    _shuxing:GetChild("n58").text = mp_zz <= 0 and "" or ("(+"..mp_zz..")")
    local attack_zz = _info.attack_zz_item
    _shuxing:GetChild("n59").text = attack_zz <= 0 and "" or ("(+"..attack_zz..")")
    local recovery_zz = _info.recovery_zz_item
    _shuxing:GetChild("n60").text = recovery_zz <= 0 and "" or ("(+"..recovery_zz..")")
    
    _shuxing:GetController("status").selectedIndex = _info.status or 0
end

function FaBaoWin:ResetShuXing2()
    _sxC1.selectedIndex = 1
    
    local iid = GetIdFromRid(_info.rid)
    local item = _listData[_list.selectedIndex+1]
    local mDef = Defs.GetDef("magic", iid)
    _shuxing:GetChild("n48").text = mDef.name.." ".."0级"
    _shuxing:GetChild("image").icon = iid.."_b"
    _shuxing:GetChildInGroup(_shuxing:GetChild("n53"), "n51").text  = mDef.desc
    _shuxing:GetChild("n20").text = mDef.hp_zz
    _shuxing:GetChild("n22").text = mDef.mp_zz
    _shuxing:GetChild("n30").text = mDef.attack_zz
    _shuxing:GetChild("n26").text = mDef.recovery_zz
    _shuxing:GetChild("n32").text = mDef.mp_attack_zz
end

function FaBaoWin:OnClickCall()
    if _info==nil then return end

    self:ShowModalWait()
    if _info.status==0 then
        NetClient.Send(NetKeys.QCMD_MAGIC_CALL_OUT, _info.rid)
    else
        NetClient.Send(NetKeys.QCMD_MAGIC_CALL_BACK)
    end
end

function FaBaoWin:OnClickZZ(context)
    if context.sender.data == nil then
        return
    end
    
    -- 资质
    self:ShowModalWait()
    NetClient.Send(NetKeys.QCMD_ADD_PET_ZZ, context.sender.data, _info.rid)
end

function FaBaoWin:OnServer_CallMagic(cmd, data)
    self:CloseModalWait()
    self:RefreshList()
end

function FaBaoWin:OnClicklhl(context)
    -- 炼化炉
    local item = _listData[_list.selectedIndex+1]
    G_FaBaoLianHuaWin:Open(item.rid)
end

-- tab 技能
function FaBaoWin:ResetSkills()
    if _info == nil then
        return
    end
    
    _skillList:RemoveChildrenToPool()
    
    for _i,skill in ipairs(_info.skills) do
        local btn = _skillList:AddItemFromPool()
        btn:SetItem(skill)
        if skill.statu == nil then
            btn.grayed = false
        else
            btn.grayed = true
        end
        if _info.not_got then
            btn.grayed = true
        end
    end
    
    if #_info.skills > 0 then
        _skillData = _info.skills[1]
        self:DoResetSkillInfo()
    end
end

function FaBaoWin:DoResetSkillInfo()
    _skills:GetChild("n29"):SetItem(_skillData)
    _skills:GetChild("n30").text = _skillData.name
    _skills:GetChild("n53").text = _skillData.def.desc
    _skills:GetChild("n54").text = _skillData.lvl
    if _skillData.exp then
        _skills:GetChild("n55").text = _skillData.exp.."/".._skillData.upgrade_exp
    else
        _skills:GetChild("n55").text = ""
    end
    _skills:GetChild("n56").text = _skillData.mod_plus
    _skills:GetChild("n57").text = _skillData.def.mod
    _skills:GetChild("n58").text = Defs.GetGameDef("attr_names", _skillData.def.deplete_attr, "")
    _skills:GetChild("n59").text = Defs.GetGameDef("attr_names", _skillData.def.hurt_attr, "")
    _skills:GetChild("n60").text = _skillData.def.cool_time
    _skills:GetChild("n61").text = _skillData.def.attack_distance
    local itemid = _skillData.def.deplete_item
    if itemid ~= nil then
        local itemDef = Defs.GetDef("item", itemid)
        _skills:GetChild("n64").text = itemDef.name
    else
        _skills:GetChild("n64").text = ""
    end
    
    if _skillData.statu == 0 or _skillData.statu == nil then
        _skills:GetChild("n29").grayed = false
        _skills:GetController("c1").selectedIndex = 0
        if _skillData.jns == nil then
            _skills:GetChild("n65").visible = false
        else
            _skills:GetChild("n65").visible = true
        end
    else
        _skills:GetChild("n29").grayed = true
        _skills:GetController("c1").selectedIndex = 1
        _skills:GetChild("n64").text = _skillData.items
    end
    if _info.not_got then
        _skills:GetChild("n29").grayed = true
        _skills:GetController("c1").selectedIndex = 2
    end
end

function FaBaoWin:OnSkillChanged(context)
    if _info==nil then return end
    
    if _info.skills == nil then return end
    
    local si = _skillList:GetChildIndex(context.data)+1
    _skillData = _info.skills[si]
    self:DoResetSkillInfo()
end

function FaBaoWin:OnClickLearnSkill()
    -- 学习技能
    self:ShowModalWait()
    NetClient.Send(NetKeys.QCMD_LEARN_SKILL, 2, _info.rid, _skillData.id)
end

function FaBaoWin:OnClickSkillLvlUp()
    -- 学习技能
    self:ShowModalWait()
    NetClient.Send(NetKeys.QCMD_SKILL_LVLUP, _info.rid, _skillData.id)
end