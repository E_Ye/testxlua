MainListWin = fgui.window_class(WindowBase)

local _listData = {} -- 列表内容
local _typeTab = nil
local _list
local _cellIndex

function MainListWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = false
    self:SetContentSource("Common","技能物品选择列表")
end

function MainListWin:DoInit()
    self:SetXY(50, 50)
    
    _typeTab=self.contentPane:GetController("c1")
    _list=self.contentPane:GetChild("list")
    local p = self
    _list.itemRenderer = function(index, obj) p:RenderItem(index, obj) end
    _list:SetVirtual()
    _list.onClickItem:Add(self.OnClickItem, self)
    
    NetClient.Listen(NetKeys.ACMD_VIEW_USER, self.OnGotMySkillInfo, self)
    NetClient.Listen(NetKeys.ACMD_MY_ITEMS, self.OnGotMyItemInfo, self)
end

function MainListWin:Open(cellIndex)
    local isYaoPin1 = _cellIndex==0 or _cellIndex==1
    local isYaoPin2 = cellIndex==0 or cellIndex==1
    _cellIndex = cellIndex
    if not self.isShowing then
        self:Show()
    elseif isYaoPin1~=isYaoPin2 then
        self:ReadyToUpdate()
    else
        self.frame.title = "设置快捷键"..(_cellIndex+1)
    end
end

function MainListWin:ReadyToUpdate()
    self.frame.title = "设置快捷键"..(_cellIndex+1)    
    if _cellIndex==0 or _cellIndex==1 then
        self:ShowModalWait(NetKeys.QCMD_MY_ITEMS)
        NetClient.Send(NetKeys.QCMD_MY_ITEMS, 1, 0, 0)
        _typeTab.selectedIndex=0
    else
        self:ShowModalWait(NetKeys.QCMD_VIEW_USER)
        NetClient.Send(NetKeys.QCMD_VIEW_USER, "", 3)
        _typeTab.selectedIndex=1
    end
end

function MainListWin:OnHide()
end

function MainListWin:OnGotMySkillInfo(cmd, data)
    if not self:CloseModalWait(NetKeys.QCMD_VIEW_USER) then return end

    _listData = data.skills
    _list.numItems = #_listData
end

function MainListWin:OnGotMyItemInfo(cmd, data)
    if not self:CloseModalWait(NetKeys.QCMD_MY_ITEMS) then return end

    _listData = data.items
    _list.numItems = #_listData
end

function MainListWin:RenderItem(index, obj)
    obj:SetItem(_listData[index+1])
end

function MainListWin:OnClickItem(context)
    local item = context.data.data
    local id = item.rid or item.id
    NetClient.Send(NetKeys.QCMD_MAIN_LIST_OP, _cellIndex, id)
end
