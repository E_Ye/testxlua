GuaJiLvlWin = fgui.window_class(WindowBase)

local _list
local lvlConf = {1, 5, 10, 15, 20, 30, 40, 50, 60}

function GuaJiLvlWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common","捡装备等级弹出框")
end

function GuaJiLvlWin:DoInit()
    self:Center()
    
    _list = self.contentPane:GetChild("n44")
    _list.onClickItem:Add(self.OnClickLvlItem, self)
end

function GuaJiLvlWin:OnShown()
end

function GuaJiLvlWin:OnHide()
end

function GuaJiLvlWin:OnClickLvlItem(context)
    local lvl = lvlConf[_list.selectedIndex+1]
    G_GuaJiWin:ResetLvl(lvl)
end
