FaBaoLianHuaWin = fgui.window_class(WindowBase)

local _data
local _id
local _mid1
local _mid2
local _panel
local _magic_new
local _magic_old

function FaBaoLianHuaWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common" , "法宝炼化炉")
end

function FaBaoLianHuaWin:DoInit()
    self:Center()
    
    _panel = self.contentPane
    _magic_new = _panel:GetChild("n22")
    _magic_old = _panel:GetChild("n21")
    _panel:GetChild("n6").onClick:Add(self.OnClickLianHua, self)
    
    NetClient.Listen(NetKeys.ACMD_MAGIC_LIANHUA_VIEW, self.OnMagicLianHuaView, self)
    
    NetClient.ListenRet(NetKeys.QCMD_MAGIC_LIANHUA, self.DefaultQCMDHandler, self)
    NetClient.ListenRet(NetKeys.QCMD_MAGIC_LIANHUA_VIEW, self.ViewQCMDHandler, self)
end

function FaBaoLianHuaWin:Open(id)
    _id = id
    self:Show()
end

function FaBaoLianHuaWin:OnShown()
    self:Refresh()
end

function FaBaoLianHuaWin:Refresh()
    if self.isShowing then
        self:ShowModalWait()
        NetClient.Send(NetKeys.QCMD_MAGIC_LIANHUA_VIEW, _id)
    end
end

function FaBaoLianHuaWin:OnMagicLianHuaView(cmd, data)
    self:CloseModalWait()
    _data = data
    local nd = {}
    nd.rid = _id
    _magic_new:SetItem(nd)
    local od = {}
    od.rid = data.old_magic
    _magic_old:SetItem(od)
    local needExp = data.needExp == nil and 0 or data.needExp
    local exp = data.exp == nil and 0 or data.exp
    _panel:GetChild("n15").text = self:FormateInfo(needExp, exp, exp >= needExp)
    local needMoney = data.needMoney == nil and 0 or data.needMoney
    local money = data.money == nil and 0 or data.money
    _panel:GetChild("n16").text = self:FormateInfo(needMoney, money, money >= needMoney)
    _panel:GetChild("n20").text = data.lucky
end

function FaBaoLianHuaWin:ViewQCMDHandler(cmd, data)
    self:CloseModalWait()
    if data.code == 11 then
        self:Hide()
        G_FaBaoWin:RefreshList()
    elseif data.code ~= 0 then
        AlertWin.Open(data.msg)
    else
        self:Refresh()
    end
end

function FaBaoLianHuaWin:FormateInfo(text, text2, enough)
    local arr = {}
    table.insert(arr, "<font color='")
    table.insert(arr, "#FFF94E")
    table.insert(arr, "'>")
    table.insert(arr, text)
    table.insert(arr, "/")
    if enough == false then
        table.insert(arr, "<font color='")
        table.insert(arr, "#FF1F1F")
        table.insert(arr, "'>")
    end
    table.insert(arr, text2)
    
    return table.concat(arr, "")
end

function FaBaoLianHuaWin:OnClickLianHua()
    self:ShowModalWait()
    NetClient.Send(NetKeys.QCMD_MAGIC_LIANHUA, _id)
end