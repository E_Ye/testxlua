ItemInfoCmpWin = fgui.window_class(WindowBase)

local _cc
local _item1
local _item2

function ItemInfoCmpWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self.animation = { "eject", ""}
    self:SetContentSource("Common" , "物品信息_对比")
end

function ItemInfoCmpWin:DoInit()
    self:Center()
    
    _cc = self.contentPane:GetController("c1")
    _item1 = self.contentPane:GetChild("n0")
    _item2 = self.contentPane:GetChild("n1")
end


function ItemInfoCmpWin:ReadyToUpdate()
    _cc.selectedIndex = self.data.cmp_item ~= nil and 1 or 0
    
    _item1:SetItem(self.data)
    if self.data.cmp_item ~= nil then
        _item2:SetItem(self.data.cmp_item)
    end
end