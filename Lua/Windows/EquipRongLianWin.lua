EquipRongLianWin = fgui.window_class(WindowBase)

local _pane
local _list
local _c1

local _listData
local _data

function EquipRongLianWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common" , "装备熔炼")
end

function EquipRongLianWin:DoInit()
    self:Center()
    
    _pane = self.contentPane
    _c1=_pane:GetController("c1")
    _list = _pane:GetChild("n2")
    _list.onClickItem:Add(self.OnWQItemClick, self)
    
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n65").onClick:Add(self.OnRonglianClick, self)
    
    NetClient.Listen(NetKeys.ACMD_RONGLIAN_ITEMS, self.OnWeaponsView, self)
    NetClient.Listen(NetKeys.ACMD_RONGLIAN_ITEM_VIEW, self.OnWeaponView, self)
    
    NetClient.ListenRet(NetKeys.QCMD_RONGLIAN_ITEM, self.OnRongLianReturn, self)
end

function EquipRongLianWin:Open()
    self:Show()
end

function EquipRongLianWin:OnShown()
    self:RefreshList()
end

function EquipRongLianWin:RefreshList()
    _c1.selectedIndex = 0
    NetClient.Send(NetKeys.QCMD_RONGLIAN_ITEMS, 1, "")
    self:ShowModalWait()
end

function EquipRongLianWin:OnWeaponsView(cmd, data)
    if self.isShowing then
        self:CloseModalWait()
        _listData = data
        self:ResetWeaponListInfo(_listData)
    end
end

function EquipRongLianWin:ResetWeaponListInfo(data)
    _list:RemoveChildrenToPool()
    if _listData.items ~= nil then
        for _i, _item in ipairs(_listData.items) do
            local btn = _list:AddItemFromPool()
            local id = GetIdFromRid(_item.rid)
            btn:GetChild("icon").icon = Defs.GetIconSrc(id)
            btn:GetChild("title").text = _item.name
            btn:GetChild("lvl").text = _item.lvl
            btn.data = _item.rid
        end
    end
    _pane:GetChild("n4"):Clear()
end

function EquipRongLianWin:Refresh()
    if _listData ~= nil and #_listData.items > _list.selectedIndex and _list.selectedIndex ~= -1 then
        self:doWeaponView(_listData.items[_list.selectedIndex+1].rid)
    end
end

function EquipRongLianWin:OnWQItemClick(context)
    self:doWeaponView(context.data.data)
end

function EquipRongLianWin:doWeaponView(rid)
    NetClient.Send(NetKeys.QCMD_RONGLIAN_ITEM_VIEW, rid)
    self:ShowModalWait()
end

function EquipRongLianWin:OnWeaponView(cmd, data)
    if self.isShowing then
        self:CloseModalWait()
        _c1.selectedIndex = 1
        _data = data
        self:ResetWeaponInfo()
    end
end

function EquipRongLianWin:ResetWeaponInfo()
    local wd = {}
    wd.rid = _data.rid
    _pane:GetChild("n4"):SetItem(wd)
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n56").text = _data.name
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n66").text = _data.damage
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n67").text = _data.cz_damage
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n68").text = _data.need_money
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n70"):SetItem(wd)
    local nd = {}
    nd.rid = _data.to_id
    nd.count = _data.count
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n71"):SetItem(nd)
end

function EquipRongLianWin:OnRonglianClick(context)
    NetClient.Send(NetKeys.QCMD_RONGLIAN_ITEM, _data.rid)
    self:ShowModalWait()
end

function EquipRongLianWin:OnRongLianReturn(cmd, data)
    self:CloseModalWait()
    if data.code~=0 then
        AlertWin.Open(data.msg)
    else
        self:RefreshList()
    end
end
