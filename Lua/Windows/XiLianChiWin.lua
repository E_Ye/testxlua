XiLianChiWin = fgui.window_class(WindowBase)

local _pane
local _tab
local _rongLianList
local _type -- 0-菩萨灭(真)(i806) 1-菩萨灭(仿)(i811)
local _rid
local _menu
local _menuList
local _ronglian
local _xilianPanel
local _xilianListData
local _attrList
local _kaiguangC
local _zhenC
local _itemData
local _attrListKG
local _jinglianListData
local _jinglianPanel
local _jinglianGroup
local _fjbqC -- 防具/兵器控制器
local _jinglianC

function XiLianChiWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common" , "洗炼池")
    
    _menu = UIPackage.CreateObject("Common" , "按钮菜单")
    _menuList = _menu:GetChild("n2")
    _menuList.onClickItem:Add(self.OnClickMenuItem, self)
end

function XiLianChiWin:DoInit()
    self:Center()
    
    _pane = self.contentPane
    _tab=_pane:GetController("tab")
    _tab.onChanged:Add(self.OnTypeChanged, self)
    
    _rongLianList = _pane:GetChild("list")
    _rongLianList.onClickItem:Add(self.OnRongLianItemClick, self)
    _ronglian = _pane:GetChild("n53")
    _ronglian:GetChild("n49").onClick:Add(self.OnClickRongLian, self)
    _ronglian:GetChild("n40").onClick:Add(self.OnClickRongLianItemInfo, self)
    self:UpdateUseMenu()
    
    _xilianPanel = _pane:GetChild("n61")
    _attrList = _xilianPanel:GetChild("n73")
    _attrList.onClickItem:Add(self.OnAttrItemClick, self)
    _xilianPanel:GetChild("n74").onClick:Add(self.OnClickXiLian, self)
    _kaiguangC=_xilianPanel:GetController("c1")
    _zhenC=_xilianPanel:GetController("c2")
    _xilianPanel:GetChild("n49").onClick:Add(self.OnClickKaiGuang, self)
    _xilianPanel:GetChild("n75").onClick:Add(self.OnClickUnLock, self)
    
    local group = _xilianPanel:GetChild("n88")
    _attrListKG = _xilianPanel:GetChildInGroup(group, "n85")
    
    _jinglianPanel = _pane:GetChild("n62")
    _jinglianGroup = _jinglianPanel:GetChild("n99")
    _jinglianPanel:GetChildInGroup(_jinglianGroup, "n81").onClick:Add(self.OnClickJingLian, self)
    _fjbqC=_jinglianPanel:GetController("c2")
    _jinglianC=_jinglianPanel:GetController("c1")
    
    NetClient.Listen(NetKeys.ACMD_XILIAN_RONGLIAN_VIEW, self.OnRongLianView, self)
    NetClient.Listen(NetKeys.ACMD_XILIANCHI_XILIAN_VIEW, self.OnXiLianView, self)
    NetClient.Listen(NetKeys.ACMD_XILIANCHI_XILIAN_ITEM_VIEW, self.OnXiLianItemView, self)
    NetClient.Listen(NetKeys.ACMD_XILIANCHI_JINGLIAN_VIEW, self.OnJingLianView, self)
    NetClient.Listen(NetKeys.ACMD_XILIANCHI_JINGLIAN_ITEM_VIEW, self.OnJingLianItemView, self)
    
    NetClient.ListenRet(NetKeys.QCMD_XILIAN_RONGLIAN_VIEW, self.DefaultQCMDHandler, self)
    NetClient.ListenRet(NetKeys.QCMD_XILIAN_RONGLIAN, self.OnRongLianReturn, self)
    NetClient.ListenRet(NetKeys.QCMD_XILIANCHI_XILIAN_ITEM, self.DefaultQCMDHandler, self)
    NetClient.ListenRet(NetKeys.QCMD_XILIANCHI_XILIAN_LOCK, self.DefaultQCMDHandler, self)
    NetClient.ListenRet(NetKeys.QCMD_XILIANCHI_XILIAN_UNLOCK, self.DefaultQCMDHandler, self)
    NetClient.ListenRet(NetKeys.QCMD_XILIANCHI_KAIGUANG, self.DefaultQCMDHandler, self)
    NetClient.ListenRet(NetKeys.QCMD_XILIANCHI_KAIGUANG_XILIAN, self.DefaultQCMDHandler, self)
    NetClient.ListenRet(NetKeys.QCMD_JINGLIAN_EQUIP, self.OnJiangLianReturn, self)
end

function XiLianChiWin:UpdateUseMenu()
    _menuList:RemoveChildrenToPool()

    local btn = _menuList:AddItemFromPool()
    btn.title = "菩萨灭.真"
    btn.name = "zhen"

    btn = _menuList:AddItemFromPool()
    btn.title = "菩萨灭.仿"
    btn.name = "fang"
end

function XiLianChiWin:OnClickMenuItem(context)
    GRoot.inst:HidePopup(_menu)
    local func = context.data.name
    
    if func=="zhen" then
        _type = 0
    elseif func=="fang" then
        _type = 1
    end
    self:Refresh()
end

function XiLianChiWin:OnShown()
    self:Refresh()
end

function XiLianChiWin:Refresh()
    if _tab.selectedIndex == 0 then
        _rongLianList:RemoveChildrenToPool()
        for _i=1, 7 do
            _ronglian:GetChild("e".._i).grayed = true
        end
        if _type ~= nil then
            NetClient.Send(NetKeys.QCMD_XILIAN_RONGLIAN_VIEW, _type)
            self:ShowModalWait()
        end
    elseif _tab.selectedIndex == 1 then
        if _rongLianList.selectedIndex > 1 then
            local rid = _xilianListData[_rongLianList.selectedIndex+1].rid
            self:XiLianItemView(rid)
        else
            NetClient.Send(NetKeys.QCMD_XILIANCHI_XILIAN_VIEW)
            self:ShowModalWait()
        end
    elseif _tab.selectedIndex == 2 then
        _rongLianList:RemoveChildrenToPool()
        NetClient.Send(NetKeys.QCMD_XILIANCHI_JINGLIAN_VIEW)
        self:ShowModalWait()
    end
end

function XiLianChiWin:OnRongLianView(cmd, data)
    self:CloseModalWait()
    _rid = data.id
    local itemdata = {}
    itemdata.rid = _rid
    _ronglian:GetChild("n40"):SetItem(itemdata)
    _ronglian:GetChild("n58").text = data.score
    _ronglian:GetChild("n69").text = data.money
    _ronglian:GetChild("n70").text = data.butianshenshi_cnt
    
    _rongLianList:RemoveChildrenToPool()
    for _i, _v in ipairs(data.items) do
        _ronglian:GetChild("i".._i).data = _v
        if _v.got then
            _ronglian:GetChild("e".._i).grayed = false
        else
            _ronglian:GetChild("e".._i).grayed = true
        end
        
        if _v.got then
            local btn = _rongLianList:AddItemFromPool()
            local id = GetIdFromRid(_v.rid)
            btn:GetChild("icon").icon = Defs.GetIconSrc(id)
            local def = Defs.GetDef("item", id)
            btn:GetChild("title").text = def.name
            btn.data = _i
        end
    end
end

function XiLianChiWin:OnClickRongLian(context)
    if _rid ~= nil then
        NetClient.Send(NetKeys.QCMD_XILIAN_RONGLIAN, _rid)
        self:ShowModalWait()
    end
end

function XiLianChiWin:OnRongLianItemClick(context)
    if _tab.selectedIndex == 0 then
        local data = context.data.data
        local item = _ronglian:GetChild("i"..data)
        item:SetItem(item.data)
    elseif _tab.selectedIndex == 1 then
        local rid = _xilianListData[context.data.data].rid
        self:XiLianItemView(rid)
    elseif _tab.selectedIndex == 2 then
        _rid = _jinglianListData[context.data.data].rid
        self:RefreshJingLianItemView()
    end
end

function XiLianChiWin:XiLianItemView(rid)
    NetClient.Send(NetKeys.QCMD_XILIANCHI_XILIAN_ITEM_VIEW, rid)
    self:ShowModalWait()
end

function XiLianChiWin:OnClickRongLianItemInfo(context)
    GRoot.inst:ShowPopup(_menu, context.sender, false)
end

function XiLianChiWin:OnRongLianReturn(cmd, data)
    if self.isShowing then
        self:CloseModalWait()
        if data.code~=0 then
            AlertWin.Open(data.msg)
        else
            self:Refresh()
            G_GotItemAlertWin:PopupOpen(data)
        end
    end
end

function XiLianChiWin:OnTypeChanged()
    self:Refresh()
end

function XiLianChiWin:OnXiLianView(cmd, data)
    self:CloseModalWait()
    _xilianListData = data.items
    _rongLianList:RemoveChildrenToPool()
    if _xilianListData ~= nil then
        for _i, _v in ipairs(_xilianListData) do
            local btn = _rongLianList:AddItemFromPool()
            local id = GetIdFromRid(_v.rid)
            btn:GetChild("icon").icon = Defs.GetIconSrc(id)
            local def = Defs.GetDef("item", id)
            btn:GetChild("title").text = def.name
            btn.data = _i
        end
    end
    
    _attrList:RemoveChildrenToPool()
end

function XiLianChiWin:OnXiLianItemView(cmd, data)
    self:CloseModalWait()
    
    _itemData = data
    _rid = data.rid
    local itemData = {}
    itemData.rid = data.rid
    _xilianPanel:GetChild("n67"):SetItem(itemData)
    _xilianPanel:GetChild("n68").text = itemData.def.name
    _xilianPanel:GetChild("n83").text = data.butianshenshi_cnt
    _xilianPanel:GetChild("n81").text = data.score
    
    _attrList:RemoveChildrenToPool()
    _attrListKG:RemoveChildrenToPool()
    if data.attrs ~= nil then
        for _i, _v in ipairs(data.attrs) do
            local btn = _attrList:AddItemFromPool()
            btn:GetChild("title").text = string.format("%s [color=#FFF94E]+%d[/color]", _v.name, _v.value)
            if data.zhen then
                if _v.lock then
                    btn:GetController("lock").selectedIndex = 1
                else
                    btn:GetController("lock").selectedIndex = 0
                end
            else
                btn:GetController("lock").selectedIndex = 2
            end
            btn.data = _i-1
        end
    end
    if data.attrs_kg ~= nil then
        for _i, _v in ipairs(data.attrs_kg) do
            local btn = _attrListKG:AddItemFromPool()
            btn:GetChild("title").text = string.format("%s [color=#FFF94E]+%d[/color]", _v.name, _v.value)
            btn:GetController("lock").selectedIndex = 2
            btn.data = _i-1
        end
    end
    
    _kaiguangC.selectedIndex = data.kaiguan == 1 and 1 or 0
    _zhenC.selectedIndex = data.zhen == 1 and 1 or 0
    
    local group = _xilianPanel:GetChild("n88")
    _xilianPanel:GetChildInGroup(group, "n78").text = data.butianshenshi_cnt_kg
    _xilianPanel:GetChildInGroup(group, "n58").text = data.score_kg
end

function XiLianChiWin:OnClickXiLian(context)
    if _rid ~= nil then
        NetClient.Send(NetKeys.QCMD_XILIANCHI_XILIAN_ITEM, _rid)
        self:ShowModalWait()
    end
end

function XiLianChiWin:OnClickKaiGuang(context)
    if _kaiguangC.selectedIndex == 0 then
        NetClient.Send(NetKeys.QCMD_XILIANCHI_KAIGUANG, _rid)
        self:ShowModalWait()
    elseif _kaiguangC.selectedIndex == 1 then
        NetClient.Send(NetKeys.QCMD_XILIANCHI_KAIGUANG_XILIAN, _rid)
        self:ShowModalWait()
    end
end

function XiLianChiWin:OnClickUnLock(context)
    NetClient.Send(NetKeys.QCMD_XILIANCHI_XILIAN_UNLOCK, _rid)
    self:ShowModalWait()
end

function XiLianChiWin:OnAttrItemClick(context)
    local data = context.data.data
    if data ~= nil and _itemData.zhen then
        NetClient.Send(NetKeys.QCMD_XILIANCHI_XILIAN_LOCK, _rid, data)
        self:ShowModalWait()
    end
end

function XiLianChiWin:OnJingLianView(cmd, data)
    self:CloseModalWait()
    _jinglianC.selectedIndex = 0
    _jinglianListData = data.items
    _rongLianList:RemoveChildrenToPool()
    if _jinglianListData ~= nil then
        for _i, _v in ipairs(_jinglianListData) do
            local btn = _rongLianList:AddItemFromPool()
            local id = GetIdFromRid(_v.rid)
            btn:GetChild("icon").icon = Defs.GetIconSrc(id)
            local def = Defs.GetDef("item", id)
            btn:GetChild("title").text = def.name
            btn.data = _i
        end
    end
end

function XiLianChiWin:RefreshJingLianItemView()
    NetClient.Send(NetKeys.QCMD_XILIANCHI_JINGLIAN_ITEM_VIEW, _rid)
    self:ShowModalWait()
end

function XiLianChiWin:OnJingLianItemView(cmd, data)
    self:CloseModalWait()
    _jinglianC.selectedIndex = 1
    _itemData = data
    _rid = data.rid
    local itemData = {}
    itemData.rid = data.rid
    _jinglianPanel:GetChild("n68"):SetItem(itemData)
    _jinglianPanel:GetChildInGroup(_jinglianGroup, "n82").text = itemData.def.name
    _jinglianPanel:GetChildInGroup(_jinglianGroup, "n100").text = data.lvl
    local type = itemData.def.type
    if string.startswith(type, "fj")  then
        _jinglianPanel:GetChildInGroup(_jinglianGroup, "n89").text = data.recovery
        _jinglianPanel:GetChildInGroup(_jinglianGroup, "n90").text = data.cz_recovery
        _fjbqC.selectedIndex = 1
    else
        _jinglianPanel:GetChildInGroup(_jinglianGroup, "n89").text = data.damage
        _jinglianPanel:GetChildInGroup(_jinglianGroup, "n90").text = data.cz_damage
        _fjbqC.selectedIndex = 0
    end
    _jinglianPanel:GetChildInGroup(_jinglianGroup, "n97").text = "+"..data.jl_lvl
    _jinglianPanel:GetChildInGroup(_jinglianGroup, "n98").text = "+"..(data.jl_lvl+1)
    _jinglianPanel:GetChildInGroup(_jinglianGroup, "n104").text = data.score
    _jinglianPanel:GetChildInGroup(_jinglianGroup, "n91").text = data.need_damoshi_cnt
    _jinglianPanel:GetChildInGroup(_jinglianGroup, "n94").text = data.need_hx_cnt
end

function XiLianChiWin:OnClickJingLian(context)
    NetClient.Send(NetKeys.QCMD_JINGLIAN_EQUIP, _rid)
    self:ShowModalWait()
end

function XiLianChiWin:OnJiangLianReturn(cmd, data)
    if self.isShowing then
        self:CloseModalWait()
        if data.code~=0 then
            AlertWin.Open(data.msg)
        else
            self:RefreshJingLianItemView()
        end
    end
end