FenXianWin = fgui.window_class(WindowBase)

local _lineList
local _id

function FenXianWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common" , "分线")
end

function FenXianWin:DoInit()
    self:Center()

    self.contentPane:GetChild("cancel").onClick:Add(function() self:Hide() end, self)
    self.contentPane:GetChild("ok").onClick:Add(self.OnClickOk, self)
    _lineList = self.contentPane:GetChild("list")
    _lineList.onClickItem:Add(self.OnClickLine, self)

    NetClient.Listen(NetKeys.ACMD_LINES, self.OnRefreshLine, self)
end

function FenXianWin:ReadyToUpdate()
	self:Refresh()
end

function FenXianWin:Refresh()
    _lineList:RemoveChildrenToPool()
	NetClient.Send(NetKeys.QCMD_LINES)
	self:ShowModalWait(NetKeys.QCMD_LINES)
end

function FenXianWin:OnRefreshLine(cmd, data)
	if not self:CloseModalWait(NetKeys.QCMD_LINES) then return end
	local ls = data.ls
	if ls ~= nil and #ls > 0 then
	   for _i, _line in ipairs(ls) do
	       local line = _lineList:AddItemFromPool()
	       line.data = _line
	       line:GetChild("n6").text = _line.na
	       local line_id = Player.data.line_id
	       line:GetChild("n7").visible = line_id ~= nil and line_id == _line.id and true or false
	       line:GetChild("n8").visible = _line.st ~= nil and _line.st > 0 and true or false
	   end
	end
end

function FenXianWin:OnClickOk(context)
    local line_id = Player.data.line_id
    if line_id ~= nil and _id ~= nil and line_id ~= _id then
        NetClient.Send(NetKeys.QCMD_SWITCH_LINE, _id)
        self:ShowModalWait(NetKeys.QCMD_SWITCH_LINE)
        self:Hide()
    end
end

function FenXianWin:OnClickLine(context)
    _id = context.data.data.id
end