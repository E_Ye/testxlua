BagWin = fgui.window_class(WindowBase)

local _typeTab
local _pagetab
local _itemList

local _listData
local _mbur
local _btnGuaShou
local _MAX_BAG = 200
local _MAX_PER_PAGE = 24

local _openType

function BagWin:ctor()
	WindowBase.ctor(self)
	
	self.asyncCreate = true
	self:SetContentSource("Common","背包")
end

function BagWin:DoInit()
	self:Center()

	_listData  = {}
	
	_typeTab=self.contentPane:GetController("type")
	_pagetab=self.contentPane:GetController("page")

	_itemList=self.contentPane:GetChild("list")
	_itemList:SetVirtual()
	_itemList.itemRenderer = function(index, obj) self.RenderListItem(self,index, obj) end
	_itemList.numItems = _MAX_BAG
	_itemList.scrollPane.onScrollEnd:Add(self.OnScrollEnd, self)
	_itemList.onClickItem:Add(self.OnClickCell, self)

	_btnGuaShou = self.contentPane:GetChild("btnGuaShou")
	
	--控制器初始化
	_typeTab.onChanged:Add(self.Refresh, self)
	_typeTab.selectedIndex=0
	
	_btnGuaShou.onClick:Add(self.OnClickSell, self)
	
	self.contentPane:GetChild("n29").onClick:Add(self.OnClickAddBeiBao, self)

	NetClient.Listen(NetKeys.ACMD_MY_ITEMS, self.OnGotItems, self)
	NetClient.Listen(NetKeys.ACMD_VIEW_ITEM, self.OnGotItem, self)
	
	NetClient.ListenRet(NetKeys.QCMD_ADD_BURTHEN, self.DefaultQCMDHandler, self)
	NetClient.ListenRet(NetKeys.QCMD_VIEW_ITEM, self.DefaultQCMDHandler, self)
	NetClient.ListenRet(NetKeys.QCMD_SALE_ITEM_TO_NPC, self.DefaultQCMDHandler, self)
	NetClient.ListenRet(NetKeys.QCMD_DROP_ITEM, self.DefaultQCMDHandler, self)
end

function BagWin:UseItemQCMDHandler(cmd, data)
    if self.isShowing then
        if data.code==0 then
            self:Refresh()
        end
        if data.msg ~= nil then
            ChatPanel.AddAlertMsg(data.msg)
        end
    end
end

function BagWin:Refresh()
	if self.isShowing then
		--NetClient.Send(NetKeys.QCMD_MY_ITEMS, _typeTab.selectedIndex, _pagetab.selectedIndex*_MAX_PER_PAGE, _MAX_PER_PAGE)
		NetClient.Send(NetKeys.QCMD_MY_ITEMS, _typeTab.selectedIndex, 0, 0)
		self:ShowModalWait(NetKeys.QCMD_MY_ITEMS)
	end
end

-- 0- 正常打开玩家背包 1- 赠送物品时打开玩家背包
function BagWin:Open(openType)
    _openType = openType
    self:Show()
end

function BagWin:OnShown()
	self:Refresh()
end

function BagWin:OnHide()
    if _openType == 1 and G_GiveItemWin.isShowing then G_GiveItemWin:Hide() end
end

function BagWin:RenderListItem(index, obj)
--    index = index % _MAX_PER_PAGE
--	obj:SetItem(_listData[index+1])
    
    if _mbur ~= nil and index < _mbur then
        obj:SetItem(_listData[index+1])
        obj:GetController("sign").selectedIndex = 0
    else
        obj:SetItem(nil)
        obj:GetController("sign").selectedIndex = 1
    end
end

function BagWin:OnScrollEnd()
    if _pagetab.selectedIndex == _itemList.scrollPane.currentPageX then
        return
    end
	_pagetab.selectedIndex = _itemList.scrollPane.currentPageX
	self:Refresh()
end

function BagWin:OnGotItem(cmd, data)
    if self.isShowing then
    	if not self:CloseModalWait(654321) then return end
        
        G_ItemInfoWin.data = data
        G_ItemInfoWin:Popup()
    end
end

function BagWin:OnGotItems(cmd, data)
    if not self:CloseModalWait(NetKeys.QCMD_MY_ITEMS) then return end

    local money = data.money
    if money == nil then
        money = 0
    end
    self.contentPane:GetChild("money").text = money
    self.contentPane:GetChild("bur").text = data.bur.."/"..data.mbur
    
    _typeTab.selectedIndex = data.type
    _listData = data.items
    _itemList.numItems = (data.mbur-(data.mbur %_MAX_PER_PAGE))+_MAX_PER_PAGE
    _mbur = data.mbur
    _itemList:RefreshVirtualList()
end

function BagWin:OnClickSell(context)
    local item = context.data
    if not G_HangOnBagWin.isShowing then
        G_HangOnBagWin:Open(Player.data.id)
    end
end

function BagWin:OnClickCell(context)
	local item = context.data
	if item.data==nil then return end
	
	if _openType == nil or _openType == 0 then
	    local it = item.data.def.type
        NetClient.Send(NetKeys.QCMD_VIEW_ITEM, 0, "", item.data.rid, "", 0)
        self:ShowModalWait(654321)
	else
	    if G_GiveItemWin.isShowing then
    	    G_GiveItemWin:OnSelectItem(item.data)
    	end
	end
end

function BagWin:OnClickAddBeiBao()
    NetClient.Send(NetKeys.QCMD_ADD_BURTHEN)
    self:ShowModalWait()
end