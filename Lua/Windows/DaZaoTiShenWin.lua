DaZaoTiShenWin = fgui.window_class(WindowBase)

local _panel
local _c1

local _maxCom
local _step = 1

local attr_def = {"ll", "tz", "wx", "fy", "fh"}

function DaZaoTiShenWin:ctor()
    WindowBase.ctor(self)

    self:SetContentSource("Common" , "打造_提升")
end

function DaZaoTiShenWin:DoInit()
    self:Center()
    
    _panel = self.contentPane
    _c1=_panel:GetController("c1")
    _panel:GetChild("n11").onClick:Add(self.OnClickDaZao, self)
    _panel:GetChild("n10"):SetIncAndDecCallBack(self.OnClickInc, self.OnClickInc, self)
    
    NetClient.ListenRet(NetKeys.QCMD_BUILD_ITEM, self.OnBuildReturn, self)
end

function DaZaoTiShenWin:OnShown()
    self:Refresh()
end

function DaZaoTiShenWin:Refresh()
    if self.data.type == 0 then
        if self.data.type2 == 7 then
            _c1.selectedIndex = 14
        else
            _c1.selectedIndex = self.data.type2
        end
    elseif self.data.type == 1 then
        if self.data.type2 == 1 then
            _c1.selectedIndex = 0
        else
            _c1.selectedIndex = self.data.type2
        end
    end
    
    _panel:GetChild("n8").text = self.data.v
    _panel:GetChild("n10").value = 0
    _panel:GetChild("n7").text = ""
end

function DaZaoTiShenWin:OnClickInc()
    local n = tonumber(_panel:GetChild("n10").value)
    _panel:GetChild("n7").text = self.data.money_name.."x"..string.eval(self.data.money_need, n).."\r\n"..self.data.item_name.."x"..string.eval(self.data.items_need, n)
end

function DaZaoTiShenWin:OnClickDaZao(context)
    NetClient.Send(NetKeys.QCMD_BUILD_ITEM, self.data.rid, tonumber(self.data.type2), tonumber(_panel:GetChild("n10").value))
    self:ShowModalWait()
end

function DaZaoTiShenWin:OnBuildReturn(cmd, data)
    self:CloseModalWait()
    if data.code~=0 then
        AlertWin.Open(data.msg)
    else
        self:Hide()
        G_EquipBuildWin:Refresh()
    end
end