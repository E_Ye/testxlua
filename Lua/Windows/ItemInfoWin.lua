ItemInfoWin = fgui.extension_class(GLabel)
-- data._type 0-自己，1-宠物，2-坐骑，3-法宝，4-兵神，5-精灵

function ItemInfoWin:ctor()
    self._menu = UIPackage.CreateObject("Common" , "按钮菜单")
    self._menuList = self._menu:GetChild("n2")
    self._menuList.onClickItem:Add(self.OnClickMenuItem, self)

    self._typeTab=self:GetController("c1")
    self._zb = self:GetChild("n43")
    self._zb:GetChild("n25").onClick:Add(self.OnClickZBRight, self)
    self._zb:GetChild("n26").onClick:Add(self.OnClickLeft, self)
    
    self._zbxw = self:GetChild("n45")
    self._zbxw:GetChild("n44").onClick:Add(self.OnClickGS, self)
    self._zbxw:GetChild("n45").onClick:Add(self.OnClickCS, self)
    self._zbxw:GetChild("n25").onClick:Add(self.OnClickDestory, self)
    
    self._yp = self:GetChild("n46")
    self._yp:GetChild("n47").onClick:Add(self.OnClickLeft, self)
    self._yp:GetChild("n46").onClick:Add(self.OnClickUse, self)
    self._yp:GetChild("n25").onClick:Add(self.OnClickDestory, self)
    
    self._bw = self:GetChild("n47")
    self._bw:GetChild("n47").onClick:Add(self.OnClickBWLeft, self)
    self._bw:GetChild("n46").onClick:Add(self.OnClickBWRight, self)
    self._bw:GetChild("n50").onClick:Add(self.OnClickGS, self)
    self._bw:GetChild("n48").onClick:Add(self.OnClickDestory, self)
    self._bw:GetChild("n49").onClick:Add(self.OnClickDestory, self)
    
    self._qt = self:GetChild("n44")
    self._qt:GetChild("n42").onClick:Add(self.OnClickGS, self)
    self._qt:GetChild("n41").onClick:Add(self.OnClickCS, self)
    self._qt:GetChild("n25").onClick:Add(self.OnClickDestory, self)
    self._qt:GetChild("n44").onClick:Add(self.OnClickDestory, self)
    self._qt:GetChild("n43").onClick:Add(self.OnClickDefUse, self)
    self._qt:GetChild("n38").onClick:Add(self.OnClickQTLeft, self)
    self._qt:GetChild("n39").onClick:Add(self.OnClickDefUse, self)
    
    NetClient.Listen(NetKeys.ACMD_CHANGE_EQUIPS, self.OnTakeOffEquips, self)
    
    NetClient.ListenRet(NetKeys.QCMD_USE_ITEM, self.UseItemQCMDHandler, self)
    NetClient.ListenRet(NetKeys.QCMD_DROP_ITEM, self.DefaultQCMDHandler, self)
    NetClient.ListenRet(NetKeys.QCMD_TAKE_OFF_EQUIP2, self.DefaultQCMDHandler, self)
end

function ItemInfoWin:DefaultQCMDHandler(cmd, data)
    if G_ItemInfoWin.isShowing then
        G_ItemInfoWin:CloseModalWait()
        if data.code~=0 then
            AlertWin.Open(data.msg)
        else
            if data.msg then  
                ChatPanel.AddAlertMsg(data.msg) 
            end
            --G_ItemInfoWin:Refresh()
        end
    end
end

function ItemInfoWin:Clear()
    self.data = nil
end

function ItemInfoWin:SetItem(data)
    local id = GetIdFromRid(data.rid)
    local def = Defs.GetDef("item", id) 
    if def == nil then
        self:Clear()
        return
    end
    data.def = def
    self.data = data
    
    local it = def.type
    if string.startswith(it, "bq") or string.startswith(it, "fj") then
        if it == "bq.xw" or it == "fj.xw" then
            self._typeTab.selectedIndex = 2
            self:ResetZBXWInfo(data)
        else
            self._typeTab.selectedIndex = 1
            self:ResetZBInfo(data)
        end
    elseif string.startswith(it, "yp") then
        self._typeTab.selectedIndex = 3
        self:ResetYPInfo(data)
    elseif string.startswith(it, "bw") then
        self._typeTab.selectedIndex = 4
        self:ResetBWInfo(data)
    elseif string.startswith(it, "qt") then
        self._typeTab.selectedIndex = 0
        self:ResetQTInfo(data)
    else
        print("---------item type error:"..(it == nil and "nil" or it).." id:"..id)
        G_ItemInfoWin:Hide()
    end
end

function ItemInfoWin:ResetZBInfo(data)
    self._zb:GetChild("n1"):SetItem(data)
    self._zb:GetChild("n2").text = data.def.name
    if data.use_faction ~= nil then
        local faction_list = Defs.GetDef("game", "faction_list")
        self._zb:GetChild("n6").text = faction_list[data.use_faction..""]
    else
        self._zb:GetChild("n6").text = ""
    end
    self._zb:GetChild("n7").text = (data.def.use_lvl == nil and "" or data.def.use_lvl) .."级"
    
    if string.startswith(data.def.type, "bq") then
        if data.jl_lvl == nil then
            self._zb:GetChild("n9").text = "lv.0"
        else
            self._zb:GetChild("n9").text = "lv."..data.jl_lvl
        end
        self._zb:GetChild("n16").text = data.damage ~= nil and data.damage or data.def.damage
        self._zb:GetChild("n17").text = data.cz_damage ~= nil and data.cz_damage or data.def.cz_damage
        self._zb:GetChild("n20").text = data.lvl_zz
        self._zb:GetChild("n21").text = data.lvl_exp
        self._zb:GetController("c3").selectedIndex = 0
        self._zb:GetChild("n28").visible = data.damage_cmp
        self._zb:GetChild("n29").visible = data.cz_damage_cmp
        self._zb:GetChild("n31").visible = data.lvl_zz_cmp
        self._zb:GetChild("n32").visible = data.lvl_exp_cmp
    else
        self._zb:GetChild("n16").text = data.recovery ~= nil and data.recovery or data.def.recovery
        self._zb:GetChild("n17").text = data.cz_recovery ~= nil and data.cz_recovery or data.def.cz_recovery
        self._zb:GetController("c3").selectedIndex = 1
        self._zb:GetChild("n28").visible = data.recovery_cmp
        self._zb:GetChild("n29").visible = data.cz_recovery_cmp
    end
    self._zb:GetChild("n30").visible = data.embed_num_cmp
    
    self._zb:GetChild("n23").value = data.xx == nil and 0 or tonumber(data.xx)
    self._zb:GetChild("n18").text = data.embed_num
    self._zb:GetChild("n19").text = data.def.value
    
    self._zb:GetController("c1").selectedIndex = data.puton == 1 and 1 or 0
    self._zb:GetController("c2").selectedIndex = self.data.def.bd and 1 or 0
    if data.puton == 1 then
        if string.startswith(data.def.type, "bq") then
            self._zb:GetController("c2").selectedIndex = 2
        else
            self._zb:GetController("c2").selectedIndex = 3
        end
    end
end

function ItemInfoWin:ResetZBXWInfo(data)
    self._zbxw:GetChild("n1"):SetItem(data)
    self._zbxw:GetChild("n2").text = data.def.name
    self._zbxw:GetChild("n7").text = data.count == nil and 1 or data.count
    if string.startswith(data.def.type, "bq") then
        self._zbxw:GetController("c2").selectedIndex = 0
    else
        self._zbxw:GetController("c2").selectedIndex = 1
    end
    
    self._zbxw:GetChild("n41").text = (data.def.use_lvl == nil and "" or data.def.use_lvl).."级"
    self._zbxw:GetChild("n42").text = data.damage
    self._zbxw:GetChild("n43").text = data.def.value
    
    self._zbxw:GetController("c1").selectedIndex = data.def.bd and 1 or 0
end

function ItemInfoWin:ResetYPInfo(data)
    self._yp:GetChild("n1"):SetItem(data)
    self._yp:GetChild("n2").text = data.def.name
    self._yp:GetChild("n7").text = data.count == nil and 1 or data.count
    
    self._yp:GetChild("n45").text = data.def.desc
    self._yp:GetChild("n41").text = Defs.GetGameDef("attr_names", data.def.use_attr, "")
    self._yp:GetChild("n42").text = data.def.use_value
    self._yp:GetChild("n43").text = data.def.value
    
    self._yp:GetController("c1").selectedIndex = data.def.bd and 1 or 0
end

function ItemInfoWin:ResetBWInfo(data)
    if data.def.type == "bw.td" then
        self._bw:GetController("c1").selectedIndex = data.def.bd and 2 or 1
    elseif data.def.type == "bw.zfk" then
        local use_obj = data.def.use_obj
    else
        self._bw:GetController("c1").selectedIndex = 0
    end
    
    self._bw:GetChild("n1"):SetItem(data)
    self._bw:GetChild("n2").text = data.def.name
    self._bw:GetChild("n7").text = data.count == nil and 1 or data.count
    
    self._bw:GetChild("n45").text = data.def.desc
    self._bw:GetChild("n43").text = data.def.cash_value
end

function ItemInfoWin:ResetQTInfo(data)
    self._qt:GetChild("n1"):SetItem(data)
    
    if data.def.bd then
        if data.def.type == "qt.xh" then
            self._qt:GetController("c1").selectedIndex = 2
        else
            self._qt:GetController("c1").selectedIndex = 3
        end
    else
        if data.def.type == "qt.xh" then
            self._qt:GetController("c1").selectedIndex = 0
        else
            self._qt:GetController("c1").selectedIndex = 1
        end
    end
    
    self._qt:GetChild("n2").text = data.def.name
    self._qt:GetChild("n7").text = data.count == nil and 1 or data.count
    
    self._qt:GetChild("n34").text = data.def.desc
    self._qt:GetChild("n36").text = data.def.value
end

function ItemInfoWin:OnClickZBRight(context)
    if self.data.puton == 1 then
        -- 已穿上就脱下
        NetClient.Send(NetKeys.QCMD_TAKE_OFF_EQUIP2, 0, '', self.data.rid)
        G_ItemInfoWin:ShowModalWait()
    else
        -- 在背包里就穿上
        NetClient.Send(NetKeys.QCMD_USE_ITEM, 0, '', self.data.rid, 0)
        G_ItemInfoWin:ShowModalWait()
    end
end

function ItemInfoWin:ResetItemInfo()
    G_HangOnItemWin:ResetItemInfo(self.data)
end

function ItemInfoWin:OnClickLeft(context)
    if self.data.puton == 1 then
        if string.startswith(self.data.def.type, "bq")  then
            self:UpdateZBMenu()
            GRoot.inst:ShowPopup(self._menu, context.sender, false)
        else
            -- 穿在身上 镶嵌
            self:OnClickEmbed(context)
        end
    else
        -- 不穿在身上的
        if self.data.def.bd then
            self:OnClickDestory(context)
        else
            self:UpdateMenu()
            GRoot.inst:ShowPopup(self._menu, context.sender, false)
        end
    end
end

-- 点击镶嵌
function ItemInfoWin:OnClickEmbed(context)
    local target = ""
    if self.data._type == 0 then
        target = Player.data.id
    end
    G_XiangQianWin:Open(self.data._type, target, self.data.rid)
end

-- 点击升级
function ItemInfoWin:OnClickShengJi(context)
    G_WeaponUpgradeWin:Open(self.data.rid)
end

function ItemInfoWin:OnClickBWLeft(context)
    self:UpdateBWMenu()
    GRoot.inst:ShowPopup(self._menu, context.sender, false)
end

function ItemInfoWin:OnClickQTLeft(context)
    self:UpdateQTMenu()
    GRoot.inst:ShowPopup(self._menu, context.sender, false)
end

function ItemInfoWin:OnClickBWRight(context)
    if self.data.def.type == "bw.zfk" then
        local use_obj = self.data.def.use_obj
        if use_obj == nil or use_obj == "" then
            self:OnClickDefUse(context)
        else
            self:UpdateBWMenuRight()
            GRoot.inst:ShowPopup(self._menu, context.sender, false)
        end
    else
        self:OnClickDefUse(context)
    end
end

function ItemInfoWin:UseItemQCMDHandler(cmd, data)
    if G_ItemInfoWin.isShowing then
        G_ItemInfoWin:CloseModalWait()
        G_ItemInfoWin:Hide()
        G_BagWin:UseItemQCMDHandler(cmd, data)
    end
end

function ItemInfoWin:OnTakeOffEquips(cmd, data)
    if G_ItemInfoWin.isShowing then
        G_ItemInfoWin:CloseModalWait()
        G_ItemInfoWin:Hide()
        G_MyInfoWin:OnTakeOffEquips(cmd, data)
    end
end

function ItemInfoWin:OnClickMenuItem(context)
    GRoot.inst:HidePopup(self._menu)
    local func = context.data.name
    if func=="guashou" then
        self:OnClickGS(context)
    elseif func=="chushou" then
        self:OnClickCS(context)
    elseif func=="renwu" then
        self:OnClickUse0(context)
    elseif func=="chongwu" then
        self:OnClickUse0(context)
--    elseif func=="mount" then
--        self:OnClickUse0(context)
    elseif func=="diuqi" then
        self:OnClickDestory(context)
    elseif func == "xianqian" then
        self:OnClickEmbed(context)
    elseif func == "shengji" then
        self:OnClickShengJi(context)
    end
end

-- 挂售
function ItemInfoWin:OnClickGS(context)
    GRoot.inst:ShowPopup(G_HangOnItemWin)
    G_HangOnItemWin:ResetItemInfo(self.data)
end

-- 出售
function ItemInfoWin:OnClickCS(context)
    GRoot.inst:ShowPopup(G_SaleWin)
    G_SaleWin:ResetItemInfo(self.data)
end

-- 丢弃
function ItemInfoWin:OnClickDestory(context)
    NetClient.Send(NetKeys.QCMD_DROP_ITEM, self.data.rid, 0)
    G_ItemInfoWin:ShowModalWait()
end

-- 直接使用
function ItemInfoWin:OnClickUse0(context)
    local name = context.data.name
    if name=="chongwu" then
        self:doClickUse(1, '', self.data.rid, 0)
        G_ItemInfoWin:ShowModalWait()
--    elseif name=="mount" then
--        self:doClickUse(2, '', self.data.rid, 0)
--        self:ShowModalWait()
    else
        self:OnClickDefUse(context)
    end
end

function ItemInfoWin:OnClickDefUse(context)
    self:doClickUse(0, '', self.data.rid, 0)
    G_ItemInfoWin:ShowModalWait()
end

function ItemInfoWin:doClickUse(flag, trid, irid, type)
    local msg = self.data.def.c_confirm_msg
    if msg ~= nil then
        ConfirmWin.Open(msg, function()
            NetClient.Send(NetKeys.QCMD_USE_ITEM, flag, trid, irid, type)
            G_ItemInfoWin:ShowModalWait()
        end)
    else
        NetClient.Send(NetKeys.QCMD_USE_ITEM, flag, trid, irid, type)
        G_ItemInfoWin:ShowModalWait()
    end
end

-- 药品使用
function ItemInfoWin:OnClickUse(context)
    self:UpdateUseMenu()
    GRoot.inst:ShowPopup(self._menu, context.sender, false)
end

--背包物品左边按钮菜单
function ItemInfoWin:UpdateMenu()
    self._menuList:RemoveChildrenToPool()

    local btn = self._menuList:AddItemFromPool()
    btn.icon = "ui://Sucai/wenzi70"
    btn.name = "guashou"

    btn = self._menuList:AddItemFromPool()
    btn.icon = "ui://Sucai/wenzi69"
    btn.name = "chushou"
end

-- 身上装备左边按钮菜单
function ItemInfoWin:UpdateZBMenu()
    self._menuList:RemoveChildrenToPool()

    local btn = self._menuList:AddItemFromPool()
    btn.icon = "ui://9024br3ymjar5t"
    btn.name = "shengji"
    
    btn = self._menuList:AddItemFromPool()
    btn.icon = "ui://9024br3ymjar5u"
    btn.name = "xianqian"
end

-- 宝物左边按钮菜单
function ItemInfoWin:UpdateBWMenu()
    self._menuList:RemoveChildrenToPool()

    local btn = self._menuList:AddItemFromPool()
    btn.icon = "ui://9024br3yfpsd6y"
    btn.name = "diuqi"
    
    btn = self._menuList:AddItemFromPool()
    btn.icon = "ui://Sucai/wenzi70"
    btn.name = "guashou"
end

-- 其它消耗左边菜单
function ItemInfoWin:UpdateQTMenu()
    self._menuList:RemoveChildrenToPool()

    local btn = self._menuList:AddItemFromPool()
    btn.icon = "ui://9024br3yiwqq2m"
    btn.name = "guashou"
    
    btn = self._menuList:AddItemFromPool()
    btn.icon = "ui://9024br3yfpsd70"
    btn.name = "chushou"
end

-- 宝物右边按钮菜单
function ItemInfoWin:UpdateBWMenuRight()
    self._menuList:RemoveChildrenToPool()

    local use_obj = self.data.def.use_obj
    
    for _i, _v in ipairs(use_obj) do
        local btn = self._menuList:AddItemFromPool()
        if _v == "user" then
            btn.icon = "ui://9024br3yfpsd6w"
            btn.name = "renwu"
        elseif _v == "pet" then
            btn.icon = "ui://9024br3yfpsd6p"
            btn.name = "chongwu"
        end
    end
end

-- 药品使用菜单
function ItemInfoWin:UpdateUseMenu()
    self._menuList:RemoveChildrenToPool()

    local btn = self._menuList:AddItemFromPool()
    btn.icon = "ui://9024br3yfpsd6w"
    btn.name = "renwu"

    btn = self._menuList:AddItemFromPool()
    btn.icon = "ui://9024br3yfpsd6p"
    btn.name = "chongwu"
end
