DeadWin = fgui.window_class(WindowBase)

local _msgText
local _reliveBtn
local _backBtn

function DeadWin:ctor()
	WindowBase.ctor(self)

    self:SetContentSource( "Common", "DeadWin" )
    self.modal = true
end

function DeadWin:DoInit()
	self:Center()
	_msgText = self.contentPane:GetChild( "msg" )
	_reliveBtn = self.contentPane:GetChild( "reliveBtn" )
	_backBtn = self.contentPane:GetChild( "backBtn" )
	_reliveBtn.onClick:Add(DeadWin.OnReliveBtnClick, self)
	_backBtn.onClick:Add(DeadWin.OnBackBtnClick, self)
end

function DeadWin:Open(killer)
	self:Show()
	if killer==nil then killer='某人' end
	_msgText.text = "你被"..killer.."杀死了"
end

function DeadWin:OnHide()
end

function DeadWin:OnReliveBtnClick()
	GRoot.inst:ShowModalWait()
	NetClient.Send(NetKeys.QCMD_RELIVE, 1)
end

function DeadWin:OnBackBtnClick()
	GRoot.inst:ShowModalWait()
	NetClient.Send(NetKeys.QCMD_RELIVE, 0)
end