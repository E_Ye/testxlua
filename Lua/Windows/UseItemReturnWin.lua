UseItemReturnWin = fgui.window_class(WindowBase)

local _c1
local _itemPanel
local _itemListPanel
local _data

function UseItemReturnWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common","使用物品获得界面")
end

function UseItemReturnWin:DoInit()
    self:Center()
    
    _c1 = self.contentPane:GetController("c1")
    _itemPanel = self.contentPane:GetChild("n0")
    _itemListPanel = self.contentPane:GetChild("n1")
end

function UseItemReturnWin:Open(data)
    if data == nil or data.items == nil or #data.items == 0 then return end
    _data = data
    self:Show()
end

function UseItemReturnWin:OnShown()
    local items = _data.items
    if #items == 1 then
        _c1.selectedIndex = 0
        self:SetItem(_data)
    else
        _c1.selectedIndex = 1
        self:SetItems(_data)
    end
end

function UseItemReturnWin:SetItem(indata)
    local data = indata.items
    _itemPanel:GetChild("n2").text = indata.yjn
    _itemPanel:GetChild("ok").onClick:Add(self.OnConfirmBtnClick, self)
    for _i,_itemInfo in ipairs(data) do
        local n9 = _itemPanel:GetChild("n9")
        local item = n9:GetChild("n0")
        item:SetItem(_itemInfo)
        n9:GetChild("n1").text = _itemInfo.def.name.."x".._itemInfo.count
        break
    end
end

function UseItemReturnWin:SetItems(indata)
    local data = indata.items
    _itemListPanel:GetChild("n2").text = indata.yjn
    _itemListPanel:GetChild("ok").onClick:Add(self.OnConfirmBtnClick, self)
    local _list = _itemListPanel:GetChild("n5")
    
    _list:RemoveChildrenToPool()
    for _i,_itemInfo in ipairs(data) do
        local btn = _list:AddItemFromPool()
        local item = btn:GetChild("n0")
        item:SetItem(_itemInfo)
        btn:GetChild("n1").text = _itemInfo.def.name.."x".._itemInfo.count
    end
end

function UseItemReturnWin:OnConfirmBtnClick(context)
    self:Hide()
end