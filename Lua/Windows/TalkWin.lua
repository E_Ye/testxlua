TalkWin = static_class()

local _view
local _msgText
local _nameText 
local _icon
local _content
local _typingEffect
local _c1

function TalkWin.Create()
    if TalkWin.created then return end

    TalkWin.created = true
    _view = UIPackage.CreateObject("Common", "TalkWin")
    _view:SetSize(GRoot.inst.width, GRoot.inst.height)
    _view:AddRelation(GRoot.inst, RelationType.Size)
    _view.sortingOrder = SortingOrder.Talk

    _c1 = _view:GetController("c1")
    _msgText = _view:GetChild("msg")
    _nameText  = _view:GetChild("name")
    _icon = _view:GetChild("icon")

    _typingEffect = TypingEffect.New(_msgText)
   
    _view:GetChild("skip").onClick:Add(TalkWin.OnSkip)
    _view.onClick:Add(TalkWin.OnClick)

    NetClient.Listen(NetKeys.ACMD_TALK, TalkWin.OnData)
end

function TalkWin.OnData(cmd, data)
    _content = data
    GRoot.inst:AddChild(_view)
    TalkWin.Update()
end

TalkWin.get.isShowing = function()
    return _view.parent ~= nil
end

function TalkWin.Hide()
    _typingEffect:Cancel()
    if _view.parent ~= nil then
        GRoot.inst:RemoveChild(_view)
    end
end

function TalkWin.OnClick()
    _typingEffect:Cancel()

    local word = _content.words[1]
    table.remove(_content.words, 1)
    if word.r_npcs~=nil then
        local npcs = string.split(word.r_npcs, ',')
        for i,v in ipairs(npcs) do 
            local critter = World.GetThing(v)
            if critter~=nil then
                World.RemoveThing(critter)
            end
        end
    end

    if #_content.words==0 then
        TalkWin.Complete()
    else
       TalkWin.Update()
    end
end

function TalkWin.Complete()
    TalkWin.Hide()
     MainUI.view.visible = true
    if _content.tid~=nil then
        G_TaskWin:Open(_content.tid)
    elseif TalkWin.openTaskOnClose~=nil then
        local tid = TalkWin.openTaskOnClose
        TalkWin.openTaskOnClose = nil
        G_TaskWin:Open(tid)
    end
end

function TalkWin.Update()
    local word = _content.words[1]
    local msg =  ScriptUtils.ParseUBB(word.cmmt)
    if word.type==1 then
        _c1.selectedIndex = 1
        _msgText.text = msg
    else
        _msgText.text = msg

        if word.role=="self" then
            _icon.icon = nil
            _c1.selectedIndex = 0
            _icon.icon = Player.me.iconSrc..'_b'
            _nameText .text = Player.data.name
        elseif word.role~=nil and string.len(world.role)>0 then
            if word.role~='-' then
                _icon.icon = Defs.GetIconSrc(GetIdFromRid(word.role))..'_b'
            else
                _icon.icon = nil
            end
            _c1.selectedIndex = 0
            TalkWin.SetSpeaker(word.role)
        elseif _content.owner~=nil and string.len(_content.owner)>0 then
            if _content.owner~='-' then
                _icon.icon = Defs.GetIconSrc(GetIdFromRid(_content.owner))..'_b'
            else
                _icon.icon = nil
            end
            _c1.selectedIndex = 0
            TalkWin.SetSpeaker(_content.owner)
        else
            _c1.selectedIndex = 1
            _icon.icon = nil
            TalkWin.SetSpeaker(nil)
        end

        MainUI.view.visible = false
        _typingEffect:Start()
        _typingEffect:PrintAll(0.01)
    end
end

function TalkWin.SetSpeaker(val)
    _nameText .text = ''
    if val==nil or string.len(val)==0 then
        return
    end

    local owner = World.GetThing(val)
    if owner~=nil then
        _nameText .text = owner.data.name
    elseif string.sub(val, 1, 1)=='i' then
        _nameText .text = Defs.GetString("item", GetIdFromRid(val), "name")
    end
end

function TalkWin.OnSkip(context)
    context:StopPropagation()
    TalkWin.Complete()
end