MountWin = fgui.window_class(WindowBase)

local _list
local _tab
local _btnCall
local _shuxing
local _tujian
local _tuJianList
local _tuJianC

local _listData
local _info
local _tuJiansData
local _tuJianData

function MountWin:ctor()
	WindowBase.ctor(self)

	self.asyncCreate = true
	self:SetContentSource("Common","坐骑")
end

function MountWin:DoInit()
	self:Center()

	_list = self.contentPane:GetChild("list")
	_list:RemoveChildrenToPool()
	_list.onClickItem:Add(self.OnMountChanged, self)
	_tab = self.contentPane:GetController("tab")
    _tab.onChanged:Add(self.OnTabChanged, self)
    
	_shuxing = self.contentPane:GetChild("shuxing")
	_tujian = self.contentPane:GetChild("tujian")
	_tuJianList = _tujian:GetChild("n72")
    _tuJianList.onClickItem:Add(self.OnTuJianChanged, self)
    local p = self
    _tuJianList.itemRenderer = function(index, obj) p:RenderItem(index, obj) end
    _tuJianList:SetVirtual()
    _tuJianC = _tujian:GetController("c1")
    _tujian:GetChild("n122").onClick:Add(self.OnGoToNpc, self)
    _tujian:GetChild("n114").onClick:Add(self.OnDuanLianLu, self)

	_btnCall = _shuxing:GetChild("btnCall")
	_btnCall.onClick:Add(self.OnClickCall, self)

	_shuxing:GetChild("btnDismiss").onClick:Add(self.OnClickDismiss, self)
	-- 12-坐骑攻击资质，13-坐骑防御资质，14-坐骑生命资质，15-坐骑法力资质
	_shuxing:GetChild("n25").onClick:Add(self.OnClickZZ, self)
	_shuxing:GetChild("n25").data = 14
	_shuxing:GetChild("n26").onClick:Add(self.OnClickZZ, self)
    _shuxing:GetChild("n26").data = 15
    _shuxing:GetChild("n31").onClick:Add(self.OnClickZZ, self)
    _shuxing:GetChild("n31").data = 12
    _shuxing:GetChild("n32").onClick:Add(self.OnClickZZ, self)
    _shuxing:GetChild("n32").data = 13

	NetClient.Listen(NetKeys.ACMD_MY_MOUNTSES, self.OnServer_MyMounts, self)
	NetClient.Listen(NetKeys.ACMD_VIEW_MOUNT, self.OnServer_ViewMount, self)
	NetClient.Listen(NetKeys.ACMD_CALL_OUT_MOUNT, self.OnServer_CallMount, self)
    NetClient.Listen(NetKeys.ACMD_CALL_BACK_MOUNT, self.OnServer_CallMount, self)
    NetClient.Listen(NetKeys.ACMD_TUJIANS, self.OnTuJians, self)
    NetClient.Listen(NetKeys.ACMD_TUJIAN_VIEW, self.OnTuJianView, self)
    
	NetClient.ListenRet(NetKeys.QCMD_RIDE_ON, self.DefaultQCMDHandler, self)
	NetClient.ListenRet(NetKeys.QCMD_JUMP_DOWN, self.DefaultQCMDHandler, self)
	NetClient.ListenRet(NetKeys.QCMD_CHUCK, self.OnChuckReturn, self)
	NetClient.ListenRet(NetKeys.QCMD_ADD_PET_ZZ, self.DefaultQCMDHandler, self)
end

function MountWin:Open()
	self:Show()
end

function MountWin:OnMountChanged()
    self:Refresh()
end

function MountWin:Refresh()
	if self.isShowing and _listData and #_listData>0 and _list.selectedIndex>=0 then
		NetClient.Send(NetKeys.QCMD_VIEW_MOUNT, _listData[_list.selectedIndex+1].rid, "")
		self:ShowModalWait()
	end
end
	
function MountWin:RefreshList()
	if self.isShowing then
		NetClient.Send(NetKeys.QCMD_MY_PETS, 1)
		self:ShowModalWait()
	end
end

function MountWin:OnShown()
	self:RefreshList()
end

function MountWin:OnTabChanged()
    local si = _tab.selectedIndex
    if si == 0 then
        self:ResetShuXing()
    elseif si == 1 then
        self:ResetTuJian()
    else
    end
end

function MountWin:OnServer_MyMounts(cmd, data)
	self:CloseModalWait()
		
	local lsi = _list.selectedIndex
	_list:RemoveChildrenToPool()
	_listData = data
	for _i,mount in ipairs(_listData) do
		local btn = _list:AddItemFromPool()
		btn.title = mount.name
		btn:GetChild("lvl").text = mount.lvl
		btn.icon = GetIdFromRid(mount.rid)
		btn:GetController("status").selectedIndex = mount.status==0 and 0 or 3
	end

	if lsi==-1 or lsi>=#_listData then
		_list.selectedIndex = 0
	elseif lsi >= 0 and lsi < #_listData then
	    _list.selectedIndex = lsi
	end
	self:Refresh()
end

-- tab 属性
function MountWin:OnServer_ViewMount(cmd, data)
	if _listData==nil or data.rid~=_listData[_list.selectedIndex+1].rid then return end

	self:CloseModalWait()

	_info = data
	self:OnTabChanged()
end

function MountWin:ResetShuXing()
    if _info == nil then
        return
    end
    
    _shuxing:GetChild("n36").text = _info.name.." ".._info.lvl.."级"
    _shuxing:GetChild("n16").value = _info.exp
    _shuxing:GetChild("n16").max = _info.upgrade_exp
    _shuxing:GetChild("n22").text = _info.hp_zz
    _shuxing:GetChild("n24").text = _info.mp_zz
    _shuxing:GetChild("n28").text = _info.attack_zz
    _shuxing:GetChild("n30").text = _info.recovery_zz
    _shuxing:GetChild("n34").text = _info.qianghuadu
    
    _shuxing:GetController("status").selectedIndex = _info.status or 0
end

function MountWin:OnClickCall()
	if _info==nil then return end

	self:ShowModalWait()
	if _info.status==0 then
		NetClient.Send(NetKeys.QCMD_RIDE_ON, _info.rid)
	else
		NetClient.Send(NetKeys.QCMD_JUMP_DOWN)
	end
end

function MountWin:OnClickDismiss()
    if _info==nil then return end

    -- 0-休息，1-观战，2-参战
    if _info.status==0 then
        self:ShowModalWait()
        NetClient.Send(NetKeys.QCMD_CHUCK, 2, _info.rid)
    end
end

function MountWin:OnServer_CallMount(cmd, data)
    self:CloseModalWait()
    self:RefreshList()
end

function MountWin:OnChuckReturn(cmd, data)
    if data == nil then
        return
    end
    
    if data.code ~= 0 then
        return
    end
    
    self:RefreshList()
    AlertWin.Open(data.msg)
end

function MountWin:OnClickZZ(context)
    if context.sender.data == nil then
        return
    end
    
    -- 资质
    self:ShowModalWait()
    NetClient.Send(NetKeys.QCMD_ADD_PET_ZZ, context.sender.data, _info.rid)
end

-- 图鉴
function MountWin:ResetTuJian()
    self:ShowModalWait()
    NetClient.Send(NetKeys.QCMD_TUJIANS, 2)
end

function MountWin:OnTuJians(cmd, data)
    self:CloseModalWait()
    if data.type ~= 2 then
        return
    end 
    
    _tuJiansData = data.list
    _tuJianList.numItems = #_tuJiansData
    
    self:RefreshTuJian()
end

function MountWin:RenderItem(index, obj)
    local data = _tuJiansData[index+1]
    local rid = data.rid
    obj:SetItem(data)
    if rid == nil then
        obj.grayed = true
    else
        obj.grayed = false
    end
end

function MountWin:OnTuJianChanged(context)
    local rid = context.data.data.rid
    if rid == nil then
        rid = context.data.data.id
    end
    self:doRefreshTuJian(rid)
end

function MountWin:RefreshTuJian(lsi)
    if lsi == nil or lsi == -1 or lsi >=#_tuJiansData then
        _tuJianList.selectedIndex = 0
        lsi = 0
    end
    local rid = _tuJiansData[lsi+1].rid
    if rid == nil then
        rid = _tuJiansData[lsi+1].id
    end
    self:doRefreshTuJian(rid)
end

function MountWin:doRefreshTuJian(rid)
    if rid == nil then
        return
    end
    
    self:ShowModalWait()
    NetClient.Send(NetKeys.QCMD_TUJIAN_VIEW, 2, rid)
end

function MountWin:OnTuJianView(cmd, data)
    self:CloseModalWait()
    if data.type ~= 2 then
        return
    end
    
    _tuJianData = data
    local id = data.id
    if id ~= nil then
        local def = Defs.GetDef("mount", id)
        _tujian:GetChild("n76").text = def.name
        _tujian:GetChild("n100").text = def.desc
        
        _tujian:GetChild("n119").text = data.target.task
        _tujian:GetChild("n121").text = data.target.cailiao
        
        local def = Defs.GetDef("mount", id)
        _tuJianC.selectedIndex = def.dl == true and 1 or 0
    else
        local def = Defs.GetDef("mount", GetIdFromRid(data.rid))
        _tujian:GetChild("n76").text = def.name
        _tujian:GetChild("n100").text = def.desc
        _tuJianC.selectedIndex = 2
    end
    
    _tujian:GetChild("n109").text = data.zr_cz_sm
    _tujian:GetChild("n110").text = data.zr_cz_fl
    _tujian:GetChild("n112").text = data.zr_cz_fy
    _tujian:GetChild("n113").text = data.zr_cz_gj
end

function MountWin:OnGoToCatchPet(context)
    AutoRouting.Start(target.eid, _tuJianData.target)
    self:Hide()
end

function MountWin:OnGoToNpc(context)
    AutoRouting.Start(target.eid, _tuJianData.target)
    self:Hide()
end

function MountWin:OnDuanLianLu(context)
    G_MountDuanLianWin:Open(_tuJianData.id)
end