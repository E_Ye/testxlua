TeamWin = fgui.window_class(WindowBase)

local _tab
local _list
local _info
local _rid
local _menu
local _menuList

function TeamWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = false
    
    self:SetContentSource("Common" , "组队")
    
    _menu = UIPackage.CreateObject("Common" , "按钮菜单")
    _menuList = _menu:GetChild("n2")
    _menuList.onClickItem:Add(self.OnClickMenuItem, self)
end

function TeamWin:DoInit()
    self:Center()

    _list = self.contentPane:GetChild("list")
    --_list.onClickItem:Add(self.OnClickCell, self)
    _list:RemoveChildrenToPool()
    
    self.contentPane:GetChild("n12").onClick:Add(self.OnClickQuit, self)
    self.contentPane:GetChild("n13").onClick:Add(self.OnAutoAcceptClick, self)
    
    _tab=self.contentPane:GetController("c1")
    self:UpdateMenu()

    NetClient.ListenRet(NetKeys.QCMD_TEAM_VIEW, self.OnViewResult, self)
    NetClient.ListenRet(NetKeys.QCMD_TEAM_QUIT, self.OnQuitTeam, self)
    NetClient.ListenRet(NetKeys.QCMD_TEAM_OPERATE, self.DefaultQCMDHandler, self)
    NetClient.ListenRet(NetKeys.QCMD_TEAM_CREATE, self.DefaultQCMDHandler, self)
    NetClient.ListenRet(NetKeys.QCMD_TEAM_JOIN, self.DefaultQCMDHandler, self)
    
    NetClient.Listen(NetKeys.ACMD_TEAM_VIEW, self.OnViewTeam, self)
end

function TeamWin:Open(rid)
    _rid = rid
    self:Show()
end

function TeamWin:OnShown()
    self:Refresh()
end

function TeamWin:Refresh()
    self:ShowModalWait()
    NetClient.Send(NetKeys.QCMD_TEAM_VIEW, _rid)
end

function TeamWin:OnHide()
end

function TeamWin:OnViewResult(cmd, data)
    if data.code==2 then
        NetClient.Send(NetKeys.QCMD_TEAM_CREATE, "")
    else
        PromptResult(data)
    end
end

function TeamWin:OnViewTeam(cmd, data)
    self:CloseModalWait()
    
    _info = data
    _list:RemoveChildrenToPool()
    local arr = _info.ms
    local dy = false
    if arr ~= nil then
        for i,m in ipairs(arr) do
            self:ResetTeamItem(m, false)
            if m.id == Player.data.id then
                dy = true
            end
        end
    end
    arr = _info.rs
    if arr ~= nil then
        for i,m in ipairs(arr) do
            self:ResetTeamItem(m, true)
        end
    end
    
    self.contentPane:GetChild("n13").selected = data.mode == 2 and true or false
    
    if data.dz == Player.data.id then
        _tab.selectedIndex = 0
        _rid = Player.data.id
    elseif dy then
        _tab.selectedIndex = 1
    -- 0-禁止加入，1-批准加入，2-自动批准
    elseif data.mode == 1 then 
        _tab.selectedIndex = 2
    elseif data.mode == 2 then
        _tab.selectedIndex = 3
    end
end

function TeamWin:ResetTeamItem(m, isReq)
    local btn = _list:AddItemFromPool()
    btn:GetChild("head").icon = UIPackage.GetItemURL("Sucai", 'u'..m.fct..m.sex)
    btn:GetChild("head").text = m.lvl
    btn:GetChild("name").text = m.name
    btn:GetChild("add_attack_percent").text = m.at_p == nil and "-" or m.at_p
    btn:GetChild("add_recovery_percent").text = m.re_p == nil and "-" or m.re_p
    if isReq then
        btn:GetChild("status").text = "申请"
        btn:GetController("c1").selectedIndex = 1
    else
        btn:GetController("c1").selectedIndex = 0
        if m.id==_info.dz then
            btn:GetChild("status").text = "队长"
            
        else
            btn:GetChild("status").text = "队员"
        end
    end
    btn.data = m
    
    btn:GetChild("n6").onClick:Add(self.OnClickMenu, self)
    if m.id ~= Player.data.id then
        btn:GetChild("head").onClick:Add(self.OnClickHead, self)
    else
        btn:GetChild("head").onClick:Remove(self.OnClickHead)
    end
end

function TeamWin:OnClickQuit()
    if _tab.selectedIndex == 0 or _tab.selectedIndex == 1 then
        NetClient.Send(NetKeys.QCMD_TEAM_QUIT)
    else
        NetClient.Send(NetKeys.QCMD_TEAM_JOIN, _rid)
    end
    self:ShowModalWait()
end

function TeamWin:OnAutoAcceptClick(context)
    NetClient.Send(NetKeys.QCMD_TEAM_OPERATE, Player.data.id, context.sender.selected and 3 or 4)
    self:ShowModalWait()
end

function TeamWin:OnQuitTeam(cmd, data)
    self:CloseModalWait()
    if data.code~=0 then
        AlertWin.Open(data.msg)
    else
        self:Hide()
    end
end

function TeamWin:OnClickHead(context)
    if context.sender.parent.data.id ~= Player.data.id then
        G_PlayerInfoQueryWin:Open(context.sender.parent.data, 0)
    end
end

function TeamWin:UpdateMenu()
    _menuList:RemoveChildrenToPool()

    local btn = _menuList:AddItemFromPool()
    btn.icon = "ui://9024br3yfypbad"
    btn.name = "jujue"
    
    btn = _menuList:AddItemFromPool()
    btn.icon = "ui://9024br3yfypbae"
    btn.name = "pizhun"
end

function TeamWin:OnClickMenu(context)
    _menuList.data = context.sender.parent.data
    GRoot.inst:ShowPopup(_menu, context.sender, false)
end

function TeamWin:OnClickMenuItem(context)
    GRoot.inst:HidePopup(_menu)
    local func = context.data.name
    if func=="jujue" then
        self:OnClickJuJue(context)
    elseif func=="pizhun" then
        self:OnClickPiZhun(context)
    end
end

function TeamWin:OnClickJuJue(context)
    NetClient.Send(NetKeys.QCMD_TEAM_OPERATE, context.sender.data.id, 2)
    self:ShowModalWait()
end

function TeamWin:OnClickPiZhun(context)
    NetClient.Send(NetKeys.QCMD_TEAM_OPERATE, context.sender.data.id, 1)
    self:ShowModalWait()
end