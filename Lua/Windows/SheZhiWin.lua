SheZhiWin = fgui.window_class(WindowBase)

local _bgSoundEnabled
local _effectSoundEnabled

function SheZhiWin:ctor()
	WindowBase.ctor(self)

	self:SetContentSource("Common" , "游戏设置")
end

function SheZhiWin:DoInit()
	self:Center()

	self.contentPane:GetChild("deleteRole").onClick:Add(self.OnDeleteRole, self)
	
	_bgSoundEnabled = self.contentPane:GetChild("bgSound")
	_effectSoundEnabled = self.contentPane:GetChild("effectSound")

	_bgSoundEnabled.onChanged:Add(self.OnbgSoundEnabled, self)
	_effectSoundEnabled.onChanged:Add(self.OnEffectSoundEnabled, self)

	_bgSoundEnabled.selected = tonumber(Player.prefs.sound_off)~=1
	_effectSoundEnabled.selected = tonumber(Player.prefs.sound_off2)~=1
end

function SheZhiWin:OnShown()
	_bgSoundEnabled.selected = tonumber(Player.prefs.sound_off)~=1
	_effectSoundEnabled.selected = tonumber(Player.prefs.sound_off2)~=1
end

function SheZhiWin:OnHide()
end

------ private function ---------
function SheZhiWin:OnReconnect()
	NetClient.Close()
	World.Close()
	RoleUI.Show()
end

function SheZhiWin:OnbgSoundEnabled()
	if _bgSoundEnabled.selected then
		Player.prefs.sound_off = 0
		NetClient.Send(NetKeys.QCMD_PRIVATE_SETUP, {psn={sound_off="0"}})
		SoundManager.inst.bgSoundOn = true
	else
		Player.prefs.sound_off = 1
		NetClient.Send(NetKeys.QCMD_PRIVATE_SETUP, {psn={sound_off="1"}})
		SoundManager.inst.bgSoundOn = false
	end
end

function SheZhiWin:OnEffectSoundEnabled()
	if _effectSoundEnabled.selected then
		Player.prefs.sound_off2 = 0
		NetClient.Send(NetKeys.QCMD_PRIVATE_SETUP, {psn={sound_off2="0"}})
		SoundManager.inst.soundOn = true
	else
		Player.prefs.sound_off2 = 1
		NetClient.Send(NetKeys.QCMD_PRIVATE_SETUP, {psn={sound_off2="1"}})
		SoundManager.inst.soundOn = false
	end
end

function SheZhiWin:OnDeleteRole()
	ConfirmWin.Open('确实要删除角色吗，此操作不可恢复！', 
		function()
    		NetClient.Send(NetKeys.QCMD_REMOVE_ROLE, Player.data.id)
    		NetClient.deletingRole = true
    	end
    )
end
