OthersPetWin = fgui.window_class(WindowBase)

local _list
local _shuxing
local _info
local _uid
local _rid

function OthersPetWin:ctor()
	WindowBase.ctor(self)

	self.asyncCreate = true
	self:SetContentSource("Common","别人的宠物")
end

function OthersPetWin:DoInit()
	self:Center()

	_list = self.contentPane:GetChild("list")
	_list:RemoveChildrenToPool()

	_shuxing = self.contentPane:GetChild("shuxing")
	
	NetClient.Listen(NetKeys.ACMD_VIEW_PET, self.OnServer_ViewPet, self)
	
	NetClient.ListenRet(NetKeys.QCMD_VIEW_PET, self.OnViewPetReply, self)
end

function OthersPetWin:Open(uid, rid)
    _uid = uid
    _rid = rid
	self:Show()
end

function OthersPetWin:OnShown()
    self:Refresh()
end

function OthersPetWin:Refresh()
    if self.isShowing then
        NetClient.Send(NetKeys.QCMD_VIEW_PET, _rid, _uid)
        self:ShowModalWait()
    end
end

function OthersPetWin:OnServer_ViewPet(cmd, data)
    self:CloseModalWait()

    _info = data
    self:ResetShuXing()
end

function OthersPetWin:ResetShuXing()
    if _info == nil then
        return
    end
    
    _shuxing:GetChild("n48").text = _info.name.." ".._info.lvl.."级"
    _shuxing:GetChild("n16").value = _info.exp == nil and 0 or _info.exp
    _shuxing:GetChild("n16").max = _info.upgrade_exp == nil and 0 or _info.upgrade_exp
    _shuxing:GetChild("n20").text = _info.hp
    _shuxing:GetChild("n22").text = _info.mp
    _shuxing:GetChild("n24").text = _info.attack
    _shuxing:GetChild("n26").text = _info.recovery
    _shuxing:GetChild("n28").text = _info.zhongcheng
    _shuxing:GetChild("n30").text = _info.zz
    _shuxing:GetChild("n32").text = _info.attack_speed
    _shuxing:GetChild("n34").text = _info.xianqi_lvl
    _shuxing:GetChild("n36").text = _info.qianghuadu
    _shuxing:GetChild("n41").text = _info.lingli.."/".._info.max_lingli
end

function OthersPetWin:OnViewPetReply(cmd, data)
    if self.isShowing then
        self:CloseModalWait()
        if data.code~=0 then
            AlertWin.Open(data.msg)
            self:Hide()
        end
    end
end