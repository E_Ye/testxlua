BuyWinShangCheng = fgui.window_class(WindowBase)

local itemId
local itemData

function BuyWinShangCheng:ctor()
	WindowBase.ctor(self)

	self.animation = {"move_left", "move_right"}
	self:SetContentSource("Common","商城购买")
end

function BuyWinShangCheng:DoInit()
	self.sortingOrder = 1
	self:SetXY(820, 200)

	self.inputForm = self.contentPane:GetChild("n10")

	self.cell = self.contentPane:GetChild("n9")
	self.type = self.contentPane:GetChild("type")
	self.title = self.contentPane:GetChild("title")
	self.price = self.contentPane:GetChild("price")
	self.des = self.contentPane:GetChild("n19"):GetChild("title")
	self.cost = self.contentPane:GetChild("txt_cost")

	self.inputForm:GetChild("btnInc").onClick:Add(self.OnChangeCount, self)
	self.inputForm:GetChild("btnDec").onClick:Add(self.OnChangeCount, self)
	self.contentPane:GetChild("n11").onClick:Add(self.OnClickOk, self)
end

function BuyWinShangCheng:Open(caller)
	self.caller = caller
	self:SetXY(820, 200)
	self:Show()

end

function BuyWinShangCheng:OnShown()
	self.inputForm.value = 1
	
end

function BuyWinShangCheng:OnHide()
	self.inputForm.value = 1
	self:Set(itemId, itemData)
end

function BuyWinShangCheng:OnClickOk()
	if itemId ~= nil then
		self.caller:DoBuy(itemId, self.inputForm.value)

	end
end

function BuyWinShangCheng:SetItem(targetId, targetData)
	itemId = targetId
	itemData = targetData
	self.inputForm.value = 1
	self:Set(itemId, itemData)
end

function BuyWinShangCheng:Set(targetId, targetData)
	self.type.text = ""
	self.title.text = ""
	self.price.text = 0
	self.des.text = ""
	self.cost.text = 0
	if targetData.name then
    	self.title.text = targetData.name
	end
 	if targetData.cash_value then
 		self.price.text = targetData.cash_value    
	end
	if targetData.desc then
 		self.des.text = itemData.desc    
	end
	if targetData.type then
		if targetData.type =="bw" then
			self.type.text = "宝物"    
		end
	end
	-- if targetData.cash_value then
 -- 		self.price.text = targetData.cash_value    
	-- end
	
	self.cell.icon = Defs.GetIconSrc(targetId)
	if self.price.text then
		self.cost.text = self.inputForm.value * self.price.text 
	end
end

function BuyWinShangCheng:OnChangeCount()
	self.cost.text = self.inputForm.value * self.price.text 
end


