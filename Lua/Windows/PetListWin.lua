PetListWin = fgui.window_class(WindowBase)

local _listData = {} -- 列表内容
local _typeTab = nil
local _list
local _type
local _pid

function PetListWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = false
    self:SetContentSource("Common","技能物品选择列表")
end

function PetListWin:DoInit()
    self:Center()
    
    _typeTab=self.contentPane:GetController("c1")
    _list=self.contentPane:GetChild("list")
    local p = self
    _list.itemRenderer = function(index, obj) p:RenderItem(index, obj) end
    _list:SetVirtual()
    _list.onClickItem:Add(self.OnClickPet, self)
    
    NetClient.Listen(NetKeys.ACMD_HECHENG_PETS, self.OnViewPets, self)
    
    NetClient.ListenRet(NetKeys.QCMD_HECHENG_PETS, self.DefaultQCMDHandler, self)
end

function PetListWin:Open(type, pid)
    _type = type
    _pid = pid
    if not self.isShowing then
        self:Show()
    end
end

function PetListWin:ReadyToUpdate()
    self:ShowModalWait()
    NetClient.Send(NetKeys.QCMD_HECHENG_PETS, _type, _pid)
    _typeTab.selectedIndex=2
end

function PetListWin:OnHide()
end

function PetListWin:OnViewPets(cmd, data)
    self:CloseModalWait()
    _listData = data.list
    _list.numItems = #_listData
end

function PetListWin:RenderItem(index, obj)
    obj:SetItem(_listData[index+1])
end

function PetListWin:OnClickPet(context)
    local item = context.data.data
    G_JiuLongLuWin:ResetSelectedPet(_type, item)
    self:Hide()
end
