SaleWin = fgui.window_class(WindowBase)

local _panel
local _data

function SaleWin:ctor()
    WindowBase.ctor(self)

    self:SetContentSource("Common","出售")
end

function SaleWin:DoInit()
    self:Center()
    
    _panel = self.contentPane
    _panel:GetChild("btnOk").onClick:Add(self.OnClickSell, self)
    _panel:GetChild("btnMax").onClick:Add(self.OnClickMax, self)
    
    NetClient.ListenRet(NetKeys.QCMD_SALE_ITEM_TO_NPC, self.SaleQCMDHandler, self)
end

function SaleWin:SaleQCMDHandler(cmd, data)
    if self.isShowing then
        self:CloseModalWait()
        if data.code~=0 then
            AlertWin.Open(data.msg)
        else
            if data.msg then  
                ChatPanel.AddAlertMsg(data.msg) 
            end
        end
        self:Hide()
        if G_ItemInfoWin.isShowing then
            G_ItemInfoWin:Hide()
        end
    end
end

function SaleWin:ResetItemInfo(data)
    if data == nil then
        return
    end
    _data = data
    _panel:GetController("c1").selectedIndex = 1
    _panel:GetChild("cell"):SetItem(data)
    _panel:GetChildInGroup(_panel:GetChild("n21"), "itemName").text = data.def.name
    local count = data.count == nil and 1 or data.count
    _panel:GetChildInGroup(_panel:GetChild("n21"), "ownCount").text = count
    _panel:GetChildInGroup(_panel:GetChild("n21"), "sell").text = data.def.value
    _panel:GetChild("input"):SetRegionAndStep(1, 1, count)
    _panel:GetChild("input"):ResetMax()
    _panel:GetChild("input"):SetIncAndDecCallBack(self.OnClickInc, self.OnClickInc, self)
    self:OnClickInc()
end

function SaleWin:OnClickInc()
    local n = tonumber(_panel:GetChild("input").value)
    local value = 1
    if _data ~= nil then
        value = _data.def.value
    end
    _panel:GetChild("money").text = n*value
end

function SaleWin:OnClickSell()
    NetClient.Send(NetKeys.QCMD_SALE_ITEM_TO_NPC, "", _data.rid, _panel:GetChild("input").value)
    self:ShowModalWait()
end

function SaleWin:OnClickMax()
    _panel:GetChild("input"):ResetMax()
end