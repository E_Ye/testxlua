SkillCell = fgui.extension_class(GLabel)

function SkillCell:ctor()
    self.hotkeySign = self:GetChild("hotkeySign") or fgui.null
    self.maskObj = self:GetChild("mask") or fgui.null
end

function SkillCell:Clear()
    self.data = nil
    self.icon = nil
    self.title = ""
    if self.hotkeySign then
        self.hotkeySign.visible = false
    end
    if self.maskObj then
        self.maskObj.fillAmount = 0
    end
end

function SkillCell:SetItem(data)
    if data~=nil then
        if type(data)=="string" then
            self.data = { rid=data }
        else
            self.data = data
        end 
        if self.data.id==nil then
            self.data.id = GetIdFromRid(self.data.rid)
        end
        if self.data.def==nil then
            --同时兼容物品
            local s = string.sub(self.data.id, 1, 1)
            if s=="i" then
                 self.data.def = Defs.GetDef("item", self.data.id)
            elseif s == "k" then
                self.data.def = Defs.GetDef("skill", self.data.id)
            elseif s == "p" then
                self.data.def = Defs.GetDef("pet", self.data.id)
            elseif s == "m" then
                self.data.def = Defs.GetDef("mount", self.data.id)
            end
        end
        self.icon = Defs.GetIconSrc(self.data.id)
        if self.data.title ~=nil then
            self.title = self.data.title
        else
            self.title = self.data.def.name
        end
        
        if self.hotkeySign then
            self.hotkeySign.visible = data.is_main_list
        end
    else
        self:Clear()
    end
end

function SkillCell:SetCD(progress)
    self.maskObj.fillAmount = 1 - progress / 100
end