MountDuanLianWin = fgui.window_class(WindowBase)

local _id
local _mid1
local _mid2
local _panel
local _mount_new
local _mount_zhu
local _mount_cong

function MountDuanLianWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common" , "坐骑锻炼炉")
end

function MountDuanLianWin:DoInit()
    self:Center()
    
    _panel = self.contentPane
    _mount_new = _panel:GetChild("n4")
    _mount_zhu = _panel:GetChild("n2")
    _mount_cong = _panel:GetChild("n3")
    _mount_zhu.onClick:Add(self.OnClickViewMounts, self)
    _mount_cong.onClick:Add(self.OnClickViewMounts, self)
    _panel:GetChild("n6").onClick:Add(self.OnClickDuanZao, self)
    
    NetClient.Listen(NetKeys.ACMD_BUILD_MOUNTS_RATE, self.OnDuanZaoView, self)
    
    NetClient.ListenRet(NetKeys.QCMD_BUILD_MOUNTS, self.DefaultQCMDHandler, self)
end

function MountDuanLianWin:Open(id)
    _id = id
    self:Show()
end

function MountDuanLianWin:OnShown()
    _mount_zhu:GetController("c1").selectedIndex = 0
    _mount_cong:GetController("c1").selectedIndex = 0
    
    local pd = {}
    pd.id = _id
    _mount_new:GetController("c1").selectedIndex = 1
    _mount_new:GetChild("n1"):SetItem(pd)
    _mount_new.grayed = true
    
    self:Refresh()
end

function MountDuanLianWin:Refresh()
    if self.isShowing then
        local type = self:GetTypeById(_id)
        NetClient.Send(NetKeys.QCMD_DUANLIANLU_VIEW, type)
        self:ShowModalWait()
    end
end

function MountDuanLianWin:OnDuanZaoView(cmd, data)
    self:CloseModalWait()
    
    local rate = data.rate
    if rate == nil then
        rate = ""
    end
    _panel:GetChild("n20").text = data.rate.."%"
    _panel:GetChild("n15").text = data.dzd_num
end

function MountDuanLianWin:OnClickViewMounts(context)
    local type = 0
    if context.sender.name == "n3" then
        type = 1
    end
    G_MountListWin:Open(type, _id)
end

function MountDuanLianWin:ResetSelectedPet(type, data)
    if type == 0 then
        _mount_zhu:GetChild("n1"):SetItem(data)
        _mount_zhu:GetController("c1").selectedIndex = 1
        _mid1 = data.rid
    else
        _mount_cong:GetChild("n1"):SetItem(data)
        _mount_cong:GetController("c1").selectedIndex = 1
        _mid2 = data.rid
    end
end

function MountDuanLianWin:OnClickDuanZao(context)
    local type = self:GetTypeById(_id)
    NetClient.Send(NetKeys.QCMD_BUILD_MOUNTS, type, _mid1, _mid2)
    self:ShowModalWait()
end

function MountDuanLianWin:GetTypeById(id)
    local type = 0
    if id == "m7" then
        type = 1
    elseif id == "m6" then
        type = 2
    elseif id == "m4" then
        type = 3
    elseif id == "m9" then
        type = 4
    end
    
    return type
end
