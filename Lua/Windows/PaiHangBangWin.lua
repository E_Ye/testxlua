PaiHangBangWin = fgui.window_class(WindowBase)
local print_r = require("Test/print_r")

local _typeTab;
local _sortedItemList;
local _paiHangitems

local currentFromIndex = 0
local everyShowCount = 5
local _MAX_Item = 100

function PaiHangBangWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common" , "排行榜")
end

function PaiHangBangWin:DoInit()
    self:Center()

	_typeTab = self.contentPane:GetController("type")
	_typeTab.onChanged:Add(self.Refresh, self)
	_typeTab.selectedIndex=0

	_sortedItemList = self.contentPane:GetChild("n23")
	_sortedItemList:SetVirtual()
	_sortedItemList.itemRenderer = function(index, obj) self.RenderListItem(self,index, obj) end
	_sortedItemList.numItems = 0

	-- _sortedItemList:RemoveChildrenToPool()
	

	NetClient.Listen(NetKeys.ACMD_SORTBOARD, self.OnSortListChaned, self)
end

function PaiHangBangWin:OnShown()
	self:Refresh()
end

function PaiHangBangWin:Refresh()
	if self.isShowing then
		currentFromIndex = _sortedItemList.scrollPane.currentPageY * everyShowCount
		PaiHangBangWin:GetSortedBoard(0, 100)
		self:ShowModalWait(NetKeys.QCMD_SORTBOARD)
	end
end

function PaiHangBangWin:GetSortedBoard(from, count)
	NetClient.Send(NetKeys.QCMD_SORTBOARD, _typeTab.selectedIndex, from,count)
end


function PaiHangBangWin:OnSortListChaned(cmd, data)
	if not self:CloseModalWait(NetKeys.QCMD_SORTBOARD) then return end
	print_r(data)
	_paiHangitems = data.ls
	_sortedItemList.numItems = data.tl	
	_sortedItemList:RefreshVirtualList()
end

function PaiHangBangWin:RenderListItem(index, item)
	-- body
	PaiHangBangWin:ResetSortListItem(index, item);
end


function PaiHangBangWin:ResetSortListItem(index, item)
	if not _paiHangitems then return end

	local target = _paiHangitems[index+1]

	item.data = _paiHangitems[index+1];
	item:GetChild("head").icon = UIPackage.GetItemURL("Sucai", 'u'..target.fct..target.sex)
    item:GetChild("head").text = target.uv --等级
    item:GetChild("name").text = target.name --角色名称
    item:GetChild("add_recovery_percent").text = target.v --排行值
    item:GetChild("head").onClick:Add(self.OnClickItemHead, self)

    local order = item:GetController("order")
    if index ==6 then
    	print(">>>>><<"..target.name)
    end
    if index >= 3 then
    	order.selectedIndex = 3
    	item:GetChild("n8").text = index + 1
    else
    	order.selectedIndex = index
    end
end

--打开查看资料
function PaiHangBangWin:OnClickItemHead( context )
	if context.sender.parent.data.id ~= Player.data.id then
		G_PlayerInfoQueryWin:Open(context.sender.parent.data, 0)
	end
end