MyInfoWin = fgui.window_class(WindowBase)

local _rid

local TAB_TO_INDEX = {1, 3, 4, 5}
local _tab
local _skillListData = {}
local _skillList
local _skillPageTab
local _skillDesc

local _myInfo
local _jingmai
local _zhufu
local _erxx
local _zhufuList
local _equipsCom
local _n231
local _xxinfoPanel

local _zsImage

local _myInfoData

function MyInfoWin:ctor()
	WindowBase.ctor(self)

	self.asyncCreate = true
	self:SetContentSource("Common","角色")
end

function MyInfoWin:DoInit()
	self:Center()
	
	_tab=self.contentPane:GetController("tab")
	_tab.onChanged:Add(self.OnTypeChanged, self)
	
	_myInfo = self.contentPane:GetChild("info")
	_myInfo:GetChild("btnAdd").onClick:Add(self.OnClickLL, self)
	_myInfo:GetChild("btnZhuRu").onClick:Add(self.OnClickZhuRu, self)
	_myInfo:GetChild("btnShuXingDian").onClick:Add(self.OnClickShuXingDian, self)

	_equipsCom = self.contentPane:GetChild("equip")
	for i = 1, 8 do
        local equipCom = _equipsCom:GetChild("c"..(i-1))
        equipCom.onClick:Add(self.OnClickEquip, self)
	end
	
     _zsImage = AniImage.New()
     _zsImage.autoPlay = true
    _equipsCom:GetChild("holder"):SetNativeObject(_zsImage)

	local skillInfoCom = self.contentPane:GetChild("n235")
    _skillList = skillInfoCom:GetChild("list")
    _skillList:SetVirtual()
    _skillList.itemRenderer = function(index, obj) self.RenderListItem(self,index, obj) end
    _skillList.numItems = 48
    _skillList.scrollPane.onScroll:Add(self.OnScroll, self)
    _skillList.onClickItem:Add(self.OnClickCell, self)
    _skillPageTab = skillInfoCom:GetController("skill_page")
    _skillDesc = skillInfoCom:GetChild("n115")
    
    _jingmai = self.contentPane:GetChild("n234")
    for i=0,7 do
        _jingmai:GetChild("b"..i).onClick:Add(self.OnClickJM, self)
    end
    
    _zhufu = self.contentPane:GetChild("n236")
    _zhufuList = _zhufu:GetChild("n182")
    
    _erxx = self.contentPane:GetChild("n237")
	_n231 = _erxx:GetChild("n231")
	_n231:GetChild("n230").onClick:Add(self.OnClickXXJoin, self)
	_xxinfoPanel = _erxx:GetChild("n233")
	_erxx:GetChild("n190").onClick:Add(self.OnClickTuiChu, self)
	_erxx:GetChildInGroup(_xxinfoPanel, "n191").onClick:Add(self.OnClickJieFeng, self)
	_erxx:GetChildInGroup(_xxinfoPanel, "n192").onClick:Add(self.OnClickGongXian, self)
	_erxx:GetChildInGroup(_xxinfoPanel, "n193").onClick:Add(self.OnClickFetchSX, self)
	for i = 1, 3 do
        _erxx:GetChildInGroup(_xxinfoPanel, "n"..(193+i)).onClick:Add(self.OnClickLearnSkill, self)
    end
	
	NetClient.Listen(NetKeys.ACMD_VIEW_USER, self.OnGotMyInfo, self)
	NetClient.Listen(NetKeys.ACMD_XIULIAN_JINGMAI, self.OnXiuLianJingMai, self)
	NetClient.Listen(NetKeys.ACMD_VIEW_ITEM, self.OnGotItem, self)
	NetClient.Listen(NetKeys.ACMD_VIEW_ERXX, self.OnERXXViewReturn, self)
	
	NetClient.ListenRet(NetKeys.QCMD_USE_ITEM, self.DefaultQCMDHandler, self)
	NetClient.ListenRet(NetKeys.QCMD_SET_EN_LINGLI, self.DefaultQCMDHandler, self)
	NetClient.ListenRet(NetKeys.QCMD_XIULIAN_JINGMAI, self.DefaultQCMDHandler, self)
	NetClient.ListenRet(NetKeys.QCMD_ADD_LINGLI, self.DefaultQCMDHandler, self)
	NetClient.ListenRet(NetKeys.QCMD_VIEW_ITEM, self.DefaultQCMDHandler, self)
	NetClient.ListenRet(NetKeys.QCMD_TAKE_OFF_EQUIP2, self.DefaultQCMDHandler, self)
	NetClient.ListenRet(NetKeys.QCMD_APPLY_ERXX, self.DefaultQCMDHandler, self)
	NetClient.ListenRet(NetKeys.QCMD_ALLOW_ERXX, self.DefaultQCMDHandler, self)
	NetClient.ListenRet(NetKeys.QCMD_JF_ERXX, self.DefaultQCMDHandler, self)
	NetClient.ListenRet(NetKeys.QCMD_FETCH_SX, self.DefaultQCMDHandler, self)
	NetClient.ListenRet(NetKeys.QCMD_QUIT_ERXX, self.DefaultQCMDHandler, self)
	NetClient.ListenRet(NetKeys.QCMD_GX_ERXX_EXP, self.DefaultQCMDHandler, self)
end

function MyInfoWin:Open(rid)
    _rid = rid
    self:Show()
end

function MyInfoWin:OnShown()
    self:Refresh()
end

function MyInfoWin:Refresh()
    if self.isShowing then
        if _tab.selectedIndex == 4 then
            self:ErxxView()
        else
            self:ShowModalWait(NetKeys.QCMD_VIEW_USER)
            NetClient.Send(NetKeys.QCMD_VIEW_USER, _rid, TAB_TO_INDEX[_tab.selectedIndex+1])
        end
    end
    
end

function MyInfoWin:OnTypeChanged()
    self:Refresh()
end

function MyInfoWin:OnGotMyInfo(cmd, data)
    if not self:CloseModalWait(NetKeys.QCMD_VIEW_USER) then return end
    
    _myInfoData = data
    
    local type = data.type
    if type == 1 then
        self:ShowUserInfo(_myInfoData)
    elseif type == 2 then
    elseif type == 3 then
        self:ShowSkillInfo(_myInfoData)
    elseif type == 4 then
        self:ResetJM()
    elseif type == 5 then
        self:ResetZhuFu()
    end
end

-- tab 角色
function MyInfoWin:ShowUserInfo(data)
    self:SetUserAttr(data)
    self:SetUserEquip(data)
    self:SetTitle(data)
end

function MyInfoWin:SetUserAttr(data)
    local faction_list = Defs.GetDef("game", "faction_list")
    _myInfo:GetChild("txtName").text = data.name.." "..faction_list[data.faction..""]
    _myInfo:GetChild("hp").max = data.max_hp
    _myInfo:GetChild("hp").value = 0
    _myInfo:GetChild("hp"):TweenValue(data.hp, 0.5)
    _myInfo:GetChild("mp").max = data.max_mp
    _myInfo:GetChild("mp").value = 0
    _myInfo:GetChild("mp"):TweenValue(data.mp, 0.5)
    _myInfo:GetChild("exp").max = data.upgrade_exp
    _myInfo:GetChild("exp").value = 0
    _myInfo:GetChild("exp"):TweenValue(data.exp, 0.5)
    _myInfo:GetChild("t0").text = data.lvl
    _myInfo:GetChild("t5").text = data.pk_num
    _myInfo:GetChild("t1").text = data.clan_name
    _myInfo:GetChild("t6").text = data.rongyu
    _myInfo:GetChild("t2").text = data.attack
    _myInfo:GetChild("t7").text = data.shengwang
    _myInfo:GetChild("t3").text = data.recovery
    _myInfo:GetChild("t8").text = data.kill_npc_count
    _myInfo:GetChild("t4").text = data.xianqi_lvl
    _myInfo:GetChild("t9").text = data.dead_count
    _myInfo:GetChild("lingli").max = data.max_lingli
    _myInfo:GetChild("lingli").value = 0
    _myInfo:GetChild("lingli"):TweenValue(data.lingli, 0.5)
end

function MyInfoWin:SetUserEquip(data)
    local equipsList = data.equips
    if equipsList == nil or #equipsList == 0 then
        return
    end
    
    for i = 1, 8 do
        local equipMap = equipsList[i]
        local equipCom = _equipsCom:GetChild("c"..(i-1))
        equipCom:SetItem(equipMap)
    end
    
    -- 0-中性，1-金，2-木，3-水，4-火，5-土
     _equipsCom:GetController("wuxing").selectedIndex = data.wh==0 and 5 or data.wh-1

    _zsImage:SetSource("user/zs_"..data.faction..data.gender)
    _zsImage:Load()
end

function MyInfoWin:OnClickEquip(context)
    local equip = context.sender.data
    if equip == nil or equip.rid == nil then
        return
    end
    
    NetClient.Send(NetKeys.QCMD_VIEW_ITEM, 0, _rid, equip.rid, "", 1)
    self:ShowModalWait(123456)
end

function MyInfoWin:OnGotItem(cmd, data)
    if self.isShowing then
        if not self:CloseModalWait(123456) then return end
        data._type = 0
        G_ItemInfoWin.data = data
        G_ItemInfoWin:Popup()
    end
end

function MyInfoWin:SetTitle(data)
    local equipsCom = self.contentPane:GetChild("equip")
    equipsCom:GetChild("title_combobox").items = data.titles
end

function MyInfoWin:OnClickLL()
    -- 灵力+
    NetClient.Send(NetKeys.QCMD_ADD_LINGLI)
    self:ShowModalWait()
end

function MyInfoWin:OnClickZhuRu()
    -- 注入
    local ll = _myInfo:GetChild("lingli").value
    if ll == nil or ll <= 0 then
        return
    end
    
    NetClient.Send(NetKeys.QCMD_SET_EN_LINGLI, 1, ll)
    self:ShowModalWait()
end

function MyInfoWin:OnTakeOffEquips(cmd, data)
    if self.isShowing and Player.data.id == data.id then
        local equips = data.equips
        local te = {}
        for _i, _equip in ipairs(equips) do
            te[_equip] = 1
        end
        for i = 1, 8 do
            local equipCom = _equipsCom:GetChild("c"..(i-1))
            if equipCom ~= nil and equipCom.data ~= nil and te[equipCom.data.rid] == nil then
                equipCom:Clear()
            end
        end
    end
end

function MyInfoWin:OnClickShuXingDian(context)
    G_ShuXingDianWin:Popup()
end

-- tab 技能
function MyInfoWin:ShowSkillInfo(data)
    _skillListData = data.skills or {}
    _skillList:RefreshVirtualList()
    
    _skillDesc.visible = false
end

function MyInfoWin:RenderListItem(index, obj)
    obj:SetItem(_skillListData[index+1])
end

function MyInfoWin:OnScroll()
    _skillPageTab.selectedIndex = _skillList.scrollPane.currentPageX
end

function MyInfoWin:OnClickCell(context)
    local item = context.data
    if item.data == nil then
        return
    end
    
    _skillDesc.visible = true
    self:SetSkillDesc(item.data, _skillList:GetChildIndex(item))
end

function MyInfoWin:SetSkillDesc(data, selIndex)
    local skillCell = _skillDesc:GetChild("n0")
    skillCell:SetItem(_skillListData[selIndex+1])
    _skillDesc:GetChild("n24").text = data.def.name
    _skillDesc:GetChild("t4").text = data.def.desc
    _skillDesc:GetChild("t9").text = data.lvl
    _skillDesc:GetChild("t10").text = data.exp
    _skillDesc:GetChild("t0").text = data.mod
    _skillDesc:GetChild("t1").text = skillCell.data.def.point
    _skillDesc:GetChild("t2").text = Defs.GetGameDef("attr_names", skillCell.data.def.deplete_attr, "123")
    _skillDesc:GetChild("t3").text = Defs.GetGameDef("attr_names", skillCell.data.def.deplete_attr, "123")
    _skillDesc:GetChild("t5").text = data.ready_time
    _skillDesc:GetChild("t6").text = skillCell.data.cool_time
    _skillDesc:GetChild("t7").text = skillCell.data.attack_distance
    _skillDesc:GetChild("t8").text = data.hurt_radius
    
    if data.is_main_list then
        _skillDesc:GetChild("n26").visible = true;
    else
        _skillDesc:GetChild("n26").visible = false;
    end
end

-- tab 经脉
function MyInfoWin:ResetJM()
    if _myInfoData.jds ~= nil then
        for i=0,7 do
            self:setJMJD(_jingmai:GetChild("g"..i), _myInfoData.jds[i+1], _jingmai:GetChild("jmd"..i))
        end
    else
        for i=0,7 do
            self:setJMJD(_jingmai:GetChild("g"..i), 0, _jingmai:GetChild("jmd"..i))
        end
    end

    if _myInfoData.cans ~= nil then
        for i=0,7 do
            _jingmai:GetChild("b"..i).visible = _myInfoData.cans[i+1]==1
        end
    else
        for i=0,7 do
            _jingmai:GetChild("b"..i).visible =  false
        end
    end
end

-- 经脉进度
function MyInfoWin:setJMJD(com, value, jmd)
    if value == 100 then
        com:GetController("c1").selectedIndex = 1
        com:GetChild("n1").value = value
        jmd:GetController("c1").selectedIndex = 1
    else
        com:GetController("c1").selectedIndex = 0
        com:GetChild("n0").value = value
        jmd:GetController("c1").selectedIndex = 0
    end
end

function MyInfoWin:OnClickJM(context)
    -- 经脉 点击+号
    self:ShowModalWait()
    NetClient.Send(NetKeys.QCMD_XIULIAN_JINGMAI, "")
end

function MyInfoWin:OnXiuLianJingMai(cmd, data)
    self:CloseModalWait()
    if data == nil then
        return
    end
    
    _myInfoData.jds = data.jds
    _myInfoData.cans = data.cans
    self:ResetJM()
    AlertWin.Open(data.msg)
end

-- tab 祝福
function MyInfoWin:ResetZhuFu()
    _zhufuList:RemoveChildrenToPool()
    if _myInfoData == nil or _myInfoData.bls == nil then
        return
    end
    
    for _i,zhufu in ipairs(_myInfoData.bls) do
        local btn = _zhufuList:AddItemFromPool()
        
        local def = Defs.GetDef("item", zhufu.id)
        btn:GetChild("title").text = def.name
        btn:GetChild("n6").text = zhufu.desc
        btn:GetChild("icon").url = zhufu.id
        btn:GetChild("n8").onClick:Add(self.ZhuFuUseClick, self)
        btn:GetChild("n8").data = zhufu
        if zhufu.desc == nil then
            btn.grayed = true
            btn:GetChild("n6").text = def.desc
        else
            btn.grayed = false
        end

        btn:GetController("c1").selectedIndex = zhufu.action == nil and 0 or zhufu.action
    end 
end

function MyInfoWin:ZhuFuUseClick(context)
    local data = context.sender.data
    if data == nil then
        return
    end
    
    if data.action == 1 then
        self:ShowModalWait()
        NetClient.Send(NetKeys.QCMD_USE_ITEM, 0, _rid, data.id, 0)
    elseif data.action == 2 then
        -- 打开商城购买
        local def = Defs.GetDef("item", data.id)
        AlertWin.Open(def.name..Defs.NO_ITEM_BUY_TIPS, self.OpenMall)
    end
end

function MyInfoWin.OpenMall()
    G_MallWin:Show()
end

-- 修仙
function MyInfoWin:ErxxView()
    NetClient.Send(NetKeys.QCMD_VIEW_ERXX, _rid)
    self:ShowModalWait()
end

function MyInfoWin:OnERXXViewReturn(cmd, data)
    self:CloseModalWait()
    if data.id == nil then
        self:ResetXXMenInfo(data, data.uid == Player.data.id)
        if data.members ~= nil and #data.members == 1 then
            self:ResetXXMenInfo2(data.members[1], 1)
        else
            self:ResetXXMenInfo2(data, 0)
        end
    else
        self:ResetXXMenInfo(data.members[1], data.uid == Player.data.id)
        self:ResetXXMenInfo2(data.members[2], 2)
        local member = data.members[1]
        if member.id ~= Player.data.id then
            member = data.members[2]
        end
        self:ResetXXInfo(data, member)
    end
end

function MyInfoWin:ResetXXMenInfo(data, isSelf)
    local n230 = _erxx:GetChild("n230")
    n230:GetChild("n220").icon = UIPackage.GetItemURL("Sucai", 'u'..data.faction..data.gender)
    n230:GetChild("n220").title = data.lvl
    n230:GetChild("n222").text = data.name
    n230:GetChildInGroup(n230:GetChild("n231"), "n224").text = data.gx
    n230:GetChildInGroup(n230:GetChild("n231"), "n226").text = data.fetch_sx
    n230:GetController("c1").selectedIndex = 0
    
    _erxx:GetChild("n190").visible = data.id ~= nil and isSelf and true or false
end

function MyInfoWin:ResetXXMenInfo2(data, flag)
    -- flag 0- 没有修仙 1- 有申请修仙 2- 已在修仙
    if flag == 0 then
        if data.uid ~= Player.data.id then
            _n231:GetController("c1").selectedIndex = 1
            _n231:GetChild("n220").icon = UIPackage.GetItemURL("Sucai", 'u'..Player.data.fct..Player.data.sex)
            _n231:GetChild("n220").title = Player.data.lvl
            _n231:GetChild("n222").text = Player.data.name
            _n231:GetChild("n230").data = Player.data.id
        else
            _n231:GetController("c1").selectedIndex = 2
        end
        _n231:GetController("c2").selectedIndex = 0
        _xxinfoPanel.visible = false
    elseif flag == 1 then
        _n231:GetController("c1").selectedIndex = 1
        _n231:GetController("c2").selectedIndex = data.id ~= Player.data.id and 1 or 0
        _n231:GetChild("n220").icon = UIPackage.GetItemURL("Sucai", 'u'..data.faction..data.gender)
        _n231:GetChild("n220").title = data.lvl
        _n231:GetChild("n222").text = data.name
        _n231:GetChild("n230").data = data.id
        _xxinfoPanel.visible = false
    else
        _n231:GetController("c1").selectedIndex = 0
        _xxinfoPanel.visible = true
        _n231:GetChild("n220").icon = UIPackage.GetItemURL("Sucai", 'u'..data.faction..data.gender)
        _n231:GetChild("n220").title = data.lvl
        _n231:GetChild("n222").text = data.name
        _n231:GetChildInGroup(_n231:GetChild("n231"), "n224").text = data.gx
        _n231:GetChildInGroup(_n231:GetChild("n231"), "n226").text = data.fetch_sx
    end
end

function MyInfoWin:ResetXXInfo(data, member)
    _erxx:GetChildInGroup(_xxinfoPanel, "n206").text = data.lvl.."/"..data.jf_lvl
    _erxx:GetChildInGroup(_xxinfoPanel, "n197").value = data.exp
    _erxx:GetChildInGroup(_xxinfoPanel, "n197").max = data.upgrade_exp
    _erxx:GetChildInGroup(_xxinfoPanel, "n197").text = member.sx
    local skills = member.skills
    for i = 1, 3 do
        local sd = {}
        sd.rid = skills[i].id
        _erxx:GetChildInGroup(_xxinfoPanel, "n"..(197+i)).SetItem(sd)
        _erxx:GetChildInGroup(_xxinfoPanel, "n"..(193+i)).visible = skills[i].status == 0 and true or false
        _erxx:GetChildInGroup(_xxinfoPanel, "n"..(193+i)).data = skills[i].id
        _erxx:GetChildInGroup(_xxinfoPanel, "n"..(210+i)).text = string.format(_erxx:GetChild("n"..(210+i)).text, skills[i].lvl)
    end
end

function MyInfoWin:OnClickXXJoin(context)
    if _n231:GetController("c2").selectedIndex == 0 then
        NetClient.Send(NetKeys.QCMD_APPLY_ERXX, _rid)
        self:ShowModalWait()
    else
        NetClient.Send(NetKeys.QCMD_ALLOW_ERXX, context.sender.id)
        self:ShowModalWait()
    end
end

function MyInfoWin:OnClickJieFeng(context)
    NetClient.Send(NetKeys.QCMD_JF_ERXX)
    self:ShowModalWait()
end

function MyInfoWin:OnClickGongXian(context)
    G_GongXianWin:InternalOpen("", Defs.GONGXIAN_EXP_TITLE, nil, MyInfoWin.OnClickGongXianOK)
end

function MyInfoWin.OnClickGongXianOK(exp)
    NetClient.Send(NetKeys.QCMD_GX_ERXX_EXP, exp)
    G_MyInfoWin:ShowModalWait()
end

function MyInfoWin:OnClickFetchSX(context)
    NetClient.Send(NetKeys.QCMD_FETCH_SX)
    self:ShowModalWait()
end

function MyInfoWin:OnClickLearnSkill(context)
    NetClient.Send(NetKeys.QCMD_ERXX_LEARN_SKILL, context.sender.data)
    self:ShowModalWait()
end

function MyInfoWin:OnClickTuiChu(context)
    ConfirmWin.Open(Defs.GONGXIAN_CONFIRM, function()
        NetClient.Send(NetKeys.QCMD_QUIT_ERXX)
        self:ShowModalWait()
    end)
end