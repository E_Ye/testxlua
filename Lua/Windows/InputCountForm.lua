InputCountForm = fgui.extension_class(GLabel)

function InputCountForm:ctor()
    self.step = 1
    self.max = 10000000000
    self.min = 0
    self.value = ""
    self._incFun = 0
    self._decFun = 0
    self._selfObj = nil
    self:GetChild("btnInc").onClick:Add(self.OnClickInc, self)
    self:GetChild("btnDec").onClick:Add(self.OnClickDec, self)
end

function InputCountForm:OnClickInc()
    local value = tonumber(self.text)
    if value == nil then
        value = 0
    end
    value = value+self.step
    if self.max ~= nil and value > self.max then
       value = self.max
    end
    self.text = value
    
    if self._incFun ~= 0 then
        self._incFun(self._selfObj)
    end
end

function InputCountForm:OnClickDec()
    local value = tonumber(self.text)
    if value == nil then
        value = 0
    end
    value = value-self.step
    if self.min ~= nil and value < self.min then
       value = self.min
    end
    self.text = value
    
    if self._decFun ~= 0 then
        self._decFun(self._selfObj)
    end
end

function InputCountForm:SetIncAndDecCallBack(incFun, decFun, selfObj)
    self._incFun = incFun
    self._decFun = decFun
    self._selfObj = selfObj
end

function InputCountForm:SetStep(value)
    if value == nil then
        self.step = 1
    else
        self.step = value
    end
end

function InputCountForm:SetRegionAndStep(step, min, max)
    self.step = step
    self.min = min
    self.max = max
end

function InputCountForm:ResetMax()
    self.value = self.max
end

local getter = tolua.initget(InputCountForm)
local setter = tolua.initset(InputCountForm)
getter.value = function(self)
    return tonumber(self.text)
end

setter.value = function(self, value)
    self.text = value
end