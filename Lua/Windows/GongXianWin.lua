GongXianWin = fgui.window_class(WindowBase)

function GongXianWin:ctor()
    WindowBase.ctor(self)
    
    self:SetContentSource("Common" , "赠送贡献确认界面")
    self:Init()
end

function GongXianWin:InternalOpen(msg, title, leftCallback, rightCallback)
    self:Show()
    self.contentPane:GetChild("msg").text = msg
    self.contentPane:GetChild("title").text = title
    self.vars.leftCallback = leftCallback
    self.vars.rightCallback = rightCallback
end

function GongXianWin:DoInit()
    self.contentPane:GetChild("ok").onClick:Add(self.OnConfirmBtnClick, self)
    self.contentPane:GetChild("cancel").onClick:Add(self.OnConfirmBtnClick, self)
    self.modal = true
    self.sortingOrder = SortingOrder.Modal
    self:Center()
end

function GongXianWin:OnShown()
    if self.vars.alertSound == nil then
        self.vars.alertSound = UIPackage.GetItemAsset("Sucai", "alert")
    end
    GRoot.inst:PlayOneShotSound(self.vars.alertSound)
end

function GongXianWin:OnConfirmBtnClick(context)
   	self:Hide()
   	local leftCallback = self.vars.leftCallback
   	local rightCallback = self.vars.rightCallback
   	self.vars.leftCallback = nil
   	self.vars.rightCallback = nil
   	if context.sender.name=='ok' then
   		if rightCallback~=nil then rightCallback(tonumber(self.contentPane:GetChild("msg").text)) end
   	else
   		if leftCallback~=nil then leftCallback() end
   	end
end
