HangOnItemInfoWin = fgui.window_class(WindowBase)

local _zb
local _data
local c1
local c2
local _value
local _uid

function HangOnItemInfoWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common" , "挂售物品信息")
end

function HangOnItemInfoWin:DoInit()
    self:Center()
    
    _zb = self.contentPane:GetChild("n44")
    c1=_zb:GetController("c1")
    _zb:GetChild("n38").onClick:Add(self.OnClickOK, self)
    _zb:GetChild("n39").onClick:Add(self.OnClickOff, self)
    _zb:GetChild("n47").onClick:Add(self.OnClickBuy, self)
    _zb:GetChild("input"):SetIncAndDecCallBack(self.OnClickInc, self.OnClickInc, self)
    
    c2=_zb:GetController("c2")
    
    NetClient.ListenRet(NetKeys.QCMD_HANG_ON_ITEM, self.OnHangOnItemReturn, self)
    NetClient.ListenRet(NetKeys.QCMD_HANG_OFF_ITEM, self.OnHangOffItemReturn, self)
    NetClient.ListenRet(NetKeys.QCMD_BUY_ITEM_FROM_USER, self.OnBuyItemReturn, self)
end

function HangOnItemInfoWin:OnClickInc()
    local n = tonumber(_zb:GetChild("input").value)
    local value = tonumber(_zb:GetChildInGroup(_zb:GetChild("n45"), "n44").text)
    _zb:GetChild("money").text = n*value
end

function HangOnItemInfoWin:Popup2(uid, data)
    _data = data
    _uid = uid
    self:Popup()
end

function HangOnItemInfoWin:OnShown()
    _zb:GetChild("n1"):SetItem(_data)
    _zb:GetChild("n2").text = _data.def.name
    local type = _data.def.type
    if string.startswith(type, "yp")  then
        c1.selectedIndex = 0
    elseif string.startswith(type, "bq")  then
        c1.selectedIndex = 1
    elseif string.startswith(type, "fj")  then
        c1.selectedIndex = 2
    elseif string.startswith(type, "bw")  then
        c1.selectedIndex = 0
    elseif string.startswith(type, "qc")  then
        c1.selectedIndex = 0
    elseif string.startswith(type, "qt")  then
        c1.selectedIndex = 0
    end  
    
    _zb:GetChildInGroup(_zb:GetChild("n45"), "n44").text = _data.price
    
    _zb:GetChild("input"):SetRegionAndStep(1, 0, _data.count)
    _zb:GetChild("input"):ResetMax()
    self:OnClickInc()
    c2.selectedIndex = _uid == Player.data.id and 0 or 1
    
    _zb:GetChild("n50").text = _data.def.desc
end

function HangOnItemInfoWin:OnClickOK(context)
    local value = tonumber(_zb:GetChild("n41").text)
    if value <= 0 then return end
    
    _value = value
    
    NetClient.Send(NetKeys.QCMD_HANG_ON_ITEM, _data.rid, value, 0)
    self:ShowModalWait()
end

function HangOnItemInfoWin:OnClickOff(context)
    NetClient.Send(NetKeys.QCMD_HANG_OFF_ITEM, _data.rid)
    self:ShowModalWait()
end

function HangOnItemInfoWin:OnClickBuy(context)
    NetClient.Send(NetKeys.QCMD_BUY_ITEM_FROM_USER, _uid, _data.rid, _zb:GetChild("input").value, tonumber(_zb:GetChildInGroup(_zb:GetChild("n45"), "n44").text))
    self:ShowModalWait()
end

function HangOnItemInfoWin:OnHangOnItemReturn(cmd, data)
    if self.isShowing then
        self:CloseModalWait()
        if data.code~=0 then
            AlertWin.Open(data.msg)
        else
            _zb:GetChildInGroup(_zb:GetChild("n45"), "n44").text = _value
        end
        self:Hide()
    end
end

function HangOnItemInfoWin:OnHangOffItemReturn(cmd, data)
    if self.isShowing then
        self:CloseModalWait()
        if data.code~=0 then
            AlertWin.Open(data.msg)
        else
            self:Hide()
            G_HangOnBagWin:Refresh()
        end
    end
end

function HangOnItemInfoWin:OnBuyItemReturn(cmd, data)
    if self.isShowing then
        self:CloseModalWait()
        if data.code~=0 then
            AlertWin.Open(data.msg)
        else
            G_HangOnBagWin:Refresh()
        end
        
        self:Hide()
    end
end