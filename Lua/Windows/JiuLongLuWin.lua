JiuLongLuWin = fgui.window_class(WindowBase)

local _id
local _pid1
local _pid2
local _panel
local _pet_hecheng
local _pet_zhu
local _pet_cong

function JiuLongLuWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common" , "九龙炉")
end

function JiuLongLuWin:DoInit()
    self:Center()
    
    _panel = self.contentPane
    _pet_hecheng = _panel:GetChild("n21")
    _pet_zhu = _panel:GetChild("n22")
    _pet_cong = _panel:GetChild("n23")
    _pet_zhu.onClick:Add(self.OnClickViewPets, self)
    _pet_cong.onClick:Add(self.OnClickViewPets, self)
    _panel:GetChild("n6").onClick:Add(self.OnClickHeCheng, self)
    
    NetClient.Listen(NetKeys.ACMD_HECHENG_PET_VIEW, self.OnHeChengView, self)
    NetClient.Listen(NetKeys.ACMD_HECHENG_PET, self.OnHeChengView, self)
    
    NetClient.ListenRet(NetKeys.QCMD_HECHENG_PET, self.DefaultQCMDHandler, self)
end

function JiuLongLuWin:Open(id)
    _id = id
    self:Show()
end

function JiuLongLuWin:OnShown()
    _pet_zhu:GetController("c1").selectedIndex = 0
    _pet_cong:GetController("c1").selectedIndex = 0
    
    local pd = {}
    pd.id = _id
    _pet_hecheng:GetController("c1").selectedIndex = 1
    _pet_hecheng:GetChild("n1"):SetItem(pd)
    _pet_hecheng.grayed = true
    self:Refresh()
end

function JiuLongLuWin:Refresh()
    if self.isShowing then
        NetClient.Send(NetKeys.QCMD_HECHENG_PET_VIEW, _id)
        self:ShowModalWait()
    end
end

function JiuLongLuWin:OnHeChengView(cmd, data)
    self:CloseModalWait()
    
    _panel:GetChild("n20").text = data.score
    _panel:GetChild("n15").text = string.formatWithWan(data.exp)
    _panel:GetChild("n16").text = data.money
    if data.fetch_rid ~= nil then
        -- 要做些什么呢 TODO
    end
end

function JiuLongLuWin:OnClickViewPets(context)
    local type = 0
    if context.sender.name == "n23" then
        type = 1
    end
    G_PetListWin:Open(type, _id)
end

function JiuLongLuWin:ResetSelectedPet(type, data)
    if type == 0 then
        _pet_zhu:GetChild("n1"):SetItem(data)
        _pet_zhu:GetController("c1").selectedIndex = 1
        _pid1 = data.rid
    else
        _pet_cong:GetChild("n1"):SetItem(data)
        _pet_cong:GetController("c1").selectedIndex = 1
        _pid2 = data.rid
    end
end

function JiuLongLuWin:OnClickHeCheng(context)
    NetClient.Send(NetKeys.QCMD_HECHENG_PET, _pid1, _pid2)
    self:ShowModalWait()
end
