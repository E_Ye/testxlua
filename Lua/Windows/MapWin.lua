MapWin = fgui.window_class(WindowBase)

local _list
local _npcListData
local _exitListData
local _userListData
local _finalList = {}
local _c1
local _mapContainer
local _indicatorPool
local _playerIndicator
local _mapScale

function MapWin:ctor()
	WindowBase.ctor(self)

	self:SetContentSource("Common" , "地图")
end

function MapWin:DoInit()
	self:Center()

	_c1 = self.contentPane:GetController("map_type")
	_c1.onChanged:Add(self.OnMapTypeChanged, self)

	_list = self.contentPane:GetChild("list")
	_list.itemRenderer = function(index, item) self:RenderListItem(index, item) end
	_list.onClickItem:Add(self.OnClickItem, self)
	_list:SetVirtual()

	self.contentPane:GetChild("cbNPC").onChanged:Add(self.OnOptionChaned, self)
	self.contentPane:GetChild("cbUser").onChanged:Add(self.OnOptionChaned, self)
	self.contentPane:GetChild("cbExit").onChanged:Add(self.OnOptionChaned, self)
	self.contentPane:GetChild("cbDan").onChanged:Add(self.OnOptionChaned, self)
	self.contentPane:GetChild("cbDuiHuan").onChanged:Add(self.OnOptionChaned, self)
	self.contentPane:GetChild("cbBoss").onChanged:Add(self.OnOptionChaned, self)
	self.contentPane:GetChild("cbMP").onChanged:Add(self.OnOptionChaned, self)

	for i=0,10 do
		local obj = self.contentPane:GetChild("w"..i)
		if obj then
			obj.onClick:Add(self.OnClickFly, self)
		end
	end

	_mapContainer = self.contentPane:GetChild("map")
	_mapContainer.onClick:Add(self.OnClickMap, self)

	_indicatorPool = GObjectPool.New(self.displayObject.cachedTransform)

	NetClient.ListenRet(NetKeys.QCMD_FLY, self.OnFlyResult, self)
end

function MapWin:OnNewScene()
	if self.contentPane==nil then return end

	_npcListData = nil
	if self.isShowing then
		self:FillListData()
	end
end

function MapWin:ReadyToUpdate()
	if _npcListData==nil then
		self:FillListData()
	end
end

function MapWin:OnHide()
end

------ private function ---------
function MapWin:RenderListItem(index, btn)
	local data = _finalList[index+1]
	btn.text = data[2]
	btn.name = data[1]
end

local function CompareItem(a,b)
	if a[2]<b[2] then return true else return false end
end

function MapWin:FillListData()
	self.frame.text = World.sceneData.name
	_mapContainer:GetChildAt(0).texture = World.thumbMap
	_mapScale = _mapContainer.width/World.mainEnv.pathInfo.mapWidth

	local cnt = _mapContainer.numChildren
	for i=1,cnt-1 do
		_indicatorPool:ReturnObject(_mapContainer:GetChildAt(i))
	end
	_mapContainer:RemoveChildren(1, -1, false)

	_npcListData = {}
	_exitListData = {}
	_userListData = {}
	local indType

	for rid,thing in pairs(World.things) do
		indType = nil
		if thing.thingType==ThingType.NPC then
			if not thing.data.kill then
				local str = thing.data.name
				if not string.isNilOrEmpty(str) then
					table.insert(_npcListData, {thing.name, str })
				end

				indType = "ui://Sucai/小地图光标1"
			end
		elseif thing.thingType==ThingType.User then
			table.insert(_userListData, {thing.name, thing.data.name })

			if thing.data.self then
				indType = "ui://Sucai/小地图光标2"
			else
				indType = "ui://Sucai/小地图光标3"
			end
		elseif thing.thingType==ThingType.Exit then
			table.insert(_exitListData, {thing.name, thing.data.name})

			--indType = "ui://Sucai/小地图光标5"
		end

		if indType then
			local ind = _indicatorPool:GetObject(indType)
			ind.name = thing.name
			ind:SetXY(thing.x*_mapScale, thing.y*_mapScale)
			_mapContainer:AddChild(ind)

			if thing.data.self then
				_playerIndicator = ind
			end
		end
	end

	table.sort(_npcListData, CompareItem)
	table.sort(_exitListData, CompareItem)
	table.sort(_userListData, CompareItem)

	self:RefreshList()
end

function MapWin:RefreshList()
	local k = 0
	if _c1.selectedIndex==0 then --local
		if self.contentPane:GetChild("cbNPC").selected then
			for i,m in ipairs(_npcListData) do
				k = k+1
				_finalList[k] = m
			end
		end

		if self.contentPane:GetChild("cbUser").selected then
			for i,m in ipairs(_userListData) do
				k = k+1
				_finalList[k] = m				
			end
		end

		if self.contentPane:GetChild("cbExit").selected then
			for i,m in ipairs(_exitListData) do
				k = k+1
				_finalList[k] = m				
			end
		end
	else --world
	end

	_list.numItems = k
end

function MapWin:NotifyMapMoved()
	if not self.isShowing or _playerIndicator==nil or World.player==nil then return end
	_playerIndicator:SetXY(World.player.x*_mapScale, World.player.y*_mapScale)
end

function MapWin:OnMapTypeChanged()
	self:RefreshList()
end

function MapWin:OnOptionChaned()
	self:RefreshList()
end

function MapWin:OnClickItem(context)
	local thing = World.things[context.data.name]
	if thing==nil then return end

	self:Hide()
	Player.ClickObject(thing)
end

function MapWin:OnClickMap(context)
	local pt = _mapContainer:GlobalToLocal(context.inputEvent.position)
	pt.x = pt.x/_mapScale
	pt.y = pt.y/_mapScale
	pt = World.mainEnv.pathInfo:GetPathPosition(pt)
	Player.ClickPoint(pt, true)
end

function MapWin:OnClickFly(context)
	local index = tonumber(string.sub(context.sender.name, 2))
	GRoot.inst:ShowModalWait()
	NetClient.Send(NetKeys.QCMD_FLY, index)
end

function MapWin:OnFlyResult(cmd, data)
	if data.code~=0 then
		GRoot.inst:CloseModalWait()
		AlertWin.Open(data.msg)
	end
end
