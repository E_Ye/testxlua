GotItemAlertWin = fgui.window_class(WindowBase)

local _data

function GotItemAlertWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common" , "物品掉落弹窗")
end

function GotItemAlertWin:PopupOpen(data)
    _data = data
    self:Popup()
end

function GotItemAlertWin:DoInit()
    self:Center()
end

function GotItemAlertWin:OnShown()
    if self.vars.alertSound == '' then
        self.vars.alertSound = UIPackage.GetItemAsset("Sucai", "alert")
    end
    GRoot.inst:PlayOneShotSound(self.vars.alertSound)
    
    self.contentPane:GetChild("n1"):SetItem(_data)
    self.contentPane:GetChild("n5").text = _data.def.name
end