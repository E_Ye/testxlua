ShuXingDianWin = fgui.window_class(WindowBase)

local _panel
local _data

local _maxCom
local _step = 1

local attr_def = {"ll", "tz", "wx", "fy", "fh"}

function ShuXingDianWin:ctor()
    WindowBase.ctor(self)

    self:SetContentSource("Common" , "角色_属性点加点")
end

function ShuXingDianWin:DoInit()
    self:Center()
    
    _panel = self.contentPane
    _panel:GetChild("btnSave").onClick:Add(self.OnClickSave, self)
    _panel:GetChild("btnDismiss").onClick:Add(self.OnClickDismiss, self)
    
    _maxCom = _panel:GetChild("n6")
    
    for i=1, 5 do
        local b = _panel:GetChild("b"..i)
        b:GetChild("btnInc").onClick:Add(self.OnClickInc, self)
        b:GetChild("btnDec").onClick:Add(self.OnClickDec, self)
    end
    
    NetClient.Listen(NetKeys.ACMD_VIEW_USER, self.OnGotMyInfo, self)
    
    NetClient.ListenRet(NetKeys.QCMD_ADD_ATTRS, self.DefaultQCMDHandler, self)
end

function ShuXingDianWin:OnShown()
    self:Refresh()
end

function ShuXingDianWin:Refresh()
    self:ShowModalWait()
    NetClient.Send(NetKeys.QCMD_VIEW_USER, "", 2)
end

function ShuXingDianWin:OnGotMyInfo(cmd, data)
    self:CloseModalWait()
    
    local type = data.type
    if type == 2 then
        self:ResetInfo(data)
    end
end

function ShuXingDianWin:ResetInfo(data)
    if data == nil then
        return
    end
    _data = data
    _panel:GetChild("b1"):GetChild("n24").text = data.ll == nil and "" or data.ll.."<font color='#06ebff'>(+"..data.ll2..")</font>"
    _panel:GetChild("b2"):GetChild("n24").text = data.tz == nil and "" or data.tz.."<font color='#06ebff'>(+"..data.tz2..")</font>"
    _panel:GetChild("b3"):GetChild("n24").text = data.wx == nil and "" or data.wx.."<font color='#06ebff'>(+"..data.wx2..")</font>"
    _panel:GetChild("b4"):GetChild("n24").text = data.fy == nil and "" or data.fy.."<font color='#06ebff'>(+"..data.fy2..")</font>"
    _panel:GetChild("b5"):GetChild("n24").text = data.fh == nil and "" or data.fh.."<font color='#06ebff'>(+"..data.fh2..")</font>"
    
    for _i=1, 5 do
        _panel:GetChild("b".._i):GetChild("title").text = ""
    end
    
    _panel:GetChild("n6").text = data.sx
end

function ShuXingDianWin:OnClickSave(context)
    local msg = {}
    msg.type = 1
    msg.rid = _data.id
    local attrs = {}
    for _i = 1, 5 do
        attrs[_i.."_"..attr_def[_i]] = tonumber(_panel:GetChild("b".._i):GetChild("title").text)
    end
    msg.attrs = attrs
    NetClient.Send(NetKeys.QCMD_ADD_ATTRS, msg)
    self:ShowModalWait()
end

function ShuXingDianWin:OnClickDismiss(context)
    for _i = 1, 5 do
        _panel:GetChild("b".._i):GetChild("title").text = ""
    end    
    _panel:GetChild("n6").text = _data.sx
end

function ShuXingDianWin:OnClickInc(context)
    local sx = context.sender.parent:GetChild("title")
    local mvalue = tonumber(_maxCom.text)
    if mvalue == nil or mvalue-_step < 0 then
       return
    end
    
    local value = tonumber(sx.text)
    if value == nil then
        value = 0
    end
    value = value+_step
    sx.text = value
    _maxCom.text = mvalue-_step
end

function ShuXingDianWin:OnClickDec(context)
    local sx = context.sender.parent:GetChild("title")
    local mvalue = tonumber(_maxCom.text)
    if mvalue == nil or mvalue+_step > _data.sx then
       return
    end
    
    local value = tonumber(sx.text)
    if value == nil then
        return
    end
    value = value-_step
    if value < 0 then
       return
    end
    if value > 0 then
        sx.text = value
    else
        sx.text = ""
    end
    _maxCom.text = mvalue+_step
end
