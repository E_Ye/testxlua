BuyWin = fgui.window_class(WindowBase)

function BuyWin:ctor()
	WindowBase.ctor(self)

	self.animation = {"move_right", "move_left"}
	self:SetContentSource("Common","购买")
end

function BuyWin:DoInit()
	self:SetXY(50, 50)

	self.inputForm = self.contentPane:GetChild("input")

	self.c1 = self.contentPane:GetController("c1")
	self.cell = self.contentPane:GetChild("cell")
	self.contentPane:GetChild("btnOk").onClick:Add(self.OnClickOk, self)
	
	self.inputForm:SetIncAndDecCallBack(self.OnClickInc, self.OnClickInc, self)
end

function BuyWin:OnClickInc()
    local n = tonumber(self.inputForm.value)
    local value = tonumber(self.contentPane:GetChildInGroup(self.contentPane:GetChild("n21"), "sell").text)
    self.contentPane:GetChild("money").text = n*value
end

function BuyWin:Open(caller)
	self.caller = caller
	self:Popup()

	self.c1.selectedIndex = 0
	self.cell:SetItem(nil)
end

function BuyWin:OnShown()
	self.inputForm.text = "1"
	self:OnClickInc()
end

function BuyWin:OnClickOk()
	if self.cell.data~=nil then
		self.caller.DoBuy(self.caller, self.cell.data.rid, self.inputForm.value)
		self:Hide()
	end
end

function BuyWin:SetItem(itemData)
	self.cell:SetItem(itemData)
	self.c1.selectedIndex = 1
	self.contentPane:GetChild("itemName").text = itemData.def.name
	self.contentPane:GetChild("sell").text = itemData.def.value
end
