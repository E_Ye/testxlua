NPCInfoWin = fgui.window_class(WindowBase)

local _icon
local _content
local _descText
local _nameText
local _linkList

function NPCInfoWin:ctor()
    WindowBase.ctor(self)

    self:SetContentSource("Common" , "NPCInfoWin")
end

function NPCInfoWin:DoInit()
    self:SetXY(50, 50)

    _icon = self.contentPane:GetChild("icon")
    _descText = self.contentPane:GetChild("desc")
    _linkList = self.contentPane:GetChild("actions")
    _linkList.onClickItem:Add(self.OnClickItem, self)
    _nameText = self.contentPane:GetChild("name")

    NetClient.Listen(NetKeys.ACMD_VIEW_NPC, self.OnServer_ViewNPC, self)
   -- NetClient.ListenRet(NetKeys.QCMD_TOUCH_NPC, self.OnServer_TouchNPCResult, self)
end

function NPCInfoWin:OnServer_ViewNPC(cmd, data)
    _content = data
    local nid = GetIdFromRid(_content.rid)
    local def = Defs.GetDef("robot", nid)

    local nameStr = {}
    if def.title~=nil then
        table.insert(nameStr, def.title)
    end
    if _content.name~=nil then
        table.insert(nameStr, _content.name)
    elseif def.name~=nil then
        table.insert(nameStr, def.name)
    end

    _nameText.text = table.concat(nameStr, '')
    _icon.icon = Defs.GetIconSrc(nid)
    
    local msgStr = {}
    if _content.desc~=nil then
        table.insert(msgStr, EncodeHTML(_content.desc))
    elseif def.desc~=nil then
        table.insert(msgStr, def.desc)
    end
    if _content.can_adopt then
        table.insert(msgStr,  '(对其使用勾魂术，成功可收养为宠物)')
    end
    _descText.text = table.concat(msgStr, '\n')

     _linkList:RemoveChildrenToPool()

    if _content.tasks~=nil then
        for i,v in ipairs(_content.tasks) do
            local item = _linkList:AddItemFromPool()
            item.title = v.name
            item.name = 'task'
            item.data = v
        end
    end
    
    if _content.sale_items then
        local item = _linkList:AddItemFromPool()
        item.title = '购买物品'
        item.name = 'buy'
    end
            
    if _content.hockshop then
        local item = _linkList:AddItemFromPool()
        item.title = '收购物品'
        item.name = 'sell'
    end
    
    if _content.can_repair_weapon then
        local item = _linkList:AddItemFromPool()
        item.title = '修理兵器'
        item.name = 'repair'

        item = _linkList:AddItemFromPool()
        item.title = '打造兵器'
        item.name = 'build'
    end
    
    if _content.can_repair_armor then
        local item = _linkList:AddItemFromPool()
        item.title = '修理防具'
        item.name = 'repair'

        item = _linkList:AddItemFromPool()
        item.title = '打造防具'
        item.name = 'build'
    end
    
    if _content.ops~=nil then
        for i,v in ipairs(_content.ops) do
             local item = _linkList:AddItemFromPool()
            item.title = v.name
            item.name = 'op'
            item.data = v
        end
    end
    
    _linkList:ResizeToFit(100000)
    
    self:Popup()
end

function NPCInfoWin:OnServer_TouchNPCResult(cmd, data)
end

function NPCInfoWin:OnShown()
end

function NPCInfoWin:OnHide()
end

function NPCInfoWin:OnClickItem(context)
    local link = context.data
    local cmd = link.name
    if cmd=='task' then
        G_TaskWin:Open(link.data.id)
        self:Hide()
    elseif cmd=="buy" then
    	G_NPCShopWin:Open(_content.rid, _content.sale_items)
    	self:Hide()
    elseif cmd=="op" then
        local op = link.data
        if op.confirm~=nil and string.len(op.confirm)>0 then
            ConfirmWin.Open(op.confirm, function()
                 NetClient.Send(NetKeys.QCMD_TOUCH_NPC, _content.rid, op.id)
            end)
        else                                                                        
            NetClient.Send(NetKeys.QCMD_TOUCH_NPC, _content.rid, op.id)
        end
    end
end
