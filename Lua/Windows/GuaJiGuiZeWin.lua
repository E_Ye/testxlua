GuaJiGuiZeWin = fgui.window_class(WindowBase)

function GuaJiGuiZeWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common","挂机规则弹出框")
end

function GuaJiGuiZeWin:DoInit()
    self:Center()
end

function GuaJiGuiZeWin:OnShown()
end

function GuaJiGuiZeWin:OnHide()
end
