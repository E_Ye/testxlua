LianYaoHuPetListWin = fgui.window_class(WindowBase)

local _listData = {} -- 列表内容
local _typeTab = nil
local _list
local _type
local _pid

function LianYaoHuPetListWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = false
    self:SetContentSource("Common","技能物品选择列表")
end

function LianYaoHuPetListWin:DoInit()
    self:Center()
    
    _typeTab=self.contentPane:GetController("c1")
    _list=self.contentPane:GetChild("list")
    local p = self
    _list.itemRenderer = function(index, obj) p:RenderItem(index, obj) end
    _list:SetVirtual()
    _list.onClickItem:Add(self.OnClickPet, self)
    
    NetClient.Listen(NetKeys.ACMD_MY_PETS, self.OnViewPets, self)
    
    NetClient.ListenRet(NetKeys.QCMD_MY_PETS, self.DefaultQCMDHandler, self)
end

function LianYaoHuPetListWin:Open(type, pid)
    _type = type
    _pid = pid
    if not self.isShowing then
        self:Show()
    end
end

function LianYaoHuPetListWin:ReadyToUpdate()
    self:ShowModalWait()
    NetClient.Send(NetKeys.QCMD_MY_PETS, 0)
    _typeTab.selectedIndex=2
end

function LianYaoHuPetListWin:OnHide()
end

function LianYaoHuPetListWin:OnViewPets(cmd, data)
    self:CloseModalWait()
    _listData = data
    _list.numItems = _listData == nil and 0 or #_listData
end

function LianYaoHuPetListWin:RenderItem(index, obj)
    obj:SetItem(_listData[index+1])
end

function LianYaoHuPetListWin:OnClickPet(context)
    local item = context.data.data
    G_LianYaoHuWin:ResetSelectedPet(item)
    self:Hide()
end
