TouHuWin = fgui.window_class(WindowBase)

local _statu
function TouHuWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Huodong" , "投壶")
end

function TouHuWin:DoInit()
    self:Center()
    for i = 0, 2 do
        local hu = self.contentPane:GetChild("n"..(23+i))
        hu.onClick:Add(self.OnClickHu, self)
        hu.data = i
    end
    self.contentPane:GetChild("n13").onClick:Add(self.OnClickReset, self)
    
    NetClient.Listen(NetKeys.ACMD_TOUHU_VIEW, self.OnView, self)
    NetClient.ListenRet(NetKeys.QCMD_TOUHU, self.DoTouHuHandler, self)
    NetClient.ListenRet(NetKeys.QCMD_TOUHU_RESET, self.DoResetDHandler, self)
end

function TouHuWin:OnShown()
    _statu = {0, 0, 0}
    self:Refresh()
end

function TouHuWin:Refresh()
    NetClient.Send(NetKeys.QCMD_TOUHU_VIEW)
    self:ShowModalWait()
end

function TouHuWin:OnView(cmd, data)
    self:CloseModalWait()
    
    local items = data.list
    local curCount = data.curCount == nil and 0 or data.curCount
    if items ~= nil then
        local num = #items
        for _i = 0, 2 do
            local hu = self.contentPane:GetChild("n"..(23+_i))
            hu:GetController("c1").selectedIndex = _statu[_i+1]
            if num == 0 then
                hu:GetChild("n1"):Clear()
            end
            hu:GetChild("n6").text = ""
        end
        for _i, _item in ipairs(items) do
            local hu = self.contentPane:GetChild("n"..(22+_i))
            if _item.rid ~= nil then
                hu:GetChild("n1"):SetItem(_item)
                local count = _item.zq_count == nil and 0 or _item.zq_count
                hu:GetChild("n6").text = count.."/"..curCount
            else
                hu:GetChild("n1"):Clear()
            end
        end
    end
    local reset_count = data.reset_count == nil and 0 or data.reset_count
    self.contentPane:GetChild("n15").text = reset_count.."/"..curCount
end

function TouHuWin:OnClickHu(context)
    NetClient.Send(NetKeys.QCMD_TOUHU, context.sender.data)
    self:ShowModalWait()
end

function TouHuWin:DoTouHuHandler(cmd, data)
    if self.isShowing then
        self:CloseModalWait()
        if data.code~=0 then
            if data.msg ~= nil and data.msg ~= "" then
                AlertWin.Open(data.msg)
            end
            if data.rcmd == NetKeys.QCMD_TOUHU then
                local index = data.index == nil and 0 or data.index
                _statu[index+1] = 1
            end
        else
            if data.rcmd == NetKeys.QCMD_TOUHU then
                local index = data.index == nil and 0 or data.index
                _statu[index+1] = 2
            end
        end
        self:Refresh()
    end
end

function TouHuWin:OnClickReset(context)
    NetClient.Send(NetKeys.QCMD_TOUHU_RESET)
    self:ShowModalWait()
end

function TouHuWin:DoResetDHandler(cmd, data)
    if self.isShowing then
        self:CloseModalWait()
        if data.code~=0 then
            if data.msg ~= nil and data.msg ~= "" then
                AlertWin.Open(data.msg)
            end
        else
            _statu = {0, 0, 0}
            self:Refresh()
        end
    end
end