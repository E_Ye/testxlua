GuaJiWin = fgui.window_class(WindowBase)

local _hangon
local _robotsList

local _cacheRobots
local _schemes
local _scheme
local _cacheInfo

function GuaJiWin:ctor()
	WindowBase.ctor(self)

	self.asyncCreate = true
	self:SetContentSource("Common","挂机")
end

function GuaJiWin:DoInit()
	self:Center()
	
	_schemes = {}
	_cacheInfo = {}
	
	_hangon = self.contentPane
    _robotsList = _hangon:GetChild("n2")
    _hangon:GetChild("n8").onClick:Add(self.OnClickClean, self)
    _hangon:GetChild("n9").onClick:Add(self.OnClickSelected, self)
    _hangon:GetChild("n11").onClick:Add(self.OnClickStart, self)

    _hangon:GetChild("n25"):SetRegionAndStep(10, 0, 100)
    _hangon:GetChild("n26"):SetRegionAndStep(10, 0, 100)
    _hangon:GetChild("n27"):SetRegionAndStep(10, 0, 100)
    _hangon:GetChild("n28"):SetRegionAndStep(10, 0, 100)
    
    _hangon:GetChild("n38").onClick:Add(self.OnClickEquipLvl, self)
    _hangon:GetChild("n10").onClick:Add(self.OnClickGuiZe, self)
    
    _hangon:GetChild("n37"):SetIncAndDecCallBack(self.OnClickInc, self.OnClickInc, self)
    
    NetClient.Listen(NetKeys.ACMD_QUERY_HANGING_SETUP, self.OnServer_Query, self)
    NetClient.Listen(NetKeys.ACMD_HANGING_GJK, self.OnServerGJK, self)
    
    NetClient.ListenRet(NetKeys.QCMD_HANGING_SETUP, self.OnHangSetup, self)
    NetClient.ListenRet(NetKeys.QCMD_QUERY_HANGING_SETUP, self.DefaultQCMDHandler, self)
end

function GuaJiWin:Open()
    self:Show()
end

function GuaJiWin:OnShown()
    self:Refresh()
end

function GuaJiWin:Refresh()
    if self.isShowing then
        NetClient.Send(NetKeys.QCMD_QUERY_HANGING_SETUP)
        self:ShowModalWait()
    end
end

function GuaJiWin:OnServer_Query(cmd, data)
    self:CloseModalWait()
    if data == nil then
        return
    end
    
    local schemes = data.schemes
    local scheme = {}
    if schemes ~= nil and #schemes > 0 then
        scheme = schemes[1]
    end
    
    _robotsList:RemoveChildrenToPool()
     _cacheRobots={}
    local robots = World.things
    --for rid,thing in pairs(World.things) do
    if robots~=nil then
        local id = ""
        for _rid,d in pairs(robots) do
            id = GetIdFromRid(_rid)
            if _cacheRobots[id] == nil then
                local def = Defs.GetDef("robot", id)
                if def ~= nil and def.kill == true and not string.find(def.name, "【云游】") then
                    local item = _robotsList:AddItemFromPool()
                    item:GetChild("title").text = def.name
                    item.data = id
                    _cacheRobots[id] = true
                    
                    if scheme.robots ~= nil and table.indexOf(scheme.robots, id) ~= nil then
                        item.selected = true
                    end
                end
            end
        end
    end
    
    _hangon:GetChild("n34").text = data.remain
    _hangon:GetChild("n13").selected = scheme.relive == 1 and true or false
    _hangon:GetChild("n15").selected = scheme.skill_card == 1 and true or false
    _hangon:GetChild("n14").selected = scheme.exp_card == 1 and true or false
    _hangon:GetChild("n16").selected = scheme.add_zhongcheng == 1 and true or false
    if scheme.pick_equip_lvl ~= nil and scheme.pick_equip_lvl > 0 then
        _hangon:GetChild("n18").selected = true
        _hangon:GetChild("n38").text = scheme.pick_equip_lvl
    else
        _hangon:GetChild("n18").selected = false
        _hangon:GetChild("n38").text = 0
    end
    if scheme.recover_hp2 ~= nil or scheme.recover_mp2 ~= nil or scheme.pet_recover_hp2 or scheme.pet_recover_mp2 then
        _hangon:GetChild("n17").selected = true
        _hangon:GetChild("n25").value = scheme.recover_hp2 == nil and 0 or scheme.recover_hp2
        _hangon:GetChild("n27").value = scheme.recover_mp2 == nil and 0 or scheme.recover_mp2
        _hangon:GetChild("n26").value = scheme.pet_recover_hp2 == nil and 0 or scheme.pet_recover_hp2
        _hangon:GetChild("n28").value = scheme.pet_recover_mp2 == nil and 0 or scheme.pet_recover_mp2
    else
        _hangon:GetChild("n17").selected = false
        _hangon:GetChild("n25").value = 0
        _hangon:GetChild("n27").value = 0
        _hangon:GetChild("n26").value = 0
        _hangon:GetChild("n28").value = 0
    end
    local use_card = scheme.use_card
    if use_card == nil then
        use_card = 0
    end
    
    if use_card == 1 then
        _hangon:GetChild("n36").selected = true
    else
        _hangon:GetChild("n36").selected = false
    end
    
    if data.guajika_cnt ~= nil then
        _hangon:GetChild("n35").text = "0/"..data.guajika_cnt
    else
        _hangon:GetChild("n35").text = ""
    end
end

function GuaJiWin:OnClickClean()
    if _cacheRobots == nil then
        return
    end
    
    local i = 0
    for _k, _v in pairs(_cacheRobots) do
        local item = _robotsList:GetChildAt(i)
        item.selected = false
        i = i+1
    end
end

function GuaJiWin:OnClickSelected()
    if _cacheRobots == nil then
        return
    end
    
    local i = 0
    for _k, _v in pairs(_cacheRobots) do
        local item = _robotsList:GetChildAt(i)
        item.selected = true
        i = i+1
    end
end

function GuaJiWin:OnClickStart()
    local scheme = {}
    _schemes[1] = scheme
    
    scheme.robots = {}
    local i = 0
    local j = 1
    for _k, _v in pairs(_cacheRobots) do
        local item = _robotsList:GetChildAt(i)
        if item.selected then
            scheme.robots[j] = item.data
            j = j+1
        end
        i = i+1
    end
    if j == 1 then scheme.robots = nil end
    
    if _hangon:GetChild("n13").selected then
        scheme.relive = 1
    end
    if _hangon:GetChild("n15").selected then
        scheme.skill_card = 1
    end 
    if _hangon:GetChild("n14").selected then
        scheme.exp_card = 1
    end
    if _hangon:GetChild("n16").selected then
        scheme.add_zhongcheng = 1
    end
    if _hangon:GetChild("n18").selected then
        scheme.pick_equip_lvl = _hangon:GetChild("n38").text
    end
    if _hangon:GetChild("n17").selected then
        scheme.recover_hp2 = _hangon:GetChild("n25").text
        scheme.recover_mp2 = _hangon:GetChild("n27").text
        scheme.pet_recover_hp2 = _hangon:GetChild("n26").text
        scheme.pet_recover_mp2 = _hangon:GetChild("n28").text
    end
    
    local rm = {}
    if _hangon:GetChild("n36").selected then
        _cacheInfo.time = _hangon:GetChild("n37").value == nil and 0 or _hangon:GetChild("n37").value
        scheme.use_card = 1
    else
        _cacheInfo.time = 0
        scheme.use_card = 0
    end
    _cacheInfo.type = 0
    
    rm.schemes = _schemes
    self:ShowModalWait()
    _scheme = scheme
    NetClient.Send(NetKeys.QCMD_HANGING_SETUP, rm)
end

function GuaJiWin:OnClickEquipLvl()
    G_GuaJiLvlWin:Popup()
end

function GuaJiWin:OnClickGuiZe()
    G_GuaJiGuiZeWin:Popup()
end

function GuaJiWin:ResetLvl(lvl)
    _hangon:GetChild("n19").text = lvl
end

function GuaJiWin:OnHangSetup(cmd, data)
    if self.isShowing then
        self:CloseModalWait()
        if data.code~=0 then
            AlertWin.Open(data.msg)
        else
            self:Hide()
            Bot.Start(_scheme, 0, _cacheInfo)
        end
    end
end

function GuaJiWin:OnClickInc()
    local count = _hangon:GetChild("n37").value
    if count == nil or count == 0 then
        _hangon:GetChild("n35").text = ""
        return
    end
    
    self:ShowModalWait()
    NetClient.Send(NetKeys.QCMD_HANGING_GJK, count)
end

function GuaJiWin:OnServerGJK(cmd, data)
    self:CloseModalWait()
    local count = data.count == nil and 0 or data.count
    local cc = data.cc == nil and 0 or data.cc
    _hangon:GetChild("n35").text = data.count.."/"..data.cc
end