ConfirmWin = fgui.window_class(WindowBase)

function ConfirmWin:ctor()
    WindowBase.ctor(self)
    
    self:SetContentSource("Common", "ConfirmWindow")
    self:Init()
end

function ConfirmWin:InternalOpen(msg, leftCallback, rightCallback)
    self.contentPane:GetChild("msg").text = msg
    self.vars.leftCallback = leftCallback
    self.vars.rightCallback = rightCallback
    self:Show()
end

function ConfirmWin:DoInit()
    self.contentPane:GetChild("ok").onClick:Add(self.OnConfirmBtnClick, self)
    self.contentPane:GetChild("cancel").onClick:Add(self.OnConfirmBtnClick, self)
    self.modal = true
    self.sortingOrder = SortingOrder.Modal
    self:Center()
end

function ConfirmWin:OnShown()
    if self.vars.alertSound == nil then
        self.vars.alertSound = UIPackage.GetItemAsset("Sucai", "alert")
    end
    GRoot.inst:PlayOneShotSound(self.vars.alertSound)
end

function ConfirmWin:OnConfirmBtnClick(context)
   	self:Hide()
   	local leftCallback = self.vars.leftCallback
   	local rightCallback = self.vars.rightCallback
   	self.vars.leftCallback = nil
   	self.vars.rightCallback = nil
   	if context.sender.name=='ok' then
   		if leftCallback~=nil then leftCallback() end
   	else
   		if rightCallback~=nil then rightCallback() end
   	end
end
