AlertWin = fgui.window_class(WindowBase)

function AlertWin:ctor()
    WindowBase.ctor(self)

    self:SetContentSource("Common", "AlertWindow")
    self.asyncCreate = false
    self:Init()
end

function AlertWin:InternalOpen(msg, callback)
    if msg==nil then msg = "" end
    self.contentPane:GetChild("msg").text = msg
    self.vars.callback = callback
    self:Show()
end

function AlertWin:DoInit()
	self.contentPane:GetChild("ok").onClick:Add(self.OnConfirmBtnClick, self)
    self.modal = true
    self:Center()
    self.sortingOrder = 2
end

function AlertWin:OnShown()
    if self.vars.alertSound == '' then
        self.vars.alertSound = UIPackage.GetItemAsset("Sucai", "alert")
    end
    GRoot.inst:PlayOneShotSound(self.vars.alertSound)
    GRoot.inst:CloseModalWait()
end

function AlertWin:OnConfirmBtnClick()
   	self:Hide()
    if self.vars.callback ~=nil then
    	local callback = self.vars.callback
    	self.vars.callback = nil
        callback()
    end
end