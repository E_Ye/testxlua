PetWin = fgui.window_class(WindowBase)

local _list
local _tab
local _btnCall

local _shuxing
local _menu
local _menuList
local _skills
local _jingmai
local _zhufu
local _tujian

local _skillList
local _zhufuList
local _tuJianList
local _tuJianC

local _listData
local _info
local _skillData
local _tuJiansData
local _tuJianData

local _blsDef

function PetWin:ctor()
	WindowBase.ctor(self)

	self.asyncCreate = true
	self:SetContentSource("Common","宠物")
	
	_menu = UIPackage.CreateObject("Common" , "按钮菜单")
    _menuList = _menu:GetChild("n2")
    _menuList.onClickItem:Add(self.OnClickMenuItem, self)
end

function PetWin:DoInit()
	self:Center()

	_list = self.contentPane:GetChild("list")
	_list:RemoveChildrenToPool()
	_list.onClickItem:Add(self.OnPetChanged, self)
	_tab = self.contentPane:GetController("tab")
	_tab.onChanged:Add(self.OnTabChanged, self)

	_shuxing = self.contentPane:GetChild("shuxing")
	_skills = self.contentPane:GetChild("skills")
	_skills:GetChild("n25").onClickItem:Add(self.OnSkillChanged, self)
	_skills:GetChild("n62").onClick:Add(self.OnClickLearnSkill, self)
	_jingmai = self.contentPane:GetChild("jingmai")
	_zhufu = self.contentPane:GetChild("zhufu")
	_zhufuList = _zhufu:GetChild("n70")
	_tujian = self.contentPane:GetChild("tujian")
	
	_skillList = _skills:GetChild("n25")

	_btnCall = _shuxing:GetChild("btnCall")
	_btnCall.onClick:Add(self.OnClickCall, self)
	_shuxing:GetChild("btnDismiss").onClick:Add(self.OnClickDismiss, self)
	_shuxing:GetChild("n38").onClick:Add(self.OnClickZCD, self)
	_shuxing:GetChild("n39").onClick:Add(self.OnClickZZ, self)
	_shuxing:GetChild("n40").onClick:Add(self.OnClickLL, self)
	_shuxing:GetChild("btnZhuRu").onClick:Add(self.OnClickZhuRu, self)
	
	_jingmai:GetChild("n57").onClick:Add(self.OnClickJM, self)
    _jingmai:GetChild("n56").onClick:Add(self.OnClickJM, self)
    _jingmai:GetChild("n55").onClick:Add(self.OnClickJM, self)
    _jingmai:GetChild("n54").onClick:Add(self.OnClickJM, self)
    _jingmai:GetChild("n53").onClick:Add(self.OnClickJM, self)
    _jingmai:GetChild("n52").onClick:Add(self.OnClickJM, self)
    _jingmai:GetChild("n51").onClick:Add(self.OnClickJM, self)
    _jingmai:GetChild("n69").onClick:Add(self.OnClickJM, self)
    
    _tuJianList = _tujian:GetChild("n72")
    _tuJianList.onClickItem:Add(self.OnTuJianChanged, self)
    local p = self
    _tuJianList.itemRenderer = function(index, obj) p:RenderItem(index, obj) end
    _tuJianList:SetVirtual()
    _tuJianC = _tujian:GetController("c1")
    _tujian:GetChild("n118").onClick:Add(self.OnGoToCatchPet, self)
    _tujian:GetChild("n119").onClick:Add(self.OnFuHuaPet, self)
    _tujian:GetChild("n116").onClick:Add(self.OnJiuLongLu, self)
    
    _tujian:GetChild("n115").onClick:Add(self.OnLianYaoHu, self)
    
    _blsDef = Defs.GetDef("game", "pet_bls")
	
	NetClient.Listen(NetKeys.ACMD_MY_PETS, self.OnServer_MyPets, self)
	NetClient.Listen(NetKeys.ACMD_VIEW_PET, self.OnServer_ViewPet, self)
	NetClient.Listen(NetKeys.ACMD_CALL_OUT_PET, self.OnServer_CallPet, self)
	NetClient.Listen(NetKeys.ACMD_CALL_BACK_PET, self.OnServer_BackPet, self)
	NetClient.Listen(NetKeys.ACMD_XIULIAN_JINGMAI, self.OnXiuLianJingMai, self)
	NetClient.Listen(NetKeys.ACMD_TUJIANS, self.OnTuJians, self)
	NetClient.Listen(NetKeys.ACMD_TUJIAN_VIEW, self.OnTuJianView, self)
	
	NetClient.ListenRet(NetKeys.QCMD_CHUCK, self.OnChuckReturn, self)
	NetClient.ListenRet(NetKeys.QCMD_USE_ITEM, self.DefaultQCMDHandler, self)
	NetClient.ListenRet(NetKeys.QCMD_ADD_PET_ZZ, self.DefaultQCMDHandler, self)
	NetClient.ListenRet(NetKeys.QCMD_SET_EN_LINGLI, self.DefaultQCMDHandler, self)
	NetClient.ListenRet(NetKeys.QCMD_XIULIAN_JINGMAI, self.DefaultQCMDHandler, self)
	NetClient.ListenRet(NetKeys.QCMD_LEARN_SKILL, self.DefaultQCMDHandler, self)
	NetClient.ListenRet(NetKeys.QCMD_TUJIAN_FUHUA, self.FuHuaQCMDHandler, self)
	NetClient.ListenRet(NetKeys.QCMD_VIEW_PET, self.DefaultQCMDHandler, self)
end

function PetWin:Open()
	self:Show()
end

function PetWin:OnShown()
    self:RefreshList()
end

function PetWin:RefreshList()
    if self.isShowing then
        NetClient.Send(NetKeys.QCMD_MY_PETS, 0)
        self:ShowModalWait()
    end
end

function PetWin:OnTabChanged()
    local si = _tab.selectedIndex
    if si == 0 then
        self:ResetShuXing()
    elseif si == 1 then
        self:ResetSkills()
    elseif si == 2 then
        self:ResetJM()
    elseif si == 3 then
        self:ResetZhuFu()
    elseif si == 4 then
        self:ResetTuJian()
    else
    end
end

function PetWin:OnPetChanged()
    self:Refresh()
end

function PetWin:Refresh()
    if self.isShowing then
        local si = _tab.selectedIndex
        if si == 4 then
            self:ResetTuJian()
        elseif _listData and #_listData>0 and _list.selectedIndex>=0 then
            NetClient.Send(NetKeys.QCMD_VIEW_PET, _listData[_list.selectedIndex+1].rid, "")
            self:ShowModalWait()
        end
    end
end

function PetWin:OnServer_MyPets(cmd, data)
    if self.isShowing then
        self:CloseModalWait()
        
        local lsi = _list.selectedIndex
        _list:RemoveChildrenToPool()
        _listData = data
        for _i,pet in ipairs(_listData) do
            local btn = _list:AddItemFromPool()
            btn.title = pet.name
            btn:GetChild("lvl").text = pet.lvl
            btn.icon = Defs.GetIconSrc(GetIdFromRid(pet.rid))
            btn:GetController("status").selectedIndex = pet.status
        end
    
        if lsi == -1 or lsi >=#_listData then
            _list.selectedIndex = 0
        elseif lsi >= 0 and lsi < #_listData then
            _list.selectedIndex = lsi
        end
        self:Refresh()
    end
end

function PetWin:OnServer_ViewPet(cmd, data)
    if self.isShowing then
        if _listData==nil or data.rid~=_listData[_list.selectedIndex+1].rid then return end
    
        self:CloseModalWait()
    
        _info = data
        self:OnTabChanged()
    end
end

-- tab 属性
function PetWin:OnClickCall(context)
    if _info==nil then return end
    
    if _info.status==0 then
        _menuList:RemoveChildrenToPool()
    
        local btn = _menuList:AddItemFromPool()
        btn.icon = "ui://Sucai/wenzi39" -- 参战
        btn.name = "canzhan"
    
        btn = _menuList:AddItemFromPool()
        btn.icon = "ui://Sucai/wenzi40" -- 观战
        btn.name = "guangzhan"
        
        GRoot.inst:ShowPopup(_menu, context.sender, false)
    else
        self:ShowModalWait()
        NetClient.Send(NetKeys.QCMD_CALL_BACK)
    end
end

function PetWin:OnClickMenuItem(context)
    GRoot.inst:HidePopup(_menu)
    local func = context.data.name
    if func=="canzhan" then
        self:OnClickCZ(2)
    elseif func=="guangzhan" then
        self:OnClickCZ(1)
    end
end

-- 状态（1-观战，2-参战）
function PetWin:OnClickCZ(status)
    if _info==nil then return end

    self:ShowModalWait()
    NetClient.Send(NetKeys.QCMD_CALL_OUT, status, _info.rid)
end

function PetWin:OnClickDismiss()
    if _info==nil then return end

    -- 0-休息，1-观战，2-参战
    if _info.status==0 then
        self:ShowModalWait()
        NetClient.Send(NetKeys.QCMD_CHUCK, 1, _info.rid)
    end
end

function PetWin:OnClickZCD()
    -- 忠诚度
    self:ShowModalWait()
    NetClient.Send(NetKeys.QCMD_USE_ITEM, 1, _info.rid, Defs.SpeItems.cwls, 0)
end

function PetWin:OnClickZZ()
    -- 资质
    self:ShowModalWait()
    NetClient.Send(NetKeys.QCMD_ADD_PET_ZZ, 1, _info.rid)
end

function PetWin:OnClickLL()
    -- 灵力
    self:ShowModalWait()
    NetClient.Send(NetKeys.QCMD_USE_ITEM, 1, _info.rid, Defs.SpeItems.xt, 0)
end

function PetWin:OnClickZhuRu()
    -- 注入
    local ll = _shuxing:GetChild("n41").value
    if ll == nil or ll <= 0 then
        return
    end
    
    self:ShowModalWait()
    NetClient.Send(NetKeys.QCMD_SET_EN_LINGLI, 2, ll)
end

function PetWin:ResetShuXing()
    if _info == nil then
        return
    end

    _shuxing:GetController("status").selectedIndex = _info.status == 0 and 0 or 1
    
    _shuxing:GetChild("n48").text = _info.name.." ".._info.lvl.."级"
    _shuxing:GetChild("n16").value = _info.exp == nil and 0 or _info.exp
    _shuxing:GetChild("n16").max = _info.upgrade_exp == nil and 0 or _info.upgrade_exp
    _shuxing:GetChild("n20").text = _info.hp
    _shuxing:GetChild("n22").text = _info.mp
    _shuxing:GetChild("n24").text = _info.attack
    _shuxing:GetChild("n26").text = _info.recovery
    _shuxing:GetChild("n28").text = _info.zhongcheng
    _shuxing:GetChild("n30").text = _info.zz
    _shuxing:GetChild("n32").text = _info.attack_speed
    _shuxing:GetChild("n34").text = _info.xianqi_lvl
    _shuxing:GetChild("n36").text = _info.qianghuadu
    _shuxing:GetChild("n41").text = _info.lingli.."/".._info.max_lingli

    _shuxing:GetChild("btnZhuRu").visible = _info.lingli > 0
end

function PetWin:OnChuckReturn(cmd, data)
    if data == nil then
        return
    end
    
    if data.code ~= 0 then
        return
    end
    
    self:RefreshList()
    AlertWin.Open(data.msg)
end

function PetWin:OnServer_CallPet(cmd, data)
    MainUI.ShowPetStatus(data.pet)
    self:CloseModalWait()
    self:RefreshList()
end

function PetWin:OnServer_BackPet(cmd, data)
    self:CloseModalWait()
    self:RefreshList()
    
    MainUI.HidePetStatus()
end

-- tab 技能
function PetWin:OnSkillChanged(context)
    if _info==nil then return end
    
    if _info.skills == nil then return end
    
    local si = _skillList:GetChildIndex(context.data)+1
    _skillData = _info.skills[si]
    self:DoResetSkillInfo()
end

function PetWin:ResetSkills()
    _skillList:RemoveChildrenToPool()
    if _info == nil then
        return
    end
    
    for _i,skill in ipairs(_info.skills) do
        local btn = _skillList:AddItemFromPool()
        btn:SetItem(skill)
        if skill.statu == nil then
            btn.grayed = false
        else
            btn.grayed = true
        end
    end
    
    if #_info.skills > 0 then
        _skillData = _info.skills[1]
        self:DoResetSkillInfo()
    else
        -- 没有技能面板应该隐藏一部分
    end
end

function PetWin:DoResetSkillInfo()
    _skills:GetChild("n29"):SetItem(_skillData)
    _skills:GetChild("n30").text = _skillData.name
    _skills:GetChild("n53").text = _skillData.def.desc
    _skills:GetChild("n54").text = _skillData.lvl
    if _skillData.exp then
        _skills:GetChild("n55").text = _skillData.exp.."/".._skillData.upgrade_exp
    else
        _skills:GetChild("n55").text = ""
    end
    _skills:GetChild("n56").text = _skillData.mod_plus
    _skills:GetChild("n57").text = _skillData.def.mod
    _skills:GetChild("n58").text = Defs.GetGameDef("attr_names", _skillData.def.deplete_attr, "")
    _skills:GetChild("n59").text = Defs.GetGameDef("attr_names", _skillData.def.hurt_attr, "")
    _skills:GetChild("n60").text = _skillData.def.cool_time
    _skills:GetChild("n61").text = _skillData.def.attack_distance
    local itemid = _skillData.def.deplete_item
    if itemid ~= nil then
        local itemDef = Defs.GetDef("item", itemid)
        _skills:GetChild("n64").text = itemDef.name
    else
        _skills:GetChild("n64").text = ""
    end
    
    if _skillData.statu == 1 then
        _skills:GetChild("n29").grayed = true
        _skills:GetChild("n62").visible = true
    elseif _skillData.statu == 2 then
        _skills:GetChild("n29").grayed = true
        _skills:GetChild("n62").visible = true
    else
        _skills:GetChild("n29").grayed = false
        _skills:GetChild("n62").visible = false
    end
end

function PetWin:OnClickLearnSkill()
    -- 学习技能
    self:ShowModalWait()
    NetClient.Send(NetKeys.QCMD_LEARN_SKILL, 0, _info.rid, _skillData.id)
end

-- tab 经脉
function PetWin:ResetJM()
    _jingmai:GetChild("n57").visible = false
    _jingmai:GetChild("n56").visible = false
    _jingmai:GetChild("n55").visible = false
    _jingmai:GetChild("n54").visible = false
    _jingmai:GetChild("n53").visible = false
    _jingmai:GetChild("n52").visible = false
    _jingmai:GetChild("n51").visible = false
    _jingmai:GetChild("n69").visible = false
    
    if _info == nil then
        self:setJMJD(_jingmai:GetChild("n44"), 0)
        self:setJMJD(_jingmai:GetChild("n45"), 0)
        self:setJMJD(_jingmai:GetChild("n46"), 0)
        self:setJMJD(_jingmai:GetChild("n47"), 0)
        self:setJMJD(_jingmai:GetChild("n48"), 0)
        self:setJMJD(_jingmai:GetChild("n49"), 0)
        self:setJMJD(_jingmai:GetChild("n50"), 0)
        self:setJMJD(_jingmai:GetChild("n66"), 0)
        return
    end
    
    self:setJMJD(_jingmai:GetChild("n44"), _info.bm_sm_p)
    self:setJMJD(_jingmai:GetChild("n45"), _info.bm_tm_p)
    self:setJMJD(_jingmai:GetChild("n46"), _info.bm_xm_p)
    self:setJMJD(_jingmai:GetChild("n47"), _info.bm_km_p)
    self:setJMJD(_jingmai:GetChild("n48"), _info.bm_zm_p)
    self:setJMJD(_jingmai:GetChild("n49"), _info.bm_rm_p)
    self:setJMJD(_jingmai:GetChild("n50"), _info.bm_mm_p)
    self:setJMJD(_jingmai:GetChild("n66"), _info.bm_dm_p)
    
    if _info.bm_dm_p == 100 then
        if _info.bm_mm_p < 100 then
            _jingmai:GetChild("n57").visible = true
        elseif _info.bm_rm_p < 100 then
            _jingmai:GetChild("n56").visible = true
        elseif _info.bm_zm_p < 100 then
            _jingmai:GetChild("n55").visible = true
        elseif _info.bm_km_p < 100 then
            _jingmai:GetChild("n54").visible = true
        elseif _info.bm_xm_p < 100 then
            _jingmai:GetChild("n53").visible = true
        elseif _info.bm_tm_p < 100 then
            _jingmai:GetChild("n52").visible = true
        elseif _info.bm_sm_p < 100 then
            _jingmai:GetChild("n51").visible = true
        end
    else
        _jingmai:GetChild("n69").visible = true
    end
end

-- 经脉进度
function PetWin:setJMJD(com, value)
    if value == 100 then
        com:GetController("c1").selectedIndex = 1
        com:GetChild("n1").value = value
    else
        com:GetController("c1").selectedIndex = 0
        com:GetChild("n0").value = value
    end
end

function PetWin:OnClickJM(context)
    -- 经脉 点击+号
    self:ShowModalWait()
    NetClient.Send(NetKeys.QCMD_XIULIAN_JINGMAI, _info.rid)
end

function PetWin:OnXiuLianJingMai(cmd, data)
    self:CloseModalWait()
    if data == nil then
        return
    end
    
    self:ResetJM2(data)
    AlertWin.Open(data.msg)
end

function PetWin:ResetJM2(data)
    local jds = data.jds
    local cans = data.cans
    
    if jds ~= nil then
        self:setJMJD(_jingmai:GetChild("n44"), jds[8])
        self:setJMJD(_jingmai:GetChild("n45"), jds[7])
        self:setJMJD(_jingmai:GetChild("n46"), jds[6])
        self:setJMJD(_jingmai:GetChild("n47"), jds[5])
        self:setJMJD(_jingmai:GetChild("n48"), jds[4])
        self:setJMJD(_jingmai:GetChild("n49"), jds[3])
        self:setJMJD(_jingmai:GetChild("n50"), jds[2])
        self:setJMJD(_jingmai:GetChild("n66"), jds[1])
    else
        self:setJMJD(_jingmai:GetChild("n44"), 0)
        self:setJMJD(_jingmai:GetChild("n45"), 0)
        self:setJMJD(_jingmai:GetChild("n46"), 0)
        self:setJMJD(_jingmai:GetChild("n47"), 0)
        self:setJMJD(_jingmai:GetChild("n48"), 0)
        self:setJMJD(_jingmai:GetChild("n49"), 0)
        self:setJMJD(_jingmai:GetChild("n50"), 0)
        self:setJMJD(_jingmai:GetChild("n66"), 0)
    end
    
    _jingmai:GetChild("n57").visible = false
    _jingmai:GetChild("n56").visible = false
    _jingmai:GetChild("n55").visible = false
    _jingmai:GetChild("n54").visible = false
    _jingmai:GetChild("n53").visible = false
    _jingmai:GetChild("n52").visible = false
    _jingmai:GetChild("n51").visible = false
    _jingmai:GetChild("n69").visible = false
    if cans ~= nil then
        if cans[1] == 1 then
            _jingmai:GetChild("n69").visible = true
        end
        if cans[2] == 1 then
            _jingmai:GetChild("n57").visible = true
        end
        if cans[3] == 1 then
            _jingmai:GetChild("n56").visible = true
        end
        if cans[4] == 1 then
            _jingmai:GetChild("n55").visible = true
        end
        if cans[5] == 1 then
            _jingmai:GetChild("n54").visible = true
        end
        if cans[6] == 1 then
            _jingmai:GetChild("n53").visible = true
        end
        if cans[7] == 1 then
            _jingmai:GetChild("n52").visible = true
        end
        if cans[8] == 1 then
            _jingmai:GetChild("n51").visible = true
        end
    end
end

-- tab 祝福
function PetWin:ResetZhuFu()
    _zhufuList:RemoveChildrenToPool()
    
    if _info == nil or _info.bls == nil then
        return
    end
    
    for _i,zhufu in ipairs(_info.bls) do
        local btn = _zhufuList:AddItemFromPool()
        
        local def = Defs.GetDef("item", zhufu.id)
        btn:GetChild("title").text = def.name
        btn:GetChild("n6").text = zhufu.desc
        btn:GetChild("icon").url = zhufu.id
        btn:GetChild("n8").onClick:Add(self.ZhuFuUseClick, self)
        btn:GetChild("n8").data = zhufu
        if zhufu.desc == nil then
            btn.grayed = true
            btn:GetChild("n6").text = def.desc
        else
            btn.grayed = false
        end
        if zhufu.action == 2 then
            btn:GetChild("n8").icon = "ui://Sucai/wenzi17"
        elseif zhufu.action == 1 then
            btn:GetChild("n8").icon = "ui://Sucai/wenzi17"
        else
            btn:GetChild("n8").icon = "ui://Sucai/wenzi17"
        end
    end 
end

function PetWin:ZhuFuUseClick(context)
    local data = context.sender.data
    if data == nil then
        return
    end
    
    if data.action == 1 then
        NetClient.Send(NetKeys.QCMD_USE_ITEM, 1, _info.rid, data.id, 0)
        self:ShowModalWait()
    elseif data.action == 2 then
        -- 打开商城购买
        local def = Defs.GetDef("item", data.id)
        AlertWin.Open(def.name..Defs.NO_ITEM_BUY_TIPS)
    end
end

-- tab 图鉴
function PetWin:ResetTuJian()
    self:ShowModalWait()
    NetClient.Send(NetKeys.QCMD_TUJIANS, 1)
end

function PetWin:OnTuJians(cmd, data)
    self:CloseModalWait()
    if data.type ~= 1 then
        return
    end 
    
    _tuJiansData = data.list
    _tuJianList.numItems = #_tuJiansData
    
    self:RefreshTuJian()
end

function PetWin:RenderItem(index, obj)
    local data = _tuJiansData[index+1]
    local rid = data.rid
    obj:SetItem(data)
    if rid == nil then
        obj.grayed = true
    else
        obj.grayed = false
    end
end

function PetWin:OnTuJianChanged(context)
    local rid = context.data.data.rid
    if rid == nil then
        rid = context.data.data.id
    end
    self:doRefreshTuJian(rid)
end

function PetWin:RefreshTuJian(lsi)
    if lsi == nil or lsi == -1 or lsi >=#_tuJiansData then
        _tuJianList.selectedIndex = 0
        lsi = 0
    end
    local rid = _tuJiansData[lsi+1].rid
    if rid == nil then
        rid = _tuJiansData[lsi+1].id
    end
    self:doRefreshTuJian(rid)
end

function PetWin:doRefreshTuJian(rid)
    if rid == nil then
        return
    end
    
    self:ShowModalWait()
    NetClient.Send(NetKeys.QCMD_TUJIAN_VIEW, 1, rid)
end

function PetWin:OnTuJianView(cmd, data)
    self:CloseModalWait()
    if data.type ~= 1 then
        return
    end
    
    _tuJianData = data
    local id = data.id
    if id ~= nil then
        local def = Defs.GetDef("pet", id)
        _tujian:GetChild("n76").text = def.name
        _tujian:GetChild("n81").text = def.desc
        -- 是否只能由九龙炉合成
        if def.jll then 
            _tuJianC.selectedIndex = 2
        elseif data.cwd then
            _tuJianC.selectedIndex = 1
        else
            _tuJianC.selectedIndex = 0
        end
    else
        local def = Defs.GetDef("pet", GetIdFromRid(data.rid))
        _tujian:GetChild("n76").text = def.name
        _tujian:GetChild("n81").text = def.desc
        _tuJianC.selectedIndex = 3
    end
    
    _tujian:GetChild("n103").text = data.hp0
    _tujian:GetChild("n104").text = data.mp0
    _tujian:GetChild("n105").text = data.zz0
    _tujian:GetChild("n106").text = data.attack0
    _tujian:GetChild("n107").text = data.recovery0
    _tujian:GetChild("n108").text = data.attack_speed
    
    _tujian:GetChild("n109").text = data.zr_cz_sm
    _tujian:GetChild("n110").text = data.zr_cz_fl
    _tujian:GetChild("n111").text = data.zr_cz_yl
    _tujian:GetChild("n112").text = data.zr_cz_fy
    _tujian:GetChild("n113").text = data.zr_cz_gj
    
    local sds = data.skills
    for i = 1, 3 do
        local btn = _tujian:GetChild("n"..(82+i))
        if sds == nil then
            btn:Clear()
        else
            local _sd = sds[i]
            if _sd ~= nil then
                if _sd.rid == nil then
                    _sd.rid = _sd.id
                    btn.grayed = true
                else
                    btn.grayed = false
                end
                btn:SetItem(_sd)
            else
                btn:Clear()
            end
        end
    end
end

function PetWin:OnGoToCatchPet(context)
    AutoRouting.Start(target.eid, _tuJianData.target)
    self:Hide()
end

function PetWin:OnFuHuaPet(context)
    if _tuJianData == nil then
        return
    end
    
    self:ShowModalWait()
    local id = _tuJianData.id 
    if id == nil then
        id = GetIdFromRid(_tuJianData.rid)
    end
    NetClient.Send(NetKeys.QCMD_TUJIAN_FUHUA, 1, id)
end

function PetWin:FuHuaQCMDHandler(cmd, data)
    self:RefreshTuJian(_tuJianList.selectedIndex)
end

function PetWin:OnJiuLongLu(context)
    G_JiuLongLuWin:Open(_tuJianData.id)
end

function PetWin:OnLianYaoHu(context)
    self:Hide()
    G_LianYaoHuWin:Open()
end