LianYaoHuWin = fgui.window_class(WindowBase)

local _panel
local _data -- 选中的宠物
local _pet
local _loop = 0
local _step = 0
local _gezis
local _gezis2 -- 当前存放物品、宠物的格子

function LianYaoHuWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common" , "炼妖壶")
end

function LianYaoHuWin:DoInit()
    self:Center()
    _panel = self.contentPane
    _panel:GetChild("n6").onClick:Add(self.OnClickBack, self)
    _panel:GetChild("n24").onClick:Add(self.OnClickGo, self)
    _pet = _panel:GetChild("ge_0")
    _pet.onClick:Add(self.OnClickPet, self)
    
    _gezis = {}
    for i = 1, 72 do
        _gezis[i] = _panel:GetChild("ge_"..(i-1))
        _gezis[i].visible = false
    end
    _gezis[1].visible = true
    
    _gezis2 = {}
    
    NetClient.Listen(NetKeys.ACMD_LIANYAOHU_GEZI, self.OnLianYaoHuView, self)
    NetClient.Listen(NetKeys.ACMD_LIANYAOHU_GO, self.OnLianYaoHuGo, self)
    
    NetClient.ListenRet(NetKeys.QCMD_LIANYAOHU_GO, self.DefaultQCMDHandler, self)
end

function LianYaoHuWin:Open()
    self:Show()
end

function LianYaoHuWin:OnShown()
    self:Refresh()
    local context = {}
    context.sender = _pet
    self:OnClickPet(context)
end

function LianYaoHuWin:Refresh()
    if self.isShowing and _data ~= nil then
        NetClient.Send(NetKeys.QCMD_GET_LIANYAOHU_GEZI, _data.rid)
        self:ShowModalWait()
    end
end

function LianYaoHuWin:OnLianYaoHuView(cmd, data)
    self:CloseModalWait()
    self:ResetInfo(data)
end

function LianYaoHuWin:ResetInfo(data)
    local lyh_count = data.lyh_count == nil and 0 or data.lyh_count
    local back_count = data.back_count == nil and 0 or data.back_count
    _panel:GetChild("n28").text = lyh_count.."/"..back_count
    
    local ahead_count = data.ahead_count == nil and 0 or data.ahead_count
    _panel:GetChild("n29").text = lyh_count.."/"..ahead_count
    
    _panel:GetChild("n16").text = data.loop
    _panel:GetChild("n15").text = data.score
    _loop = data.loop
    _step = data.step
    
    if data.elements ~= nil then
        for _i, gezi in ipairs(_gezis2) do
            gezi:Clear()
            gezi.visible = false
        end
        _gezis2 = {}
        for _i, _element in ipairs(data.elements) do
            local gezi = _gezis[_element.seq+1]
            local _d = {}
            _d.rid = _element.id
            gezi:SetItem(_d)
            gezi.visible = true
            _gezis2[_i] = gezi
        end
    end
    
    _pet:Clear()
    _pet = _gezis[data.pos+1]
    _pet:SetItem(_data)
    
    if data.rid ~= nil then
        _data.rid = data.rid
    end
end

function LianYaoHuWin:OnClickBack(context)
    NetClient.Send(NetKeys.QCMD_LIANYAOHU_GO, 2, _data.rid)
    self:ShowModalWait()
end

function LianYaoHuWin:OnClickGo(context)
    NetClient.Send(NetKeys.QCMD_LIANYAOHU_GO, 1, _data.rid)
    self:ShowModalWait()
end

function LianYaoHuWin:OnLianYaoHuGo(cmd, data)
    self:CloseModalWait()
    self:ResetInfo(data)
end

function LianYaoHuWin:OnClickPet(context)
    if _loop ~= 0 or _step ~= 0 then
        return
    end
    
    GRoot.inst:ShowPopup(G_LianYaoHuPetListWin, context.sender, false)
end

function LianYaoHuWin:ResetSelectedPet(data)
    _pet:SetItem(data)
    _data = data
    self:Refresh()
end