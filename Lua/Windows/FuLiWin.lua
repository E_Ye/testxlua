FuLiWin = fgui.window_class(WindowBase)

local _typeTab
local _signPanel
local _signGroup
local _signList
local _day
local _qitianPanel
local _mubiaoList
local _mubiaoItemsList
local _mubiaoLeft
local _mubiaoRight
local _jlGroup
local _hdPanel
local _hdList
local _lhdList

local _mubiaoData

function FuLiWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common" , "福利")
end

function FuLiWin:DoInit()
    self:Center()
    
    _typeTab=self.contentPane:GetController("type")
    _typeTab.onChanged:Add(self.Refresh, self)
    _signPanel = self.contentPane:GetChild("n5")
    _signGroup = _signPanel:GetChild("n15")
    _signList = _signPanel:GetChild("list")
    _signList.onClickItem:Add(self.OnClickSign, self)
    
    _day = 1
    _qitianPanel = self.contentPane:GetChild("n16")
    _mubiaoList = _qitianPanel:GetChild("n27")
    _mubiaoItemsList = _qitianPanel:GetChild("n28")
    _mubiaoLeft = _qitianPanel:GetChild("n21")
    _mubiaoLeft.onClick:Add(self.OnClickLeft, self)
    _mubiaoRight = _qitianPanel:GetChild("n20")
    _mubiaoRight.onClick:Add(self.OnClickRight, self)
    _jlGroup = _qitianPanel:GetChild("n43")
    _qitianPanel:GetChildInGroup(_jlGroup, "n32").onClick:Add(self.OnClickMuBiaoJianLi, self)
    
    _hdPanel = self.contentPane:GetChild("n46")
    _hdList = _hdPanel:GetChild("n4")
    _lhdList = _hdPanel:GetChild("n12")
    
    NetClient.Listen(NetKeys.ACMD_SIGN_INFO, self.OnSignInfo, self)
    NetClient.Listen(NetKeys.ACMD_QITIAN_VIEW, self.OnQiTianInfo, self)
    NetClient.Listen(NetKeys.ACMD_HD_INFO, self.OnHuoDongInfo, self)
     
    NetClient.ListenRet(NetKeys.QCMD_SIGN_INFO, self.DefaultQCMDHandler, self)
    NetClient.ListenRet(NetKeys.QCMD_DO_SIGN, self.OnClickSignReturn, self)
    NetClient.ListenRet(NetKeys.QCMD_QITIAN_VIEW, self.DefaultQCMDHandler, self)
    NetClient.ListenRet(NetKeys.QCMD_QITIAN_LINQU, self.OnMuBiaoLinQuReturn, self)
    
    NetClient.ListenRet(NetKeys.QCMD_HD_INFO, self.DefaultQCMDHandler, self)
end

function FuLiWin:OnShown()
    self:Refresh()
end

function FuLiWin:Refresh()
    if self.isShowing then
        local index = _typeTab.selectedIndex
        if index == -1 or index == 0 then
            NetClient.Send(NetKeys.QCMD_SIGN_INFO)
            self:ShowModalWait()
        elseif index == 1 then
            NetClient.Send(NetKeys.QCMD_QITIAN_VIEW, _day)
            self:ShowModalWait()
        elseif index == 2 then
            _hdList:RemoveChildrenToPool()
            _lhdList:RemoveChildrenToPool()
            NetClient.Send(NetKeys.QCMD_HD_INFO)
            self:ShowModalWait()
        end
    end
end

function FuLiWin:OnSignInfo(cmd, data)
    self:CloseModalWait()
    _signPanel:GetChildInGroup(_signGroup, "n8").text  = data.title
    _signPanel:GetChildInGroup(_signGroup, "count").text  = data.signs_total
    _signList:RemoveChildrenToPool()
    if data.signs ~= nil then
        for _i, _sign in ipairs(data.signs) do
            local btn = _signList:AddItemFromPool()
            btn:SetItem(_sign)
        end
    end
end

function FuLiWin:OnClickSign(context)
    NetClient.Send(NetKeys.QCMD_DO_SIGN)
    self:ShowModalWait(NetKeys.QCMD_DO_SIGN)
end

function FuLiWin:OnClickSignReturn(cmd, data)
    if self.isShowing then
        self:CloseModalWait()
        if data.code~=0 then
            AlertWin.Open(data.msg)
        else
            self:Refresh()
            G_GotItemAlertWin:PopupOpen(data)
        end
    end
end

function FuLiWin:OnQiTianInfo(cmd, data)
    self:CloseModalWait()
    _qitianPanel:GetChild("n44").text = _day
    
    _mubiaoData = data
    _mubiaoList:RemoveChildrenToPool()
    if data.mubiaos ~= nil then
        for _i, _mubiao in ipairs(data.mubiaos) do
            local btn = _mubiaoList:AddItemFromPool()
            btn.title = _mubiao.name
            btn.data = _mubiao
            if _mubiao.statu == nil or _mubiao.statu == 0 then
                btn:GetController("c1").selectedIndex = 0
            else
                btn:GetController("c1").selectedIndex = 1
            end
            if _mubiao.max_count == nil or _mubiao.max_count == 0 then
                btn:GetController("c2").selectedIndex = 0
            else
                btn:GetController("c2").selectedIndex = 1
                local count = _mubiao.count == nil and 0 or _mubiao.count
                btn:GetChild("n36").text = "(".._mubiao.count.."/".._mubiao.max_count..")"
            end
        end
    end
    _mubiaoItemsList:RemoveChildrenToPool()
    if _mubiaoData.items ~= nil then
        for _i, _item in ipairs(_mubiaoData.items) do
            local btn = _mubiaoItemsList:AddItemFromPool()
            btn:GetChild("n39"):SetItem(_item)
            btn:GetChild("n40").text = _item.def.name.." x".._item.count
        end
    end
    
    local count = 0
    if data.mubiaos ~= nil then
        count = #(data.mubiaos)
    end
    _qitianPanel:GetChildInGroup(_jlGroup, "n30").text  = "("..data.complete_count.."/"..count..")"
    
    if data.statu ~= nil and data.statu== 2 then
        _qitianPanel:GetChildInGroup(_jlGroup, "n32").grayed = true
    else
        _qitianPanel:GetChildInGroup(_jlGroup, "n32").grayed = false
    end
end

function FuLiWin:OnClickLeft(context)
    if _day == 1 then
        return
    end
    _day = _day-1
    NetClient.Send(NetKeys.QCMD_QITIAN_VIEW, _day)
    self:ShowModalWait()
end

function FuLiWin:OnClickRight(context)
    if _day == 7 then
        return
    end
    _day = _day+1
    NetClient.Send(NetKeys.QCMD_QITIAN_VIEW, _day)
    self:ShowModalWait()
end

function FuLiWin:OnClickMuBiaoJianLi(context)
    NetClient.Send(NetKeys.QCMD_QITIAN_LINQU, _day)
    self:ShowModalWait()
end

function FuLiWin:OnMuBiaoLinQuReturn(cmd, data)
    if self.isShowing then
        self:CloseModalWait()
        if data.code~=0 then
            AlertWin.Open(data.msg)
        else
            self:Refresh()
--            G_GotItemAlertWin:PopupOpen(data)
        end
    end
end

function FuLiWin:OnHuoDongInfo(cmd, data)
    self:CloseModalWait()
    local hds = data.hds
    if hds ~= nil then
        for _i, _hd in ipairs(hds) do
            local btn = _hdList:AddItemFromPool()
            btn.title = _hd.name
            btn:GetChild("n7").text = _hd.time
            btn.data = _hd
        end
    end
end