HangOnBagWin = fgui.window_class(WindowBase)

local _typeTab
local _pagetab
local _itemList

local _uid
local _listData
local _btnBag
local _MAX_BAG = 200
local _MAX_PER_PAGE = 24

function HangOnBagWin:ctor()
	WindowBase.ctor(self)
	
	self.asyncCreate = true
	self:SetContentSource("Common","挂售背包")
end

function HangOnBagWin:DoInit()
	self:Center()

	_listData  = {}
	
	_typeTab=self.contentPane:GetController("type")
	_pagetab=self.contentPane:GetController("page")

	_itemList=self.contentPane:GetChild("list")
	_itemList:SetVirtual()
	_itemList.itemRenderer = function(index, obj) self.RenderListItem(self,index, obj) end
	_itemList.numItems = _MAX_BAG
	_itemList.scrollPane.onScrollEnd:Add(self.OnScrollEnd, self)
	_itemList.onClickItem:Add(self.OnClickCell, self)

	_btnBag = self.contentPane:GetChild("btnBag")
	_btnBag.onClick:Add(self.OnClickBag, self)
	
	--控制器初始化
	_typeTab.onChanged:Add(self.Refresh, self)
	_typeTab.selectedIndex=0
	
	NetClient.Listen(NetKeys.ACMD_LIST_USER_ITEMS, self.OnGotItems, self)
	NetClient.Listen(NetKeys.ACMD_VIEW_ITEM, self.OnGotItem, self)
	
	NetClient.ListenRet(NetKeys.QCMD_ADD_BURTHEN, self.DefaultQCMDHandler, self)
	NetClient.ListenRet(NetKeys.QCMD_VIEW_ITEM, self.DefaultQCMDHandler, self)
	NetClient.ListenRet(NetKeys.QCMD_SALE_ITEM_TO_NPC, self.DefaultQCMDHandler, self)
	NetClient.ListenRet(NetKeys.QCMD_DROP_ITEM, self.DefaultQCMDHandler, self)
end

function HangOnBagWin:UseItemQCMDHandler(cmd, data)
    if self.isShowing then
        if data.code==0 then
            self:Refresh()
        elseif data.msg ~= nil then
            AlertWin.Open(data.msg)
        end
    end
end

function HangOnBagWin:Open(uid)
    _uid = uid
    self:Show()
end

function HangOnBagWin:Refresh()
	if self.isShowing then
		NetClient.Send(NetKeys.QCMD_LIST_USER_ITEMS, _uid, _typeTab.selectedIndex)
		self:ShowModalWait()
	end
end

function HangOnBagWin:OnShown()
	self:Refresh()
end

function HangOnBagWin:RenderListItem(index, obj)
    index = index % _MAX_PER_PAGE
	obj:SetItem(_listData[index+1])
end

function HangOnBagWin:OnScrollEnd()
    if _pagetab.selectedIndex == _itemList.scrollPane.currentPageX then
        return
    end
	_pagetab.selectedIndex = _itemList.scrollPane.currentPageX
	self:Refresh()
end

function HangOnBagWin:OnGotItem(cmd, data)
    if self.isShowing then
    	self:CloseModalWait()
        G_ItemInfoWin:Popup()
        G_ItemInfoWin.data = data
    end
end

function HangOnBagWin:OnGotItems(cmd, data)
    if not self:CloseModalWait() then return end
    
    local money = data.money
    if money == nil then
        money = 0
    end
    self.contentPane:GetChild("money").text = money
    self.contentPane:GetChild("bur").text = data.bur.."/"..data.mbur
    
    _typeTab.selectedIndex = data.type
    _listData = data.items
    _itemList.numItems = data.mbur
    _itemList:RefreshVirtualList()
end

function HangOnBagWin:OnClickBag(context)
    local item = context.data
    self:Hide()
    if not G_BagWin.isShowing then
        G_BagWin:Show()
    end
end

function HangOnBagWin:OnClickCell(context)
	local item = context.data
	if item.data==nil then return end
	
	G_HangOnItemInfoWin:Popup2(_uid, item.data)
end