WeaponUpgradeWin = fgui.window_class(WindowBase)

local _pane
local _list
local _rid
local _c1

local _listData
local _data

function WeaponUpgradeWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common" , "兵器升级")
end

function WeaponUpgradeWin:DoInit()
    self:Center()
    
    _pane = self.contentPane
    _c1=_pane:GetController("c1")
    _list = _pane:GetChild("n2")
    _list.onClickItem:Add(self.OnWQItemClick, self)
    
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n65").onClick:Add(self.OnUpgradeClick, self)
    
    NetClient.Listen(NetKeys.ACMD_UPGRADE_WEAPONS, self.OnWeaponsView, self)
    NetClient.Listen(NetKeys.ACMD_UPGRADE_WEAPON_VIEW, self.OnWeaponView, self)
    
    NetClient.ListenRet(NetKeys.QCMD_UPGRADE_WEAPON, self.DefaultQCMDHandler, self)
end

function WeaponUpgradeWin:Open(rid)
    _rid = rid
    self:Show()
end

function WeaponUpgradeWin:OnShown()
    _c1.selectedIndex = 0
    self:RefreshList()
end

function WeaponUpgradeWin:RefreshList()
    NetClient.Send(NetKeys.QCMD_UPGRADE_WEAPONS, 0, _rid)
    self:ShowModalWait()
end

function WeaponUpgradeWin:OnWeaponsView(cmd, data)
    self:CloseModalWait()
    _listData = data
    self:ResetWeaponListInfo(_listData)
end

function WeaponUpgradeWin:ResetWeaponListInfo(data)
    _list:RemoveChildrenToPool()
    if _listData.items ~= nil then
        for _i, _item in ipairs(_listData.items) do
            local btn = _list:AddItemFromPool()
            local id = GetIdFromRid(_item.rid)
            btn:GetChild("icon").icon = Defs.GetIconSrc(id)
            btn:GetChild("title").text = _item.name
            btn:GetChild("lvl").text = _item.lvl
            btn.data = _item.rid
        end
    end
    _pane:GetChild("n4"):Clear()
end

function WeaponUpgradeWin:Refresh()
    if _listData ~= nil and #_listData.items > _list.selectedIndex and _list.selectedIndex ~= -1 then
        NetClient.Send(NetKeys.QCMD_UPGRADE_WEAPON_VIEW, _listData.items[_list.selectedIndex+1].rid)
        self:ShowModalWait()
    end
end

function WeaponUpgradeWin:OnWQItemClick(context)
    NetClient.Send(NetKeys.QCMD_UPGRADE_WEAPON_VIEW, 0, context.data.data)
    self:ShowModalWait()
end

function WeaponUpgradeWin:OnWeaponView(cmd, data)
    self:CloseModalWait()
    _c1.selectedIndex = 1
    _data = data
    self:ResetWeaponInfo()
end

function WeaponUpgradeWin:ResetWeaponInfo()
    local wd = {}
    wd.rid = _data.rid
    _pane:GetChild("n4"):SetItem(wd)
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n56").text = _data.name
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n66").text = _data.damage
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n67").text = _data.cz_damage
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n68").text = _data.need_money
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n79").text = _data.need_items
    local lvl = _data.lvl == nil and 0 or _data.lvl
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n82").text = lvl
    _pane:GetChildInGroup(_pane:GetChild("n75"), "n83").text = lvl+1
end

function WeaponUpgradeWin:OnUpgradeClick(context)
    NetClient.Send(NetKeys.QCMD_UPGRADE_WEAPON, 2, _rid)
    self:ShowModalWait()
end
