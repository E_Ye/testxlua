MallWin = fgui.window_class(WindowBase)


local _typeTab
local _pageList
local _productItems
local _btnDeposit --充值按钮
local _txtMoney
local _MAX_PER_PAGE = 8 
local initFlash = false
local currentTypeIndex = 0



local print_r = require("Test/print_r")

function MallWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common" , "商城")
end

function MallWin:DoInit()
    self:Center()

    _typeTab = self.contentPane:GetController("type")
	_typeTab.onChanged:Add(self.OnTabRefresh, self)
	_typeTab.selectedIndex = 0

	_productItemList = self.contentPane:GetChild("list")
	_productItemList:SetVirtual()
	_productItemList.itemRenderer = function(index, obj) self.RenderListItem(self,index, obj) end
	_productItemList.numItems = 0
	_productItemList.onClickItem:Add(self.OnClickItemCell, self)
	_productItemList.scrollPane.onScrollEnd:Add(self.OnScrollEnd, self)
	_productItemList.scrollPane.onScroll:Add(self.OnScroll, self)
    
	_pageList = self.contentPane:GetChild("page")
	_pageList.selectedIndex = 0 
	_btnDeposit = self.contentPane:GetChild("chongzhi")
	_txtMoney =self.contentPane:GetChild("n16")
	
	NetClient.Listen(NetKeys.ACMD_MALL_OPEN, self.OnMallOpenChanged, self)
	NetClient.ListenRet(NetKeys.QCMD_MALL_BUY, self.OnBuyHandler, self)
end

function MallWin:OnShown()
	self:OnTabRefresh()
end

function MallWin:OnHide()
	if G_BuyWinShangCheng.isShowing then
		G_BuyWinShangCheng:Hide()
	end
end

function MallWin:OnTabRefresh()
	_pageList.selectedIndex = 0

	currentTypeIndex = _typeTab.selectedIndex + 1
	-- _pageList
	if self.isShowing then
		self:GetMallBoard(currentTypeIndex)
		self:ShowModalWait(NetKeys.QCMD_MALL_OPEN)
	end
end

function MallWin:GetMallBoard(typeId)
	NetClient.Send(NetKeys.QCMD_MALL_OPEN, typeId)
end


function MallWin:OnMallOpenChanged(cmd, data)
	if not self:CloseModalWait(NetKeys.QCMD_MALL_OPEN) then return end
	print_r(data)
	self:UpdatePageCount(_pageList, #data.items, _MAX_PER_PAGE)

	_txtMoney.text = data.cash
	_productItems = data.items
	_productItemList.numItems = #_productItems
	_productItemList:RefreshVirtualList()
	_productItemList:AddSelection(0, true)
end

function MallWin:RenderListItem(index, obj)
	self:ResetProductItem(index, obj);
end


function MallWin:ResetProductItem(index,itemObj)
	local targetId = _productItems[index+1]
	
	itemObj.data = Defs.GetDef("item", targetId);
	itemObj.data.id = targetId
	itemObj:GetChild("n23").icon = Defs.GetIconSrc(targetId)
	
	if itemObj.data.name then
    	itemObj:GetChild("title").text = itemObj.data.name
	end
 	if itemObj.data.cash_value then
 		itemObj:GetChild("price").text = itemObj.data.cash_value.."仙币"--    
	end
		
end

function MallWin:OnClickItemCell(context)
	local item = context.data

	-- if not G_BuyWinShangCheng.isShowing then
		G_BuyWinShangCheng:Open(self)
		-- GRoot.inst:ShowPopup(G_BuyWinShangCheng)
	-- end

	G_BuyWinShangCheng:SetItem(item.data.id, item.data)
end

function MallWin:OnScrollEnd()
	
	if _pageList.selectedIndex == 
		_productItemList.scrollPane.currentPageX then
        return
    end

	_pageList.selectedIndex = _productItemList.scrollPane.currentPageX
	-- self:OnTabRefresh()
end

function MallWin:OnScroll()
	if G_BuyWinShangCheng.isShowing then
		G_BuyWinShangCheng:Hide()
	end
end

function MallWin:UpdatePageCount(pageList, productCount, everyPageCount)

	local pageCount = productCount/everyPageCount
	if productCount%everyPageCount > 0 then
		pageCount = pageCount + 1
	end
	pageList:RemoveChildrenToPool()
	for i=1,pageCount do
		pageList:AddItemFromPool()
	end
	pageList.selectedIndex = 0
end

function MallWin:DoBuy(iid, count)
	NetClient.Send(NetKeys.QCMD_MALL_BUY, iid, count)
end

function MallWin:OnBuyHandler(cmd, data)
	-- print_r(data)
	if data.code == 0 then
		if G_BuyWinShangCheng.isShowing then
			G_BuyWinShangCheng:Hide()
		end
	end
	AlertWin.Open(data.msg)
end