FriendsMode={
	inst,
	caller,
	friendsItems = {data,count=0},
	mailItems = {},
	data = {}

}


local print_r = require("Test/print_r")

local _friendItemUrl
local _mailItemUrl

local _friendsList
local _typeTab
local _mailDetail
local _mailDetailItemList
local _mailDetailTab
local _inputTxt
local _inputSearchTxt 
local _searchBtn
local _clearBtn

local initLoadCount = 4
local everyLoadCount = 8
local currentLastIndex = 0
local currentLoadTotal = 0

function FriendsMode:Init(_caller)
	if self.inst == nil then
		self.inst = self
 		self.caller = _caller

 		_friendItemUrl = UIPackage.GetItemURL("Common", "好友item")
 		_mailItemUrl = UIPackage.GetItemURL("Common", "邮件item")

 		_typeTab = self.caller.contentPane:GetController("haoyou")
    	_typeTab.onChanged:Add(self.OnTabChanged, self)
    	
    	_mailDetail = self.caller.contentPane:GetChild("n38")
    	_mailDetailTab = self.caller.contentPane:GetController("mail_detail") 
    	_mailDetailItemList = _mailDetail:GetChild("n44")
    	-- _mailDetailTab.onChanged:Add(self.OnMailTabChanged, self)
    	
    	_friendsList = self.caller.contentPane:GetChild("friendsList")
    	_friendsList:SetVirtual()
		_friendsList.itemRenderer = function(index, obj) self.RenderListItem(self,index, obj) end
    	_friendsList.itemProvider = function(index) return self:GetListItem(index) end
		
		-- _friendsList.numItems = initLoadCount
	    -- _friendsList.scrollPane.onScrollEnd:Add(self.OnScrollEnd,self)
    	
    	_inputTxt = self.caller.contentPane:GetChild("input")
    	_inputSearchTxt = self.caller.contentPane:GetChildInGroup(self.caller.contentPane:GetChild("n37"), "input")
    	_searchBtn = self.caller.contentPane:GetChild("n36")
    	_searchBtn.onClick:Add(self.OnSearchBtnClick, self)
    	_clearBtn = self.caller.contentPane:GetChild("n35")
    	_clearBtn.onClick:Add(self.OnClearBtnClick, self)


    	NetClient.Listen(NetKeys.ACMD_FRIENDS, self.OnGotFriendsList, self)
    	NetClient.Listen(NetKeys.ACMD_FIND_USER, self.OnGotFindUserList, self)
    	NetClient.Listen(NetKeys.ACMD_MAILS, self.OnGotMailsList, self)
 		NetClient.ListenRet(NetKeys.QCMD_FRIEND_ADD, function ()
 			self:Operation(_typeTab.selectedIndex)
 		end, self)
    	NetClient.ListenRet(NetKeys.QCMD_FRIEND_REMOVE, function ()
 			self:Operation(_typeTab.selectedIndex)
 		end, self)
    -- self.contentPane:GetChild("sendButton").onClick:Add(ChatWin.OnClickSend, self)
 		-- print("...".. _typeTab.selectedIndex)
	end
end

function FriendsMode:Refresh(_caller)
 	self:Init(_caller)
 	self:Operation(_typeTab.selectedIndex)
end

function FriendsMode:OnTabChanged(context)
	self:Operation(_typeTab.selectedIndex)
end

function FriendsMode:Operation(tabIndex)
	-- print(">>>>BangPaiWin:Operation..tab:"..tabIndex)
	if tabIndex == 0 then
		-- print(">>>好友")
		self:GetFriends(tabIndex)
	
	elseif tabIndex == 1 then
		-- print(">>>黑名单")
		self:GetFriends(tabIndex)
	
	elseif tabIndex == 2 then
		-- print(">>>仇人")
		self:GetFriends(tabIndex)

	elseif tabIndex == 3 then
		-- print(">>>对手")
		self:GetFriends(tabIndex)
	
	elseif tabIndex == 4 then
		_friendsList.numItems = 0 
		_inputSearchTxt.text = string.Empty
	
	elseif tabIndex == 5 then
		-- print(">>>邮箱")
		self:GetMails(1,0,0)
		
	end
end



function FriendsMode:OnGotFriendsList(cmd, data)
	if not self.caller:CloseModalWait(NetKeys.QCMD_FRIENDS) then return end
	-- print_r(data)
	
	local count = #data.ls
	self.friendsItems.count = count
	self.friendsItems.data = data
	-- print_r(self.friendsItems.data)

	_friendsList.numItems = count
	-- currentLastIndex = _logList.numItems;
end

function FriendsMode:OnGotFindUserList(cmd, data)
	if not self.caller:CloseModalWait(NetKeys.QCMD_FIND_USER) then return end
	-- print_r(data)
	local count = #data.ls
	self.friendsItems.count = count
	self.friendsItems.data = data
	_friendsList.numItems = count
	if count == 0 then
		AlertWin.Open("该玩家不存在。")
	end
	-- currentLastIndex = _logList.numItems;
end

function FriendsMode:OnGotMailsList(cmd, data)
	if not self.caller:CloseModalWait(NetKeys.QCMD_MAILS) then return end
	-- data = {mails={{id=1,sub="ssssss",stm="2017 12 6",sta=0,eta=1,txt="dcvxcvxcvxvxcvcxvxcvcx"}}}
	-- print(#data.mails)
	self.friendsItems.count = #data.mails
	self.friendsItems.data = data
	_friendsList.numItems = #data.mails
	-- currentLastIndex = _logList.numItems;
end

function FriendsMode:RenderListItem(index, obj)
	self:ResetItem(index, obj)
end

function FriendsMode:GetListItem(index)
	local data = self.friendsItems.data
	
	if data then
		if data.mails then
			return _mailItemUrl
		elseif data.ls then
			return _friendItemUrl
		end
	end
end

function FriendsMode:SetData(data)
	for i,v in ipairs(data) do
		self.friendsItems[currentLoadTotal + i] = v	
	end
end

function FriendsMode:ResetItem(index,itemObj)
	local  itemData 
	if self.friendsItems.count > 0 then
		if self.friendsItems.data.ls then
			itemData = self.friendsItems.data.ls[index + 1]
		elseif self.friendsItems.data.mails then
			itemData = self.friendsItems.data.mails[index + 1]
		end
		itemObj.data = itemData
		if _typeTab.selectedIndex ~=5 then
			
			itemObj:GetChild("head").icon = UIPackage.GetItemURL("Sucai", 'u'..itemData.fct..itemData.sex)
	    	itemObj:GetChild("head").text = itemData.lvl --等级
	    	itemObj:GetChild("name").text = itemData.name --角色名称
	    	itemObj:GetChild("line").text = itemData.line --所在线
	    	itemObj:GetChild("head").onClick:Add(self.OnItemHeadClick, self)
			-- itemObj.onClick:Add(self.OnFriendItemClick, self)
		
		elseif _typeTab.selectedIndex == 5 then
	    	
	    	itemObj:GetController("read").selectedIndex = itemData.sta
	    	itemObj.onClick:Add(self.OnMailItemClick, self)
	    	itemObj:GetChild("name").text = string.format("%s %s", itemData.sub, itemData.stm)

		
		end	
	end
end

function FriendsMode:OnItemHeadClick(context)
	-- print(">>>..click head")
	if context.sender.parent.data.id ~= Player.data.id then
		G_PlayerInfoQueryWin:Open(context.sender.parent.data, 0)
	end
end
function FriendsMode:OnFriendItemClick(context)
	-- print(">>>..click head")
	-- if context.sender.data.id ~= Player.data.id then
	-- 	G_PlayerInfoQueryWin:Open(context.sender.data, 0)
	-- end
end
function FriendsMode:OnMailItemClick(context)
	local data = context.sender.data

	_mailDetailTab.selectedIndex = 1
	_mailDetail:GetChild("n39").title = data.txt
	_mailDetailItemList:RemoveChildrenToPool()
	for i,v in ipairs(data.items) do
		-- print(i,v)
		-- _mailDetailItemList:RemoveChildrenToPool()
		_mailDetailItemList:AddItemFromPool()
	end
	-- self:SetMailstatus()
end

function FriendsMode:OnClearBtnClick(context)
	local msg
	local data = self.friendsItems

	if _typeTab.selectedIndex == 2 then
		msg = "是否一键清空仇人列表？"
	elseif _typeTab.selectedIndex == 3 then 
		msg = "是否一键清空对手列表？" 
	end
	
	ConfirmWin.Open(msg, function()
		for i,v in ipairs(data) do
			self:ClearKillUser(_typeTab.selectedIndex, v.id)
		end
		
	end,function()
		
	end)
end

function FriendsMode:OnSearchBtnClick(context)
	self:FindUser(0, _inputSearchTxt.text)
end

function FriendsMode:OnScrollEnd(context)
	-- if _friendsList.scrollPane.isBottomMost then
	-- 	if _friendsList.numItems >= initLoadCount then
	-- 		self:GetClanLogs(currentLastIndex, everyLoadCount)
	-- 	end
	-- end
end



--0-好友，1-黑名单，2-仇人，3-对手
function FriendsMode:GetFriends(type)
	NetClient.Send(NetKeys.QCMD_FRIENDS, type)	
	if self.caller then self.caller:ShowModalWait(NetKeys.QCMD_FRIENDS) end
end

--获取Mail 0-仅新邮件，1-所有邮件
function FriendsMode:GetMails(type, minId, count)
	NetClient.Send(NetKeys.QCMD_MAILS, type, minId, count)	
	if self.caller then self.caller:ShowModalWait(NetKeys.QCMD_MAILS) end
end

--发送Mail
function FriendsMode:SendMail(type, uid, sub, content)
	NetClient.Send(NetKeys.QCMD_MAIL_SEND, uid, sub, content)	
	if self.caller then self.caller:ShowModalWait(NetKeys.QCMD_MAIL_SEND) end
end

--设置邮件状态 0-已读，1-删除
function FriendsMode:SetMailstatus(type, _list)
	NetClient.Send(NetKeys.QCMD_MAIL_SET_STATUS, type, _list)	
	if self.caller then self.caller:ShowModalWait(NetKeys.QCMD_MAIL_SET_STATUS) end
end

--查找玩家 type 0-所有，1-男，2-女 
function FriendsMode:FindUser(type, name)
	NetClient.Send(NetKeys.QCMD_FIND_USER, type, name)	
	if self.caller then self.caller:ShowModalWait(NetKeys.QCMD_FIND_USER) end
end

--清空玩家 type 2-仇人，3-对手
function FriendsMode:ClearKillUser(type, uid)
	NetClient.Send(NetKeys.QCMD_FRIEND_REMOVE, type, uid)	
	if self.caller then self.caller:ShowModalWait(NetKeys.QCMD_FRIEND_REMOVE) end
end
