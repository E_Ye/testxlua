ChatWin = fgui.window_class(WindowBase)
require "Windows/ChatAndFriends/FriendsMode"

local _list
local _msgCache = {}
local _channel
local _curListData
local _item1Url
local _item2Url
local _item3Url
local _speekBtn
local _touid
local _touser
local _tempChatUid
local _tempChatUser

local _enableSpeak
local _channelTab
local _maxCache = 10

function ChatWin:ctor()
    WindowBase.ctor(self)

    self:SetContentSource("Common", "聊天面板")
    self.asyncCreate = false
    self.animation = {"move_right", "move_left"}

    _item1Url = UIPackage.GetItemURL("Common", "聊天item")
    _item2Url = UIPackage.GetItemURL("Common", "聊天item2")
    _item3Url = UIPackage.GetItemURL("Common", "聊天item3")

    for i=0,5 do
    	_msgCache[i] = {}
    end
    _channel = 1
    _curListData = _msgCache[_channel]
end

function ChatWin:DoInit()
    self:SetXY(0, 10)
    self.contentPane:GetChild("closeButton").onClick:Add(function() self:Hide() end)
    _channelTab = self.contentPane:GetController("channel")
    _channelTab.onChanged:Add(ChatWin.OnChannelChanged, self)
    self.contentPane:GetChild("sendButton").onClick:Add(ChatWin.OnClickSend, self)
    self.contentPane:GetController("mode").onChanged:Add(ChatWin.OnModeChanged, self)
    _list = self.contentPane:GetChild("list")
    _list.itemRenderer = function(index, item) self:RenderListItem(index, item) end
    _list.itemProvider = function(index) return self:GetListItem(index) end
    _list.onClickItem:Add(ChatWin.OnClickMessage, self)
    _list:SetVirtual()

    _speekBtn = self.contentPane:GetChild("speakButton")
	_speekBtn.onTouchBegin:Add(ChatWin.StartRecord)
	_speekBtn.onTouchEnd:Add(ChatWin.StopRecord)
	self:UpdateSpeakStatus()

    NetClient.Listen(NetKeys.ACMD_TEAM_CHANGE, self.OnTeamChange, self)
    NetClient.Listen(NetKeys.ACMD_CHANGE_ATTRS2, self.OnClanChange, self)
end

-- 0-世界 1-私聊 2-队伍 3-帮派 4-系统 5-综合 
function ChatWin:Open(channel, touid, touser)
    if touid then _touid = touid  end
    if touser then _touser = touser end
    if channel then _channel = channel end
	self:Show()
	if touid then _channelTab.selectedIndex = 1 end
    self.contentPane:GetController("mode").selectedIndex = 0
    self:Refresh()
end

function ChatWin:OpenFriends()
	self:Show()
	self.contentPane:GetController("mode").selectedIndex = 1
	FriendsMode:Refresh(self)
end

function ChatWin:Refresh()
	_list.numItems = #_curListData
	_list.scrollPane:ScrollBottom(true)

	self.contentPane:GetChild("input").text = ""

	if string.isNilOrEmpty(Player.data.team) then
		self.contentPane:GetChild("n3").enabled = false
	end

	if string.isNilOrEmpty(Player.data.clan) then
		self.contentPane:GetChild("n4").enabled = false
	end
	
    self.contentPane:GetChild("n2").enabled = _touid == nil and false or true
    local index = self.contentPane:GetController("channel").selectedIndex
	-- if _channel==0 then
	-- 	index=1
	-- elseif _channel==1 then
	-- 	index=0
	-- elseif _channel==2 then
	-- 	index=3
	-- elseif _channel==3 then
	-- 	index=2
 --    elseif _channel == 4 then
 --        index = 4
 --    elseif _channel == 5 then
 --        index = 5
	-- end
	self:DoChannelChanged(index)
end

function ChatWin:OnShown()
	-- self:Refresh()
end

function ChatWin:DisableSpeak()
	if _speekBtn == nil then
		return
	end
	_enableSpeak = false
	self:UpdateSpeakStatus()
end

function ChatWin:EnableSpeak()
	if _speekBtn == nil then
		return
	end
	_enableSpeak = true
	self:UpdateSpeakStatus()
end

function ChatWin.UpdateSpeakStatus()
	_speekBtn.grayed = not _enableSpeak
	_speekBtn.touchable = _enableSpeak
end

function ChatWin:OnChannelChanged(context)
	local index = context.sender.selectedIndex
	self:DoChannelChanged(index)
end

function ChatWin:OnModeChanged(context)
	if context.sender.selectedIndex == 1 then 
		FriendsMode:Refresh(self)
	end
end

function ChatWin:DoChannelChanged(index)
	local ch 

	if index==0 then
		ch=1
	elseif index==1 then
		ch=0
	elseif index==2 then
		ch=3
	elseif index==3 then
		ch=2
    elseif index == 4 then
        ch = 4
    elseif index == 5 then
        ch = 5
	end

	if ch == _channel and ch~=0 then
		return
	end

	_channel = ch

	self:ShowModalWait()
	self:EnableSendMsg()
	if _channel == 1 then
		Voice.JoinChannel("世界")
	elseif _channel == 0 then
		Voice.JoinChannel("世界")
		self.contentPane:GetChild("n21").text = string.format("和%s交谈中", _touser == nil and "" or "<font color='#75ACD6'>[".._touser.."]</font>")
	elseif _channel == 3 then
		Voice.JoinChannel(Player.data.team)
	elseif _channel == 2 then
		Voice.JoinChannel(Player.data.clan)
	elseif _channel == 5 or _channel == 4 then
		self:DisableSendMsg()
		ChatWin.OnJoinChannel()
	end
end

function ChatWin.OnJoinChannel()
	G_ChatWin:CloseModalWait()
	if _touid == nil then _touid = _tempChatUid _touser = _tempChatUser end
	if _channel == 0 then
		_curListData = _msgCache[_channel][_touid] or {}
	else
		_curListData = _msgCache[_channel]
	end

	if _list ~= nil then
		_list.numItems = #_curListData
		_list.scrollPane:ScrollBottom(true)
	end
end

function ChatWin:OnClickSend()
	local msg = self.contentPane:GetChild("input").text
	if string.len(msg)==0 then return end
	NetClient.Send(NetKeys.QCMD_CHAT_MSG, _channel, _touid == nil and "" or _touid, msg)
end

function ChatWin:AddMsg(data)
 --    if data.type == 0 then
	-- 	_touid = data.tid
	--     _touser = data.fname
	-- end
	-- table.insert(_msgCache[data.type], data)
	-- table.insert(_msgCache[5], data)--综合

	if data.type == 0 then
		if data.fid ~= Player.data.id then
			-- _touid = data.fid
			-- _touser = data.fname
			_tempChatUid = data.fid
			_tempChatUser = data.fname
	  	else
			_tempChatUid = data.tid
			_tempChatUser = data.fname
		end
		--私聊时记录使用对方玩家id做key存储
	    _msgCache[data.type][_tempChatUid] = _msgCache[data.type][_tempChatUid] or {}
	    
	    --私聊的记录大于时清除聊天
	    if #_msgCache[data.type][_tempChatUid] > _maxCache then _msgCache[data.type][_tempChatUid] = {} end
	  
	    table.insert(_msgCache[data.type][_tempChatUid], data)
	else
		table.insert(_msgCache[data.type], data)
	end
	
	table.insert(_msgCache[5], data)--综合

	if _list == nil then return end
	if data.self then self.contentPane:GetChild("input").text="" end
	
	if _channel == data.type then
		if data.type == 0 and _tempChatUid == _touid then
			_curListData = _msgCache[data.type][_touid]
		elseif data.type ~= 0 then
			_curListData = _msgCache[data.type]
		end

		local oldPos = _list.scrollPane.percY
		_list.numItems = #_curListData
		if oldPos==1 then _list.scrollPane:ScrollBottom(true) end
	end
end

function ChatWin:RenderListItem(index, item)
	local data = _curListData[index+1]
	item.data = data

	if item.resourceURL==_item1Url or item.resourceURL==_item2Url then
		local icon = item:GetChild("icon")
		icon.text = data.flvl
		icon.icon = UIPackage.GetItemURL("Sucai", 'u'..data.ffct..data.fsex)
	end
	if item.resourceURL==_item1Url then
		local name = item:GetChild("sender")
		name.text = data.fname
	end

	local tt = item:GetChild("msg")
	tt.width = tt.initWidth
	if data.message ~= nil then
	    local second = math.floor(data.message.Duration / 1000)
		tt.text = "<img src='ui://Sucai/voice'/>     <font color='#aaaaaa'>"..second.."''</font>"
	else
		tt.text = data.msg
	end
	tt.width = tt.textWidth
end

function ChatWin:OnClickMessage(context)
	local item = context.data
	if item.data.message~=nil then --voice
		Voice.PlayItem(item)
	end
end

function ChatWin:GetListItem(index)
	local data = _curListData[index+1]
	if data then
		if string.isNilOrEmpty(data.fname) then
			return _item3Url
		elseif data.self then
			return _item2Url
		else
			return _item1Url
		end
	end
end

function ChatWin.StartRecord()
	Voice.StartRecord(_currentChannel)
end

function ChatWin.StopRecord()
	Voice.StopRecord("{\"channel\":".._channel.."}", _touid)
end

function ChatWin:OnTeamChange(cmd, data)
    if data.uid ~= Player.data.id then
    	return
    end
    local type = data.type
    if type == 0 or type == 1 then
		self.contentPane:GetChild("n3").enabled = true
	elseif type == 2 or type == 3 or type == 4 then
		self.contentPane:GetChild("n3").enabled = false
	    local c = self.contentPane:GetController("channel")
	    if c.selectedIndex == 2 then
	    	c.selectedIndex = 0
	    end
    end
end

function ChatWin:OnClanChange(cmd, data)
    if data.id ~= nil and data.id ~= "" and data.id ~= Player.data.id then
    	return
    end
	for k,v in pairs(data.attrs) do
		if k=="clan" then
			if v == nil or v == "" then
				self.contentPane:GetChild("n4").enabled = false
			else
				self.contentPane:GetChild("n4").enabled = true
			end
		end
	end
end

function ChatWin:EnableSendMsg()
	self.contentPane:GetChild("sendButton").enabled = true
end

function ChatWin:DisableSendMsg()
	self.contentPane:GetChild("sendButton").enabled = false
end