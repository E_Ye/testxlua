CorpseWin = fgui.window_class(WindowBase)

local _list
local _corpse

function CorpseWin:ctor()
	WindowBase.ctor(self)

    self:SetContentSource( "Common", "物品掉落" )
end

function CorpseWin:DoInit()
	_list = self.contentPane:GetChild( "list" )
	_list.onClickItem:Add(self.OnClickCell, self)
	self.contentPane:GetChild( "btn" ).onClick:Add(self.OnClickPickAll, self)

	self:Center()
	
	NetClient.Listen(NetKeys.ACMD_PICK_ITEM, self.HandlePickItem, self)
end

function CorpseWin:Open(corpse)
	_corpse = corpse.data
	if self.isShowing then
		self:Update()
	else
		self:Show()
	end
end

function CorpseWin:OnShown()
    for i=0, 5 do
        _list:GetChildAt(i):SetItem(nil)
    end
	self:Update()
end

function CorpseWin:Update()
	local title
	if _corpse.name then
		title = _corpse.name..'的尸体'
	elseif _corpse.org_rid then
		title = Defs.GetString("robot",  GetIdFromRid(_corpse.org_rid), "name", "怪物")..'的尸体'
	else
		title = '怪物的尸体'
	end
	self.contentPane:GetChild( "title" ).text = title

	local i = 0
	for k,v in pairs(_corpse.items) do
		if i<6 then
		    local iv = {}
		    iv.rid = k
		    iv.count = v
			_list:GetChildAt(i):SetItem(iv)
			i = i+1
		end
	end
end

function CorpseWin:OnHide()
end

function CorpseWin:OnClickCell(context)
	local item = context.data
	if item.data~=nil then
		NetClient.Send(NetKeys.QCMD_PICK_ITEM, _corpse.rid, item.data.id)
	end
end

function CorpseWin:OnClickPickAll()
	NetClient.Send(NetKeys.QCMD_PICK_ITEM, _corpse.rid, '*')
	self:Hide()
end

function CorpseWin:HandlePickItem(cmd, data)
    if data == nil then
        return
    end
    
    
    local cc = 0
    for count = 0, 5 do
        local item = _list:GetChildAt(count)
        if item ~= nil and item.data ~= nil and item.data.iid ~= nil then
            if data[item.data.iid] ~= nil then
                item:SetItem(nil)
            else
                cc = cc+1
            end
        end
    end
    
    if cc == 0 then
        self:Hide()
    end
end
