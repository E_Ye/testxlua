TaskWin = fgui.window_class(WindowBase)

local _list
local _msgText
local _nameText
local _linkList
local _selectedId
local _task

function TaskWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = false
    
    self:SetContentSource("Common" , "TaskWin")
end

function TaskWin:DoInit()
    self:Center()

    _list = self.contentPane:GetChild("list")
    _list.onClickItem:Add(self.OnClickTaskItem, self)
    _msgText = self.contentPane:GetChild("msg")
    _linkList = self.contentPane:GetChild('links')
    _linkList.onClickItem:Add(self.OnClickItem, self)
    _nameText = self.contentPane:GetChild("name")
    self.contentPane:GetChild("btnGo").onClick:Add(self.OnClickGo, self)

    NetClient.Listen(NetKeys.ACMD_TASKS, self.OnTasks, self)
end

function TaskWin:Open(tid)
    _selectedId = tid
    self:Show()
end

function TaskWin:OnTasks(cmd, data)
    self:CloseModalWait()

    _list:RemoveChildrenToPool()
    _task = nil
    for _,task in ipairs(data.tasks) do
        local taskItem = _list:AddItemFromPool()
        taskItem:GetChild("n4").text = task.name
        taskItem.data = task
        if task.id==_selectedId then
            _task = task
            break
        end
    end

    if _task==nil then
        for _,task in ipairs(data.tasks) do
            if task.name=='主线任务' then
                _task = task
                break
            end
        end
    end

    self:ResetTask()
end

function TaskWin:ResetTask()
    if _task==nil then return end

    _nameText.text = _task.name
    _msgText.text = EncodeHTML(_task.cmmt)

    _linkList:RemoveChildrenToPool()
    if _task.targets then
        for i,target in ipairs(_task.targets) do
            local item = _linkList:AddItemFromPool()
            item.title = '寻找'..target.name
        end
    end
    _linkList:ResizeToFit(100000)
end

function TaskWin:OnShown()
    self:ShowModalWait()
    NetClient.Send(NetKeys.QCMD_TASKS)
end

function TaskWin:OnHide()
    G_NPCShopWin:Hide()
end

function TaskWin:OnClickItem(context)
    local link = context.data
    local target = _task.targets[_linkList:GetChildIndex(link)+1]
    AutoRouting.Start(target.eid, target)
    self:Hide()
end

function TaskWin:OnClickGo(context)
    if _task == nil then
        return
    end

    if Bot.active then Bot.Stop() end
    if  _task.targets then
        local target = _task.targets[1]
        if #_task.targets > 1 and _task.ready ~= nil and _task.ready == false then
            target = _task.targets[2]
        end
        AutoRouting.Start(target.eid, target)
    end
    self:Hide()
end

function TaskWin:OnClickTaskItem(context)
    _task = context.data.data
    self:ResetTask()
end