ItemCell = fgui.extension_class(GLabel)

function ItemCell:ctor()
    self.bang = self:GetChild("bang")
    self.bang.visible = false
    self.count = self:GetChild("count")
end

function ItemCell:Clear()
    self.data = nil
    self.icon = nil
    self.count.text = ""
    self.bang.visible = false
end

function ItemCell:SetItem(data)
	if data~=nil then
		if type(data)=="string" then
			self.data = { rid=data }
		else
			self.data = data
		end
		if self.data.id==nil then
			self.data.id = GetIdFromRid(self.data.rid)
		end
		if self.data.def==nil then
			--同时兼容物品
            local s = string.sub(self.data.id, 1, 1)
            if s=="i" or self.data.id == "money" then
                 self.data.def = Defs.GetDef("item", self.data.id)
            elseif s == "p" then
                self.data.def = Defs.GetDef("pet", self.data.id)
            end
		end
		self.icon = Defs.GetIconSrc(self.data.id)
		if self.data.count~=nil and self.data.count > 1 then
			self.count.text = self.data.count
		else
			self.count.text = ""
		end
		if self.data.def.bd then
		    self.bang.visible = true
		else
		    self.bang.visible = false
		end
		
		if self.data.sign == nil or self.data.sign == 0 then
		    self:GetController("sign").selectedIndex = 0
		elseif self.data.sign == 1 or self.data.sign == 2 then
		    self:GetController("sign").selectedIndex = 2
		end
	else
		self:Clear()
	end
end

function ItemCell:AddCount(num)
    if self.data.count ~= nil and self.data.count > 0 then
        self.data.count = self.data.count+num
        if self.data.count <= 0 then
            self.data.count = 0
            self.count.text = ""
        else
            self.count.text = self.data.count
        end
    end
    
    return self.data.count
end