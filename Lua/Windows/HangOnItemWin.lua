HangOnItemWin = fgui.window_class(WindowBase)

local _panel
local _data

function HangOnItemWin:ctor()
    WindowBase.ctor(self)

    self:SetContentSource("Common","挂售")
end

function HangOnItemWin:DoInit()
    self:Center()
    
    _panel = self.contentPane
    _panel:GetChild("btnOk").onClick:Add(self.OnClickHangOn, self)
    
    NetClient.ListenRet(NetKeys.QCMD_HANG_ON_ITEM, self.OnHangOnItemReturn, self)
end

function HangOnItemWin:OnHangOnItemReturn(cmd, data)
    if self.isShowing then
        self:CloseModalWait()
        if data.code~=0 then
            AlertWin.Open(data.msg)
        end
        self:Hide()
        if G_ItemInfoWin.isShowing then
            G_ItemInfoWin:Hide()
        end
    end
end

function HangOnItemWin:ResetItemInfo(data)
    if data == nil then
        return
    end
    _data = data
    _panel:GetController("c1").selectedIndex = 1
    _panel:GetChild("cell"):SetItem(data)
    _panel:GetChildInGroup(_panel:GetChild("n21"), "itemName").text = data.def.name
    _panel:GetChildInGroup(_panel:GetChild("n21"), "ownCount").text = data.count == nil and 1 or data.count
end

function HangOnItemWin:OnClickHangOn()
    local count = _panel:GetChild("n27").text
    if count == nil or count == "" then
        return
    end
    
    NetClient.Send(NetKeys.QCMD_HANG_ON_ITEM, _data.rid, tonumber(count), 0)
    self:ShowModalWait()
end