PlayerInfoQueryWin = fgui.window_class(WindowBase)

local _data
local _type
local c1
local c2
local c3
local chatBtn1
local chatBtn2

function PlayerInfoQueryWin:ctor()
    WindowBase.ctor(self)
    
    self:SetContentSource("Common" , "查看资料界面")
end

function PlayerInfoQueryWin:Open(data, type)
    _data = data
    -- 0- 在排行榜、好友、帮派、队伍等界面点击玩家头像
    -- 1- 点击地图玩家或直接点击队伍中玩家
    _type = type 
    self:Popup()
end

function PlayerInfoQueryWin:DoInit()
    local group = self.contentPane:GetChild("n29")
    self.contentPane:GetChildInGroup(group, "n19").onClick:Add(self.OnQueryClick, self)
    self.contentPane:GetChildInGroup(group, "n20").onClick:Add(self.OnGiveItemClick, self)
    self.contentPane:GetChildInGroup(group, "n21").onClick:Add(self.OnGiveMoneyClick, self)
    self.contentPane:GetChildInGroup(group, "n22").onClick:Add(self.OnAddFriendClick, self)
    self.contentPane:GetChildInGroup(group, "n23").onClick:Add(self.OnDelFriendClick, self)
    self.contentPane:GetChildInGroup(group, "n24").onClick:Add(self.OnQueryTeamClick, self)
    self.contentPane:GetChildInGroup(group, "n25").onClick:Add(self.OnBuyItemClick, self)
    chatBtn1 = self.contentPane:GetChildInGroup(group, "n26")
    chatBtn1.onClick:Add(self.OnChatClick, self)
    self.contentPane:GetChildInGroup(group, "n27").onClick:Add(self.OnBlackListClick, self)
    
    group = self.contentPane:GetChild("n36")
    self.contentPane:GetChildInGroup(group, "n30").onClick:Add(self.OnQueryClick, self)
    self.contentPane:GetChildInGroup(group, "n31").onClick:Add(self.OnQueryPetClick, self)
    self.contentPane:GetChildInGroup(group, "n32").onClick:Add(self.OnAddFriendClick, self)
    self.contentPane:GetChildInGroup(group, "n33").onClick:Add(self.OnQueryTeamClick, self)
    self.contentPane:GetChildInGroup(group, "n34").onClick:Add(self.OnBuyItemClick, self)
    chatBtn2 = self.contentPane:GetChildInGroup(group, "n35")
    chatBtn2.onClick:Add(self.OnChatClick, self)
    
    c1 = self.contentPane:GetController("c1")
    c2 = self.contentPane:GetController("c2")
    c3 = self.contentPane:GetController("c3")
    
    self:Center()
    
    NetClient.ListenRet(NetKeys.QCMD_GIVE_MONEY, self.DefaultQCMDHandler, self)
    NetClient.ListenRet(NetKeys.QCMD_CLICK_HEAD, self.DefaultQCMDHandler, self)
    NetClient.ListenRet(NetKeys.QCMD_FRIEND_ADD, self.OnReciveAddFriend, self)
    NetClient.ListenRet(NetKeys.QCMD_FRIEND_REMOVE, self.OnReciveAddFriend, self)
    
    NetClient.Listen(NetKeys.ACMD_CLICK_HEAD, self.OnClickHeadReturn, self)
end

function PlayerInfoQueryWin:OnShown()
    c1.selectedIndex = _type
    self.contentPane:GetChild("n9").icon = UIPackage.GetItemURL("Sucai", 'u'.._data.fct.._data.sex)
    self.contentPane:GetChild("n9").title = _data.lvl
    self.contentPane:GetChild("n10").text = _data.name
    
    if self.vars.alertSound == nil then
        self.vars.alertSound = UIPackage.GetItemAsset("Sucai", "alert")
    end
    GRoot.inst:PlayOneShotSound(self.vars.alertSound)
    
    NetClient.Send(NetKeys.QCMD_CLICK_HEAD, _data.id)
    self:ShowModalWait()
end

function PlayerInfoQueryWin:OnQueryClick(context)
   	self:Hide()
   	G_MyInfoWin:Open(_data.id)
end

function PlayerInfoQueryWin:OnGiveItemClick(context)
    self:Hide()
    G_BagWin:Open(1)
    G_GiveItemWin:Open(_data.id)
    G_GiveItemWin.x = 0--G_BagWin.x - G_GiveItemWin.contentPane.width/2
    --G_BagWin.x = G_BagWin.x + G_GiveItemWin.contentPane.width/2
end

function PlayerInfoQueryWin:OnGiveMoneyClick(context)
    self:Hide()
    G_GongXianWin:InternalOpen("", Defs.GIVE_MONEY, nil, PlayerInfoQueryWin.OnClickGiveMoneyOK)
end

function PlayerInfoQueryWin.OnClickGiveMoneyOK(num)
    NetClient.Send(NetKeys.QCMD_GIVE_MONEY, _data.id, num)
    G_PlayerInfoQueryWin:ShowModalWait()
end

function PlayerInfoQueryWin:OnAddFriendClick(context)
    if c2.selectedIndex == 0 then
        NetClient.Send(NetKeys.QCMD_FRIEND_ADD, 0, _data.id)
    else
        NetClient.Send(NetKeys.QCMD_FRIEND_REMOVE, 0, _data.id)
    end
    self:ShowModalWait()
end
function PlayerInfoQueryWin:OnReciveAddFriend(cmd,data)
    
    if self.isShowing then
        NetClient.Send(NetKeys.QCMD_CLICK_HEAD, _data.id)

        self:CloseModalWait()
        if data.code~=0 then
            AlertWin.Open(data.msg)
        else
            if data.msg then  
                ChatPanel.AddAlertMsg(data.msg) 
            end
            self:Refresh()
        end
    end
end

function PlayerInfoQueryWin:OnDelFriendClick(context)
    NetClient.Send(NetKeys.QCMD_FRIEND_REMOVE, 0, _data.id)
    self:ShowModalWait()
end

function PlayerInfoQueryWin:OnQueryTeamClick(context)
    self:Hide()
    G_TeamWin:Open(_data.id)
end

function PlayerInfoQueryWin:OnBuyItemClick(context)
    self:Hide()
    G_HangOnBagWin:Open(_data.id)
end

function PlayerInfoQueryWin:OnChatClick(context)
    self:Hide()
    G_ChatWin:Open(1, _data.id, _data.name)
end

function PlayerInfoQueryWin:OnBlackListClick(context)
    if c3.selectedIndex == 0 then
        NetClient.Send(NetKeys.QCMD_FRIEND_ADD, 1, _data.id)
    else
        NetClient.Send(NetKeys.QCMD_FRIEND_REMOVE, 1, _data.id)
    end
    self:ShowModalWait()
end

function PlayerInfoQueryWin:OnQueryPetClick(context)
    self:Hide()
    G_OthersPetWin:Open(_data.id, "")
end

function PlayerInfoQueryWin:OnClickHeadReturn(cmd, data)
    self:CloseModalWait()
    c2.selectedIndex = data.isfriend == false and 0 or 1
    c3.selectedIndex = data.isblacklist == false and 0 or 1
    if c3.selectedIndex == 1 then 
        chatBtn1.enabled = false 
        chatBtn2.enabled = false 
    else
        chatBtn1.enabled = true 
        chatBtn2.enabled = true 
    end
end