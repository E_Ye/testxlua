BangPaiWin = fgui.window_class(WindowBase)


local print_r = require("Test/print_r")

local _clanName				--帮派名称 
local _clanLvl				--等级
local _typeTab
local _clanGoal		

local _clanBossTxt 			--帮主
local _clanMaxCount			--最大成员数
local _clanCurrentCount		--当前成员数
local _clanMemberTxt		--
local _clanDeputyBossTxt	--副帮主
local _clanMoney			--帮派资金
local _clanExp    			--帮派经验
local _clanMgrBtn			--帮派管理按钮
local _clanLeaveBtn			--退出btn
local _clanDonateMoneyBtn   --捐献资金
local _clanDonateEXPBtn
local _clanSceneEnterBtn    --议事厅
local _clanViewRuleBtn    	--规则
local _clanItemsList		--保护物品	
local _clanNoItemCom		--无保护物品所显示			


local infoFrame
local memberTabFrame
local clanListFrame
local clanWarsFrame
local logTabFrame

local initLoadCount = 5
local everyLoadCount = 10
local currentLastIndex = 0

function BangPaiWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common" , "帮派")
end

function BangPaiWin:DoInit()
    self:Center()

    _typeTab = self.contentPane:GetController("type")
    _typeTab.onChanged:Add(self.OnTabChanged, self)
    _typeTab.selectedIndex = 0

   	infoFrame = self.contentPane:GetChild("n3")
    memberTabFrame = self.contentPane:GetChild("n80")
	clanListFrame = self.contentPane:GetChild("n113") 
	logTabFrame = self.contentPane:GetChild("n105") 
	clanWarsFrame = self.contentPane:GetChild("n100") 

    _clanName = self.contentPane:GetChild("frame"):GetChild("title")
    _clanBossTxt = infoFrame:GetChild("n66")
    _clanDeputyBossTxt = infoFrame:GetChild("n67")
    _clanLvl = infoFrame:GetChild("n68")
    _clanMemberTxt = infoFrame:GetChild("n69")
    _clanMoney = infoFrame:GetChild("n70")
    _clanGoal = infoFrame:GetChild("n71")
    _clanExp = infoFrame:GetChild("n54")
   	_clanItemsList = infoFrame:GetChild("78")
    
    _clanLeaveBtn = infoFrame:GetChild("n74")
	_clanMgrBtn = infoFrame:GetChild("ClanMgrBtn")
	_clanDonateMoneyBtn = infoFrame:GetChild("n61")
	_clanDonateEXPBtn = infoFrame:GetChild("n60")
	_clanSceneEnterBtn = infoFrame:GetChild("n75")
	_clanViewRuleBtn = infoFrame:GetChild("n73")
	_clanItemsList = infoFrame:GetChild("n78")
	_clanNoItemCom = infoFrame:GetChild("n72")

    _clanLeaveBtn.onClick:Add(self.OnLeaveBtnClick, self)
	_clanDonateMoneyBtn.onClick:Add(self.OnDonateMonetBtnClick, self)
	_clanDonateEXPBtn.onClick:Add(self.OnDonateClanEXPBtnClick, self)
	_clanSceneEnterBtn.onClick:Add(self.OnEnterClanSceneBtnClick, self)
	_clanViewRuleBtn.onClick:Add(self.OnViewRuleBtnClick, self)

    
    NetClient.Listen(NetKeys.ACMD_VIEW_CLAN, self.OnViewClanInfo, self)

	NetClient.ListenRet(NetKeys.QCMD_VIEW_CLAN, function(cmd,data) end, self)
	NetClient.ListenRet(NetKeys.QCMD_QUIT_CLAN, function(cmd,data) 
		ClanMgr.inst:ClearData()
	end, self)

end


function BangPaiWin:OnShown()
	self:Operation(_typeTab.selectedIndex)
end

function BangPaiWin:OnHide()
	ClanMgr.isActive = false
end

function BangPaiWin:OnLeaveBtnClick( context )
	local msg = "确定退出 "..ClanMgr.clanData.Name.." ?"
	ConfirmWin.Open(msg, function()
		self:LeaveClan()
		print("ok")
	end,function()
		
	end)
end

function BangPaiWin:OnDonateMonetBtnClick( context )
	local title = "捐献银两"
	G_GongXianWin.Open("", title, nil, function(money)
		NetClient.Send(NetKeys.QCMD_DONATE_CLAN_MONEY, money)
	end)
end

function BangPaiWin:OnDonateClanEXPBtnClick( context )
	NetClient.Send(NetKeys.QCMD_DONATE_CLAN_EXP)
end
function BangPaiWin:OnEnterClanSceneBtnClick( context )
	NetClient.Send(NetKeys.QCMD_ENTER_CLAN_SCENE,"")
end
function BangPaiWin:OnViewRuleBtnClick()
	
end
function BangPaiWin:LeaveClan()
	NetClient.Send(NetKeys.QCMD_QUIT_CLAN)
end

function BangPaiWin:OnClanMgrBtn( context )
	G_ClanPopMenuWin:OpenClanMgrBtnMemu(context.sender,self.MgrMenuItemClick,true) 
end

--管理菜单
function BangPaiWin:MgrMenuItemClick(context)
	if ClanMgr.currentClanWinTab ~= 0 then return end

	local item = context.data

	if item.data == ClanMgr.MenuKey.AddBranch then
		--增分堂
		if not G_AddBranchWin.isShowing then
			G_AddBranchWin:Show()
		end 
	elseif item.data == ClanMgr.MenuKey.FaJiangLi then
		--发奖励
		if not G_BonusWin.isShowing then
			G_BonusWin:Show()
		end 
		
	elseif item.data == ClanMgr.MenuKey.Modify then
		--修改资料
		if not G_ModifyClanWin.isShowing then
			G_ModifyClanWin:ShowByIndex(0)
		end 
	end
end


function BangPaiWin:OnViewClanInfo(cmd,data)
	
	if not self:CloseModalWait(NetKeys.QCMD_VIEW_CLAN) then return end
	_clanName.text = tostring(data.name)
	_clanBossTxt.text = tostring(data.chair_name)
	
	_clanLvl.text = tostring(data.lvl)
	_clanCurrentCount = tostring(data.member)
	_clanMaxCount = tostring(data.max_member)
	_clanGoal.text = tostring(data.tenet)
	_clanMemberTxt.text = _clanCurrentCount.."/".._clanMaxCount
	_clanMoney.text = tostring(data.money)
	_clanExp.max = tostring(data.upgrade_exp)
	
	_clanExp.value = tostring(data.exp)=="NULL" and data.exp or 0
	

	-- if data.items then 
	-- 	if tostring(data.items)=="" then 
	-- 		_clanNoItemCom.visible = true 
	-- 	else
	-- 		-- for i=1,#data.items do
	-- 		-- 	_clanItemsList:RemoveFubang
	-- 		-- end
	-- 	end
	-- else
	-- 	_clanNoItemCom.visible = true 
	-- end
	if ClanMgr.clanData.bossId == Player.data.id then
    	_clanMgrBtn.visible = true
    	_clanMgrBtn.onClick:Add(self.OnClanMgrBtn, self)
    else
    	_clanMgrBtn.visible = false 
    end
	
end

function BangPaiWin:ResetClanWarItems(targetList,itemData)
    -- targetList
end


function BangPaiWin:OnTabChanged(context)
    ClanMgr.currentClanWinTab = _typeTab.selectedIndex 
    if self.isShowing then
		self:Operation(_typeTab.selectedIndex)
	end
end

function BangPaiWin:Operation(tabIndex)
	-- print(">>>>BangPaiWin:Operation..tab:"..tabIndex)
	if tabIndex == 0 then
		ClanMgr:ViewClanInfo("", self)
	elseif tabIndex == 1 then
		ClanMgr:ViewClanMemberTab(memberTabFrame)
	elseif tabIndex == 2 then
		ClanMgr:ViewClanWarsTab(clanWarsFrame, self)
	elseif tabIndex == 3 then
		ClanMgr:ViewClanLogTab(logTabFrame, self)
	elseif tabIndex == 4 then
		ClanMgr:ViewClanListTab(clanListFrame, self)
	end
end
	-- MenuKey =
	-- {
	-- 	SetFuBang = "任命为副帮主",
	-- 	RemoveFubang = "撤销副帮主",
	-- 	SetTangZhu = "任命为堂主",
	-- 	RemoveTangZhu = "撤销堂主",
	-- 	MoveTo = "移至其他分堂",
	-- 	GiveTo = "禅让帮主",
	-- 	kickOut = "踢出帮派",

	-- 	CancelBranch = "解散分堂",

	-- 	FaJiangLi = "发放奖金",
	-- 	AddBranch = "增设分堂",
	-- 	Modify = "修改资料"
	-- }