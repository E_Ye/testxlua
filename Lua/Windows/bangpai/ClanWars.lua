ClanWars = {
	inst,
	caller,
	data = {},
	warsItems = {},
	mainCaller
}
local print_r = require("Test/print_r")

local _warItemList
local _enterWarBtn 

local initLoadCount = 4
local everyLoadCount = 8
local currentLastIndex = 0
local currentLoadTotal = 0

function ClanWars:Init(_caller, _main)
	if self.inst == nil then
		self.inst = self
 		self.caller = _caller
 		self.mainCaller = _main
 		
 		_enterWarBtn = self.caller:GetChild("n102")
 		_enterWarBtn.onClick:Add(self.OnEnterWarBtnClick, self)
 		_warItemList = self.caller:GetChild("n103")
 		
 		-- print(_warItemList.name.."ClanWars:Init")
		_warItemList:SetVirtual()
		_warItemList.itemRenderer = function(index, obj) self.RenderListItem(self,index, obj) end
		_warItemList.numItems = 0
		_warItemList.onClickItem:Add(self.OnClickWarItem, self)
	    
		NetClient.Listen(NetKeys.ACMD_VIEW_CLAN_ITEMS, self.OnGotClanWarItems, self)
	end
end

function ClanWars:Refresh(_caller, main)
 	self:Init(_caller, main)
	self:GetClanWarItems()
end

function ClanWars:GetClanWarItems()
	NetClient.Send(NetKeys.QCMD_CLAN_ITEMS)	
	if self.mainCaller then self.mainCaller:ShowModalWait(NetKeys.QCMD_CLAN_ITEMS) end
end

function ClanWars:OnGotClanWarItems(cmd, data)
	if not self.mainCaller:CloseModalWait(NetKeys.QCMD_CLAN_ITEMS) then return end
	-- print_r(data)
	local warItems = data.items
	-- local warItems = {[1]={iid="i251",total=10,pmoney=2,cid=nil,cname="dsf",seizers=1}}

	self:SetData(warItems) 

	if warItems then _warItemList.numItems = #warItems else _warItemList.numItems = 0 end
	_warItemList:RefreshVirtualList()
	-- currentLastIndex = _warItemList.numItems;
end

function ClanWars:RenderListItem(index, obj)
	self:ResetWarsItem(index, obj)
end

function ClanWars:SetData(data)
	-- print(">>>ClanWars:SetData")
	self.warsItems = data	
end

function ClanWars:ResetWarsItem(index,itemObj)
	local itemData
	local targetItemDef 
	-- print(">>>ClanWars:ResetWarsItem length:"..#self.warsItems )
	if #self.warsItems > 0 then
		itemData = self.warsItems[index + 1]
		targetItemDef = Defs.GetDef("item", itemData.iid)
		itemObj.data ={[0]=itemData,[1]=targetItemDef}

		itemObj:GetChild("n105").text = targetItemDef.name
		itemObj:GetChild("n106").text = itemData.total  --参考销售金额
		itemObj:GetChild("n107").text = itemData.pmoney  --参考提成
		itemObj:GetChild("n108").text = itemData.cname  --帮派名字
		itemObj:GetChild("n110").text = itemData.seizers  --抢夺帮派数
	end
end

function ClanWars:OnClickWarItem(context)
	-- print_r(context.data.data)
	G_ClanWarsItemWin:SetData(context.data.data)
	if not G_ClanWarsItemWin.isShowing then
		G_ClanWarsItemWin:Show()
	end 
end

function ClanWars:OnEnterWarBtnClick(context)
	-- print("进入战场")


end