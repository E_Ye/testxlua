require "Windows/bangpai/ClanMembers"
require "Windows/bangpai/ClanLogs"
require "Windows/bangpai/ClanList"
require "Windows/bangpai/ClanWars"


ClanMgr = 
{
	inst,
	clanData = {Id,Name,bossId,fuBoss,autoJoin,branchs,Data},
	currentClanWinTab = 0,
	isActive = false,
	playerCach = {
		HasJoinedClan = nil,
	},
	
	MenuKey =
	{
		SetFuBang = "任命为副帮主",
		RemoveFuBang = "撤销副帮主",
		SetTangZhu = "任命为堂主",
		RemoveTangZhu = "撤销堂主",
		MoveTo = "移至其他分堂",
		GiveTo = "禅让帮主",
		KickOut = "踢出帮派",

		CancelBranch = "解散分堂",

		FaJiangLi = "发放奖金",
		AddBranch = "增设分堂",
		Modify = "修改资料"
	}

}



function ClanMgr:Init()
	if self.inst == nil then
		self.inst = self
		-- self.MgrMenuKey = G_ClanPopMenuWin:GetMenuKey();
		-- print(self.playerCach.Id)
    	NetClient.Listen(NetKeys.ACMD_VIEW_CLAN, self.OnViewClanInfo, self)

		NetClient.ListenRet(NetKeys.QCMD_VIEW_CLAN, self.OnGotClanInfo,self)
		-- NetClient.ListenRet(NetKeys.QCMD_VIEW_CLAN, self.OnViewClanInfo,self)
	end
end  


function ClanMgr:SetData(_data)
	self.clanData.Id = _data.id
	self.clanData.bossId = _data.chair
	self.clanData.Name =_data.name
	self.clanData.autoJoin = _data.auto_join
	self.clanData.branchs = _data.branchs
	self.clanData.fuBoss = _data.chairs
	self.clanData.Data = _data
	-- print(">>>>> clanId:"..self.clanData.Id.." fubossID:"..self.clanData.bossId)
end  

function ClanMgr:ClearData()
	-- body
	self.playerCach.HasJoinedClan = false
	self.clanData.Id = ""
	self.clanData.bossId = ""
	self.clanData.Name =""
	self.clanData.autoJoin = false
end

function ClanMgr:GetClanInfo()
	NetClient.Send(NetKeys.QCMD_VIEW_CLAN, "")
end

function ClanMgr:OnViewClanInfo(cmd, data) 
	if not self.isActive then
		-- print(">>>>code:"..data.id)
		--data.code=1 查不到帮派，没加入 
		if data.id then
			self.playerCach.HasJoinedClan = true
		end

		self:ShowWindowByBool(self.playerCach.HasJoinedClan)
		self.isActive = true
	end
	self:SetData(data)
end

function ClanMgr:OnGotClanInfo(cmd, data)
	-- print("<<<<>>>>code:"..data.code)
	--data.code=1 查不到帮派，没加入 
	self.playerCach.HasJoinedClan = data.code~=1
	self:ShowWindowByBool(self.playerCach.HasJoinedClan)
end

function ClanMgr:ShowWindowByBool(joined)
	if joined then
		G_BangPaiWin:Show()
	else
		G_BangPaiListWin:Show()
	end
end

function ClanMgr:ShowClanWindow()	
	if self.HasJoinedClan == nil then 
		self:Init()
		self:GetClanInfo()
	else
		self:ShowWindowByBool(self.HasJoinedClan)
	end
end

function ClanMgr:RefreshClanInfo()
	self:ViewClanInfo("")
end


function ClanMgr:ViewClanInfo(clanId,caller)
	NetClient.Send(NetKeys.QCMD_VIEW_CLAN, clanId)
	if caller then
		caller:ShowModalWait(NetKeys.QCMD_VIEW_CLAN)
	end
end



function ClanMgr:ApplyToJoinClan(clanId,caller)
	NetClient.Send(NetKeys.QCMD_JOIN_CLAN, clanId)
end



--子功能Tab
function ClanMgr:ViewClanMemberTab(caller)
	ClanMembers:Refresh(caller)
end

function ClanMgr:ViewClanLogTab(caller,main)
	ClanLogs:Refresh(caller,main)
end

function ClanMgr:ViewClanListTab(caller,main)
	ClanList:Refresh(caller,main)
end

function ClanMgr:ViewClanWarsTab(caller,main)
	ClanWars:Refresh(caller,main)
end