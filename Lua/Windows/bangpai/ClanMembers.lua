ClanMembers = {
	inst,
	caller,
	data = {}
}
local print_r = require("Test/print_r")
local _typeTab
local _membersList
local _autoJoinBtn

local initLoadCount = 4
local everyLoadCount = 8
local currentLastIndex = 0
local branchItemUrl
local memberItemUrl
local currentBranchId = 0 

function ClanMembers:Init(_caller)
	if self.inst == nil then
		self.inst = self
 		self.caller = _caller

 		branchItemUrl = UIPackage.GetItemURL("Common", "分堂item")
 		memberItemUrl = UIPackage.GetItemURL("Common", "帮派成员item")
 		
 		for i=1,3 do
 			self.data[i-1] = {Items, lastIndex=0} 
 		end
 		
 		_membersList = self.caller:GetChild("n84")
 		_autoJoinBtn = self.caller:GetChild("n94")
 		_autoJoinBtn.onChanged:Add(self.OnAutoJoinBtnChanged, self)
 		_typeTab = self.caller:GetController("type")
 		_typeTab.onChanged:Add(self.OnTabChanged, self)
    	_typeTab.selectedIndex = 0

		NetClient.Listen(NetKeys.ACMD_LIST_CLAN_MEMBER, self.OnGotMembersList, self)
		NetClient.Listen(NetKeys.ACMD_LIST_JOIN_CLAN_REQS, self.OnGotJoinedList, self)
		
		self:Operation(_typeTab.selectedIndex)
	end
end

function ClanMembers:Refresh(_caller)
 	self:Init(_caller)
 	_typeTab.selectedIndex = 0
end

function ClanMembers:Operation(index)
	_membersList:RemoveChildrenToPool(1, _membersList.numItems - 1)
	if index == 0 then 
		--查看普通成员
		self:ViewClanMembers(ClanMgr.clanData.Id,0,0,everyLoadCount)
	elseif index == 1 then
		--查看分堂列表
		_membersList:GetChildAt(0).title = "人数0"
		self:ViewClanBranches(ClanMgr.clanData.branchs)
	elseif index == 2 then
		--查看入帮申请
		_autoJoinBtn.selected = ClanMgr.inst.clanData.autoJoin
		self:ViewClanJoinReqs(currentLastIndex,everyLoadCount)
	end   
end


function ClanMembers:OnTabChanged(context)
	ClanMgr.currentClanWinTab = _typeTab.selectedIndex + 1
	self:Operation(_typeTab.selectedIndex)
end


function ClanMembers:OnGotMembersList(cmd, data)
	if data.total> 0 then
		local list = data.members
		_membersList:GetChildAt(0).title = "人数"..data.total

		for i,itemData in ipairs(list) do
			--同时传递玩家的帮派信息和分堂信息
			self:ResetMemberItem({[0]=itemData,[1]=data.branch+1}, i)
		end
	else
		_membersList:GetChildAt(0).title = "人数"..data.total
	end
end

function ClanMembers:OnGotJoinedList(cmd, data)
	_membersList:GetChildAt(0).title = "人数"..data.total
	
	if data.total> 0 then
		local list = data.reqs
		for i,itemData in ipairs(list) do
			self:ResetMemberItem(itemData,i)
		end
		self.data[_typeTab.selectedIndex].Items = _membersList:GetChildren()
	
	end
end

local targetIndex = 0
function ClanMembers:ResetMemberItem(_itemData,index)
	local item
	local fuBossList = ClanMgr.clanData.fuBoss
	local itemData = _itemData[0]
	local branchId = _itemData[1]
	-- _typeTab.selectedIndex 2=入帮申请 1=分堂成员 0=普通成员
	if _typeTab.selectedIndex == 2 then
		item = _membersList:AddItemFromPool()
		item.data = itemData
		item:GetChild("n3").text = ""--攻击力
	

		--如果当前玩家是帮主
		if ClanMgr.clanData.bossId == Player.data.id then
			item:GetController("type").selectedIndex = 1
			item:GetChild("approve").onClick:Add(self.OnClickItemApproveJoin, self)
			item:GetChild("refuse").onClick:Add(self.OnClickItemRefuseJoin, self)
		else
			item:GetController("type").selectedIndex = 0
		end

	elseif _typeTab.selectedIndex == 1 then 
		-- item = _membersList:AddItemFromPool()
		local obj = _membersList:GetFromPool(memberItemUrl)
		targetIndex = targetIndex + 1
		item = _membersList:AddChildAt(obj,targetIndex)

		--如果当前玩家是帮主
		item.data = {[0]=itemData}
		if ClanMgr.clanData.bossId == Player.data.id then
			
			--当前item 是帮主或其他分堂管理管理
			if item.data.id == ClanMgr.clanData.bossId then
				-- print(">>>bb")
				item:GetController("type").selectedIndex = 2
			elseif self:IsBranchBoss(itemData.id,branchId) then
				-- if not item.data[1] then
				item.data[1] = {
					ClanMgr.MenuKey.RemoveTangZhu,
					ClanMgr.MenuKey.GiveTo,
					ClanMgr.MenuKey.KickOut
				}
				item:GetChild("n3").text = ClanMgr.clanData.branchs[branchId].rector_title--堂主主

				-- end
			else
				-- print(">>>cc")
				-- if not item.data[1] then
					item.data[1] = {
						ClanMgr.MenuKey.SetTangZhu,
						ClanMgr.MenuKey.SetFuBang,
						ClanMgr.MenuKey.MoveTo,
						ClanMgr.MenuKey.GiveTo,
						ClanMgr.MenuKey.KickOut
					}
				-- end
		
				item:GetChild("n3").text = ClanMgr.clanData.branchs[branchId].name.."堂众"--堂主主
				
			end

			item:GetController("type").selectedIndex = 0
			if item:GetChild("memberMgrBtn").onClick.isEmpty then
				item:GetChild("memberMgrBtn").onClick:Add(self.OnClickItemMemberMgr, self)
			end
		elseif self:IsBranchBoss(Player.data.id .id, branchId) then
			-- print(">>>>>>>>ClanMgr.clanData.Data.rector_name")
			if self:IsBranchBoss(itemData.id,branchId) then
				item:GetChild("n3").text = ClanMgr.clanData.branchs[branchId].rector_title 
			else
				item:GetChild("n3").text = ClanMgr.clanData.branchs[branchId].name.."堂众"
			end
		elseif self:IsChairs(Player.data.id) then
			
			if self:IsBranchBoss(itemData.id,branchId) then

				item.data[1] = {
					ClanMgr.MenuKey.RemoveTangZhu,
					ClanMgr.MenuKey.GiveTo,
					ClanMgr.MenuKey.KickOut
				}
				item:GetChild("n3").text = ClanMgr.clanData.branchs[branchId].rector_title--堂主主

			else
				item.data[1] = {
					ClanMgr.MenuKey.SetTangZhu,
					ClanMgr.MenuKey.MoveTo,
					ClanMgr.MenuKey.KickOut
				}

				item:GetChild("n3").text = ClanMgr.clanData.branchs[branchId].name.."堂众"--堂主主
				
			end
			item:GetController("type").selectedIndex = 0
			if item:GetChild("memberMgrBtn").onClick.isEmpty then
				item:GetChild("memberMgrBtn").onClick:Add(self.OnClickItemMemberMgr, self)
			end

		else
			item:GetController("type").selectedIndex = 2
		end



	elseif _typeTab.selectedIndex == 0 then
		item = _membersList:AddItemFromPool()
		item.data = {[0]=itemData}
		
		--如果当前玩家是帮主
		if ClanMgr.clanData.bossId == Player.data.id then
			--当前item 是帮主或其他分堂管理管理
			if itemData.id == ClanMgr.clanData.bossId then
				item:GetChild("n3").text = ClanMgr.clanData.Data.chair_title--帮主
				item:GetController("type").selectedIndex = 2

			elseif self:IsChairs(itemData.id) then 
				item:GetController("type").selectedIndex = 0
				item:GetChild("n3").text = "副"..ClanMgr.clanData.Data.chair_title--帮主

				if not item.data[1] then
					item.data[1] = {
						ClanMgr.MenuKey.RemoveFuBang,
						ClanMgr.MenuKey.GiveTo,
						ClanMgr.MenuKey.KickOut
					}
				end
				item:GetController("type").selectedIndex = 0
			else
				item:GetChild("n3").text = "帮众"
			end
			item:GetChild("memberMgrBtn").onClick:Add(self.OnClickItemMemberMgr, self)

		elseif self:IsChairs(Player.data.id) then 
			if itemData.id == ClanMgr.clanData.bossId then
				item:GetController("type").selectedIndex = 2
				item:GetChild("n3").text = ClanMgr.clanData.Data.chair_title--帮主
				
			elseif self:IsChairs(itemData.id) then 
				item:GetChild("n3").text = "副"..ClanMgr.clanData.Data.chair_title--帮主
				item:GetController("type").selectedIndex = 2
				
			else
				item.data[1] = {
						ClanMgr.MenuKey.MoveTo,
						ClanMgr.MenuKey.KickOut
					}
				item:GetController("type").selectedIndex = 0
				item:GetChild("n3").text = "帮众"
				item:GetChild("memberMgrBtn").onClick:Add(self.OnClickItemMemberMgr, self)
			end
			
		else
			item:GetController("type").selectedIndex = 2
			if itemData.id == ClanMgr.clanData.bossId then
				item:GetChild("n3").text = ClanMgr.clanData.Data.chair_title--帮主
			elseif self:IsChairs(itemData.id) then 
				item:GetChild("n3").text = "副"..ClanMgr.clanData.Data.chair_title--帮主
			else
				item:GetChild("n3").text = "帮众"
			end
		end
		
	end

	item:GetChild("head").icon = UIPackage.GetItemURL("Sucai", 'u'..itemData.faction..itemData.gender)
	item:GetChild("head").title = itemData.lvl --等级
	item:GetChild("name").text = itemData.name --角色名称
	item:GetChild("n4").text = itemData.line
	
	item:GetChild("head").onClick:Add(self.OnClickItemHead, self)
	
end

function ClanMembers:IsChairs(uid)
	
	for i,v in ipairs(ClanMgr.clanData.fuBoss) do
		if uid == v.id then return true end
	end
	return false
end

function ClanMembers:IsBranchBoss(uid,branchId)
	local tepmBranch = ClanMgr.clanData.branchs[branchId]
	if tepmBranch then return tepmBranch.rector == uid end
	return false
end

--点击头像
function ClanMembers:OnClickItemHead(context)
	-- -- print("data id:"..context.sender.parent.data.id)
	-- local data 
	-- if context.sender.parent.data.id then 
	-- 	data = context.sender.parent.data
	-- else
	-- 	data = context.sender.parent.data[0]
	-- end
	-- if context.sender.parent.data.id ~= Player.data.id then
	-- 	G_PlayerInfoQueryWin:Open(context.sender.parent.data, 0)
	-- end
end

--批准加入帮派
function ClanMembers:OnClickItemApproveJoin(context)
	self:ApproveToJoinClan(context.sender.parent)
end

--拒绝加入帮派
function ClanMembers:OnClickItemRefuseJoin(context)
	self:RejectToJoinClan(context.sender.parent)
end

--成员管理
function ClanMembers:OnClickItemMemberMgr(context)
	if G_ClanPopMenuWin.isShowing then G_ClanPopMenuWin:HidePopupMemu() end

	local menu = context.sender.parent.data[1]

	G_ClanPopMenuWin:SetData(context.sender.parent.data)
	G_ClanPopMenuWin:OpenMemberMgrMemu(context.sender, self.BranchMenuItemClick, menu)
end




function ClanMembers:OnAutoJoinBtnChanged(context)
	NetClient.Send(NetKeys.QCMD_AUTOJOIN_CLAN, ClanMgr.clanData.Id)
	ClanMgr:ViewClanInfo("")
end



--查看分堂列表和成员
function ClanMembers:ViewClanBranches(branchLists )
	for i=2,#branchLists do
		self:ResetBranchItem(branchLists[i])
	end
end

function ClanMembers:ResetBranchItem(data)
	local item = _membersList:AddItemFromPool(branchItemUrl)
	item:GetChild("name").text = data.name
	if data.rector_name then
		item:GetChild("n8").text = data.member+1
	else
		item:GetChild("n8").text = data.member
	end
	item:GetChild("n4").text = data.rector_name
	local expand = item:GetController("expanded")
	expand.selectedIndex = 0 
	item.data = {[0]=data,[1]=expand}

	if item:GetChild("n11").onClick.isEmpty then
		item:GetChild("n11").onClick:Add(self.OnExpandClick,self)
	end
	if ClanMgr.clanData.bossId == Player.data.id or self:IsChairs(Player.data.id) then
		item:GetChild("branchMgrBtn").visible = true
		item:GetChild("branchMgrBtn").onClick:Add(self.OnClickBranchMgrBtn,self)
	else
		item:GetChild("branchMgrBtn").visible = false
	
	end
end
function ClanMembers:OnExpandClick(context) 
	local item = context.sender.parent
	local expand = item.data[1]
	local data = item.data[0]

	targetIndex = _membersList:GetChildIndex(item)
	currentBranchId = data.id 
	if expand.selectedIndex == 0 then 
		self:ViewClanMembers(ClanMgr.clanData.Id, data.id ,0,everyLoadCount)
		expand.selectedIndex = 1 
	else
		if data.member > 0  then 
			self:RemoveBranchMembersFrom(targetIndex + 1, data.member, _membersList)
		end
		if data.rector_name then
			self:RemoveBranchMembersFrom(targetIndex + 1, data.member + 1, _membersList)
		else

		end
		expand.selectedIndex = 0 
	end
end


--分堂成员管理
function ClanMembers:OnClickBranchMgrBtn(context) 
	
	G_ClanPopMenuWin:SetData(context.sender.parent.data)
	G_ClanPopMenuWin:OpenBranchMgrMemu(context.sender,self.BranchMenuItemClick,true) 
end

function ClanMembers:RemoveBranchMembersFrom(start, count ,targetList)
	targetList:RemoveChildrenToPool(start,start+count-1)
end

function ClanMembers:BranchMenuItemClick(context)
	if ClanMgr.currentClanWinTab == 0 then return end

	local item = context.data
	local branchData = G_ClanPopMenuWin:GetData()

	if item.data == ClanMgr.MenuKey.CancelBranch then
		
		--解散分堂
		local msg = "确定解散分堂 "..branchData[0].name.." ?"
		ConfirmWin.Open(msg, function()
			ClanMembers:RemoveClanBranch(branchData[0].id)
			print("ok ")
		end,function()
		
		end)
	
	elseif item.data == ClanMgr.MenuKey.Modify then
		-- 修改资料
		if not G_ModifyClanWin.isShowing then
			G_ModifyClanWin:ShowByIndex(1, branchData[0])
		end 

	elseif item.data == ClanMgr.MenuKey.SetTangZhu then
		--任命堂主
		ClanMembers:AppontClanDuty(2, currentBranchId, branchData[0].id)
	
	elseif item.data == ClanMgr.MenuKey.SetFuBang then
		--任命副帮
		ClanMembers:AppontClanDuty(1, currentBranchId, branchData[0].id)
	
	elseif item.data == ClanMgr.MenuKey.MoveTo then
		--移动到其他
		local tempMenuTable = {}
		for i,v in ipairs(ClanMgr.clanData.branchs) do
			if i>1 then tempMenuTable[i-1] = v.name end
		end

		G_ClanPopMenuWin:OpenChildMenu(context.sender.parent, ClanMembers.MoveToMenuItemClick, tempMenuTable)
	
	elseif item.data == ClanMgr.MenuKey.GiveTo then
		--禅让帮主
		ClanMembers:AppontClanDuty(3, currentBranchId, branchData[0].id)
	
	elseif item.data == ClanMgr.MenuKey.KickOut then	
		--踢出帮派
		ClanMembers:KickOutMember(branchData[0].id)
	
	elseif item.data == ClanMgr.MenuKey.RemoveFuBang then
		--罢免副帮
		ClanMembers:ReCallClanDuty(1, branchData[0].id)
	
	elseif item.data == ClanMgr.MenuKey.RemoveTangZhu then
		--罢免堂主
		ClanMembers:ReCallClanDuty(2, branchData[0].id)
	
	end
	--reflesh
	ClanMgr:ViewClanInfo("")   
	ClanMembers:Operation(_typeTab.selectedIndex)
end


--成员移至分堂菜单
function ClanMembers:MoveToMenuItemClick(context)

	if ClanMgr.currentClanWinTab == 0 then return end
	local _list = context.sender
	local item = context.data
	local branchId = _list:GetChildIndex(item) + 1 
	local userData = G_ClanPopMenuWin:GetData()
	
	NetClient.Send(NetKeys.QCMD_DEPLOY_CLAN_MEMBER,branchId, userData[0].id)
end



--查看帮派成员
function ClanMembers:ViewClanMembers(cid, branch, from, count)
	NetClient.Send(NetKeys.QCMD_LIST_CLAN_MEMBER, cid, branch,from ,count)
end

--入帮申请列表
function ClanMembers:ViewClanJoinReqs(from,count)
	NetClient.Send(NetKeys.QCMD_LIST_JOIN_CLAN_REQS, from, count)
end

--批准入帮
function ClanMembers:ApproveToJoinClan(caller)
	NetClient.Send(NetKeys.QCMD_ALLOW_JOIN_CLAN, caller.data.id)	
end

--拒绝入帮
function ClanMembers:RejectToJoinClan(caller)
	NetClient.Send(NetKeys.QCMD_REJECT_JOIN_CLAN, caller.data.id)	
end

--踢出帮派
function ClanMembers:KickOutMember(userId)
	NetClient.Send(NetKeys.QCMD_KICKOUT_CLAN_MEMBER, userId)	
end

--任命职务 1-任命副帮主,2-任命堂主,3-禅让帮主
function ClanMembers:AppontClanDuty(typeId, branch, userId)
	NetClient.Send(NetKeys.QCMD_APPOINT_CLAN_DUTY, typeId, branch, userId)	
end

--罢免职务 1副帮 2堂主
function ClanMembers:ReCallClanDuty(typeId, userId)
	NetClient.Send(NetKeys.QCMD_RECALL_CLAN_DUTY, typeId, userId)	
end

--解散分堂
function ClanMembers:RemoveClanBranch(branchId)
	NetClient.Send(NetKeys.QCMD_REMOVE_CLAN_BRANCH, branchId)	
end