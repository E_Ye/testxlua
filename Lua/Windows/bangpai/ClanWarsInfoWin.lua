ClanWarsInfoWin = fgui.window_class(WindowBase)

local _itemNameTxt
local _defendClanTxt    --防御帮派
local _flagHPTxt        --旗子生命
local _peopleCountTxt   --剩余人数

local _clanList
local _refreshBtn 
local _itemData 

local itemId 
local print_r = require("Test/print_r")


function ClanWarsInfoWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common" , "帮派_战场信息")
end

function ClanWarsInfoWin:DoInit()
    self:Center()

    _itemNameTxt = self.contentPane:GetChild("title")
    _defendClanTxt = self.contentPane:GetChild("n30")
    _flagHPTxt = self.contentPane:GetChild("n33")
    _peopleCountTxt = self.contentPane:GetChild("n31")

    _refreshBtn = self.contentPane:GetChild("n25")
    _refreshBtn.onClick:Add(self.OnRefreshBtnClick, self)
    
    _clanList = self.contentPane:GetChild("n53")

    NetClient.Listen(NetKeys.ACMD_VIEW_CLAN_ITEM_STATUS, self.OnViewClanItemStatus, self)

   	-- NetClient.ListenRet()
end


function ClanWarsInfoWin:OnOkBtnClick()
    -- if _selectedTab.selectedIndex ~= 0 then
    --     print(">>".._selectedTab.selectedIndex)
        
    --     NetClient.Send(NetKeys.QCMD_ALLOT_CLAN_MONEY, _selectedTab.selectedIndex, _moneyTxt.text)
    -- end
end

function ClanWarsInfoWin:OnShown()
	self:Refresh()
end

function ClanWarsInfoWin:OnHide()
    -- self:Operation(_typeTab.selectedIndex)
    itemData = nil
end

function ClanWarsInfoWin:ShowByItemId(iid)
    itemId = iid
    self:Show()
end

function ClanWarsInfoWin:Refresh()
    if itemId then self:ViewClanItemStaus(itemId) end
    _itemNameTxt = ""
    _defendClanTxt = ""
    _flagHPTxt = ""
    _peopleCountTxt = 0

end

--查询指定战场战况
function ClanWarsInfoWin:ViewClanItemStaus(iid)
    NetClient.Send(NetKeys.QCMD_VIEW_CLAN_ITEM_STATUS, iid)
end

function ClanWarsInfoWin:OnViewClanItemStatus(cmd, data)
    _itemData = data
    
end

function ClanWarsInfoWin:OnRefreshBtnClick(cmd, data)
    self:Refresh()
end

