AddBranchWin = fgui.window_class(WindowBase)

local _okBtn 
local _branchName
local _branchBossCaller 


local print_r = require("Test/print_r")


function AddBranchWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common" , "帮派_增设分堂")
end

function AddBranchWin:DoInit()
    self:Center()

    _okBtn = self.contentPane:GetChild("n25")
    _branchName = self.contentPane:GetChild("n35"):GetChild("n44")
    _branchBossCaller = self.contentPane:GetChild("n24"):GetChild("n44")

    _okBtn.onClick:Add(self.OnOkBtnClick, self)

   	-- NetClient.ListenRet(N)
end


function AddBranchWin:OnOkBtnClick()
	if _branchName.text ~="" or _branchBossCaller.text~="" then
		print(_branchName.text)
		NetClient.Send(NetKeys.QCMD_ADD_CLAN_BRANCH,_branchName.text, _branchBossCaller.text, 2)
		
	end

	
end


function AddBranchWin:OnShown()
	-- self:Operation(_typeTab.selectedIndex)
end


