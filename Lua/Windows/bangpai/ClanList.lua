ClanList = {
	inst,
	caller,
	listItems = {},
	mainCaller
}
local print_r = require("Test/print_r")

local _clanList
local _searchBtn
local _contentTxt 

local initLoadCount = 4
local everyLoadCount = 8
local currentLastIndex = 0
local currentLoadTotal = 0


function ClanList:Init(_caller, _main)
	if self.inst == nil then
		self.inst = self
 		self.caller = _caller
 		self.mainCaller = _main
 		
 		_clanList = self.caller:GetChild("n128")
		_clanList:SetVirtual()
		_clanList.itemRenderer = function(index, obj) self.RenderListItem(self,index, obj) end
		_clanList.numItems = 0
	    _clanList.scrollPane.onScrollEnd:Add(self.OnScrollEnd,self)
	    _searchBtn = self.caller:GetChild("n126")
    	_searchBtn.onClick:Add(self.OnSearchBtnClick, self)
    	_contentTxt = self.caller:GetChild("n125")
    	_contentTxt.text = 0

	    currentLastIndex = _clanList.numItems

		NetClient.Listen(NetKeys.ACMD_LIST_CLAN, self.OnGotClanList, self)
	end
end

function ClanList:Refresh(_caller, main)
 	self:Init(_caller, main)
 	if _clanList.numItems <= initLoadCount then
		self:GetClanList(0, everyLoadCount)
	end
end


function ClanList:OnGotClanList(cmd, data)
	if not self.mainCaller:CloseModalWait(NetKeys.QCMD_LIST_CLAN) then return end
	self:SetData(data.clans) 

	currentLoadTotal = currentLoadTotal + everyLoadCount
	if currentLoadTotal > data.total then currentLoadTotal = data.total end

	_clanList.numItems = currentLoadTotal
	currentLastIndex = _clanList.numItems;
end

function ClanList:RenderListItem(index, obj)
	self:ResetClanItem(index, obj)
end

function ClanList:SetData(data)
	for i,v in ipairs(data) do
		self.listItems[currentLoadTotal + i] = v	
	end
end

function ClanList:ResetClanItem(index,itemObj)
	local  itemData 
	if #self.listItems > 0 then
	
		itemData = self.listItems[index + 1]
		itemObj.data = itemData
		itemObj:GetChild("n130").text = itemData.name --.."."..index+1
		itemObj:GetChild("n131").text = itemData.chair_name
		itemObj:GetChild("n132").text = itemData.lvl
		itemObj:GetChild("n133").text = itemData.member.."/"..itemData.max_member
	end
end

function ClanList:OnScrollEnd(context)
	if _clanList.scrollPane.isBottomMost then
		if _clanList.numItems >= initLoadCount then
			self:GetClanLogs(currentLastIndex, everyLoadCount)
		end
	end
end

function ClanList:OnSearchBtnClick(context)
	self:GetClanList(0,0, _contentTxt.text)
end

function ClanList:GetClanList(from, count, content)
	if self.mainCaller then self.mainCaller:ShowModalWait(NetKeys.QCMD_LIST_CLAN) end
	if content ~= nil then
		self:SearchClanByName(content,caller)
	else
		NetClient.Send(NetKeys.QCMD_LIST_CLAN, from, count, "")
	end
end

function ClanList:SearchClanByName(content,caller)
	NetClient.Send(NetKeys.QCMD_LIST_CLAN, 0, 0, content)
end
