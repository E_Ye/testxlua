ClanLogs = {
	inst,
	caller,
	data = {},
	logItems = {},
	mainCaller
}
local print_r = require("Test/print_r")

local _logList

local initLoadCount = 4
local everyLoadCount = 8
local currentLastIndex = 0
local currentLoadTotal = 0

function ClanLogs:Init(_caller, _main)
	if self.inst == nil then
		self.inst = self
 		self.caller = _caller
 		self.mainCaller = _main
 		
 		_logList = self.caller:GetChild("n108")
		_logList:SetVirtual()
		_logList.itemRenderer = function(index, obj) self.RenderListItem(self,index, obj) end
		_logList.numItems = 0
	    _logList.scrollPane.onScrollEnd:Add(self.OnScrollEnd,self)
	    currentLastIndex = _logList.numItems

		NetClient.Listen(NetKeys.ACMD_VIEW_CLAN_LOGS, self.OnGotClanLogList, self)
	end
end

function ClanLogs:Refresh(_caller, main)
 	self:Init(_caller, main)
 	if _logList.numItems <= initLoadCount then
		self:GetClanLogs(0, everyLoadCount)
	end
end

function ClanLogs:GetClanLogs(start,count)
	NetClient.Send(NetKeys.QCMD_VIEW_CLAN_LOGS,start,count)	
	if self.mainCaller then self.mainCaller:ShowModalWait(NetKeys.QCMD_VIEW_CLAN_LOGS) end
end

function ClanLogs:OnGotClanLogList(cmd, data)
	if not self.mainCaller:CloseModalWait(NetKeys.QCMD_VIEW_CLAN_LOGS) then return end
	self:SetData(data.logs) 

	currentLoadTotal = currentLoadTotal + everyLoadCount
	if currentLoadTotal > data.total then currentLoadTotal = data.total end

	_logList.numItems = currentLoadTotal
	currentLastIndex = _logList.numItems;
end

function ClanLogs:RenderListItem(index, obj)
	self:ResetLogItem(index, obj)
end

function ClanLogs:SetData(data)
	for i,v in ipairs(data) do
		self.logItems[currentLoadTotal + i] = v	
	end
end

function ClanLogs:ResetLogItem(index,itemObj)
	local  itemData 
	if #self.logItems > 0 then
		itemData = self.logItems[index + 1]
		itemObj:GetChild("title").text = itemData 
	end
end

function ClanLogs:OnScrollEnd(context)
	if _logList.scrollPane.isBottomMost then
		if _logList.numItems >= initLoadCount then
			self:GetClanLogs(currentLastIndex, everyLoadCount)
		end
	end
end