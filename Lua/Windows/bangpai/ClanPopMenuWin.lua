ClanPopMenuWin = fgui.window_class(WindowBase)


require "Windows/bangpai/ClanMgr"
local print_r = require("Test/print_r")
local _menuList 
local contentList
local itemUrl 
local height
local currentCaller=""
local caller

local callBackTable 
local tempFlag = false

local _childMenu 
local _childMenuList
local data  --其他窗口传递
-- local  callBackFunc 

local BranchMgrMenu = {
	ClanMgr.MenuKey.CancelBranch,
	ClanMgr.MenuKey.Modify
}
local ClanMgrMenu = {
	ClanMgr.MenuKey.FaJiangLi,
	ClanMgr.MenuKey.AddBranch,
	ClanMgr.MenuKey.Modify
}
local MemberMgrMenu = {
	ClanMgr.MenuKey.SetFuBang,
	ClanMgr.MenuKey.MoveTo,
	ClanMgr.MenuKey.GiveTo,
	ClanMgr.MenuKey.KickOut
}

function ClanPopMenuWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common" , "按钮菜单")
    self.contentPane = UIPackage.CreateObject(self._packageName, self._itemId)

end

function ClanPopMenuWin:OnInit()
	self:DoInit()
end

function ClanPopMenuWin:DoInit()
	itemUrl = UIPackage.GetItemURL("Common", "按钮无边框")
    _menuList = self.contentPane:GetChild("n2")
    _menuList:RemoveChildrenToPool()
    _menuList.width = 115
    height = _menuList.height
end

function ClanPopMenuWin:DoShowAnimation()
	self:AddBtnItem(_menuList, contentList, callBackTable)
end

function ClanPopMenuWin:DoHideAnimation()
    _menuList:RemoveChildrenToPool()
    self:HideImmediately()
end

function ClanPopMenuWin:OpenMenu(_caller,_contentList,_funcTable ,isDown)
	tempFlag = false
	
	caller = _caller
	contentList = _contentList
	callBackTable =_funcTable
	
    if isDown ~= nil then tempFlag = isDown end
	GRoot.inst:ShowPopup(self,caller,tempFlag)
end

function ClanPopMenuWin:AddBtnItem(targetList ,contentList,_callBackTable)

	local item 

	for i=1,#contentList do

		item = targetList:AddItemFromPool(itemUrl)
		item:GetChild("title").textFormat.size = 18
		item:GetChild("title").text = contentList[i]
		item.data = contentList[i]
	end
	
	targetList.height = height + (height/2-3)*(#contentList - 2)
	self.height =_menuList.height

	currentCaller = caller.name
	if type(_callBackTable) == type(self.AddBtnItem) then
		targetList.onClickItem:Add(_callBackTable, self)
	end
	
end

function ClanPopMenuWin:OpenMemberMgrMemu(caller,funcTable,menu,isDown)
	local _MemberMgrMenu = MemberMgrMenu
	if menu then _MemberMgrMenu = menu end 
	self:OpenMenu(caller,_MemberMgrMenu,funcTable,isDown)
end

function ClanPopMenuWin:OpenClanMgrBtnMemu(caller,callBackClickItem,isDown)
	self:OpenMenu(caller,ClanMgrMenu,callBackClickItem,isDown)
end

function ClanPopMenuWin:OpenBranchMgrMemu(caller,funcTable)
	self:OpenMenu(caller,BranchMgrMenu,funcTable)
end

function ClanPopMenuWin:SetData(_data)
	data = _data 
end

function ClanPopMenuWin:GetData()
	return data 
end

function ClanPopMenuWin:OpenChildMenu(caller,funcTable,menu,isDown)
	if _childMenu ==nil then
		_childMenu = UIPackage.CreateObject(self._packageName, self._itemId)
    	_childMenuList = _childMenu:GetChild("n2")

    	_childMenuList.width = 115
    	_childMenuList.height =400
	end
	_childMenuList:RemoveChildrenToPool()

	self:AddBtnItem(_childMenuList,menu,funcTable)
	GRoot.inst:ShowPopup(_childMenu,caller,false)
end

function ClanPopMenuWin:HidePopupMemu()
	GRoot.inst:HidePopup(self)
end


G_ClanPopMenuWin = ClanPopMenuWin.New()