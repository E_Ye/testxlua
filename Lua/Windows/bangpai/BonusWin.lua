BonusWin = fgui.window_class(WindowBase)

local _okBtn 
local _moneyTxt
local _allTxt
local _selectedTab  --发放类型(1-帮主,2-副帮主,3-堂主,4-帮众,5-所有成员)

local print_r = require("Test/print_r")


function BonusWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common" , "帮派_发放奖金")
end

function BonusWin:DoInit()
    self:Center()

    _okBtn = self.contentPane:GetChild("n6")
    _selectedTab = self.contentPane:GetController("selected")
    _moneyTxt = self.contentPane:GetChild("n13")
    -- _branchName = self.contentPane:GetChild("n35"):GetChild("n44")
    -- _branchBossCaller = self.contentPane:GetChild("n24"):GetChild("n44")

    _okBtn.onClick:Add(self.OnOkBtnClick, self)

   	-- NetClient.ListenRet()
end


function BonusWin:OnOkBtnClick()
    if _selectedTab.selectedIndex ~= 0 then
        print(">>".._selectedTab.selectedIndex)
        
        NetClient.Send(NetKeys.QCMD_ALLOT_CLAN_MONEY, _selectedTab.selectedIndex, _moneyTxt.text)
    end
end


function BonusWin:OnShown()
	-- self:Operation(_typeTab.selectedIndex)
end


