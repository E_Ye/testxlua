ModifyClanWin = fgui.window_class(WindowBase)

local _okBtn 
local _clanNameTxt
local _bossCallerTxt
local _clanGoal
local _typeTab


local _branchName
local _branchBossCaller
local _branchMaxMember


local tempTabIndex 
local data

local print_r = require("Test/print_r")


function ModifyClanWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common" , "帮派_修改资料")
end

function ModifyClanWin:DoInit()
    self:Center()

    _typeTab = self.contentPane:GetController("type")
    _typeTab.selectedIndex = 0
    _okBtn = self.contentPane:GetChild("n6")
    _clanNameTxt = self.contentPane:GetChild("n35"):GetChild("n44")
    _bossCallerTxt = self.contentPane:GetChild("n4"):GetChild("n44")
    _clanGoal = self.contentPane:GetChild("n37"):GetChild("n46")
    _okBtn.onClick:Add(self.OnOkBtnClick, self)
    
    _branchName = self.contentPane:GetChild("n38"):GetChild("n44")
    _branchBossCaller = self.contentPane:GetChild("n39"):GetChild("n44")
    _branchMaxMember = self.contentPane:GetChild("n40"):GetChild("n44")

   	-- NetClient.ListenRet()
end

function ModifyClanWin:ShowByIndex(tabSelectedIndex, _data)     
    tempTabIndex = tabSelectedIndex     
    data = _data
    self:Show()
end


function ModifyClanWin:OnOkBtnClick()

    if _typeTab.selectedIndex == 0 then --帮派修改
        NetClient.Send(NetKeys.QCMD_MODIFY_CLAN, _clanNameTxt.text, _bossCallerTxt.text, _clanGoal.text)
    elseif _typeTab.selectedIndex == 1 then --分堂修改
        if data ~= nil then
            NetClient.Send(NetKeys.QCMD_MODIFY_CLAN_BRANCH, data.id, _branchName.text, _branchBossCaller.text, _branchMaxMember.text)
        end
    end 
	
end


function ModifyClanWin:OnShown()
    _typeTab.selectedIndex = tempTabIndex

end


