ClanWarsItemWin = fgui.window_class(WindowBase)

local _clanNameTxt 
local _itemNameTxt 
local _itemLvlTxt 
local _itemExpTxt
local _itemHpTxt 
local _itemDefendTxt  --防御

local _addExpBtn 
local _fightForBtn --争夺
local _checkWarsInfoBtn --查看战况
local _itemData 

local print_r = require("Test/print_r")


function ClanWarsItemWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common" , "帮派_保护物品信息")
end

function ClanWarsItemWin:DoInit()
    self:Center()

    _itemNameTxt = self.contentPane:GetChild("title")
    _clanNameTxt = self.contentPane:GetChild("n30")
    _itemLvlTxt = self.contentPane:GetChild("n31")
    _itemExpTxt = self.contentPane:GetChild("n32")
    _itemHpTxt = self.contentPane:GetChild("n33")
    _itemDefendTxt = self.contentPane:GetChild("n34")

    _fightForBtn = self.contentPane:GetChild("n25")
    _addExpBtn = self.contentPane:GetChild("n35")
    _checkWarsInfoBtn = self.contentPane:GetChild("n36")
    _checkWarsInfoBtn.onClick:Add(self.OnCheckBtnClick, self)
    -- _okBtn = self.contentPane:GetChild("n6")
    -- _selectedTab = self.contentPane:GetController("selected")
    -- _moneyTxt = self.contentPane:GetChild("n13")
    -- -- _branchName = self.contentPane:GetChild("n35"):GetChild("n44")
    -- -- _branchBossCaller = self.contentPane:GetChild("n24"):GetChild("n44")

    -- _okBtn.onClick:Add(self.OnOkBtnClick, self)
    NetClient.Listen(NetKeys.ACMD_VIEW_CLAN_ITEM_FLAG, self.OnViewClanItemFlag, self)

   	-- NetClient.ListenRet()
end


function ClanWarsItemWin:OnOkBtnClick()
    -- if _selectedTab.selectedIndex ~= 0 then
    --     print(">>".._selectedTab.selectedIndex)
        
    --     NetClient.Send(NetKeys.QCMD_ALLOT_CLAN_MONEY, _selectedTab.selectedIndex, _moneyTxt.text)
    -- end
end

function ClanWarsItemWin:OnShown()
	self:Refresh()
end

function ClanWarsItemWin:OnHide()
    -- self:Operation(_typeTab.selectedIndex)
    itemData = nil
end

function ClanWarsItemWin:Refresh()
    local itemData
    local itemDef
    if _itemData then
        itemData = _itemData[0]
        itemDef =_itemData[1]
        
        _itemNameTxt.text = itemDef.name
        -- _clanNameTxt.text = 
    end
    -- self:Operation(_typeTab.selectedIndex)
end

function ClanWarsItemWin:SetData(data)
    _itemData = data
    -- self:Operation(_typeTab.selectedIndex)
end

function ClanWarsItemWin:ViewClanItemFlag(iid)
    NetClient.Send(NetKeys.QCMD_VIEW_CLAN_ITEM_FLAG,iid)
end

function ClanWarsItemWin:OnViewClanItemFlag(cmd, data)
    -- _itemData = data
    -- self:Operation(_typeTab.selectedIndex)
end

--参与或放弃type 1-争夺,2-放弃
function ClanWarsItemWin:SeizClanItem(type, iid)
    NetClient.Send(NetKeys.QCMD_SEIZE_CLAN_ITEM, type, iid)
end

--升级旗帜 type 1等级
function ClanWarsItemWin:UpgradeClanItemFlag(type, value, iid)
    NetClient.Send(NetKeys.QCMD_UPGRADE_CLAN_ITEM_FLAG, type, value, iid)
end

--查询指定战场战况
function ClanWarsItemWin:ViewClanItemStaus(iid)
    NetClient.Send(NetKeys.QCMD_VIEW_CLAN_ITEM_STATUS, iid)
end

function ClanWarsItemWin:OnCheckBtnClick(context)
    if _itemData.iid then 
        G_ClanWarsInfoWin:ShowByItemId(item.data.id)
    end
end