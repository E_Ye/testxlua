BangPaiListWin = fgui.window_class(WindowBase)
-- require "Windows/bangpai/ClanPopMenuWin"
-- require("ClanMgr")
local _clanList
local _typeTab
local _searchBtn
local _clearBtn
local _currentSearchClan
local _buildClanName	--创建帮派名字
local _buildClanBossCall--帮主称呼
local _buildClanGoal	--帮派宗旨	
local _buildCost		--创建花费
local _buildBtn

local initLoadCount = 5
local everyLoadCount = 10
local currentLastIndex = 0

local clanItems
local print_r = require("Test/print_r")


function BangPaiListWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = true
    self:SetContentSource("Common" , "帮派_列表和创建")
end
-- local test = {id,e}
function BangPaiListWin:DoInit()
    self:Center()
    self.clanItems = {}
    _clanList = self.contentPane:GetChild("n27")
    _clanList:SetVirtual()
	_clanList.itemRenderer = function(index, obj) self.RenderListItem(self,index, obj) end
	_clanList.numItems = initLoadCount
    _clanList.scrollPane.onScrollEnd:Add(self.OnScrollEnd,self)
    currentLastIndex = _clanList.numItems
    -- _clanList:RemoveChildrenToPool()

    _typeTab = self.contentPane:GetController("type")
    
    _searchBtn = self.contentPane:GetChild("n18")
    _searchBtn.onClick:Add(self.OnSearchBtnClick, self)

    _clearBtn = self.contentPane:GetChild("n19")
    _currentSearchClan = self.contentPane:GetChild("n17")

    _buildClanName = self.contentPane:GetChild("n35"):GetChild("n44")
    -- _buildClanName.onClick:Add(self.OnBuildBtnTestClick, self)
    _buildClanBossCall = self.contentPane:GetChild("n36"):GetChild("n44")
    _buildClanGoal = self.contentPane:GetChild("n37"):GetChild("n46")
    _buildCost = self.contentPane:GetChild("n41")
    _buildBtn = self.contentPane:GetChild("n42")
    _buildBtn.onClick:Add(self.OnBuildBtnClick, self)

	NetClient.Listen(NetKeys.ACMD_LIST_CLAN, self.OnGotClanList, self)
	NetClient.ListenRet(NetKeys.QCMD_JOIN_CLAN, self.DefaultQCMDHandler, self)

end




function BangPaiListWin:OnShown()
	self:Refresh()
end

function BangPaiListWin:OnHide()
	-- print(">>>>>>>>>>>hide ")
	ClanMgr.isActive = false
end

function BangPaiListWin:Refresh()
	-- body
	if _clanList.numItems <= initLoadCount then
		self:GetClanList(0, initLoadCount)
	end
end


function BangPaiListWin:OnSearchBtnClick(context)
	self:GetClanList(0,100, _currentSearchClan.text)
end
function BangPaiListWin:OnBuildBtnTestClick(context)

		NetClient.Send(NetKeys.QCMD_CREATE_CLAN, _buildClanName.text, _buildClanBossCall.text, _buildClanGoal.text)
	-- G_ClanPopMenuWin:OpenMemberMgrMemu(context.sender,self.OnMgrMemuItemClick) 
	
end
function BangPaiListWin:OnBuildBtnClick(context)
	-- print(#_buildClanName.text )
	if _buildClanName.text ~= "" 
		or  _buildClanBossCall.text ~= "" 
		or _buildClanGoal.text ~= "" then
		
		NetClient.Send(NetKeys.QCMD_CREATE_CLAN, _buildClanName.text, _buildClanBossCall.text, _buildClanGoal.text)
		-- G_ClanPopMenuWin:OpenClanMgrBtnMemu(context.sender,self.OnMgrMemuItemClick) 
	end
end


function BangPaiListWin:RecJoin(cmd, data)
	AlertWin.Open(data.msg)
end

--触底
function BangPaiListWin:OnScrollEnd(context)
	if _clanList.scrollPane.isBottomMost then
		if _clanList.numItems >= initLoadCount then
			self:GetClanList(currentLastIndex, everyLoadCount)
			_clanList.numItems = _clanList.numItems + everyLoadCount;
		end
	end
end

function BangPaiListWin:OnGotClanList(cmd, data)
	if not self:CloseModalWait(NetKeys.QCMD_LIST_CLAN) then return end

	self.clanItems = data.clans
	print_r(self.clanItems)
	_clanList.numItems = data.total
	-- local xData = {count=}
	
	currentLastIndex = _clanList.numItems;
end

function BangPaiListWin:RenderListItem(index, obj)
	self:ResetClanItem(index, obj)
end

function BangPaiListWin:ResetClanItem(index,itemObj)
	local  itemData 
	if #self.clanItems > 0 then
	
		itemData = self.clanItems[index + 1]
		itemObj.data = itemData
		
		itemObj:GetChild("n21").text = itemData.name --.."."..index+1
		itemObj:GetChild("n22").text = itemData.chair_name
		itemObj:GetChild("n23").text = itemData.lvl
		itemObj:GetChild("n24").text = itemData.member.."/"..itemData.max_member
		itemObj:GetChild("n25").onClick:Add(self.OnClickJoinItem, self)

	end
end


function BangPaiListWin:GetClanList(from, to, content)
	self:ShowModalWait(NetKeys.QCMD_LIST_CLAN)
	if content ~= nil then
		self:SearchClanByName(content)
	else
		NetClient.Send(NetKeys.QCMD_LIST_CLAN, from, to, "")
	end
end

function BangPaiListWin:SearchClanByName(content)
	NetClient.Send(NetKeys.QCMD_LIST_CLAN, 0, 0, content)
end

function BangPaiListWin:OnClickJoinItem(context)
	-- print(">>>"..context.sender.parent.data.name)
	local id = context.sender.parent.data.id
	ClanMgr:ApplyToJoinClan(id)
end

function BangPaiListWin:ClearClanList(targetList)
	if targetList then
		targetList.numItems = 0
	end
end


