MountListWin = fgui.window_class(WindowBase)

local _listData = {} -- 列表内容
local _typeTab = nil
local _list
local _type
local _pid

function MountListWin:ctor()
    WindowBase.ctor(self)

    self.asyncCreate = false
    self:SetContentSource("Common","技能物品选择列表")
end

function MountListWin:DoInit()
    self:SetXY(50, 50)
    
    _typeTab=self.contentPane:GetController("c1")
    _list=self.contentPane:GetChild("list")
    local p = self
    _list.itemRenderer = function(index, obj) p:RenderItem(index, obj) end
    _list:SetVirtual()
    _list.onClickItem:Add(self.OnClickMount, self)
    
    NetClient.Listen(NetKeys.ACMD_DUANLIAN_MOUNTS, self.OnViewMounts, self)
    
    NetClient.ListenRet(NetKeys.QCMD_DUANLIAN_MOUNTS, self.DefaultQCMDHandler, self)
end

function MountListWin:Open(type, pid)
    _type = type
    _pid = pid
    if not self.isShowing then
        self:Show()
    end
end

function MountListWin:ReadyToUpdate()
    self:ShowModalWait()
    NetClient.Send(NetKeys.QCMD_DUANLIAN_MOUNTS, _type, _pid)
    _typeTab.selectedIndex=3
end

function MountListWin:OnHide()
end

function MountListWin:OnViewMounts(cmd, data)
    self:CloseModalWait()
    _listData = data.list
    _list.numItems = #_listData
end

function MountListWin:RenderItem(index, obj)
    obj:SetItem(_listData[index+1])
end

function MountListWin:OnClickMount(context)
    local item = context.data.data
    G_MountDuanLianWin:ResetSelectedPet(_type, item)
    self:Hide()
end
