require "Common/class"
require "Common/FairyGUI"
require "Common/FairyGame"
require "Common/MyUtils"
require "Common/Defs"
require "Common/Rect"
require "Common/Array"

require "Windows/Windows"

require "MainUI/MainUI"
require "Net/NetClient"

--require "Guide/GuideCenter"

require "ServerAddress"
require "Partner/PartnerManager"

require "Login/LoginUI"
require "Login/RoleUI"
require "World/World"

local cur_frame_rate = 0
local last_frame = 0
local last_time = 0
local function UpdateFrameRate()
    local dt = Time.fixedTime - last_time
    if dt >= 0.5 then
        local df = Time.frameCount - last_frame
        cur_frame_rate = math.floor(df / dt)
        last_frame = Time.frameCount
        last_time = Time.fixedTime
    end
end

local perfCom
local function UpdateStat()
	perfCom.text = "FPS:"..cur_frame_rate
end

function Main()
	fgui.register_extension("ui://Common/装备格子", ItemCell)
	fgui.register_extension("ui://Common/技能格子", SkillCell)
	fgui.register_extension("ui://Common/技能格子带标题", SkillCell)
	fgui.register_extension("ui://Main/技能格子", SkillCell)
	fgui.register_extension("ui://Common/数量输入_带加减", InputCountForm)
    fgui.register_extension("ui://Common/物品信息", ItemInfoWin)
    
	FixedUpdateBeat:Add(UpdateFrameRate)
	ShowFPS()
	PartnerManager.Init()
	LoginUI.Show()
end

function ShowFPS()
	if perfCom==nil then
	    perfCom = UIPackage.CreateObject("Common", "性能信息")
	    perfCom.sortingOrder = SortingOrder.PerfMsg
	    GRoot.inst:AddChild(perfCom)
		UpdateBeat:Add(UpdateStat)
	end
end

function PromptResult(data)
	if data.code~=0 then
		AlertWin.Open(data.msg)
	else
		ChatPanel.AddAlertMsg(msg)
	end
end

function OnDeviceBackKey()
	local topWin = GRoot.inst:GetTopWindow()
	if topWin~=nil then
		if topWin.closeButton~=nil then
			topWin.closeButton.onClick:BubbleCall(InputEvent())
		elseif topWin==G_ConfirmWin.window then
			G_ConfirmWin.contentPane:GetChild("cancel").onClick:BubbleCall(InputEvent())
		elseif topWin==G_AlertWin.window then
			G_AlertWin.contentPane:GetChild("ok").onClick:BubbleCall(InputEvent())
		end
	else
		ConfirmWin.Open('是否退出游戏？', function() Application.Quit() end)
	end
end

function OnDeviceMenuKey()
end
