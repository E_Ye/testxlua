LineGenerator = class()

function LineGenerator:ctor()
	self.x0 = 0
	self.y0 = 0
	self.x1 = 0
	self.y1 = 0
	self.i = 0
	self.steep = 0
	self.sx = 0
	self.sy = 0
	self.dx = 0
	self.dy = 0
	self.e = 0
	self.segSize = 0
end

function LineGenerator:Start(origin, terminal)
	self.x0 = math.floor(origin.x)
	self.y0 = math.floor(origin.y)
	self.x1 = math.floor(terminal.x)
	self.y1 = math.floor(terminal.y)

	self.dx = math.abs(self.x1 - self.x0)
	if self.x1-self.x0>0 then
		self.sx = 1
	else
		self.sx = -1
	end
	self.dy = math.abs(self.y1 - self.y0)
	if self.y1-self.y0>0 then
		self.sy = 1
	else
		self.sy = -1
	end

	if self.dy > self.dx then
		self.steep = 0
		local tmpswap
		tmpswap = self.x0
		self.x0 = self.y0
		self.y0 = tmpswap
		tmpswap = self.dx
		self.dx = self.dy
		self.dy = tmpswap
		tmpswap = self.sx
		self.sx = self.sy
		self.sy = tmpswap
	else
		self.steep = 1
	end
	self.e = self.dy*2 - self.dx
	self.i = 0
end

function LineGenerator:Reset()
	self.dx = -1
end

LineGenerator.get.count = function(self)
	return self.dx + 1
end

LineGenerator.get.hasNext = function(self)
	return self.i <= self.dx
end

function LineGenerator:Next()
	local pt = Vector2.New()
	if self.steep ~= 0 then
		pt.x = self.x0
		pt.y = self.y0
	else
		pt.x = self.y0
		pt.y = self.x0
	end

	if self.i == self.dx then
		self.i = self.i + 1
	else
		while self.e >= 0 do
			self.y0 = self.y0 + self.sy
			self.e = self.e - self.dx*2
		end
		self.x0 = self.x0 + self.sx
		self.e = self.e + self.dy*2
		self.i = self.i + 1
	end
	return pt
end

function LineGenerator:Skip(cnt)
	for j=0,cnt-1 do
		if self.i>=self.dx then break end
		while self.e >= 0 do
			self.y0 = self.y0 + self.sy
			self.e = self.e - self.dx*2
		end
		self.x0 = self.x0 + self.sx
		self.e = self.e + self.dy*2

		self.i = self.i + 1
	end
end