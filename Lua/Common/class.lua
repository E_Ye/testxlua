--创建一个简单的类，这里不搞继承，但支持getter和setter

local function class_index(t, key)
	local mt = getmetatable(t)
	local getter = rawget(mt, "get")
	local func = getter[key]
	if func~=nil then
		return func(t)
	else
		local ret = rawget(t, key)
		if ret==nil then ret = rawget(mt, key) end
		return ret
	end
end

local function class_newindex(t, key, value)
	local mt = getmetatable(t)
	local setter = rawget(mt, "set")
	local func = setter[key]
	if func~=nil then
		return func(t, value)
	else
		return rawset(t, key, value)
	end
end

local function static_class_index(t, key)
	local getter = rawget(t, "get")
	local func = getter[key]
	if func~=nil then
		return func()
	else
		return rawget(t, key)
	end
end

local function static_class_newindex(t, key, value)
	local setter = rawget(t, "set")
	local func = setter[key]
	if func~=nil then
		return func(value)
	else
		return rawset(t, key, value)
	end
end

function class()
	local t = {}
	t.get = {}
	t.set = {}
	t.__index = class_index
	t.__newindex = class_newindex
	
	t.New = function(...)
		local ins = {}
		setmetatable(ins, t)

		if t.ctor then
			t.ctor(ins, ...)
		end

		return ins
	end

	return t
end

function static_class()
	local ins = {}
	ins.get = {}
	ins.set = {}
	local t = {}
	t.__index = static_class_index
	t.__newindex = static_class_newindex
	setmetatable(ins, t)
	return ins
end