Rect = class()

function Rect:ctor(x,y,width,height)
	self.x = x or 0
	self.y = y or 0
	self.width = width or 0
	self.height = height or 0
end

Rect.get.xMin = function(self)
	return self.x
end

Rect.set.xMin = function(self, value)
	self.x  = value
end

Rect.get.yMin = function(self)
	return self.y
end

Rect.set.yMin = function(self, value)
	self.y  = value
end

Rect.get.xMax = function(self)
	return self.x + self.width
end

Rect.set.xMax = function(self, value)
	self.width  = value - self.x
end

Rect.get.yMax = function(self)
	return self.y + self.height
end

Rect.set.yMax = function(self, value)
	self.height  = value - self.y
end

function Rect:Set(x,y,width,height)
	self.x = x
	self.y = y
	self.width = width or 0
	self.height = height or 0
end

function Rect:CopyFrom(r)
	self.x = r.x
	self.y = r.y
	self.width = r.width
	self.height = r.height
end

Rect.get.nativeRect = function(self)
	return UnityEngine.Rect.New(self.x, self.y, self.width, self.height)
end

function Rect:SetEmpty()
	self.x = 0
	self.y = 0
	self.width = 0
	self.height = 0
end

function Rect:Multiply(scale)
	self.x = self.x * scale
	self.y = self.y * scale
	self.width = self.width * scale
	self.height = self.height * scale
end

function Rect:Contains(x, y)
	return self.x <= x and x <= self.xMax and self.y <= y and y <= self.yMax
end