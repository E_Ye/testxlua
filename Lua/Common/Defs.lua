local json = require "cjson"
require "World/SkillEffectDef"

Defs = {}

Defs.ChatChannelNames = { "私聊","世界","帮派","队伍", "综合","系统", }
Defs.ItemTypeYP = "药品"

Defs.SpeItems = {}
Defs.SpeItems.cwls = "i421" -- 宠物粮食(i421)
--Defs.SpeItems.xycwls = "i422" -- 稀有宠物粮食(i422)
--Defs.SpeItems.xycwlsb = "i699" -- 稀有宠物粮食(绑)(i699)
Defs.SpeItems.xt = "i360" --仙桃(i360)
Defs.SpeItems.vipyk = "i441" --VIP月卡(i441)
Defs.SpeItems.EXP_CARD = "i442" --1.5倍经验卡(i442)
Defs.SpeItems.ITEM_SKILL_EXP_CARD = "i443" --1.5倍技能卡(i443)
Defs.SpeItems.MONEY_CARD = "i477" --1.5倍银两卡(i477)
Defs.SpeItems.CWMZK = "i514" --宠物免战牌(i514)
Defs.SpeItems.SMCB = "i445" --生命储备(i445)
Defs.SpeItems.FLCB = "i446" --法力储备(i446)
Defs.SpeItems.JYDD = "i460" --经验丹(低)(i460)
Defs.SpeItems.JYDG = "i461" --经验丹(高)(i461)
Defs.SpeItems.JYD = "i462" --经验丹(i462)
Defs.SpeItems.KBS = "i463" --狂暴石(i463)

Defs.NO_ITEM_BUY_TIPS = "已经用完了，是否前往商城购买？"
Defs.GONGXIAN_EXP_TITLE = "贡献经验"
Defs.GIVE_MONEY = "赠送银两"
Defs.GONGXIAN_CONFIRM = "退出二人修仙后，修仙等级不再保留，是否退出？"

--用于挂机自动跳转
Defs.GuaJiAutoFb = 
{
	--妖王洞
	"s20,s21,s22",
	--狂蜂山
	"s96,s97,s109,s98",
	--蚩尤密窟
	"s99,s100,s101",
	--黄帝秘境
	"s102,s103,s104,s105,s106,s107",
	--海底珊瑚阵
	"s143,s144,s145,s146,s153,s146,s145,s144,s149,s144,s148,s154,s155,s157",
	--昆仑五行阵
	"s164,s165",
	--花神仙宠园
	"s190,s191",
	--兵神剑阵
	"s198,s199",
	--魔罗结界
	"s253,s254,s255,s256,s257",
	--通天古穴
	"s281,s276,s277,s278,s279,s280,s281",
	--雪界祭坛
	"s216,s217",
	--监斩台
	"s227,s228",
	--古墓
	"s262,s263,s264,s265,s266,s267,s268,s269,s270",
	--云谷
	"s356,s357,s358,s359"
}

function Defs.Init()
	Defs.all = json.decode(AssetManager.inst:LoadText("data","defs"))
	Defs.all.effect = json.decode(AssetManager.inst:LoadText("data","effects"))

	local col = Defs.all.scene
	for eid,scene in pairs(col) do
		scene.id = eid
		if scene.img==nil then
			scene.img = eid
		end
		if scene.path==nil then
			scene.path = scene.img
		end
		if scene.zid~=nil then
			scene.parent = Defs.all.scene[scene.zid]
		end
	end

	col = Defs.all.effect
	for kid,effect in pairs(col) do
		SkillEffectDef[kid] = SkillEffectDef.Parse(effect)
	end
	SkillEffectDef["*"] = SkillEffectDef.Parse("hit3")

	local gongEffects = SkillEffectDef.Parse("hit2,<arrow6")
	local jianEffects = SkillEffectDef.Parse("hit3")
	local gunEffects = SkillEffectDef.Parse("hit4")

	col = Defs.all.skill
	for kid,skill in pairs(col) do
		skill.id = kid
		if skill.is_attack and SkillEffectDef[kid]==nil then
			if skill.equip_type=='弓' then
				SkillEffectDef[kid] = gongEffects
			elseif skill.equip_type=='剑' then
				SkillEffectDef[kid] = jianEffects
			elseif skill.equip_type=='棍' then
				SkillEffectDef[kid] = gunEffects
			end
		end
	end

	Defs.basicSkills = {
		[1]=col.k1,
		[2]=col.k14,
		[3]=col.k27,
		[4]=col.k40,
		[5]=col.k198
	}

	col.k56.is_learn = true
	
	Defs.all.game = Defs.all[""]["game"]
end

function Defs.FindMapPath(eid1, eid2)
	local workings = {}
	if eid1==eid2 then return workings end
	table.insert(workings, {eid1})

	local scene
	while #workings>0 do
		local w = workings[1]
		table.remove(workings, 1)
		scene = Defs.all.scene[w[#w]]
		local ended = true
		for i,s in ipairs(scene.exits) do
			if s==eid2 then --found
				table.remove(w, 1) --remove eid1
				table.insert(w, s)
				return w
			end
			scene = Defs.all.scene[s]
			if table.indexOf(w, s)==nil then
				local nw = {}
				for i=1,#w do
					nw[i] = w[i]
				end
				nw[#w+1] = s
				table.insert(workings, nw)
				ended = false
			end
		end
	end
end

function Defs.GetDef(t, id, allowNil)
	local def = Defs.all[t][id]
	if def==nil and not allowNil then
		def = { name='无名', ['type']='unknown' }
		Defs.all[t][id] = def
	end
	return def
end

function Defs.GetDef2(t)
    local def = Defs.all[t]
    if def==nil then
        def = { name='无名', ['type']='unknown' }
        Defs.all[t] = def
    end
    return def
end

function Defs.GetInt(t, id, prop, defv)
	local def = Defs.GetDef(t, id)
	if def[prop]~=nil then
		return def[prop]
	elseif defv~=nil then
		return defv
	else
		return 0
	end
end

function Defs.GetString(t, id, prop, defv)
	local def = Defs.GetDef(t, id)
	if def[prop]~=nil then
		return def[prop]
	else
		return defv
	end
end

function Defs.GetGameDef(id, prop, defv)
    local def = Defs.all.game[id]
    if def~=nil then
        return def[prop]
    else
        return defv
    end
end

function Defs.GetIconSrc(iid)
	if iid==nil then
		return ""
	end

	local s = string.sub(iid, 1, 1)
	if s=="i" then
		local def = Defs.GetDef("item", iid)
		if def~=nil and def.org_id~=nil then
			return def.org_id
		else
			return iid
		end
	elseif s=="r" then
		local def = Defs.GetDef("robot", iid)
		if def~=nil and def.src_id~=nil then
			return def.src_id
		else
			return iid
		end
	elseif s=="p" then
		local def = Defs.GetDef("pet", iid)
		if def~=nil and def.src_id~=nil then
			return def.src_id
		else
			return iid
		end
	else
		return iid
	end
end

function Defs.GetNPCSpeakWord(npcId, fighting)
	local def = Defs.all.robot[npcId]
	if not def then return end
		
	local word
	if fighting then
		if not def.attack_words then return end

		if def.attack_words_array then
			def.attack_words_idx = def.attack_words_idx + 1
			if def.attack_words_idx>#def.attack_words_array then
				def.attack_words_idx = 1
			end
			word = def.attack_words_array[def.attack_words_idx]
		else
			def.attack_words_array = string.split(def.attack_words, "|")
			def.attack_words_idx = 1
			word = def.attack_words_array[1]
		end
	else
		if not def.words then return end
			
		if def.words_array then
			def.words_idx = def.words_idx + 1
			if def.words_idx>#def.words_array then
				def.words_idx = 1
			end
			word = def.words_array[def.words_idx]
		else
			def.words_array = string.split(def.words, "|")
			def.words_idx = 1
			word = def.words_array[1]
		end
	end
	return word
end