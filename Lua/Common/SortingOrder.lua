SortingOrder = 
{
    Talk = 1000, --NPC对话框
    Guide = 2000, --指引
    LevelUp = 4001, --升级提示
    PowerUp = 4002, --战斗力提示
    Drag = 5000, --拖动支持
    Modal = 100000, --最高层级的提示
    PerfMsg = 9999999,
}