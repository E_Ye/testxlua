Flags = class()

function Flags:ctor()
	self._value = 0
	self._counter = 0
	self._owners = {}
end

function Flags:Set(requestor, value)
	local oldValue = self._owners[requestor]
	if oldValue~=nil then
		self._value = self._value - oldValue
		if value~=nil then
			self._value = self._value + value
			self._owners[requestor] = value
		end
	else
		self._counter = self._counter + 1
		self._value = self._value + value
		self._owners[requestor] = value
	end
end

function Flags:Unset(requestor)
	local oldValue = self._owners[requestor]
	if oldValue~=nil then
		self._counter = self._counter - 1
		self._value = self._value - oldValue
		self._owners[requestor] = nil
	end
end

Flags.get.isTrue = function(self)
	return self._counter > 0
end

Flags.get.value = function(self)
	return self._value
end

function Flags:Clear()
	if self._counter>0 then
		self._counter = 0
		self._owners = {}
	end	
	self._value = 0	
end
