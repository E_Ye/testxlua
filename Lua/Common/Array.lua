Array = class()
setmetatable(Array, {__call = Array.New})

function Array:ctor()
	self.length = 0
end

function Array:Push(value)
	self.length = self.length+1
	self[self.length] = value
end

function Array:PushTable(value)
	local len = value.length or #value
	for i=1,len do
		self[self.length+i] = value[i]
	end
	self.length = self.length+len
end

function Array:Pop()
	local value = self[self.length]
	self.length = self.length-1
	return value
end

function Array:Shift()
	local value = self[1]
	table.remove(self, 1)
	self.length = self.length-1
	return value
end 

function Array:Unshift(value)
	self.length = self.length+1
	table.insert(self, 1, value)
end

Array.get.first = function(self)
	if self.length>0 then
		return self[0]
	else
		return nil
	end
end

Array.get.last = function(self)
	if self.length>0 then
		return self[self.length]
	else
		return nil
	end
end
