GameConfig  = FairyGame.GameConfig
SoundManager = FairyGame.SoundManager
Mapping = FairyGame.Mapping
ByteArray = FairyGame.ByteArray
SocketProcessor = FairyGame.SocketProcessor
AssetManager  = FairyGame.AssetManager
AnimationManager = FairyGame.AnimationManager
IconManager = FairyGame.IconManager
ScriptUtils = FairyGame.ScriptUtils
TweenUtils = FairyGame.TweenUtils
SimpleWWW = FairyGame.SimpleWWW
UISource = FairyGame.UISource
JavaUtils = FairyGame.JavaUtils
PathInfo = FairyGame.PathInfo
PlaySettings = FairyGame.PlaySettings
AniImage = FairyGame.AniImage
MyGLoader = FairyGame.MyGLoader
WWWEx = FairyGame.WWWEx
DOTween = DG.Tweening.DOTween
Ease = DG.Tweening.Ease
UtilsDir = FairyGame.UtilsDir
ArrayList = System.Collections.ArrayList

PlayerPrefs = UnityEngine.PlayerPrefs

GameLink = Gotye.GameLink
GLErrorCode = Gotye.GLErrorCode
GLMessage = Gotye.GLMessage
GLMessageStatus = Gotye.GLMessageStatus
GLMessageType = Gotye.GLMessageType
GLTargetType = Gotye.GLTargetType

fgui.null = false

fgui.container = function(base)
	local o = {}

	local base = base or FairyGame.LuaContainer
    setmetatable(o, base)

	o.__index = o
	o.base = base

	o.New = function(...)
		local t = {}
	    setmetatable(t, o)

		local ins = FairyGame.LuaContainer.New()
		ins.peerTable = t
	    tolua.setpeer(ins, t)
	    ins.EventDelegates = {}
	    if t.ctor then
	    	t.ctor(ins,...)
	   	end
	    
	    return ins
	end

	return o
end

fgui.gcomponent = function(base)
	local o = {}

	local base = base or FairyGame.GLuaComponent2
    setmetatable(o, base)

	o.__index = o
	o.base = base

	o.New = function(...)
		local t = {}
	    setmetatable(t, o)

		local ins = FairyGame.GLuaComponent2.New()
		ins.peerTable = t
	    tolua.setpeer(ins, t)
	    ins.EventDelegates = {}
	    if t.ctor then
	    	t.ctor(ins,...)
	   	end
	    
	    return ins
	end

	return o
end