Countdown = class()

function Countdown:ctor(callback, ...)
	self.time = 0
	self.current = 0
	self.callback = callback
	self.callbackParam = {...}
end

function Countdown:Start(time)
	self.time = time
	self.current = time
end

function Countdown:Run()
	if self.time<=0 then return end
	
	self.current = self.current - Time.deltaTime
	if self.current<0 then
		self.time = 0

		self.callback(unpack(self.callbackParam))
	end
end

function Countdown:Reset()
	self.time = 0
end