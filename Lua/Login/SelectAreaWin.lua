SelectAreaWin = fgui.window_class(WindowBase)

local _serverlist

function SelectAreaWin:ctor()
	WindowBase.ctor(self)

    self:SetContentSource("Login", "选择服务器")
    self.OnSelect = event("OnSelect")
    self.asyncCreate = false
    self:Init()
end

function SelectAreaWin:DoInit()
    _serverlist = self.contentPane:GetChild("serverList")
    _serverlist:RemoveChildrenToPool()
    _serverlist.onClickItem:Add(self.OnClickServer, self)

    self:Center()
end

function SelectAreaWin:SetData(areas)
    _serverlist:RemoveChildrenToPool()
    if areas ~= nil then
		for i=1,#areas do
        	local area = areas[i]
			local item = _serverlist:AddItemFromPool()
            local status = area.sta
            local str
            if status==0 then str="(未开放)" elseif status==2 then str="(维护)" else str="" end
            item.title = area.name..str
            item.data = area
        end
    end
end

function SelectAreaWin:OnClickGroup()
end

function SelectAreaWin:OnClickServer()
    local area = _serverlist:GetChildAt(_serverlist.selectedIndex).data
    self.OnSelect(area)
    self:Hide()
end