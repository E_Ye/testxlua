local _view
local _selectView
local _createView
local _roleList
local _randomNames
local _firstTime = true
local _newCreate

RoleUI = {
    created = false,
    visible = false
}

function RoleUI.Show(data)
    if not RoleUI.created then 
        RoleUI.Create()
    end

    RoleUI.visible = true

    _randomNames = {'',''}
    _newCreate = false

    GRoot.inst:AddChild(_view)
    _view:GetController("c1"):SetSelectedIndex(0)    
    --_view:GetChild("bg").asLoader.texture = NTexture.New(UnityEngine.Resources.Load("welcome"))
    
    NetClient.Listen(NetKeys.ACMD_WELCOME, RoleUI.OnServerIdentityList)
    NetClient.Listen(NetKeys.ACMD_RANDOM_NAME, RoleUI.OnServerRandomName)

    NetClient.ListenRet(NetKeys.QCMD_LOGIN, RoleUI.OnLoginResult)
    NetClient.ListenRet(NetKeys.QCMD_CREATE_ROLE, RoleUI.OnServerCreateResult)
    NetClient.ListenRet(NetKeys.QCMD_REMOVE_ROLE, RoleUI.OnServerDelResult)

    if data==nil then
        GRoot.inst:ShowModalWait()
        NetClient.ConnectAndLogin()
    else
        if #data.rs == 0 then
            _view:GetController("c1").selectedIndex = 1
        else
            _view:GetController("c1").selectedIndex = 0
            RoleUI.SetIdentityList(data)
        end
    end
end

function RoleUI.Hide()
   RoleUI.visible = false

    if _view.parent ~= nil then
        GRoot.inst:CloseModalWait()
        GRoot.inst:RemoveChild(_view)
    end

    NetClient.Unlisten(NetKeys.ACMD_WELCOME, RoleUI.OnServerIdentityList)
    NetClient.UnlistenRet(NetKeys.QCMD_CREATE_ROLE, RoleUI.OnServerCreateResult)
    NetClient.UnlistenRet(NetKeys.QCMD_REMOVE_ROLE, RoleUI.OnServerDelResult)
end

function RoleUI.Create()
    RoleUI.created = true

    _view = UIPackage.CreateObject("Login", "选人和创建")
    _view:SetSize(GRoot.inst.width, GRoot.inst.height)
    _view:AddRelation(GRoot.inst, RelationType.Size)

    _view:GetController("c1").onChanged:Add(RoleUI.OnViewChanged)
    _selectView = _view:GetChild("selectView")
    _createView = _view:GetChild("createView")

    _selectView:GetChild("startBtn").onClick:Add(RoleUI.OnClickStart)
    --[[_selectView:GetChild("backBtn").onClick:Add(function()
        NetClient.Close()
        RoleUI.Hide()
        LoginUI.Show()
    end)]]

    _roleList = _selectView:GetChild("list")
    _roleList:RemoveChildrenToPool()
    _roleList.onClickItem:Add(RoleUI.OnClickRole)

    _createView:GetChild("startBtn").onClick:Add(RoleUI.OnClickStart)
    _createView:GetChild("randomBtn").onClick:Add(RoleUI.OnClickRandomName)
    _createView:GetChild("backBtn").onClick:Add(function()
        _view:GetController("c1").selectedIndex = 0
    end)

    _createView:GetController("faction").onChanged:Add(RoleUI.OnRoleChanged)  
    _createView:GetController("gender").onChanged:Add(RoleUI.OnRoleChanged)  
end

function RoleUI.OnViewChanged()
    if _view:GetController("c1").selectedIndex == 1 then
        _createView:GetChild("backBtn").visible = _roleList.numChildren > 0

        if string.len(_randomNames[1])==0 then
            RoleUI.OnClickRandomName()
        end
    end
end

function RoleUI.OnRoleChanged()
    _createView:GetController("role").selectedIndex =  
        _createView:GetController("faction").selectedIndex*2+_createView:GetController("gender").selectedIndex

    if _createView:GetController("gender").selectedIndex == 1 then
        _createView:GetChild("name").text = _randomNames[1]
    else
        _createView:GetChild("name").text = _randomNames[2]
    end
end

function RoleUI.OnClickStart()
    if _view:GetController("c1").selectedIndex == 0 then
        if _roleList.selectedIndex < 0 then return end
        local charData = _roleList:GetChildAt(_roleList.selectedIndex).data
        
        GRoot.inst:ShowModalWait()
        NetClient.Send(NetKeys.QCMD_LOGIN, charData.id, "", "")
    else
        local name = _createView:GetChild("name").text
        if string.len(name)==0 then
            AlertWin.Open('请输入名称')
            return
        end
        GRoot.inst:ShowModalWait()
        local faction = _createView:GetController("faction").selectedIndex+1
        local gender = _createView:GetController("gender").selectedIndex+1
        NetClient.Send(NetKeys.QCMD_CREATE_ROLE, gender, faction, name)
    end
end

function RoleUI.OnClickRandomName()
    _createView:GetChild("randomBtn"):GetTransition("shake"):Play()
    GRoot.inst:ShowModalWait()

    NetClient.Send(NetKeys.QCMD_RANDOM_NAME, 0)
end

function RoleUI.OnClickRole(context)
    local obj = context.data
    if obj.data == nil then
        _view:GetController("c1").selectedIndex = 1
        _roleList.selectedIndex = 0
    end
end

function RoleUI.OnDeleteRole(context)
    ConfirmWin.Open("确定删除该角色？", function()
        local charData = _roleList:GetChildAt(_roleList.selectedIndex).data
        GRoot.inst:ShowModalWait()
        NetClient.Send(NetKeys.QCMD_REMOVE_ROLE, charData.id, NetClient.CKey)
    end)
end

function RoleUI.OnServerIdentityList(cmd, data)
    GRoot.inst:CloseModalWait()
    if #data.rs == 0 then
        _view:GetController("c1").selectedIndex = 1
    else
        _view:GetController("c1").selectedIndex = 0
        RoleUI.SetIdentityList(data)
    end
end

function RoleUI.SetIdentityList(data)
    local roleListData = data.rs
    _roleList:RemoveChildrenToPool()
    for i=1,#roleListData do
    	local rd = roleListData[i]
        local item = _roleList:AddItemFromPool()
        item:GetController("c1").selectedIndex = 0

        item.title = rd.na
        item.icon = UIPackage.GetItemURL("Sucai",'u'..rd.fc..rd.sx)
        item:GetChild("icon").text = rd.lv
        item:GetChild("deleteBtn").onClick:Set(RoleUI.OnDeleteRole)
        item.scrollPane.posX = 0
        item.scrollPane.touchEffect = true

        item.data = rd
    end
    if #roleListData < 4 then
        for i = #roleListData,3 do
        	local item = _roleList:AddItemFromPool()
            item:GetController("c1").selectedIndex = 1
            item.scrollPane.posX = 0
            item.scrollPane.touchEffect = false
            item.data = nil
        end
    end

    if _newCreate then
        _roleList.selectedIndex = #roleListData-1
    else
        _roleList.selectedIndex = 0
    end
end

function RoleUI.OnServerLoginResult(cmd, data)
    GRoot.inst:CloseModalWait();

    AlertWin.Open(data.msg)
end

function RoleUI.OnServerRandomName(cmd, data)
    GRoot.inst:CloseModalWait()

    _randomNames[1] = data.boy
    _randomNames[2] = data.girl

    if _createView:GetController("gender").selectedIndex == 1 then
        _createView:GetChild("name").text = _randomNames[1]
    else
        _createView:GetChild("name").text = _randomNames[2]
    end
end

function RoleUI.OnLoginResult(cmd, data)
    if data.code~=0 then
        GRoot.inst:CloseModalWait()
        AlertWin.Open(data.msg)
    end
end

function RoleUI.OnServerCreateResult(cmd, data)
    GRoot.inst:CloseModalWait()

    if data.code==0 then
        _newCreate = true
        GRoot.inst:ShowModalWait()
        NetClient.Send(NetKeys.QCMD_WELCOME, NetClient.VER, NetClient.USER_KEY)
    else
        AlertWin.Open(data.msg)
    end
end

function RoleUI.OnServerDelResult(cmd, data)
    GRoot.inst:CloseModalWait()
    if data.code==0 then
        GRoot.inst:ShowModalWait()
        NetClient.Send(NetKeys.QCMD_WELCOME, NetClient.VER, NetClient.USER_KEY)
    else
        AlertWin.Open(data.msg)
    end
end