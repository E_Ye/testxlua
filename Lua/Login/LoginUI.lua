local json = require "cjson"
require 'Login/SelectAreaWin'

local _view
local _selectAreaWin
local _accountPanel
local _c1

LoginUI = {
    created = false,
    visible = false
}

function LoginUI.Create()
    LoginUI.created = true

    _view = UIPackage.CreateObject("Login", "登录")
    _view:SetSize(GRoot.inst.width, GRoot.inst.height)
    _view:AddRelation(GRoot.inst, RelationType.Size)

    _view:GetChild("version").text = GameConfig.appVersion

    _c1 = _view:GetController("c1")
    _accountPanel = _view:GetChild("accountPanel")
    _accountPanel:GetChild("login").onClick:Add(LoginUI.OnClickLogin)
    _accountPanel:GetChild("quickRegister").onClick:Add(LoginUI.OnClickRegister)

    _selectAreaWin = SelectAreaWin.New()
    _selectAreaWin.OnSelect:Add(
        function(area)
            _view:GetChild("area").text = area.name
            _view:GetChild("area").data = area
        end
    )
    _view:GetChild("changeArea").onClick:Add( 
        function()
            _selectAreaWin:Show()
        end
    )

    local btnStart = _view:GetChild("start")
    btnStart.onClick:Add(PartnerManager.Login)
end

function LoginUI.Show()
    if not LoginUI.created then
        LoginUI.Create()
    end

    LoginUI.visible = true
    GRoot.inst:AddChild(_view)
    coroutine.start(LoginUI.DoGetAreas)

    local account = PlayerPrefs.GetString("account", '')
    local password = PlayerPrefs.GetString("password", '')
    if string.len(account)>0 then
        account = ScriptUtils.DecryptString(account)
    end
    if string.len(password)>0 then
        password = ScriptUtils.DecryptString(password)
    end
    
    _accountPanel:GetChild("account").text = account
    _accountPanel:GetChild("password").text = password

    if string.len(account)==0 or string.len(password)==0 then
        LoginUI.ShowAccountPanel()
    else
        _c1.selectedIndex = 0
    end
end

function LoginUI.Hide()
    LoginUI.visible = false

    if _view.parent ~= nil then
        GRoot.inst:RemoveChild(_view)
    end
end

function LoginUI.ShowAccountPanel()
    _c1.selectedIndex = 1
end

function LoginUI.LoginDirect()
    local account = PlayerPrefs.GetString("account", '')
    local password = PlayerPrefs.GetString("password", '')
    if string.len(account)>0 and string.len(password)>0 then
        LoginUI.OnClickLogin()
    else
        LoginUI.ShowAccountPanel()
    end
end

function LoginUI.OnClickLogin()
    coroutine.start(LoginUI.DoLogin)
end

function LoginUI.OnClickRegister()
    coroutine.start(LoginUI.DoQuickRegister)
end

function LoginUI.OnPartnerLoginSuccess()
    coroutine.start(LoginUI.DoEnter)
end

function LoginUI.DoGetAreas()
--[[
    {
        "id": "",       //服务器标识
        "name": "",     //服务器名称
        "sta": 0,       //状态（0-未开放，1-开放，2-维护）
        "ot": "",       //开服时间
        "pri": 0,       //优先级（1为特别推荐）
        "rna": "",      //最近登录的角色名称
        "rid": "",      //最近登录的角色id
        "rlv": "",      //最近登录的角色等级
        "rfc": "",      //最近登录的角色门派
        "rse": "",      //最近登录的角色性别
        "rtm": "",      //最近登录时间
    },
]]
    print "get areas"

    _view:GetChild("area").text = ''
    _view:GetChild("area").data = nil
    _selectAreaWin:SetData()

    GRoot.inst:ShowModalWait()
    
    local form = {}
    form.client_id=GameConfig.terminalId
    form.qd=GameConfig.partnerId
    local account = PlayerPrefs.GetString("account", '')
    if string.len(account)>0 then
        form.uid = ScriptUtils.DecryptString(account)
    else
        form.uid = ''
    end    
    form.ver=1

    local www = SimpleWWW.New(ServerAddress.GetAreas, form)
    while not www.isDone do
        coroutine.step()
    end

    GRoot.inst:CloseModalWait()
    if www.error~=nil and string.len(www.error)>0 then
        AlertWin.Open(www.error)
    else
        local data = ScriptUtils.Decompress(www.bytes)
        local str = ScriptUtils.GetUtf8String(data)

        local resp = json.decode(str)
        if resp == json.null then
            AlertWin.Open("unknow response - "..string.sub(str, 1, 20))
        elseif resp.code ~= nil and tonumber(resp.code)~= 0 then
            AlertWin.Open(resp.msg)
        else
            ServerAddress.Enter = resp.entry
            ServerAddress.Pay = resp.fill

            local priArea = nil
            local areas = resp.areas
            for i = 1, #areas do
                if tonumber(areas[i].pri) == 1 then
                    priArea = areas[i]
                    break
                end
            end
            if priArea == nil then
                priArea = areas[1]
            end

            _view:GetChild("area").text = priArea.name
            _view:GetChild("area").data = priArea

            _selectAreaWin:SetData(areas)
        end
    end
end

function LoginUI.DoQuickRegister()
    print "quick register"

    local account = CreateRandomPassword(6)
    local password = CreateRandomPassword(6)

    local form = {}
    form.qd = GameConfig.partnerId
    form.client_id = GameConfig.terminalId
    form.name = account
    form.pwd = password
    if UNITY_IPHONE then
        form.ttp=1
    else
        form.ttp=0
    end
    form.ckey = ScriptUtils.md5(GameConfig.partnerId..GameConfig.terminalId..account..password..PartnerManager.ENTRY_KEY)
    local www = SimpleWWW.New(ServerAddress.Register, form)

    GRoot.inst:ShowModalWait()
    while not www.isDone do
        coroutine.step()
    end

    GRoot.inst:CloseModalWait()
    if www.error~=nil and string.len(www.error)>0 then
        AlertWin.Open(www.error)
    else
        local str = ScriptUtils.GetUtf8String(www.bytes)
        local resp = json.decode(str)
        if resp == json.null then
            AlertWin.Open("unknow response - "..string.sub(str, 1, 20))
        elseif resp.code ~= nil and tonumber(resp.code) ~= 0 then
            AlertWin.Open(resp.msg)
        else
            _accountPanel:GetChild("account").text = account
            _accountPanel:GetChild("password").text = password

            PlayerPrefs.SetString("account", ScriptUtils.EncryptString(account))
        end
    end
end

function LoginUI.DoLogin()
    local randomCode = CreateRandomPassword(6)

    local account = _accountPanel:GetChild("account").text
    local password = _accountPanel:GetChild("password").text
    
    PlayerPrefs.SetString("account", ScriptUtils.EncryptString(account))
    PlayerPrefs.SetString("password", ScriptUtils.EncryptString(password))

    local form = {}
    form.qd = GameConfig.partnerId
    form.client_id = GameConfig.terminalId
    if UNITY_IPHONE then
        form.ttp = 1
    else
        form.ttp = 0
    end
    form.uid = account
    --MD5(id + cpwd + gid + code)
    local digestedPwd = ScriptUtils.md5(account..password.."xym"..randomCode)
    form.pwd = digestedPwd
    form.code = randomCode
    ---MD5(qd+client_id+uid+pwd+code+entry_key)
    form.ckey = ScriptUtils.md5(GameConfig.partnerId..GameConfig.terminalId..account..digestedPwd..randomCode..PartnerManager.ENTRY_KEY)

    local www = SimpleWWW.New(ServerAddress.Login, form)
    GRoot.inst:ShowModalWait()
    while not www.isDone do
        coroutine.step()
    end

    GRoot.inst:CloseModalWait()
    if www.error~=nil and string.len(www.error)>0 then
        LoginUI.ShowAccountPanel()
        AlertWin.Open(www.error)
    else
        local str = ScriptUtils.GetUtf8String(www.bytes)
        local resp = json.decode(str)
        if resp == json.null then
            AlertWin.Open("unknow response - "..string.sub(str, 1, 20))
        elseif resp.code ~= nil and tonumber(resp.code) ~= 0 then
            LoginUI.ShowAccountPanel()
            AlertWin.Open(resp.msg)
        else
            PartnerManager.proxy.OnLoginOk(resp)
        end
    end
end

function LoginUI.DoEnter()
    print "enter game"

    local areaInfo = _view:GetChild("area").data
    if areaInfo == nil then
        LoginUI.DoGetAreas()
        return
	end

    local form = {}
    form.q=GameConfig.partnerId
    form.i=GameConfig.terminalId
    form.a=areaInfo.id
    if UNITY_IPHONE then
        form.ttp=1
    else
        form.ttp=0
    end
    PartnerManager.FillEnterForm(form)

    local www = SimpleWWW.New(ServerAddress.Enter, form)

    GRoot.inst:ShowModalWait()
    while not www.isDone do
        coroutine.step()
    end

    GRoot.inst:CloseModalWait()
    if www.error~=nil and string.len(www.error)>0 then
        AlertWin.Open(www.error)
    else
        local str = ScriptUtils.GetUtf8String(www.bytes)
        local resp = json.decode(str)
        if resp == json.null then
            AlertWin.Open("unknow response - "..str)
        elseif resp.code ~= nil and tonumber(resp.code) ~= 0 then
            AlertWin.Open(resp.msg)
        elseif resp.key==nil or string.len(resp.key)==0 then
            AlertWin.Open("unknow response - "..str)
        else
            NetClient.USER_KEY = resp.key
            NetClient.HOST = resp.host
            NetClient.PORT = resp.port
            NetClient.AREA_ID = areaInfo.id
            NetClient.AREA_NAME = areaInfo.name
            if NetClient.PORT == 0 then NetClient.PORT = 80 end

            LoginUI.DoConnect()
        end
    end
end

function LoginUI.DoConnect()
    GRoot.inst:ShowModalWait()
    LoginUI.StartGame()
end

function LoginUI.StartGame()
    if Defs.all==nil then
        Defs.Init()
        NetClient.Init()
        World.Create()
    end

    NetClient.Listen(NetKeys.ACMD_WELCOME, LoginUI.OnServerIdentityList)
    NetClient.ConnectAndLogin()
end

function LoginUI.OnServerIdentityList(cmd, data)
    NetClient.Unlisten(NetKeys.ACMD_WELCOME, LoginUI.OnServerIdentityList)

    GRoot.inst:CloseModalWait()
    LoginUI.Hide()
    RoleUI.Show(data)
end
