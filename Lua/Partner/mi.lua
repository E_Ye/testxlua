local mi = {}

local _miSDK
local _session

function mi.Init()
	_miSDK = JavaUtils.GetJavaClass("com.yytou.xiahun3d.Mi")
    JavaUtils.CallJavaStatic(_miSDK, "Init", {"2882303761517357342", "5411735716342"}) --//AppId, AppKey, --"2882303761517354656", "5841735458656"
    PartnerManager.SupportReLogin = false
end

function mi.Login()
	GRoot.inst:ShowModalWait()
	JavaUtils.CallJavaStatic(_miSDK, "Login", nil)
end

function mi.ReLogin()
	GRoot.inst:ShowModalWait()
	JavaUtils.CallJavaStatic(_miSDK, "Login", nil)
end

function mi.FillEnterForm(form)
    form.u=NetClient.USER_ID
    form.k=_session
    form.ckey=ScriptUtils.md5(form.i..NetClient.USER_ID..form.a.._session..Partner.ENTRY_KEY)
end

function mi.Pay(amount, data)
	--public static void Pay(String orderId, String cpUserInfo, String productCode,String amount, 
			--String serverName, String userId, String userName, String userLvl, String userVip, String userBalance)
	local roleAttr = Player.instance.attribute
	JavaUtils.CallJavaStatic(_miSDK, "Pay", {data.oid, "dummy",  "",  ""..amount, 
	 	NetClient.AREA_NAME, NetClient.USER_ID, Player.instance.name, ""..roleAttr:GetAttr('当前等级'), 
	 	"vip"..roleAttr:GetAttr('Vip等级'), ""..roleAttr:GetAttr('元宝')}) 
end

function mi.JavaCallback(cmd, param)
	if cmd=="Login" then
		GRoot.inst:CloseModalWait()
		local arr = string.split(param, ",")
		local ret = arr[1]
		if ret=="success" then
			NetClient.USER_ID = arr[2]
			_session = arr[3]
			PartnerManager.LoginSuccess()
        elseif ret=="cancel" then
        	print("mi login "..ret)
        elseif ret=="wait" then
        	print("mi login "..ret)
        elseif ret=="fail" then
        	print("mi login "..ret)
        else
        	print("mi login "..ret)
        end
	elseif cmd=="Pay" then

	end
end

return mi