local json = require "cjson"

PartnerManager = {}
PartnerManager.SupportReLogin = true

PartnerManager.ENTRY_KEY = "u24HUNG74hzjHADQOF1EoNId2NAQlIvN"

local _proxy

function PartnerManager.Init()
	if GameConfig.partnerId=="mi" then
		_proxy = dofile("Partner/mi")
	else
		_proxy = dofile("Partner/yytou")
	end
    PartnerManager.proxy = _proxy
end

function PartnerManager.Login()
	_proxy.Login()
end

function PartnerManager.ReLogin()
	_proxy.ReLogin()
end

function PartnerManager.LoginSuccess()
    LoginUI.OnPartnerLoginSuccess()
end

function PartnerManager.FillEnterForm(form)
	_proxy.FillEnterForm(form)
end

function PartnerManager.EnterMain()
	_proxy.EnterMain()
end

function PartnerManager.Pay(amount)
	coroutine.start(PartnerManager.DoPay, amount)
end

function PartnerManager.DoPay(amount)
	GRoot.inst:ShowModalWait()

	local form = {}
    form.qd=GameConfig.partnerId
    form.cid=GameConfig.terminalId
    form.sid=NetClient.AREA_ID
    form.uif=NetClient.USER_ID
    form.amount=amount
    if UNITY_IPHONE then
        form.ttp=1
    else
        form.ttp=0
    end
    form.ckey=ScriptUtils.md5(form.cid..form.sid..form.uif..Partner.ENTRY_KEY) --i+u+a+t+e+dkey

    local www = SimpleWWW(ServerAddress.Pay, form)

    GRoot.inst:ShowModalWait()
    while not www.isDone do
        coroutine.step()
    end

    GRoot.inst:CloseModalWait()
    if www.error~=nil and string.len(www.error)>0 then
        AlertWin.Open(www.error)
    else
        local str = ScriptUtils.GetUtf8String(www.bytes)
        print(str)
        local resp = json.decode(str)
        if resp == json.null then
            AlertWin.Open("unknow response - "..str)
        elseif resp.code ~= nil and tonumber(resp.code) ~= 0 then
            AlertWin.Open(resp.msg)
        else
        	_proxy.Pay(amount, resp)
        end
    end
end

function PartnerManager.JavaCallback(cmd, param)
	_proxy.JavaCallback(cmd, param)
end

