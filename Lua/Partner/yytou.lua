local yytou = {}

local _loginJTWin
local _userName = ''
local _dKey = ''
local _ext = ''
local _time = 0

function yytou.Init()
end

function yytou.Login()        
    LoginUI.LoginDirect()
end

function yytou.ReLogin()
	LoginUI.ShowAccountPanel()
end

function yytou.OnLoginOk(data)
    NetClient.USER_ID = data.uid
    _userName = data.name
    _dKey = data.dkey
    _time = data.time
    _ext = data.ext
    if _ext == nil then _ext = '' end

    PartnerManager.LoginSuccess()
end

function yytou.FillEnterForm(form)
    form.u=NetClient.USER_ID
    form.t=_time
    form.e=_ext
    form.ckey=ScriptUtils.md5(form.i..NetClient.USER_ID..form.a.._time.._ext.._dKey) --i+u+a+t+e+dkey
end

function yytou.EnterMain()
	_loginJTWin:Dispose()
    _loginJTWin = nil
end

function yytou.Pay(amount, data)
    AlertWin.Open("此渠道未开放充值")
end

return yytou