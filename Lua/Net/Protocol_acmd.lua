--[[  这是自动生成的文件，请勿修改  ]]
--stc.xml ver 1.0

NetKeys.ACMD_ACTIVE = 0
NetKeys.ACMD_WELCOME = 1
NetKeys.ACMD_LINES = 2
NetKeys.ACMD_RET = 3
NetKeys.ACMD_RET_EXT = 4
NetKeys.ACMD_ENTER_QUEUE_INFO = 5
NetKeys.ACMD_RANDOM_NAME = 6
NetKeys.ACMD_REDIRECT = 7
NetKeys.ACMD_ALERT = 21
NetKeys.ACMD_ALERT_EXT = 22
NetKeys.ACMD_SYS_STATUS = 23
NetKeys.ACMD_CHANGE_ATTRS = 24
NetKeys.ACMD_CHANGE_ATTRS2 = 25
NetKeys.ACMD_SYNC_ROLE = 26
NetKeys.ACMD_TASK_TRACKS = 27
NetKeys.ACMD_FINISH_TASK = 28
NetKeys.ACMD_USER_STATUS = 29
NetKeys.ACMD_TALK = 30
NetKeys.ACMD_OBTAIN_EXP = 31
NetKeys.ACMD_UPGRADE = 32
NetKeys.ACMD_OPEN_WIN = 33
NetKeys.ACMD_MOVE_NPC = 34
NetKeys.ACMD_THINGS = 101
NetKeys.ACMD_ADD_USER = 102
NetKeys.ACMD_ADD_ROBOT = 103
NetKeys.ACMD_ADD_ITEM = 104
NetKeys.ACMD_ADD_EXIT = 105
NetKeys.ACMD_REMOVE_USER = 106
NetKeys.ACMD_REMOVE_ROBOT = 107
NetKeys.ACMD_REMOVE_ITEM = 108
NetKeys.ACMD_REMOVE_EXIT = 109
NetKeys.ACMD_MOVE_USER = 110
NetKeys.ACMD_MOVE_ROBOT = 111
NetKeys.ACMD_MOVE_ITEM = 112
NetKeys.ACMD_CALL_OUT_PET = 113
NetKeys.ACMD_CALL_OUT_MOUNT = 114
NetKeys.ACMD_CALL_OUT_MAGIC = 115
NetKeys.ACMD_CALL_BACK_PET = 116
NetKeys.ACMD_CALL_BACK_MOUNT = 117
NetKeys.ACMD_CALL_BACK_MAGIC = 118
NetKeys.ACMD_OBJECT_TASK_STATUS = 119
NetKeys.ACMD_CHANGE_EQUIPS = 120
NetKeys.ACMD_QUERY_ATTRS = 201
NetKeys.ACMD_MAIN_LIST = 207
NetKeys.ACMD_VIEW_USER = 251
NetKeys.ACMD_XIULIAN_JINGMAI = 253
NetKeys.ACMD_VIEW_ERXX = 256
NetKeys.ACMD_CLICK_HEAD = 264
NetKeys.ACMD_MY_ITEMS = 301
NetKeys.ACMD_OBTAIN_ITEM = 302
NetKeys.ACMD_PICK_ITEM = 304
NetKeys.ACMD_VIEW_ITEM = 306
NetKeys.ACMD_USE_ITEM = 309
NetKeys.ACMD_LIST_USER_ITEMS = 310
NetKeys.ACMD_MY_MAGICS = 351
NetKeys.ACMD_VIEW_MAGIC = 352
NetKeys.ACMD_MAGIC_LIANHUA_VIEW = 356
NetKeys.ACMD_MY_PETS = 401
NetKeys.ACMD_VIEW_PET = 402
NetKeys.ACMD_TUJIANS = 408
NetKeys.ACMD_TUJIAN_VIEW = 409
NetKeys.ACMD_HECHENG_PET_VIEW = 411
NetKeys.ACMD_HECHENG_PET = 412
NetKeys.ACMD_HECHENG_PETS = 413
NetKeys.ACMD_LIANYAOHU_GEZI = 414
NetKeys.ACMD_LIANYAOHU_GO = 415
NetKeys.ACMD_MY_MOUNTSES = 451
NetKeys.ACMD_VIEW_MOUNT = 452
NetKeys.ACMD_BUILD_MOUNTS_RATE = 456
NetKeys.ACMD_DUANLIAN_MOUNTS = 457
NetKeys.ACMD_ATTACK = 501
NetKeys.ACMD_ATTACK_FIRE = 502
NetKeys.ACMD_ATTACK_PREPARE = 503
NetKeys.ACMD_TASKS = 601
NetKeys.ACMD_VIEW_NPC = 651
NetKeys.ACMD_FRIENDS = 701
NetKeys.ACMD_FIND_USER = 702
NetKeys.ACMD_CHAT_MSG = 703
NetKeys.ACMD_MAILS = 801
NetKeys.ACMD_TEAM_VIEW = 901
NetKeys.ACMD_TEAM_CHANGE = 902
NetKeys.ACMD_SORTBOARD = 1001
NetKeys.ACMD_MALL_OPEN = 1101
NetKeys.ACMD_HD_INFO = 1201
NetKeys.ACMD_HD_NEW = 1202
NetKeys.ACMD_SIGN_INFO = 1203
NetKeys.ACMD_QITIAN_VIEW = 1210
NetKeys.ACMD_QUERY_HANGING_SETUP = 1300
NetKeys.ACMD_HANGING_GJK = 1304
NetKeys.ACMD_EMBED_VIEW = 1350
NetKeys.ACMD_UPGRADE_WEAPONS = 1353
NetKeys.ACMD_UPGRADE_WEAPON_VIEW = 1354
NetKeys.ACMD_BUILD_ITEMS = 1357
NetKeys.ACMD_BUILD_ITEM_VIEW = 1358
NetKeys.ACMD_RONGLIAN_ITEMS = 1360
NetKeys.ACMD_RONGLIAN_ITEM_VIEW = 1361
NetKeys.ACMD_XILIAN_RONGLIAN_VIEW = 1363
NetKeys.ACMD_XILIANCHI_XILIAN_VIEW = 1364
NetKeys.ACMD_XILIANCHI_XILIAN_ITEM_VIEW = 1365
NetKeys.ACMD_XILIANCHI_JINGLIAN_VIEW = 1372
NetKeys.ACMD_XILIANCHI_JINGLIAN_ITEM_VIEW = 1373
NetKeys.ACMD_LIST_CLAN = 1400
NetKeys.ACMD_VIEW_CLAN = 1402
NetKeys.ACMD_LIST_CLAN_MEMBER = 1403
NetKeys.ACMD_LIST_JOIN_CLAN_REQS = 1404
NetKeys.ACMD_VIEW_CLAN_LOGS = 1422
NetKeys.ACMD_VIEW_CLAN_ITEM_SEIZERS = 1423
NetKeys.ACMD_VIEW_CLAN_ITEM_FLAG = 1424
NetKeys.ACMD_VIEW_CLAN_ITEM_STATUS = 1427
NetKeys.ACMD_VIEW_CLAN_ITEMS = 1430
NetKeys.ACMD_DIAOYU_VIEW = 1450
NetKeys.ACMD_DO_DIAOYU = 1451
NetKeys.ACMD_TOUHU_VIEW = 1452
NetKeys.ACMD_TOUHU = 1453

local names = NetKeys.ACMD_NAMES
local handlers = NetKeys.UnpackHandlers
local json = require 'cjson'
local UnpackJSONPacket = function(packet)
	return json.decode(packet:ReadJSONString())
end

names[0] = 'ACMD_ACTIVE'
handlers[0] = function(packet)
	local m0 = {}
	m0.client_time = int64.tonum2(packet:ReadLong())
	m0.server_time = int64.tonum2(packet:ReadLong())
	return m0
end

names[1] = 'ACMD_WELCOME'
handlers[1] = UnpackJSONPacket

names[2] = 'ACMD_LINES'
handlers[2] = UnpackJSONPacket

names[3] = 'ACMD_RET'
handlers[3] = function(packet)
	local m0 = {}
	m0.rcmd = packet:ReadShort()
	m0.code = packet:ReadByte()
	m0.msg = packet:ReadString()
	return m0
end

names[4] = 'ACMD_RET_EXT'
handlers[4] = UnpackJSONPacket

names[5] = 'ACMD_ENTER_QUEUE_INFO'
handlers[5] = function(packet)
	local m0 = {}
	m0.no = packet:ReadInt()
	m0.time = packet:ReadInt()
	return m0
end

names[6] = 'ACMD_RANDOM_NAME'
handlers[6] = function(packet)
	local m0 = {}
	m0.type = packet:ReadByte()
	m0.boy = packet:ReadString()
	m0.girl = packet:ReadString()
	return m0
end

names[7] = 'ACMD_REDIRECT'
handlers[7] = function(packet)
	local m0 = {}
	m0.host = packet:ReadString()
	m0.port = packet:ReadShort()
	m0.id = packet:ReadString()
	m0.time = packet:ReadInt()
	m0.ckey = packet:ReadString()
	m0.param = packet:ReadString()
	return m0
end

names[21] = 'ACMD_ALERT'
handlers[21] = function(packet)
	local m0 = {}
	m0.type = packet:ReadByte()
	m0.msg = packet:ReadString()
	return m0
end

names[22] = 'ACMD_ALERT_EXT'
handlers[22] = UnpackJSONPacket

names[23] = 'ACMD_SYS_STATUS'
handlers[23] = function(packet)
	local m0 = {}
	m0.type = packet:ReadByte()
	m0.value = packet:ReadInt()
	return m0
end

names[24] = 'ACMD_CHANGE_ATTRS'
handlers[24] = function(packet)
	local m0 = {}
	m0.type = packet:ReadByte()
	m0.id = packet:ReadString()
	local m1 = {}
	local cnt1 = packet:ReadUShort()
	local key1
	for i=1,cnt1 do
		key1 = packet:ReadString()
		m1[key1] = int64.tonum2(packet:ReadLong())
	end
	m0.attrs = m1
	return m0
end

names[25] = 'ACMD_CHANGE_ATTRS2'
handlers[25] = function(packet)
	local m0 = {}
	m0.type = packet:ReadByte()
	m0.id = packet:ReadString()
	local m1 = {}
	local cnt1 = packet:ReadUShort()
	local key1
	for i=1,cnt1 do
		key1 = packet:ReadString()
		m1[key1] = packet:ReadString()
	end
	m0.attrs = m1
	return m0
end

names[26] = 'ACMD_SYNC_ROLE'
handlers[26] = UnpackJSONPacket

names[27] = 'ACMD_TASK_TRACKS'
handlers[27] = UnpackJSONPacket

names[28] = 'ACMD_FINISH_TASK'
handlers[28] = function(packet)
	return packet:ReadString()
end

names[29] = 'ACMD_USER_STATUS'
handlers[29] = function(packet)
	local m0 = {}
	m0.type = packet:ReadByte()
	m0.value = int64.tonum2(packet:ReadLong())
	return m0
end

names[30] = 'ACMD_TALK'
handlers[30] = UnpackJSONPacket

names[31] = 'ACMD_OBTAIN_EXP'
handlers[31] = function(packet)
	local m0 = {}
	m0.money = int64.tonum2(packet:ReadLong())
	m0.type = packet:ReadByte()
	m0.exp = int64.tonum2(packet:ReadLong())
	m0.rid = packet:ReadString()
	return m0
end

names[32] = 'ACMD_UPGRADE'
handlers[32] = function(packet)
	local m0 = {}
	m0.type = packet:ReadByte()
	m0.lvl = packet:ReadShort()
	m0.id = packet:ReadString()
	return m0
end

names[33] = 'ACMD_OPEN_WIN'
handlers[33] = function(packet)
	local m0 = {}
	m0.op = packet:ReadString()
	m0.win = packet:ReadString()
	m0.rid = packet:ReadString()
	return m0
end

names[34] = 'ACMD_MOVE_NPC'
handlers[34] = function(packet)
	local m0 = {}
	m0.type = packet:ReadByte()
	m0.target = packet:ReadString()
	local m1 = {}
	local cnt1 = packet:ReadUShort()
	for i=1,cnt1 do
		table.insert(m1, packet:ReadShort())
	end
	m0.tracks = m1
	return m0
end

names[101] = 'ACMD_THINGS'
handlers[101] = UnpackJSONPacket

names[102] = 'ACMD_ADD_USER'
handlers[102] = UnpackJSONPacket

names[103] = 'ACMD_ADD_ROBOT'
handlers[103] = UnpackJSONPacket

names[104] = 'ACMD_ADD_ITEM'
handlers[104] = UnpackJSONPacket

names[105] = 'ACMD_ADD_EXIT'
handlers[105] = UnpackJSONPacket

names[106] = 'ACMD_REMOVE_USER'
handlers[106] = function(packet)
	return packet:ReadString()
end

names[107] = 'ACMD_REMOVE_ROBOT'
handlers[107] = function(packet)
	return packet:ReadString()
end

names[108] = 'ACMD_REMOVE_ITEM'
handlers[108] = function(packet)
	return packet:ReadString()
end

names[109] = 'ACMD_REMOVE_EXIT'
handlers[109] = function(packet)
	return packet:ReadString()
end

names[110] = 'ACMD_MOVE_USER'
handlers[110] = function(packet)
	local m0 = {}
	m0.type = packet:ReadByte()
	m0.target = packet:ReadString()
	local m1 = {}
	local cnt1 = packet:ReadUShort()
	for i=1,cnt1 do
		table.insert(m1, Vector2.New(packet:ReadUShort(),packet:ReadUShort()))
	end
	m0.tracks = m1
	return m0
end

names[111] = 'ACMD_MOVE_ROBOT'
handlers[111] = function(packet)
	local m0 = {}
	m0.type = packet:ReadByte()
	m0.target = packet:ReadString()
	local m1 = {}
	local cnt1 = packet:ReadUShort()
	for i=1,cnt1 do
		table.insert(m1, Vector2.New(packet:ReadUShort(),packet:ReadUShort()))
	end
	m0.tracks = m1
	return m0
end

names[112] = 'ACMD_MOVE_ITEM'
handlers[112] = function(packet)
	local m0 = {}
	m0.type = packet:ReadByte()
	m0.target = packet:ReadString()
	local m1 = {}
	local cnt1 = packet:ReadUShort()
	for i=1,cnt1 do
		table.insert(m1, Vector2.New(packet:ReadUShort(),packet:ReadUShort()))
	end
	m0.tracks = m1
	return m0
end

names[113] = 'ACMD_CALL_OUT_PET'
handlers[113] = UnpackJSONPacket

names[114] = 'ACMD_CALL_OUT_MOUNT'
handlers[114] = UnpackJSONPacket

names[115] = 'ACMD_CALL_OUT_MAGIC'
handlers[115] = UnpackJSONPacket

names[116] = 'ACMD_CALL_BACK_PET'
handlers[116] = function(packet)
	return packet:ReadString()
end

names[117] = 'ACMD_CALL_BACK_MOUNT'
handlers[117] = function(packet)
	return packet:ReadString()
end

names[118] = 'ACMD_CALL_BACK_MAGIC'
handlers[118] = function(packet)
	return packet:ReadString()
end

names[119] = 'ACMD_OBJECT_TASK_STATUS'
handlers[119] = function(packet)
	local m0 = {}
	local cnt0 = packet:ReadUShort()
	local key0
	for i=1,cnt0 do
		key0 = packet:ReadString()
		m0[key0] = packet:ReadByte()
	end
	return m0
end

names[120] = 'ACMD_CHANGE_EQUIPS'
handlers[120] = function(packet)
	local m0 = {}
	m0.id = packet:ReadString()
	local m1 = {}
	local cnt1 = packet:ReadUShort()
	for i=1,cnt1 do
		table.insert(m1, packet:ReadString())
	end
	m0.equips = m1
	return m0
end

names[201] = 'ACMD_QUERY_ATTRS'
handlers[201] = function(packet)
	local m0 = {}
	local cnt0 = packet:ReadUShort()
	local key0
	for i=1,cnt0 do
		key0 = packet:ReadString()
		m0[key0] = int64.tonum2(packet:ReadLong())
	end
	return m0
end

names[207] = 'ACMD_MAIN_LIST'
handlers[207] = UnpackJSONPacket

names[251] = 'ACMD_VIEW_USER'
handlers[251] = UnpackJSONPacket

names[253] = 'ACMD_XIULIAN_JINGMAI'
handlers[253] = UnpackJSONPacket

names[256] = 'ACMD_VIEW_ERXX'
handlers[256] = UnpackJSONPacket

names[264] = 'ACMD_CLICK_HEAD'
handlers[264] = function(packet)
	local m0 = {}
	m0.isfriend = packet:ReadBool()
	m0.isblacklist = packet:ReadBool()
	return m0
end

names[301] = 'ACMD_MY_ITEMS'
handlers[301] = UnpackJSONPacket

names[302] = 'ACMD_OBTAIN_ITEM'
handlers[302] = function(packet)
	local m0 = {}
	m0.iid = packet:ReadString()
	m0.cnt = packet:ReadInt()
	return m0
end

names[304] = 'ACMD_PICK_ITEM'
handlers[304] = function(packet)
	local m0 = {}
	local cnt0 = packet:ReadUShort()
	local key0
	for i=1,cnt0 do
		key0 = packet:ReadString()
		m0[key0] = packet:ReadInt()
	end
	return m0
end

names[306] = 'ACMD_VIEW_ITEM'
handlers[306] = UnpackJSONPacket

names[309] = 'ACMD_USE_ITEM'
handlers[309] = function(packet)
	local m0 = {}
	m0.type = packet:ReadByte()
	m0.uid = packet:ReadString()
	m0.use_attr = packet:ReadByte()
	m0.use_value = int64.tonum2(packet:ReadLong())
	m0.cur_value = int64.tonum2(packet:ReadLong())
	return m0
end

names[310] = 'ACMD_LIST_USER_ITEMS'
handlers[310] = UnpackJSONPacket

names[351] = 'ACMD_MY_MAGICS'
handlers[351] = UnpackJSONPacket

names[352] = 'ACMD_VIEW_MAGIC'
handlers[352] = UnpackJSONPacket

names[356] = 'ACMD_MAGIC_LIANHUA_VIEW'
handlers[356] = UnpackJSONPacket

names[401] = 'ACMD_MY_PETS'
handlers[401] = function(packet)
	local m0 = {}
	local cnt0 = packet:ReadUShort()
	for i=1,cnt0 do
		local m1 = {}
		m1.rid = packet:ReadString()
		m1.name = packet:ReadString()
		m1.lvl = packet:ReadInt()
		m1.color = packet:ReadInt()
		m1.lunhui = packet:ReadInt()
		m1.status = packet:ReadInt()
		table.insert(m0, m1)
	end
	return m0
end

names[402] = 'ACMD_VIEW_PET'
handlers[402] = UnpackJSONPacket

names[408] = 'ACMD_TUJIANS'
handlers[408] = UnpackJSONPacket

names[409] = 'ACMD_TUJIAN_VIEW'
handlers[409] = UnpackJSONPacket

names[411] = 'ACMD_HECHENG_PET_VIEW'
handlers[411] = function(packet)
	local m0 = {}
	m0.score = packet:ReadInt()
	m0.exp = int64.tonum2(packet:ReadLong())
	m0.money = int64.tonum2(packet:ReadLong())
	return m0
end

names[412] = 'ACMD_HECHENG_PET'
handlers[412] = UnpackJSONPacket

names[413] = 'ACMD_HECHENG_PETS'
handlers[413] = UnpackJSONPacket

names[414] = 'ACMD_LIANYAOHU_GEZI'
handlers[414] = UnpackJSONPacket

names[415] = 'ACMD_LIANYAOHU_GO'
handlers[415] = UnpackJSONPacket

names[451] = 'ACMD_MY_MOUNTSES'
handlers[451] = function(packet)
	local m0 = {}
	local cnt0 = packet:ReadUShort()
	for i=1,cnt0 do
		local m1 = {}
		m1.rid = packet:ReadString()
		m1.name = packet:ReadString()
		m1.lvl = packet:ReadInt()
		m1.color = packet:ReadInt()
		m1.lunhui = packet:ReadInt()
		m1.status = packet:ReadInt()
		table.insert(m0, m1)
	end
	return m0
end

names[452] = 'ACMD_VIEW_MOUNT'
handlers[452] = UnpackJSONPacket

names[456] = 'ACMD_BUILD_MOUNTS_RATE'
handlers[456] = function(packet)
	local m0 = {}
	m0.type = packet:ReadByte()
	m0.rate = packet:ReadInt()
	m0.dzd_num = packet:ReadInt()
	return m0
end

names[457] = 'ACMD_DUANLIAN_MOUNTS'
handlers[457] = UnpackJSONPacket

names[501] = 'ACMD_ATTACK'
handlers[501] = UnpackJSONPacket

names[502] = 'ACMD_ATTACK_FIRE'
handlers[502] = UnpackJSONPacket

names[503] = 'ACMD_ATTACK_PREPARE'
handlers[503] = UnpackJSONPacket

names[601] = 'ACMD_TASKS'
handlers[601] = UnpackJSONPacket

names[651] = 'ACMD_VIEW_NPC'
handlers[651] = UnpackJSONPacket

names[701] = 'ACMD_FRIENDS'
handlers[701] = UnpackJSONPacket

names[702] = 'ACMD_FIND_USER'
handlers[702] = UnpackJSONPacket

names[703] = 'ACMD_CHAT_MSG'
handlers[703] = function(packet)
	local m0 = {}
	m0.time = packet:ReadInt()
	m0.type = packet:ReadByte()
	m0.fid = packet:ReadString()
	m0.fname = packet:ReadString()
	m0.flvl = packet:ReadShort()
	m0.ffct = packet:ReadByte()
	m0.fsex = packet:ReadByte()
	m0.fline = packet:ReadString()
	m0.tid = packet:ReadString()
	m0.msg = packet:ReadLongString()
	return m0
end

names[801] = 'ACMD_MAILS'
handlers[801] = UnpackJSONPacket

names[901] = 'ACMD_TEAM_VIEW'
handlers[901] = UnpackJSONPacket

names[902] = 'ACMD_TEAM_CHANGE'
handlers[902] = function(packet)
	local m0 = {}
	m0.type = packet:ReadByte()
	m0.tid = packet:ReadString()
	m0.uid = packet:ReadString()
	m0.uname = packet:ReadString()
	return m0
end

names[1001] = 'ACMD_SORTBOARD'
handlers[1001] = UnpackJSONPacket

names[1101] = 'ACMD_MALL_OPEN'
handlers[1101] = function(packet)
	local m0 = {}
	m0.type = packet:ReadByte()
	m0.zk = packet:ReadByte()
	m0.cash = int64.tonum2(packet:ReadLong())
	local m1 = {}
	local cnt1 = packet:ReadUShort()
	for i=1,cnt1 do
		table.insert(m1, packet:ReadString())
	end
	m0.items = m1
	return m0
end

names[1201] = 'ACMD_HD_INFO'
handlers[1201] = UnpackJSONPacket

names[1202] = 'ACMD_HD_NEW'
handlers[1202] = function(packet)
	local m0 = {}
	return m0
end

names[1203] = 'ACMD_SIGN_INFO'
handlers[1203] = UnpackJSONPacket

names[1210] = 'ACMD_QITIAN_VIEW'
handlers[1210] = UnpackJSONPacket

names[1300] = 'ACMD_QUERY_HANGING_SETUP'
handlers[1300] = UnpackJSONPacket

names[1304] = 'ACMD_HANGING_GJK'
handlers[1304] = function(packet)
	local m0 = {}
	m0.count = packet:ReadInt()
	m0.cc = packet:ReadInt()
	return m0
end

names[1350] = 'ACMD_EMBED_VIEW'
handlers[1350] = UnpackJSONPacket

names[1353] = 'ACMD_UPGRADE_WEAPONS'
handlers[1353] = UnpackJSONPacket

names[1354] = 'ACMD_UPGRADE_WEAPON_VIEW'
handlers[1354] = UnpackJSONPacket

names[1357] = 'ACMD_BUILD_ITEMS'
handlers[1357] = UnpackJSONPacket

names[1358] = 'ACMD_BUILD_ITEM_VIEW'
handlers[1358] = UnpackJSONPacket

names[1360] = 'ACMD_RONGLIAN_ITEMS'
handlers[1360] = UnpackJSONPacket

names[1361] = 'ACMD_RONGLIAN_ITEM_VIEW'
handlers[1361] = UnpackJSONPacket

names[1363] = 'ACMD_XILIAN_RONGLIAN_VIEW'
handlers[1363] = UnpackJSONPacket

names[1364] = 'ACMD_XILIANCHI_XILIAN_VIEW'
handlers[1364] = UnpackJSONPacket

names[1365] = 'ACMD_XILIANCHI_XILIAN_ITEM_VIEW'
handlers[1365] = UnpackJSONPacket

names[1372] = 'ACMD_XILIANCHI_JINGLIAN_VIEW'
handlers[1372] = UnpackJSONPacket

names[1373] = 'ACMD_XILIANCHI_JINGLIAN_ITEM_VIEW'
handlers[1373] = UnpackJSONPacket

names[1400] = 'ACMD_LIST_CLAN'
handlers[1400] = UnpackJSONPacket

names[1402] = 'ACMD_VIEW_CLAN'
handlers[1402] = UnpackJSONPacket

names[1403] = 'ACMD_LIST_CLAN_MEMBER'
handlers[1403] = UnpackJSONPacket

names[1404] = 'ACMD_LIST_JOIN_CLAN_REQS'
handlers[1404] = UnpackJSONPacket

names[1422] = 'ACMD_VIEW_CLAN_LOGS'
handlers[1422] = UnpackJSONPacket

names[1423] = 'ACMD_VIEW_CLAN_ITEM_SEIZERS'
handlers[1423] = UnpackJSONPacket

names[1424] = 'ACMD_VIEW_CLAN_ITEM_FLAG'
handlers[1424] = UnpackJSONPacket

names[1427] = 'ACMD_VIEW_CLAN_ITEM_STATUS'
handlers[1427] = UnpackJSONPacket

names[1430] = 'ACMD_VIEW_CLAN_ITEMS'
handlers[1430] = function(packet)
	local m0 = {}
	local cnt0 = packet:ReadUShort()
	local key0
	for i=1,cnt0 do
		key0 = packet:ReadString()
		m0[key0] = int64.tonum2(packet:ReadLong())
	end
	return m0
end

names[1450] = 'ACMD_DIAOYU_VIEW'
handlers[1450] = UnpackJSONPacket

names[1451] = 'ACMD_DO_DIAOYU'
handlers[1451] = UnpackJSONPacket

names[1452] = 'ACMD_TOUHU_VIEW'
handlers[1452] = UnpackJSONPacket

names[1453] = 'ACMD_TOUHU'
handlers[1453] = UnpackJSONPacket

