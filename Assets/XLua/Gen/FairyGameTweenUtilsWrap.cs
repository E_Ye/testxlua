﻿#if USE_UNI_LUA
using LuaAPI = UniLua.Lua;
using RealStatePtr = UniLua.ILuaState;
using LuaCSFunction = UniLua.CSharpFunctionDelegate;
#else
using LuaAPI = XLua.LuaDLL.Lua;
using RealStatePtr = System.IntPtr;
using LuaCSFunction = XLua.LuaDLL.lua_CSFunction;
#endif

using XLua;
using System.Collections.Generic;


namespace XLua.CSObjectWrap
{
    using Utils = XLua.Utils;
    public class FairyGameTweenUtilsWrap 
    {
        public static void __Register(RealStatePtr L)
        {
			ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			Utils.BeginObjectRegister(typeof(FairyGame.TweenUtils), L, translator, 0, 0, 0, 0);
			
			
			
			
			
			Utils.EndObjectRegister(typeof(FairyGame.TweenUtils), L, translator, null, null,
			    null, null, null);

		    Utils.BeginClassRegister(typeof(FairyGame.TweenUtils), L, __CreateInstance, 9, 0, 0);
			Utils.RegisterFunc(L, Utils.CLS_IDX, "TweenFloat", _m_TweenFloat_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "TweenVector2", _m_TweenVector2_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "TweenVector3", _m_TweenVector3_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "SetEase", _m_SetEase_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "OnComplete", _m_OnComplete_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "SetDelay", _m_SetDelay_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "SetLoops", _m_SetLoops_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "SetTarget", _m_SetTarget_xlua_st_);
            
			
            
            Utils.RegisterObject(L, translator, Utils.CLS_IDX, "UnderlyingSystemType", typeof(FairyGame.TweenUtils));
			
			
			Utils.EndClassRegister(typeof(FairyGame.TweenUtils), L, translator);
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int __CreateInstance(RealStatePtr L)
        {
            return LuaAPI.luaL_error(L, "FairyGame.TweenUtils does not have a constructor!");
        }
        
		
        
		
        
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_TweenFloat_xlua_st_(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            
            try {
                
                {
                    float start = (float)LuaAPI.lua_tonumber(L, 1);
                    float end = (float)LuaAPI.lua_tonumber(L, 2);
                    float duration = (float)LuaAPI.lua_tonumber(L, 3);
                    System.Action<object, float> OnUpdate = translator.GetDelegate<System.Action<object, float>>(L, 4);
                    System.Action<object> onComplete = translator.GetDelegate<System.Action<object>>(L, 5);
                    object self = translator.GetObject(L, 6, typeof(object));
                    
                        DG.Tweening.Tweener __cl_gen_ret = FairyGame.TweenUtils.TweenFloat( start, end, duration, OnUpdate, onComplete, self );
                        translator.Push(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_TweenVector2_xlua_st_(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            
            try {
                
                {
                    UnityEngine.Vector2 start;translator.Get(L, 1, out start);
                    UnityEngine.Vector2 end;translator.Get(L, 2, out end);
                    float duration = (float)LuaAPI.lua_tonumber(L, 3);
                    System.Action<object, UnityEngine.Vector2> OnUpdate = translator.GetDelegate<System.Action<object, UnityEngine.Vector2>>(L, 4);
                    System.Action<object> onComplete = translator.GetDelegate<System.Action<object>>(L, 5);
                    object self = translator.GetObject(L, 6, typeof(object));
                    
                        DG.Tweening.Tweener __cl_gen_ret = FairyGame.TweenUtils.TweenVector2( start, end, duration, OnUpdate, onComplete, self );
                        translator.Push(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_TweenVector3_xlua_st_(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            
            try {
                
                {
                    UnityEngine.Vector3 start;translator.Get(L, 1, out start);
                    UnityEngine.Vector3 end;translator.Get(L, 2, out end);
                    float duration = (float)LuaAPI.lua_tonumber(L, 3);
                    System.Action<object, UnityEngine.Vector3> OnUpdate = translator.GetDelegate<System.Action<object, UnityEngine.Vector3>>(L, 4);
                    System.Action<object> onComplete = translator.GetDelegate<System.Action<object>>(L, 5);
                    object self = translator.GetObject(L, 6, typeof(object));
                    
                        DG.Tweening.Tweener __cl_gen_ret = FairyGame.TweenUtils.TweenVector3( start, end, duration, OnUpdate, onComplete, self );
                        translator.Push(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_SetEase_xlua_st_(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            
            try {
                
                {
                    DG.Tweening.Tweener tweener = (DG.Tweening.Tweener)translator.GetObject(L, 1, typeof(DG.Tweening.Tweener));
                    DG.Tweening.Ease ease;translator.Get(L, 2, out ease);
                    
                    FairyGame.TweenUtils.SetEase( tweener, ease );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_OnComplete_xlua_st_(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            
			int __gen_param_count = LuaAPI.lua_gettop(L);
            
            try {
                if(__gen_param_count == 2&& translator.Assignable<DG.Tweening.Tweener>(L, 1)&& translator.Assignable<System.Action<object>>(L, 2)) 
                {
                    DG.Tweening.Tweener tweener = (DG.Tweening.Tweener)translator.GetObject(L, 1, typeof(DG.Tweening.Tweener));
                    System.Action<object> func = translator.GetDelegate<System.Action<object>>(L, 2);
                    
                    FairyGame.TweenUtils.OnComplete( tweener, func );
                    
                    
                    
                    return 0;
                }
                if(__gen_param_count == 3&& translator.Assignable<DG.Tweening.Tweener>(L, 1)&& translator.Assignable<System.Action<object>>(L, 2)&& translator.Assignable<object>(L, 3)) 
                {
                    DG.Tweening.Tweener tweener = (DG.Tweening.Tweener)translator.GetObject(L, 1, typeof(DG.Tweening.Tweener));
                    System.Action<object> func = translator.GetDelegate<System.Action<object>>(L, 2);
                    object self = translator.GetObject(L, 3, typeof(object));
                    
                    FairyGame.TweenUtils.OnComplete( tweener, func, self );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
            return LuaAPI.luaL_error(L, "invalid arguments to FairyGame.TweenUtils.OnComplete!");
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_SetDelay_xlua_st_(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            
            try {
                
                {
                    DG.Tweening.Tweener tweener = (DG.Tweening.Tweener)translator.GetObject(L, 1, typeof(DG.Tweening.Tweener));
                    float delay = (float)LuaAPI.lua_tonumber(L, 2);
                    
                    FairyGame.TweenUtils.SetDelay( tweener, delay );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_SetLoops_xlua_st_(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            
			int __gen_param_count = LuaAPI.lua_gettop(L);
            
            try {
                if(__gen_param_count == 2&& translator.Assignable<DG.Tweening.Tweener>(L, 1)&& LuaTypes.LUA_TNUMBER == LuaAPI.lua_type(L, 2)) 
                {
                    DG.Tweening.Tweener tweener = (DG.Tweening.Tweener)translator.GetObject(L, 1, typeof(DG.Tweening.Tweener));
                    int loops = LuaAPI.xlua_tointeger(L, 2);
                    
                    FairyGame.TweenUtils.SetLoops( tweener, loops );
                    
                    
                    
                    return 0;
                }
                if(__gen_param_count == 3&& translator.Assignable<DG.Tweening.Tweener>(L, 1)&& LuaTypes.LUA_TNUMBER == LuaAPI.lua_type(L, 2)&& LuaTypes.LUA_TBOOLEAN == LuaAPI.lua_type(L, 3)) 
                {
                    DG.Tweening.Tweener tweener = (DG.Tweening.Tweener)translator.GetObject(L, 1, typeof(DG.Tweening.Tweener));
                    int loops = LuaAPI.xlua_tointeger(L, 2);
                    bool yoyo = LuaAPI.lua_toboolean(L, 3);
                    
                    FairyGame.TweenUtils.SetLoops( tweener, loops, yoyo );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
            return LuaAPI.luaL_error(L, "invalid arguments to FairyGame.TweenUtils.SetLoops!");
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_SetTarget_xlua_st_(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            
            try {
                
                {
                    DG.Tweening.Tweener tweener = (DG.Tweening.Tweener)translator.GetObject(L, 1, typeof(DG.Tweening.Tweener));
                    object target = translator.GetObject(L, 2, typeof(object));
                    
                    FairyGame.TweenUtils.SetTarget( tweener, target );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        
        
        
        
        
		
		
		
		
    }
}
