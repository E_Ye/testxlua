﻿#if USE_UNI_LUA
using LuaAPI = UniLua.Lua;
using RealStatePtr = UniLua.ILuaState;
using LuaCSFunction = UniLua.CSharpFunctionDelegate;
#else
using LuaAPI = XLua.LuaDLL.Lua;
using RealStatePtr = System.IntPtr;
using LuaCSFunction = XLua.LuaDLL.lua_CSFunction;
#endif

using XLua;
using System.Collections.Generic;


namespace XLua.CSObjectWrap
{
    using Utils = XLua.Utils;
    public class KBEngineMemoryStreamWrap 
    {
        public static void __Register(RealStatePtr L)
        {
			ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			Utils.BeginObjectRegister(typeof(KBEngine.MemoryStream), L, translator, 0, 38, 2, 2);
			
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "data", _m_data);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "setData", _m_setData);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "copy", _m_copy);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "readInt8", _m_readInt8);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "readInt16", _m_readInt16);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "readInt32", _m_readInt32);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "readInt64", _m_readInt64);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "readUint8", _m_readUint8);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "readUint16", _m_readUint16);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "readUint32", _m_readUint32);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "readUint64", _m_readUint64);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "readFloat", _m_readFloat);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "readDouble", _m_readDouble);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "readString", _m_readString);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "readBlob", _m_readBlob);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "readPackXZ", _m_readPackXZ);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "readPackY", _m_readPackY);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "writeInt8", _m_writeInt8);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "writeInt16", _m_writeInt16);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "writeInt32", _m_writeInt32);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "writeInt64", _m_writeInt64);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "writeUint8", _m_writeUint8);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "writeUint16", _m_writeUint16);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "writeUint32", _m_writeUint32);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "writeUint64", _m_writeUint64);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "writeFloat", _m_writeFloat);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "writeDouble", _m_writeDouble);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "writeBlob", _m_writeBlob);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "writeString", _m_writeString);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "append", _m_append);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "readSkip", _m_readSkip);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "space", _m_space);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "length", _m_length);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "readEOF", _m_readEOF);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "done", _m_done);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "clear", _m_clear);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "getbuffer", _m_getbuffer);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "toString", _m_toString);
			
			
			Utils.RegisterFunc(L, Utils.GETTER_IDX, "rpos", _g_get_rpos);
            Utils.RegisterFunc(L, Utils.GETTER_IDX, "wpos", _g_get_wpos);
            
			Utils.RegisterFunc(L, Utils.SETTER_IDX, "rpos", _s_set_rpos);
            Utils.RegisterFunc(L, Utils.SETTER_IDX, "wpos", _s_set_wpos);
            
			Utils.EndObjectRegister(typeof(KBEngine.MemoryStream), L, translator, null, null,
			    null, null, null);

		    Utils.BeginClassRegister(typeof(KBEngine.MemoryStream), L, __CreateInstance, 2, 0, 0);
			
			
            Utils.RegisterObject(L, translator, Utils.CLS_IDX, "BUFFER_MAX", KBEngine.MemoryStream.BUFFER_MAX);
            
            Utils.RegisterObject(L, translator, Utils.CLS_IDX, "UnderlyingSystemType", typeof(KBEngine.MemoryStream));
			
			
			Utils.EndClassRegister(typeof(KBEngine.MemoryStream), L, translator);
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int __CreateInstance(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			try {
				if(LuaAPI.lua_gettop(L) == 1)
				{
					
					KBEngine.MemoryStream __cl_gen_ret = new KBEngine.MemoryStream();
					translator.Push(L, __cl_gen_ret);
                    
					return 1;
				}
				
			}
			catch(System.Exception __gen_e) {
				return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
			}
            return LuaAPI.luaL_error(L, "invalid arguments to KBEngine.MemoryStream constructor!");
            
        }
        
		
        
		
        
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_data(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        byte[] __cl_gen_ret = __cl_gen_to_be_invoked.data(  );
                        LuaAPI.lua_pushstring(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_setData(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    byte[] data = LuaAPI.lua_tobytes(L, 2);
                    
                    __cl_gen_to_be_invoked.setData( data );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_copy(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    KBEngine.MemoryStream ms = (KBEngine.MemoryStream)translator.GetObject(L, 2, typeof(KBEngine.MemoryStream));
                    
                    __cl_gen_to_be_invoked.copy( ms );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_readInt8(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        sbyte __cl_gen_ret = __cl_gen_to_be_invoked.readInt8(  );
                        LuaAPI.xlua_pushinteger(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_readInt16(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        short __cl_gen_ret = __cl_gen_to_be_invoked.readInt16(  );
                        LuaAPI.xlua_pushinteger(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_readInt32(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        int __cl_gen_ret = __cl_gen_to_be_invoked.readInt32(  );
                        LuaAPI.xlua_pushinteger(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_readInt64(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        long __cl_gen_ret = __cl_gen_to_be_invoked.readInt64(  );
                        LuaAPI.lua_pushint64(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_readUint8(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        byte __cl_gen_ret = __cl_gen_to_be_invoked.readUint8(  );
                        LuaAPI.xlua_pushinteger(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_readUint16(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        ushort __cl_gen_ret = __cl_gen_to_be_invoked.readUint16(  );
                        LuaAPI.xlua_pushinteger(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_readUint32(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        uint __cl_gen_ret = __cl_gen_to_be_invoked.readUint32(  );
                        LuaAPI.xlua_pushuint(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_readUint64(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        ulong __cl_gen_ret = __cl_gen_to_be_invoked.readUint64(  );
                        LuaAPI.lua_pushuint64(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_readFloat(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        float __cl_gen_ret = __cl_gen_to_be_invoked.readFloat(  );
                        LuaAPI.lua_pushnumber(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_readDouble(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        double __cl_gen_ret = __cl_gen_to_be_invoked.readDouble(  );
                        LuaAPI.lua_pushnumber(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_readString(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        string __cl_gen_ret = __cl_gen_to_be_invoked.readString(  );
                        LuaAPI.lua_pushstring(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_readBlob(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        byte[] __cl_gen_ret = __cl_gen_to_be_invoked.readBlob(  );
                        LuaAPI.lua_pushstring(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_readPackXZ(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        UnityEngine.Vector2 __cl_gen_ret = __cl_gen_to_be_invoked.readPackXZ(  );
                        translator.PushUnityEngineVector2(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_readPackY(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        float __cl_gen_ret = __cl_gen_to_be_invoked.readPackY(  );
                        LuaAPI.lua_pushnumber(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_writeInt8(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    sbyte v = (sbyte)LuaAPI.xlua_tointeger(L, 2);
                    
                    __cl_gen_to_be_invoked.writeInt8( v );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_writeInt16(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    short v = (short)LuaAPI.xlua_tointeger(L, 2);
                    
                    __cl_gen_to_be_invoked.writeInt16( v );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_writeInt32(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    int v = LuaAPI.xlua_tointeger(L, 2);
                    
                    __cl_gen_to_be_invoked.writeInt32( v );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_writeInt64(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    long v = LuaAPI.lua_toint64(L, 2);
                    
                    __cl_gen_to_be_invoked.writeInt64( v );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_writeUint8(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    byte v = (byte)LuaAPI.lua_tonumber(L, 2);
                    
                    __cl_gen_to_be_invoked.writeUint8( v );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_writeUint16(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    ushort v = (ushort)LuaAPI.xlua_tointeger(L, 2);
                    
                    __cl_gen_to_be_invoked.writeUint16( v );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_writeUint32(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    uint v = LuaAPI.xlua_touint(L, 2);
                    
                    __cl_gen_to_be_invoked.writeUint32( v );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_writeUint64(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    ulong v = LuaAPI.lua_touint64(L, 2);
                    
                    __cl_gen_to_be_invoked.writeUint64( v );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_writeFloat(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    float v = (float)LuaAPI.lua_tonumber(L, 2);
                    
                    __cl_gen_to_be_invoked.writeFloat( v );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_writeDouble(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    double v = LuaAPI.lua_tonumber(L, 2);
                    
                    __cl_gen_to_be_invoked.writeDouble( v );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_writeBlob(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    byte[] v = LuaAPI.lua_tobytes(L, 2);
                    
                    __cl_gen_to_be_invoked.writeBlob( v );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_writeString(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    string v = LuaAPI.lua_tostring(L, 2);
                    
                    __cl_gen_to_be_invoked.writeString( v );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_append(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    byte[] datas = LuaAPI.lua_tobytes(L, 2);
                    int offset = LuaAPI.xlua_tointeger(L, 3);
                    int size = LuaAPI.xlua_tointeger(L, 4);
                    
                    __cl_gen_to_be_invoked.append( datas, offset, size );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_readSkip(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    int v = LuaAPI.xlua_tointeger(L, 2);
                    
                    __cl_gen_to_be_invoked.readSkip( v );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_space(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        int __cl_gen_ret = __cl_gen_to_be_invoked.space(  );
                        LuaAPI.xlua_pushinteger(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_length(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        int __cl_gen_ret = __cl_gen_to_be_invoked.length(  );
                        LuaAPI.xlua_pushinteger(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_readEOF(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        bool __cl_gen_ret = __cl_gen_to_be_invoked.readEOF(  );
                        LuaAPI.lua_pushboolean(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_done(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                    __cl_gen_to_be_invoked.done(  );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_clear(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                    __cl_gen_to_be_invoked.clear(  );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_getbuffer(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        byte[] __cl_gen_ret = __cl_gen_to_be_invoked.getbuffer(  );
                        LuaAPI.lua_pushstring(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_toString(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        string __cl_gen_ret = __cl_gen_to_be_invoked.toString(  );
                        LuaAPI.lua_pushstring(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _g_get_rpos(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
                LuaAPI.xlua_pushinteger(L, __cl_gen_to_be_invoked.rpos);
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 1;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _g_get_wpos(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
                LuaAPI.xlua_pushinteger(L, __cl_gen_to_be_invoked.wpos);
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 1;
        }
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _s_set_rpos(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
                __cl_gen_to_be_invoked.rpos = LuaAPI.xlua_tointeger(L, 2);
            
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 0;
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _s_set_wpos(RealStatePtr L)
        {
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            try {
			
                KBEngine.MemoryStream __cl_gen_to_be_invoked = (KBEngine.MemoryStream)translator.FastGetCSObj(L, 1);
                __cl_gen_to_be_invoked.wpos = LuaAPI.xlua_tointeger(L, 2);
            
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            return 0;
        }
        
		
		
		
		
    }
}
