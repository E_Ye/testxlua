﻿#if USE_UNI_LUA
using LuaAPI = UniLua.Lua;
using RealStatePtr = UniLua.ILuaState;
using LuaCSFunction = UniLua.CSharpFunctionDelegate;
#else
using LuaAPI = XLua.LuaDLL.Lua;
using RealStatePtr = System.IntPtr;
using LuaCSFunction = XLua.LuaDLL.lua_CSFunction;
#endif

using XLua;
using System.Collections.Generic;


namespace XLua.CSObjectWrap
{
    using Utils = XLua.Utils;
    public class KBEngineKBELuaUtilWrap 
    {
        public static void __Register(RealStatePtr L)
        {
			ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			Utils.BeginObjectRegister(typeof(KBEngine.KBELuaUtil), L, translator, 0, 0, 0, 0);
			
			
			
			
			
			Utils.EndObjectRegister(typeof(KBEngine.KBELuaUtil), L, translator, null, null,
			    null, null, null);

		    Utils.BeginClassRegister(typeof(KBEngine.KBELuaUtil), L, __CreateInstance, 17, 0, 0);
			Utils.RegisterFunc(L, Utils.CLS_IDX, "Utf8ToByte", _m_Utf8ToByte_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "ByteToUtf8", _m_ByteToUtf8_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "GetByteFromBytes", _m_GetByteFromBytes_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "ModiyByteWithIndex", _m_ModiyByteWithIndex_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "ArrayCopy", _m_ArrayCopy_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "Log", _m_Log_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "LogWarning", _m_LogWarning_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "LogError", _m_LogError_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "SetCallLuaFunction", _m_SetCallLuaFunction_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "ClearCallLuaFunction", _m_ClearCallLuaFunction_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "CallMethod", _m_CallMethod_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "createFile", _m_createFile_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "loadFile", _m_loadFile_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "deleteFile", _m_deleteFile_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "bytesToString", _m_bytesToString_xlua_st_);
            Utils.RegisterFunc(L, Utils.CLS_IDX, "stringToBytes", _m_stringToBytes_xlua_st_);
            
			
            
            Utils.RegisterObject(L, translator, Utils.CLS_IDX, "UnderlyingSystemType", typeof(KBEngine.KBELuaUtil));
			
			
			Utils.EndClassRegister(typeof(KBEngine.KBELuaUtil), L, translator);
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int __CreateInstance(RealStatePtr L)
        {
            return LuaAPI.luaL_error(L, "KBEngine.KBELuaUtil does not have a constructor!");
        }
        
		
        
		
        
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_Utf8ToByte_xlua_st_(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            
            try {
                
                {
                    object utf8 = translator.GetObject(L, 1, typeof(object));
                    
                        byte[] __cl_gen_ret = KBEngine.KBELuaUtil.Utf8ToByte( utf8 );
                        LuaAPI.lua_pushstring(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_ByteToUtf8_xlua_st_(RealStatePtr L)
        {
            
            
            
            try {
                
                {
                    byte[] bytes = LuaAPI.lua_tobytes(L, 1);
                    
                        string __cl_gen_ret = KBEngine.KBELuaUtil.ByteToUtf8( bytes );
                        LuaAPI.lua_pushstring(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_GetByteFromBytes_xlua_st_(RealStatePtr L)
        {
            
            
            
            try {
                
                {
                    byte[] source = LuaAPI.lua_tobytes(L, 1);
                    long index = LuaAPI.lua_toint64(L, 2);
                    
                        byte __cl_gen_ret = KBEngine.KBELuaUtil.GetByteFromBytes( source, index );
                        LuaAPI.xlua_pushinteger(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_ModiyByteWithIndex_xlua_st_(RealStatePtr L)
        {
            
            
            
            try {
                
                {
                    byte[] source = LuaAPI.lua_tobytes(L, 1);
                    long targetIndex = LuaAPI.lua_toint64(L, 2);
                    byte content = (byte)LuaAPI.lua_tonumber(L, 3);
                    
                    KBEngine.KBELuaUtil.ModiyByteWithIndex( ref source, targetIndex, content );
                    LuaAPI.lua_pushstring(L, source);
                        
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_ArrayCopy_xlua_st_(RealStatePtr L)
        {
            
            
            
            try {
                
                {
                    byte[] srcdatas = LuaAPI.lua_tobytes(L, 1);
                    long srcLen = LuaAPI.lua_toint64(L, 2);
                    byte[] dstdatas = LuaAPI.lua_tobytes(L, 3);
                    long dstLen = LuaAPI.lua_toint64(L, 4);
                    long len = LuaAPI.lua_toint64(L, 5);
                    
                    KBEngine.KBELuaUtil.ArrayCopy( srcdatas, srcLen, dstdatas, dstLen, len );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_Log_xlua_st_(RealStatePtr L)
        {
            
            
            
            try {
                
                {
                    string str = LuaAPI.lua_tostring(L, 1);
                    
                    KBEngine.KBELuaUtil.Log( str );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_LogWarning_xlua_st_(RealStatePtr L)
        {
            
            
            
            try {
                
                {
                    string str = LuaAPI.lua_tostring(L, 1);
                    
                    KBEngine.KBELuaUtil.LogWarning( str );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_LogError_xlua_st_(RealStatePtr L)
        {
            
            
            
            try {
                
                {
                    string str = LuaAPI.lua_tostring(L, 1);
                    
                    KBEngine.KBELuaUtil.LogError( str );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_SetCallLuaFunction_xlua_st_(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            
            try {
                
                {
                    KBEngine.KBELuaUtil.CallLuaFunction clf = translator.GetDelegate<KBEngine.KBELuaUtil.CallLuaFunction>(L, 1);
                    
                    KBEngine.KBELuaUtil.SetCallLuaFunction( clf );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_ClearCallLuaFunction_xlua_st_(RealStatePtr L)
        {
            
            
            
            try {
                
                {
                    
                    KBEngine.KBELuaUtil.ClearCallLuaFunction(  );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_CallMethod_xlua_st_(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            
            try {
                
                {
                    string module = LuaAPI.lua_tostring(L, 1);
                    string func = LuaAPI.lua_tostring(L, 2);
                    object[] args = translator.GetParams<object>(L, 3);
                    
                        object[] __cl_gen_ret = KBEngine.KBELuaUtil.CallMethod( module, func, args );
                        translator.Push(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_createFile_xlua_st_(RealStatePtr L)
        {
            
            
            
            try {
                
                {
                    string path = LuaAPI.lua_tostring(L, 1);
                    string name = LuaAPI.lua_tostring(L, 2);
                    byte[] datas = LuaAPI.lua_tobytes(L, 3);
                    
                    KBEngine.KBELuaUtil.createFile( path, name, datas );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_loadFile_xlua_st_(RealStatePtr L)
        {
            
            
            
            try {
                
                {
                    string path = LuaAPI.lua_tostring(L, 1);
                    string name = LuaAPI.lua_tostring(L, 2);
                    bool printerr = LuaAPI.lua_toboolean(L, 3);
                    
                        byte[] __cl_gen_ret = KBEngine.KBELuaUtil.loadFile( path, name, printerr );
                        LuaAPI.lua_pushstring(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_deleteFile_xlua_st_(RealStatePtr L)
        {
            
            
            
            try {
                
                {
                    string path = LuaAPI.lua_tostring(L, 1);
                    string name = LuaAPI.lua_tostring(L, 2);
                    
                    KBEngine.KBELuaUtil.deleteFile( path, name );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_bytesToString_xlua_st_(RealStatePtr L)
        {
            
            
            
            try {
                
                {
                    byte[] bytes = LuaAPI.lua_tobytes(L, 1);
                    
                        string __cl_gen_ret = KBEngine.KBELuaUtil.bytesToString( bytes );
                        LuaAPI.lua_pushstring(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_stringToBytes_xlua_st_(RealStatePtr L)
        {
            
            
            
            try {
                
                {
                    string str = LuaAPI.lua_tostring(L, 1);
                    
                        byte[] __cl_gen_ret = KBEngine.KBELuaUtil.stringToBytes( str );
                        LuaAPI.lua_pushstring(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        
        
        
        
        
		
		
		
		
    }
}
