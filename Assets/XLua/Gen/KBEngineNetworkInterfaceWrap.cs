﻿#if USE_UNI_LUA
using LuaAPI = UniLua.Lua;
using RealStatePtr = UniLua.ILuaState;
using LuaCSFunction = UniLua.CSharpFunctionDelegate;
#else
using LuaAPI = XLua.LuaDLL.Lua;
using RealStatePtr = System.IntPtr;
using LuaCSFunction = XLua.LuaDLL.lua_CSFunction;
#endif

using XLua;
using System.Collections.Generic;


namespace XLua.CSObjectWrap
{
    using Utils = XLua.Utils;
    public class KBEngineNetworkInterfaceWrap 
    {
        public static void __Register(RealStatePtr L)
        {
			ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			Utils.BeginObjectRegister(typeof(KBEngine.NetworkInterface), L, translator, 0, 9, 0, 0);
			
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "sock", _m_sock);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "reset", _m_reset);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "close", _m_close);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "packetReceiver", _m_packetReceiver);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "valid", _m_valid);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "_onConnectionState", _m__onConnectionState);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "connectTo", _m_connectTo);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "send", _m_send);
			Utils.RegisterFunc(L, Utils.METHOD_IDX, "process", _m_process);
			
			
			
			
			Utils.EndObjectRegister(typeof(KBEngine.NetworkInterface), L, translator, null, null,
			    null, null, null);

		    Utils.BeginClassRegister(typeof(KBEngine.NetworkInterface), L, __CreateInstance, 2, 0, 0);
			
			
            Utils.RegisterObject(L, translator, Utils.CLS_IDX, "TCP_PACKET_MAX", KBEngine.NetworkInterface.TCP_PACKET_MAX);
            
            Utils.RegisterObject(L, translator, Utils.CLS_IDX, "UnderlyingSystemType", typeof(KBEngine.NetworkInterface));
			
			
			Utils.EndClassRegister(typeof(KBEngine.NetworkInterface), L, translator);
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int __CreateInstance(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
			try {
				if(LuaAPI.lua_gettop(L) == 1)
				{
					
					KBEngine.NetworkInterface __cl_gen_ret = new KBEngine.NetworkInterface();
					translator.Push(L, __cl_gen_ret);
                    
					return 1;
				}
				
			}
			catch(System.Exception __gen_e) {
				return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
			}
            return LuaAPI.luaL_error(L, "invalid arguments to KBEngine.NetworkInterface constructor!");
            
        }
        
		
        
		
        
        
        
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_sock(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.NetworkInterface __cl_gen_to_be_invoked = (KBEngine.NetworkInterface)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        System.Net.Sockets.Socket __cl_gen_ret = __cl_gen_to_be_invoked.sock(  );
                        translator.Push(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_reset(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.NetworkInterface __cl_gen_to_be_invoked = (KBEngine.NetworkInterface)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                    __cl_gen_to_be_invoked.reset(  );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_close(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.NetworkInterface __cl_gen_to_be_invoked = (KBEngine.NetworkInterface)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                    __cl_gen_to_be_invoked.close(  );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_packetReceiver(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.NetworkInterface __cl_gen_to_be_invoked = (KBEngine.NetworkInterface)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        KBEngine.PacketReceiver __cl_gen_ret = __cl_gen_to_be_invoked.packetReceiver(  );
                        translator.Push(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_valid(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.NetworkInterface __cl_gen_to_be_invoked = (KBEngine.NetworkInterface)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                        bool __cl_gen_ret = __cl_gen_to_be_invoked.valid(  );
                        LuaAPI.lua_pushboolean(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m__onConnectionState(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.NetworkInterface __cl_gen_to_be_invoked = (KBEngine.NetworkInterface)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    KBEngine.NetworkInterface.ConnectState state = (KBEngine.NetworkInterface.ConnectState)translator.GetObject(L, 2, typeof(KBEngine.NetworkInterface.ConnectState));
                    
                    __cl_gen_to_be_invoked._onConnectionState( state );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_connectTo(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.NetworkInterface __cl_gen_to_be_invoked = (KBEngine.NetworkInterface)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    string ip = LuaAPI.lua_tostring(L, 2);
                    int port = LuaAPI.xlua_tointeger(L, 3);
                    KBEngine.NetworkInterface.ConnectCallback callback = translator.GetDelegate<KBEngine.NetworkInterface.ConnectCallback>(L, 4);
                    object userData = translator.GetObject(L, 5, typeof(object));
                    
                    __cl_gen_to_be_invoked.connectTo( ip, port, callback, userData );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_send(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.NetworkInterface __cl_gen_to_be_invoked = (KBEngine.NetworkInterface)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    KBEngine.MemoryStream stream = (KBEngine.MemoryStream)translator.GetObject(L, 2, typeof(KBEngine.MemoryStream));
                    
                        bool __cl_gen_ret = __cl_gen_to_be_invoked.send( stream );
                        LuaAPI.lua_pushboolean(L, __cl_gen_ret);
                    
                    
                    
                    return 1;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        [MonoPInvokeCallbackAttribute(typeof(LuaCSFunction))]
        static int _m_process(RealStatePtr L)
        {
            
            ObjectTranslator translator = ObjectTranslatorPool.Instance.Find(L);
            
            
            KBEngine.NetworkInterface __cl_gen_to_be_invoked = (KBEngine.NetworkInterface)translator.FastGetCSObj(L, 1);
            
            
            try {
                
                {
                    
                    __cl_gen_to_be_invoked.process(  );
                    
                    
                    
                    return 0;
                }
                
            } catch(System.Exception __gen_e) {
                return LuaAPI.luaL_error(L, "c# exception:" + __gen_e);
            }
            
        }
        
        
        
        
        
        
		
		
		
		
    }
}
