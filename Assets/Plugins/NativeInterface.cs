﻿#if UNITY_IOS
using System.Runtime.InteropServices;
#else
using UnityEngine;
#endif
using System.Runtime;
using System;
public  class NativeInterface
{
#if UNITY_ANDROID && !UNITY_EDITOR
	private static AndroidJavaObject _javaObj;
	private static AndroidJavaObject javaObj
	{
		get
		{
			if ( _javaObj != null )
				return _javaObj;
			return _javaObj = new AndroidJavaClass( "cn.smitear.unitylib.MobileInfo" );
		}
	}
#elif UNITY_IOS
	[DllImport( "__Internal" )]
	private static extern float _GetBatteryLevel();

	[DllImport( "__Internal" )]
	private static extern int _GetBatteryState();
#endif

	public static void Dispose()
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		if ( _javaObj != null )
		{
			_javaObj.Dispose();
			_javaObj = null;
		}
#endif
	}

	private static T CallStatic<T>( string methodName, params object[] args )
	{
#if UNITY_ANDROID && !UNITY_EDITOR
		return javaObj.CallStatic<T>( methodName, args );
#else
		return default( T );
#endif
	}

	public static int BatteryLevel()
	{
#if UNITY_EDITOR || UNITY_WSA
        return 0;
#elif UNITY_ANDROID
		return CallStatic<int>( "BatteryLevel" );
#elif UNITY_IOS
		return ( int )( _GetBatteryLevel() * 100f );
#elif UNITY_WSA
        return 0;
#endif
    }

    public static int BatteryState()
	{
#if UNITY_EDITOR || UNITY_WSA
        return 0;
#elif UNITY_ANDROID
		return CallStatic<int>( "BatteryState" );
#elif UNITY_IOS
		return _GetBatteryState();
#elif UNITY_WSA
        return 0;
#endif
    }

    public static int GetWifiSignalStrength()
	{
#if UNITY_EDITOR
		return 0;
#elif UNITY_ANDROID
		return CallStatic<int>( "GetWIFISignalStrength" );
#else
		return 0;
#endif
	}

	public static int GetTeleSignalStrength()
	{
#if UNITY_EDITOR
		return 0;
#elif UNITY_ANDROID
		return CallStatic<int>( "GetTeleSignalStrength" );
#else
		return 0;
#endif
	}

	public static int GetSignalLevel( int value )
	{
#if UNITY_EDITOR
		return 0;
#elif UNITY_ANDROID
		//1、当信号大于等于 - 85dBm时候，信号显示满格  
		//2、当信号大于等于 - 95dBm时候，而小于 - 85dBm时，信号显示4格  
		//3、当信号大于等于 - 105dBm时候，而小于 - 95dBm时，信号显示3格，不好捕捉到。  
		//4、当信号大于等于 - 115dBm时候，而小于 - 105dBm时，信号显示2格，不好捕捉到。  
		//5、当信号大于等于 - 140dBm时候，而小于 - 115dBm时，信号显示1格，不好捕捉到。  
		if ( value > -85 )
			return 5;
		if ( value < -85 && value > -95 )
			return 4;
		if ( value < -95 && value > -105 )
			return 3;
		if ( value < -105 && value > -115 )
			return 2;
		if ( value < -115 && value > -140 )
			return 1;
		return 0;
#else
		return 0;
#endif
	}
}
