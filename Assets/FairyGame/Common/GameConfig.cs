﻿using System;
using System.Collections;
using System.IO;
using System.Text;
using UnityEngine;

namespace FairyGame
{
	public class GameConfig
	{
		public static string appVersion { get; internal set; }

		public static string terminalId { get; set; }

		public static string partnerId { get; set; }

		//是否从网络上下载资源，如果否，则只从_staticResPath载入
		public static bool downloadResource { get; set; }

		public static string versionCheckURL { get; set; }
		public static int packageResVer { get; private set; }

		/*从网络上下载的资源放到本地后的地址，DownloadResource为true时使用。此地址在Application.persistentDataPath，即:
		 * win:C:\Users\<用户名>\AppData\LocalLow\<CompanyName>\<ProductName>\
		 * android:/sdcard/Android/data/<APPID>/files/
		 * ios:/Application/<APPID>/Documents/
		 */
		public static string localResPath;
		public static string localResURL;

		//打包在包里的资源的地址， 如果在编辑器里，那就是streamingAssetsPath。
		public static string staticResPath;
		public static string staticResURL;

		//临时目录
		public static string tempPath;

		public static string bundleNameSubfix;

		public static string LogFile;

		public static bool loaderLogEnabled;
		public static bool netTraceEnabled;
		public static bool showFPS;

		public static IEnumerator Init()
		{
			/*if (Application.platform == RuntimePlatform.Android)
			{
				GameConfig.appVersion = new AndroidJavaClass("com.yytou.xiahun3d.Utils").CallStatic<string>("GetAppVersion");

				AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
				AndroidJavaObject mainActivity = jc.GetStatic<AndroidJavaObject>("currentActivity");
				string pkgName = mainActivity.Call<string>("getPackageName");
				string pkgNameBase = "com.yytou.fairyland";
				if (!pkgName.StartsWith(pkgNameBase))
					throw new Exception("Invalid package");

				if (pkgName.Length == pkgNameBase.Length)
					partnerId = "yytou";
				else
					partnerId = pkgName.Substring(pkgNameBase.Length + 1);
			}
			else*/
			{
				appVersion = string.Empty;
				partnerId = "yytou";
			}
			ReadTerminalId();

			GameMain bs = GameObject.Find("GameMain").GetComponent<GameMain>();
			if (Application.isEditor)
				downloadResource = bs.DownloadResource;
			else
				downloadResource = true;
			loaderLogEnabled = bs.LoaderLogEnabled;
			netTraceEnabled = bs.NetTraceEnabled;
			showFPS = bs.ShowFPS;

			versionCheckURL = bs.VersionCheckURL;
			bundleNameSubfix = "";

#if !UNITY_ANDROID || UNITY_EDITOR
			staticResPath = Path.Combine(Application.streamingAssetsPath, "Res/");
#else
			staticResPath = Application.dataPath + "!assets/Res/";
#endif

			staticResURL = Application.streamingAssetsPath + "/Res/";
#if !UNITY_ANDROID || UNITY_EDITOR
			if (!staticResURL.StartsWith("/"))
				staticResURL = "file:///" + staticResURL;
			else
				staticResURL = "file://" + staticResURL;
#endif

			tempPath = Application.persistentDataPath + "/Temp/";
			if (!Directory.Exists(tempPath))
				Directory.CreateDirectory(tempPath);

			packageResVer = -1;
			WWW www = new WWW(staticResURL + "ver.o");
			yield return www;
			if (string.IsNullOrEmpty(www.error))
			{
				string txt = Encoding.UTF8.GetString(www.bytes);

				IniConfig tm = new IniConfig(txt);
				packageResVer = tm.GetInt("res", -1);
			}
			www.Dispose();

			//如果安装带资源的包，那么用这个内置资源的版本号来建立动态下载目录。因为内置不同版本资源的包不能共用动态升级，否则会出问题。
			localResPath = Application.persistentDataPath + "/Res" + (packageResVer != -1 ? ("" + packageResVer) : "") + "/";
			Debug.Log("localResPath=" + localResPath);
			if (!Directory.Exists(localResPath))
				Directory.CreateDirectory(localResPath);
			if (!localResPath.StartsWith("/"))
				localResURL = "file:///" + localResPath.Replace("\\", "/");
			else
				localResURL = "file://" + localResPath.Replace("\\", "/");

			LogFile = Application.persistentDataPath + "/runtime_log.txt";
		}

		static void ReadTerminalId()
		{
			string per_path = Application.persistentDataPath;
			if (string.IsNullOrEmpty(per_path))
				per_path = Application.dataPath;
			if (!string.IsNullOrEmpty(per_path)) //有持久数据目录
			{
				string file = Path.Combine(per_path, "terminal");
				string tid = null;
				if (File.Exists(file))
					tid = File.ReadAllText(file).Trim();
				if (string.IsNullOrEmpty(tid))
				{
					tid = Guid.NewGuid().ToString();
					File.WriteAllText(file, tid);
				}
				terminalId = tid;
			}
			else
				terminalId = Guid.NewGuid().ToString();
		}
	}
}
