﻿using System;
using System.Collections.Generic;
using FairyGUI;
using UnityEngine;
using DG.Tweening;

namespace FairyGame
{
	public class Boot_ConfirmWin : Window
	{
		public delegate void Callback();

		Callback _ok;
		Callback _cancel;

		public Boot_ConfirmWin()
		{
			this.contentPane = UIPackage.CreateObject("Boot", "更新提示").asCom;
			this.contentPane.GetChild("ok").onClick.Add(() => { _ok(); });
			this.contentPane.GetChild("cancel").onClick.Add(() => { _cancel(); });

			this.Center();
		}

		public void Open(string msg, Callback onOk, Callback onCancel)
		{
			this._ok = onOk;
			this._cancel = onCancel;

			this.contentPane.GetChild("desc").text = msg;
			this.Show();
		}

		override protected void DoShowAnimation()
		{
			this.SetPivot(this.width / 2, this.height / 2);
			this.SetScale(0.1f, 0.1f);
			this.TweenScale(new Vector2(1, 1), 0.3f).SetEase(Ease.OutBack).OnComplete(this.OnShown);
		}
	}

}
