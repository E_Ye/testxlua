﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FairyGUI;
using DG.Tweening;
using System.IO;

namespace FairyGame
{
	public class GameMain : MonoBehaviour
	{
		public bool DownloadResource;
		public bool LoaderLogEnabled;
		public bool NetTraceEnabled;
		public bool ShowFPS;
		public string VersionCheckURL;

		ResourceUpdater _resourceUpdater;

		GComponent _loadingCom;
		GProgressBar _progressBar;
		GTextField _progressText;
		GTextField _errorMsgText;
		Boot_ConfirmWin _confirmWin;
		Controller _stepController;
		GObject _modalWaitPane;
		void Start()
		{
			Debug.Log("Game start...");
            
			Application.runInBackground = true;
			Application.targetFrameRate = 60;
			Screen.sleepTimeout = SleepTimeout.NeverSleep;

			UIPackage.AddPackage("Boot");
			UIConfig.defaultFont = "Droid Sans Fallback, LTHYSZK, Helvetica-Bold, Microsoft YaHei, SimHei";
			FontManager.GetFont(UIConfig.defaultFont).customBold = true;
			UIConfig.defaultScrollStep = 60;
			UIConfig.defaultScrollBarDisplay = ScrollBarDisplayType.Auto;
			UIConfig.defaultScrollBounceEffect = true;
			UIConfig.defaultScrollTouchEffect = true;
			UIObjectFactory.SetPackageItemExtension(UIPackage.GetItemURL("Boot", "ModalWaiting"), typeof(ModalWaiting));
			GRoot.inst.SetContentScaleFactor(1136, 640);
			//float ww = (float)Stage.inst.stageWidth / GRoot.contentScaleFactor;
			//if (ww < 960) //安全区域是960x640，如果小于960（一般是比较方的pad了），这里要重新调整contentScaleFactor
			//	GRoot.inst.SetContentScaleFactor(960, 0);

			_loadingCom = UIPackage.CreateObject("Boot", "Loading").asCom;
			_loadingCom.SetSize(GRoot.inst.width, GRoot.inst.height);
			_loadingCom.GetChild("bg").asLoader.texture = new NTexture((Texture2D)Resources.Load("welcome", typeof(Texture2D)));
			_loadingCom.name = "FirstLoading";
			GRoot.inst.AddChild(_loadingCom);

			_progressBar = _loadingCom.GetChild("progress").asProgress;
			_progressText = _loadingCom.GetChild("message").asTextField;
			_errorMsgText = _loadingCom.GetChild("message2").asTextField;
			_stepController = _loadingCom.GetController("c1");
			_loadingCom.GetChild("btn").onClick.Add(OnClickBtn);

			_confirmWin = new Boot_ConfirmWin();
			_resourceUpdater = new ResourceUpdater();

			_modalWaitPane = UIPackage.CreateObject("Boot", "ModalWaiting");
			_modalWaitPane.name = "ModalWaiting";
			_modalWaitPane.SetSize(_loadingCom.width, _loadingCom.height);

			_progressBar.visible = false;
			_progressText.text = "";

			StartCoroutine(Start2());
		}

		IEnumerator Start2()
		{
			yield return StartCoroutine(GameConfig.Init());

			if (GameConfig.downloadResource && !string.IsNullOrEmpty(VersionCheckURL))
				CheckUpdate(); 
			else
				StartLoad2();
		}

		void CheckUpdate()
		{
			_progressBar.visible = false;
			_progressText.text = "正在连线检查新版本";
			ShowModalWait();
			_stepController.selectedIndex = 0;

			StartCoroutine(_resourceUpdater.CheckUpdate(
				() =>
				{
					if (_resourceUpdater.newVersion) //有更新
					{
						_progressText.text = string.Empty;
						if (_resourceUpdater.firstRun)
							StartDownload();
						else
							_confirmWin.Open(_resourceUpdater.releaseNotes, StartDownload, StartLoad2);
					}
					else
						StartLoad2();
				},

				(string error) =>
				{
					if (_resourceUpdater.firstRun)
					{
						_stepController.selectedIndex = 1;
						_errorMsgText.text = "请检查网络连接: " + error;
					}
					else
						StartLoad2();
				},
				10f));
		}

		void StartDownload()
		{
			_progressBar.visible = true;
			_progressBar.value = 0;
			_progressText.text = "下载资源更新...";
			CloseModalWait();
			_stepController.selectedIndex = 0;
			_confirmWin.Hide();

			StartCoroutine(_resourceUpdater.UpdateRes(StartUnzip,
				(string error) =>
				{
					if (_resourceUpdater.firstRun)
					{
						_stepController.selectedIndex = 1;
						_errorMsgText.text = "下请检查网络连接:\n" + error;
					}
					else
						StartLoad();
				},
				(int total, int recved) =>
				{
					float progress = total == 0 ? 0 : (float)recved / total;
					_progressText.text = "下载资源更新：" + MiscUtil.GetSizeName(recved, 2) + "/" + MiscUtil.GetSizeName(total, 2);
					_progressBar.value = (int)(progress * 100);
				})
				);
		}

		void StartUnzip()
		{
			_progressBar.value = 0;
			_progressText.text = "正在解压资源，请不要退出...";

			StartCoroutine(_resourceUpdater.UnzipRes(StartLoad,
				(string error) =>
				{
					if (_resourceUpdater.firstRun)
					{
						_stepController.selectedIndex = 1;
						_errorMsgText.text = "解压资源失败:\n" + error;
					}
					else
						StartLoad();
				},

				(int total, int recved) =>
				{
					float progress = (float)recved / total;
					_progressText.text = "正在解压资源，请不要退出...";
					_progressBar.value = (int)(progress * 100);
				})
				);
		}

		void StartLoad()
		{
			_stepController.selectedIndex = 2;
		}

		void StartLoad2()
		{
			_progressBar.visible = false;
			_progressText.text = "载入游戏，马上就好！";
			_stepController.selectedIndex = 0;

			ShowModalWait();
			StartCoroutine(StartLoad3());
		}

		IEnumerator StartLoad3()
		{
			yield return null; //让界面有机会先更新

			Debug.Log("Load start...");

			_resourceUpdater = null;

			DOTween.defaultTimeScaleIndependent = true;
			_progressBar.value = 0;

			AssetManager.Instantiate();
            //LuaLoader.Instantiate();

            if (!GameConfig.downloadResource && Application.isEditor)
            { } //LuaConst.luaDir = Application.dataPath + "/../Lua/";
            else
            {
                if (Application.platform == RuntimePlatform.IPhonePlayer)
                {
                    byte[] bytes;
                    bytes = AssetManager.inst.LoadBytes("data", "aul_ios");
                    //((LuaLoader)LuaLoader.Instance).AddBundle(bytes, false);
                    bytes = AssetManager.inst.LoadBytes("data", "aulbase_ios");
                    //((LuaLoader)LuaLoader.Instance).AddBundle(bytes, true);
                }
                else
                {
                    byte[] bytes;
                    bytes = AssetManager.inst.LoadBytes("data", "aul");
                    //((LuaLoader)LuaLoader.Instance).AddBundle(bytes, false);
                    bytes = AssetManager.inst.LoadBytes("data", "aulbase");
                    //((LuaLoader)LuaLoader.Instance).AddBundle(bytes, true);
                }
            }
            //Debug.Log(">>>>load sucai before");
            AssetManager.inst.LoadAsset("ui1", "sucai");
            //Debug.Log(">>>>load sucai after");

            AssetManager.inst.LoadAsset("ui1", "common");
			AssetManager.inst.LoadAsset("ui", "login");

			while (!AssetManager.inst.isDone)
				yield return null;

			LoadCompleted();
		}

		void LoadCompleted()
		{
            //clean upAudioClip AudioClip
            GRoot.inst.RemoveChild(_loadingCom, true);

			_loadingCom = null;
			_progressBar = null;
			_progressText = null;

			_confirmWin.Dispose();
			_confirmWin = null;

			_modalWaitPane.Dispose();
			_modalWaitPane = null;

			UIPackage.RemovePackage("Boot");

			UIConfig.globalModalWaiting = "ui://Common/ModalWaiting";
			UIConfig.windowModalWaiting = "ui://Common/ModalWaiting";
			UIConfig.loaderErrorSign = "ui://Common/图标错误";
			UIConfig.buttonSound = (AudioClip)UIPackage.GetItemAsset("Sucai", "click");

			UIObjectFactory.SetLoaderExtension(typeof(MyGLoader));
			UIObjectFactory.SetPackageItemExtension(UIConfig.globalModalWaiting, typeof(ModalWaiting));

			AnimationManager.Instantiate();
			SoundManager.Instantiate();
            gameObject.AddComponent<LuaManager>();
            gameObject.AddComponent<KBEMain>();
            //gameObject.AddComponent<UI>();

            //LuaManager.Instantiate();
            //LuaManager

            
            IconManager.Instantiate();
		}

		void OnClickBtn()
		{
			if (_stepController.selectedIndex == 1)
				CheckUpdate();
			else
				StartLoad2();
		}

		void ShowModalWait()
		{
			GRoot.inst.AddChild(_modalWaitPane);
		}

		void CloseModalWait()
		{
			if (_modalWaitPane.parent != null)
				GRoot.inst.RemoveChild(_modalWaitPane);
		}


        

        void Update()
		{
#if ENABLE_VOICE
			GameLink.Update();
#endif
		}

		void OnApplicationQuit()
		{
#if ENABLE_VOICE
			GameLink.Dispose();
#endif
			//NativeInterface.Dispose();
		}
	}
}
