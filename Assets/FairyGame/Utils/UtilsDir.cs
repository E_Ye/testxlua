﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace FairyGame
{
	public class UtilsDir
	{
		public const float CONST_RAD_TO_DEG = (float)(180d / Math.PI);
		public const float CONST_DEG_TO_RAD = (float)(Math.PI / 180d);

		private static int[] degDirMap;
		public static int GetDirOfDeg(int deg)
		{
			if (degDirMap == null)
			{
				degDirMap = new int[361];
				int d;
				for (int i = -180; i <= 180; i++)
				{
					if (i >= -20 && i <= 20)
						d = 1;
					else if (i > 20 && i < 70)
						d = 2;
					else if (i >= 70 && i <= 110)
						d = 3;
					else if (i > 110 && i < 160)
						d = 4;
					else if (i >= 160 || i <= -160)
						d = 5;
					else if (i > -160 && i < -110)
						d = 6;
					else if (i >= -110 && i <= -70)
						d = 7;
					else
						d = 8;
					degDirMap[i+180] = d;
				}
			}
			return degDirMap[deg+180];
		}

		private static int[] dirDegMap = new int[] { 0, 0, 63, 80, 107, 180, 243, 270, 297 };
		public static int GetDegOfDir(int dir)
		{
			return dirDegMap[dir];
		}

		public static int GetAngle(Vector2 pt1, Vector2 pt2)
		{
			float x = pt2.x - pt1.x;
			float y = pt1.y - pt2.y;
			int deg = 90 - (int)(Mathf.Atan2(y, x) * CONST_RAD_TO_DEG);
			if (deg > 180)
				deg -= 360;
			return deg;
		}

		public static int GetDirectionOfPoints(Vector2 pt1, Vector2 pt2)
		{
			float x = pt2.x - pt1.x;
			float y = pt1.y - pt2.y;
			int deg = 90 - (int)(Mathf.Atan2(y, x) * CONST_RAD_TO_DEG);
			if (deg > 180)
				deg -= 360;
			return GetDirOfDeg(deg);
		}

		public static int GetDirectionOfPath(Vector2 pt1, Vector2 pt2, bool nearby = false, bool diamond = true)
		{
			int ret;
			if (!nearby)
			{
				float x = pt2.x - pt1.x;
				float y = pt1.y - pt2.y;
				int deg = 90 - (int)(Mathf.Atan2(y, x) * UtilsDir.CONST_RAD_TO_DEG);
				if (deg > 180)
					deg -= 360;
				ret = UtilsDir.GetDirOfDeg(deg);
			}
			else
			{
				if (pt2.x == pt1.x && pt2.y < pt1.y)
					ret = 1;
				else if (pt2.x > pt1.x && pt2.y < pt1.y)
					ret = 2;
				else if (pt2.x > pt1.x && pt2.y == pt1.y)
					ret = 3;
				else if (pt2.x > pt1.x && pt2.y > pt1.y)
					ret = 4;
				else if (pt2.x == pt1.x && pt2.y > pt1.y)
					ret = 5;
				else if (pt2.x < pt1.x && pt2.y > pt1.y)
					ret = 6;
				else if (pt2.x < pt1.x && pt2.y == pt1.y)
					ret = 7;
				else if (pt2.x < pt1.x && pt2.y < pt1.y)
					ret = 8;
				else
					ret = 1;
			}

			if (diamond)
				ret++;
			if (ret > 8)
				ret = 1;
			return ret;
		}

		public static Vector2 GetNearbyCell(Vector2 pt, int dir, int distance = 1, bool diamond = true)
		{
			Vector2 ret = new Vector2();
			if (diamond)
			{
				dir--;
				if (dir <= 0)
					dir = 8;
			}
			switch (dir)
			{
				case 1:
					ret.x = pt.x;
					ret.y = pt.y - distance;
					break;
				case 2:
					ret.x = pt.x + distance;
					ret.y = pt.y - distance;
					break;
				case 3:
					ret.x = pt.x + distance;
					ret.y = pt.y;
					break;
				case 4:
					ret.x = pt.x + distance;
					ret.y = pt.y + distance;
					break;
				case 5:
					ret.x = pt.x;
					ret.y = pt.y + distance;
					break;
				case 6:
					ret.x = pt.x - distance;
					ret.y = pt.y + distance;
					break;
				case 7:
					ret.x = pt.x - distance;
					ret.y = pt.y;
					break;
				case 8:
					ret.x = pt.x - distance;
					ret.y = pt.y - distance;
					break;
			}
			if (ret.x < 0)
				ret.x = 0;
			if (ret.y < 0)
				ret.y = 0;
			return ret;
		}

		public static int GetNextDirection(int dir)
		{
			dir++;
			if (dir > 8)
				dir = 1;
			return dir;
		}

		public static int GetPrevDirection(int dir)
		{
			dir--;
			if (dir <= 0)
				dir = 8;
			return dir;
		}

		public static int GetDiagonalDirection(int dir)
		{
			dir += 4;
			if (dir > 8)
				dir -= 8;
			return dir;
		}

		public static bool IsUpwardDirection(int dir)
		{
			return dir == 1 || dir == 2 || dir == 7 || dir == 8;
		}
	}
}
