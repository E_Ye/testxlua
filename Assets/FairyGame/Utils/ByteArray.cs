﻿using System;
using System.Collections;
using System.Text;
using System.IO;
using ICSharpCode.SharpZipLib.Zip.Compression;
using ICSharpCode.SharpZipLib.Zip.Compression.Streams;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace FairyGame
{
	public class ByteArray
	{
		int _pointer;
		byte[] _data;
		int _offset;
		bool _littleEndian;
		bool _isSystemEndian;

		static byte[] _tmp = new byte[8];

		/// <summary>
		/// 是否LITTLE_ENDIAN字节顺序
		/// </summary>
		public bool isLittleEndian
		{
			get { return _littleEndian; }
			set
			{
				_littleEndian = value;
				_isSystemEndian = (BitConverter.IsLittleEndian == _littleEndian);
			}
		}

		public int length { get; private set; }

		public byte[] data { get { return this._data; } }

		public int position
		{
			get { return this._pointer; }
			set
			{
				this._pointer = value;
			}
		}

		public int bytesAvailable
		{
			get { return this._data.Length - this._pointer; }
		}

		public int cmd;

		public ByteArray(byte[] data)
		{
			if (data == null)
				throw new ArgumentException("Parameter is null.");

			this._data = data;
			this.length = data.Length;
			this._offset = 0;
			this._pointer = 0;
			this.isLittleEndian = false;
		}

		public ByteArray(byte[] data, int offset, int len)
		{
			if (data == null)
				throw new ArgumentException("Parameter is null.");

			this._data = data;
			this.length = len;
			this._offset = offset;
			this._pointer = 0;
			this.isLittleEndian = false;
		}

		public int SkipBytes(int numberBytes)
		{
			_pointer += numberBytes;
			return _pointer;
		}

		public byte ReadByte()
		{
			byte result = this._data[_offset + this._pointer++];
			return result;
		}

		public byte[] ReadBytes()
		{
			int len = this.ReadUShort();
			if (len == 0)
				return new byte[0];
			byte[] output = new byte[len];
			Array.Copy(this._data, this._pointer, output, 0, len);
			this._pointer += len;
			return output;
		}

		public byte[] ReadBytes(ref byte[] output, int startIndex, int destIndex, int length)
		{
			Array.Copy(this._data, startIndex + _offset, output, destIndex, length);
			return output;
		}

		public byte[] ReadBytes(ref byte[] output, int destIndex, int length)
		{
			Array.Copy(this._data, this._pointer + _offset, output, destIndex, length);
			this._pointer += length;
			return output;
		}

		public int ReadInt()
		{
			Array.Copy(this._data, this._pointer + _offset, _tmp, 0, 4);
			if (!_isSystemEndian)
				Array.Reverse(_tmp, 0, 4);
			int result = BitConverter.ToInt32(_tmp, 0);
			this._pointer += 4;
			return result;
		}

		public uint ReadUInt()
		{
			Array.Copy(this._data, this._pointer + _offset, _tmp, 0, 4);
			if (!_isSystemEndian)
				Array.Reverse(_tmp, 0, 4);
			uint result = BitConverter.ToUInt32(_tmp, 0);
			this._pointer += 4;
			return result;
		}

		public float ReadFloat()
		{
			Array.Copy(this._data, this._pointer + _offset, _tmp, 0, 4);
			if (!_isSystemEndian)
				Array.Reverse(_tmp, 0, 4);
			float result = BitConverter.ToSingle(_tmp, 0);
			this._pointer += 4;
			return result;
		}

		public long ReadLong()
		{
			Array.Copy(this._data, this._pointer + _offset, _tmp, 0, 8);
			if (!_isSystemEndian)
				Array.Reverse(_tmp, 0, 8);
			long result = BitConverter.ToInt64(_tmp, 0);
			this._pointer += 8;
			return result;
		}

		public double ReadDouble()
		{
			Array.Copy(this._data, this._pointer + _offset, _tmp, 0, 8);
			if (!_isSystemEndian)
				Array.Reverse(_tmp, 0, 8);
			double result = BitConverter.ToDouble(_tmp, 0);
			this._pointer += 8;
			return result;
		}

		public short ReadShort()
		{
			Array.Copy(this._data, this._pointer + _offset, _tmp, 0, 2);
			if (!_isSystemEndian)
				Array.Reverse(_tmp, 0, 2);
			short result = BitConverter.ToInt16(_tmp, 0);
			this._pointer += 2;
			return result;
		}

		public ushort ReadUShort()
		{
			Array.Copy(this._data, this._pointer + _offset, _tmp, 0, 2);
			if (!_isSystemEndian)
				Array.Reverse(_tmp, 0, 2);
			ushort result = BitConverter.ToUInt16(_tmp, 0);
			this._pointer += 2;
			return result;
		}

		public char ReadChar()
		{
			Array.Copy(this._data, this._pointer + _offset, _tmp, 0, 2);
			char result = BitConverter.ToChar(_tmp, 0);
			if (!_isSystemEndian)
				Array.Reverse(_tmp, 0, 2);
			this._pointer += 2;
			return result;
		}

		public bool ReadBool()
		{
			bool result = BitConverter.ToBoolean(this._data, this._pointer + _offset);
			this._pointer++;
			return result;
		}

		public string ReadLongString()
		{
			int len = this.ReadUShort();
			if (len == 0)
				return string.Empty;
			string result = Encoding.UTF8.GetString(_data, _pointer + _offset, len);
			_pointer += len;
			return result;
		}

		public string ReadString()
		{
			int len = this.ReadByte();
			if (len == 0)
				return string.Empty;
			string result = Encoding.UTF8.GetString(_data, _pointer + _offset, len);
			_pointer += len;
			return result;
		}

		public Hashtable ReadJSON()
		{
			bool compressed = this.ReadByte() != 0;
			if (compressed)
				_Decompress();

			string json = ReadJSONString();
			return (Hashtable)MiniJSON.JsonDecode(json);
		}

		public string ReadJSONString()
		{
			bool compressed = this.ReadByte() != 0;
			if (compressed)
				_Decompress();

			int len = _data.Length - _pointer;
			if (len == 0)
				return string.Empty;
			string result = Encoding.UTF8.GetString(_data, _pointer + _offset, len);
			_pointer += len;
			return result;
		}

		public void Decompress()
		{
			_pointer = 0;
			_Decompress();
		}

		void _Decompress()
		{
			_data = Compression.Decompress(_data, _offset + _pointer, _data.Length - _pointer - _offset);
			_offset = 0;
			_pointer = 0;
		}
	}
}
