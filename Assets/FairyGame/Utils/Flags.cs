﻿using System;
using System.Collections;
using System.Collections.Generic;
using FairyGUI;
using UnityEngine;

namespace FairyGame
{
	public class Flags
	{
		private int iCounter;
		private float iValue;
		private Dictionary<object, float> iOwners;

		public Flags()
		{
			iOwners = new Dictionary<object, float>();
		}

		public void Set(object requestor)
		{
			float oldValue;
			if (iOwners.TryGetValue(requestor, out oldValue))
			{
				iValue -= oldValue;
			}
			else
			{
				iCounter++;
				iOwners[requestor] = value;
			}
		}

		public void Set(object requestor, float value)
		{
			float oldValue;
			if (iOwners.TryGetValue(requestor, out oldValue))
				iValue -= oldValue;
			else
				iCounter++;
			iValue += value;
			iOwners[requestor] = value;
		}

		public void Unset(object requestor)
		{
			float oldValue;
			if (iOwners.TryGetValue(requestor, out oldValue))
			{
				iCounter--;
				iValue -= oldValue;
				iOwners.Remove(requestor);
			}
		}

		public bool IsTrue
		{
			get { return iCounter > 0; }
		}

		public float value
		{
			get { return iValue; }
		}

		public void Clear()
		{
			iOwners.Clear();
			iCounter = 0;
			iValue = 0;
		}
	}
}
