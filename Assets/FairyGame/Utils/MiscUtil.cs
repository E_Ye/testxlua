﻿/********************************************************************
	filename: 	MiscUtil.cs
	created:	2014/05/17 12:32
	author:		Moncat
*********************************************************************/

using System;
using System.Collections;
using System.Text;

namespace FairyGame
{
    /// <summary>
    /// 一些杂项功能
    /// </summary>
    public static class MiscUtil
    {
        public static readonly DateTime DATETIME_ST1970 = new DateTime(1970, 1, 1).ToLocalTime();

        /// <summary>
        /// 获得系统时间毫秒数（1970年1月1日起）
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static long GetTimeMillis(this DateTime dt)
        {
            return (long)(dt - DATETIME_ST1970).TotalMilliseconds;
        }

        /// <summary>
        /// 获得系统时间秒数（1970年1月1日起）
        /// </summary>
        /// <param name="dt"></param>
        /// <returns></returns>
        public static long GetTimeSeconds(this DateTime dt)
        {
            return (long)(dt - DATETIME_ST1970).TotalSeconds;
        }

        /// <summary>
        /// 交换数组的两个值
        /// </summary>
        /// <param name="ar">数组</param>
        /// <param name="a">第一个数组下标</param>
        /// <param name="b">另一个数组下标</param>
        public static void Swap(this object[] ar, int a, int b)
        {
            Swap(ar, a, ar, b);
        }

        /// <summary>
        /// 交换两数组中的指定值
        /// </summary>
        /// <param name="ar1">数组a</param>
        /// <param name="a">数组a的位置</param>
        /// <param name="ar2">数组b</param>
        /// <param name="b">数组b的位置</param>
        public static void Swap(object[] ar1, int a, object[] ar2, int b)
        {
            if (ar1[a] != ar2[b])
            {
                object c = ar1[a];
                ar1[a] = ar2[b];
                ar2[b] = c;
            }
        }

        /// <summary>
        /// 交换列表的两个值
        /// </summary>
        /// <param name="ls">列表</param>
        /// <param name="a">第一个数组位置</param>
        /// <param name="b">另一个列表位置</param>
        public static void Swap(this IList ls, int a, int b)
        {
            Swap(ls, a, ls, b);
        }

        /// <summary>
        /// 交换两列表中的指定值
        /// </summary>
        /// <param name="ls1">列表a</param>
        /// <param name="a">列表a的位置</param>
        /// <param name="ls2">列表b</param>
        /// <param name="b">列表b的位置</param>
        public static void Swap(IList ls1, int a, IList ls2, int b)
        {
            if (ls1[a] != ls2[b])
            {
                object o = ls1[a];
                ls1[a] = ls2[b];
                ls2[b] = o;
            }
        }

        /// <summary>
        /// 头尾颠倒列表顺序
        /// </summary>
        /// <param name="ls">列表</param>
        public static void Reverse(this IList ls)
        {
            int len = ls.Count;
            int count = len / 2;
            for (int i = 0; i < count; i++)
                Swap(ls, i, len - i - 1);
        }

        public static string GetSizeName(int val, int digits = 2)
        {
            if (val < 1024)
                return val + "B";
            else if (val < 1024 * 1024)
                return ((float)val / 1024).ToString("f" + digits) + "K";
            else
                return ((float)val / (1024 * 1024)).ToString("f" + digits) + "M";
        }

        /**
		 * 内部密匙
		 */
        private static byte[] cryptoKey = new byte[] { (byte) 0xC8, (byte) 0x10, (byte) 0x15,
				(byte) 0x26, (byte) 0xBF, (byte) 0x2C, (byte) 0x29, (byte) 0x62, (byte) 0x5D,
				(byte) 0x1F, (byte) 0x1C, (byte) 0x7A, (byte) 0xA7, (byte) 0x97, (byte) 0x92,
				(byte) 0x8C, (byte) 0xA2, (byte) 0x29, (byte) 0x2C, (byte) 0xAE, (byte) 0x54,
				(byte) 0x5B, (byte) 0xBA, (byte) 0x20 };

        /// <summary>
        /// 解密数据
        /// </summary>
        /// <param name="value">密文</param>
        /// <returns>返回明文</returns>
        public static string DecryptValue(string value)
        {
            byte[] v = Convert.FromBase64String(value);
            if (v == null)
                return value;
            //v = DesUtil.TripleDesDecrypt(v, cryptoKey);
            //if (v != null)
            //    return Encoding.UTF8.GetString(v);
            //else
                return value;
        }

        /// <summary>
        /// 加密数据
        /// </summary>
        /// <param name="value">明文</param>
        /// <returns>返回密文</returns>
        public static string EncryptValue(string value)
        {
            //byte[] v = DesUtil.TripleDesEncrypt(Encoding.UTF8.GetBytes(value), cryptoKey);
            //if (v != null)
            //    return Convert.ToBase64String(v);
            //else
                return value;
        }
    }
}
