﻿using LuaInterface;
using System.Collections;
using UnityEngine;
using XLua;

namespace FairyGame
{
	public class JavaUtils
	{
#if !(UNITY_IPHONE)
		static AndroidJavaObject mainActivity;
        private static ArrayList args = new ArrayList();

        public static AndroidJavaObject GetMainActivity()
		{
			if (mainActivity == null)
			{
				AndroidJavaClass jc = new AndroidJavaClass("com.unity3d.player.UnityPlayer");
				mainActivity = jc.GetStatic<AndroidJavaObject>("currentActivity");
			}
			return mainActivity;
		}

		public static AndroidJavaClass GetJavaClass(string className)
		{
			return new AndroidJavaClass(className);
		}

		public static void CallJava(AndroidJavaObject jo, string methodName, LuaTable table)
		{
			if (table != null && table.Length > 0)
			{
                args.Clear();
                table.ForEach<object, object>((k, v) => { args.Add(v); });
				table.Dispose();
				jo.Call(methodName, args.ToArray());
			}
			else
				jo.Call(methodName);
		}

		public static string CallJava_string(AndroidJavaObject jo, string methodName, LuaTable table)
		{
			if (table != null && table.Length > 0)
			{
                args.Clear();
                table.ForEach<object, object>((k, v) => { args.Add(v); });
                table.Dispose();
                return jo.Call<string>(methodName, args);
			}
			else
				return jo.Call<string>(methodName);
		}

        public static void CallJavaStatic(AndroidJavaClass jc, string methodName, LuaTable table)
		{
			if (table != null && table.Length > 0)
			{
                args.Clear();
                table.ForEach<object, object>((k, v) => { args.Add(v); });
                table.Dispose();
                jc.CallStatic(methodName, args);
			}
			else
				jc.CallStatic(methodName);
		}

		public static string CallJavaStatic_string(AndroidJavaClass jc, string methodName, LuaTable table)
		{
			if (table != null && table.Length > 0)
			{
                args.Clear();
                table.ForEach<object, object>((k, v) => { args.Add(v); });
                table.Dispose();
                return jc.CallStatic<string>(methodName, args);
			}
			else
				return jc.CallStatic<string>(methodName);
		}
#endif
	}
}
