﻿using LuaInterface;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using XLua;

namespace FairyGame
{
	public class SimpleWWW
	{
		WWW www;

		public SimpleWWW(string url)
		{
			www = new WWW(url);
		}

		public SimpleWWW(string url, LuaTable fields)
		{
			WWWForm form = new WWWForm();
            fields.ForEach<string, string>((fileName, value) => { form.AddField(fileName, value); });
			www = new WWW(url, form);
		}

		public SimpleWWW(string url, LuaTable fields, LuaTable headers)
		{
			byte[] postData = null;
			if (fields != null)
			{
				WWWForm form = new WWWForm();
                fields.ForEach<string, string>((fileName, value) => { form.AddField(fileName, value); });
                postData = form.data;
			}

			Dictionary<string, string> wwwHeaders = null;
			if (headers != null)
			{
				wwwHeaders = new Dictionary<string, string>();
                fields.ForEach<string, string>((fileName, value) => { wwwHeaders.Add(fileName, value); });
			}

			www = new WWW(url, postData, wwwHeaders);
		}

		public static string EscapeURL(string s)
		{
			return WWW.EscapeURL(s);
		}

		public bool isDone
		{
			get { return www.isDone; }
		}

		public byte[] bytes
		{
			get { return www.bytes; }
		}

		public string error
		{
			get { return www.error; }
		}

		public string GetResponseHeader(string name)
		{
			string ret = null;
			www.responseHeaders.TryGetValue(name, out ret);
			return ret;
		}
	}
}
