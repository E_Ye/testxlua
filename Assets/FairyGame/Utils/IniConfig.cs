﻿/********************************************************************
	filename: 	IniConfig.cs
	created:	2014-07-27 16:54:54
	author:		Moncat
*********************************************************************/
using System;
using System.IO;
using System.Collections;
using System.Text;

namespace FairyGame
{
	/// <summary>
	/// INI配置文件
	/// </summary>
	public class IniConfig : Mapping
	{
		/// <summary>
		/// 从文件载入INI配置信息
		/// </summary>
		/// <param name="path">文件路径</param>
		/// <param name="encoding">编码方式</param>
		/// <returns>返回IniConfig对象</returns>
		public static IniConfig LoadFromFile(string path, Encoding encoding = null)
		{
			string content = File.ReadAllText(path, encoding ?? Encoding.UTF8);
			return new IniConfig(content);
		}

		/// <summary>
		/// 将INI配置信息写入文件
		/// </summary>
		/// <param name="ini">IniConfig对象</param>
		/// <param name="path">文件路径</param>
		/// <param name="encoding">编码方式</param>
		public static void SaveToFile(IniConfig ini, string path, Encoding encoding = null)
		{
			string content = ini.Encode();
			File.WriteAllText(path, content, encoding ?? Encoding.UTF8);
		}

		/// <summary>
		/// 构造器
		/// </summary>
		/// <param name="content">配置信息字符串内容</param>
		public IniConfig(string content = null)
		{
			if (content != null)
				Decode(content);
		}

		/// <summary>
		/// 从配置信息文本内容中解析Ini配置项
		/// </summary>
		/// <param name="content"></param>
		public void Decode(string content)
		{
            using (StringReader sr = new StringReader(content))
            {
                do
                {
                    string line = sr.ReadLine();
                    if (line == null) //结束
                        break;
                    line = line.Trim();
                    if (line.Length == 0) //空行
                        continue;
                    if (line.StartsWith("#") || line.StartsWith(";")) //注释
                        continue;
                    int pos = line.IndexOf('=');
                    if (pos == -1)
                        continue;
                    this[line.Substring(0, pos).Trim()] = line.Substring(pos + 1).Trim();
                }
                while (true);
            }
		}

		/// <summary>
		/// 将配置项编码成文本内容
		/// </summary>
		/// <returns></returns>
		public string Encode()
		{
			StringBuilder sb = new StringBuilder();
			foreach (DictionaryEntry de in this)
			{
				string key = TypeUtil.GetString(de.Key);
				string value = TypeUtil.GetString(de.Value);
				sb.Append(key).Append("=").AppendLine(value);
			}
			return sb.ToString();
		}
	}
}