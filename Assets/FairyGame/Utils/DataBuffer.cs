﻿/********************************************************************
	filename: 	DataBuffer.cs
	created:	2014/06/17 17:41
	author:		Moncat
*********************************************************************/

using System;

namespace FairyGame
{
	/// <summary>
	/// 动态容量数据缓冲区
	/// 智能分配和释放缓冲区空间，最大限度地减少数据的重复拷贝。
	/// </summary>
	public class DataBuffer : Buffer
	{
		//缓冲区
		private byte[] _buffer = null;

		//初始缓冲区大小
		private int _initBufferSize = 1024;

		//第一个buffer中数据的起始位置
		private int _firstOffset = 0;

		//缓冲区中数据长度
		private int _length = 0;

		/// <summary>
		/// 构造器
		/// </summary>
		/// <param name="littleEndian">是否LITTLE_ENDIAN字节顺序，默认为false</param>
		public DataBuffer(bool littleEndian = false)
			: base(littleEndian)
		{
			_buffer = new byte[_initBufferSize];
		}

		/// <summary>
		/// 构造器
		/// </summary>
		/// <param name="initBufferSize">初始缓冲区大小</param>
		/// <param name="littleEndian">是否LITTLE_ENDIAN字节顺序，默认为false</param>
		public DataBuffer(int initBufferSize, bool littleEndian = false)
			: base(littleEndian)
		{
			if (initBufferSize < 0)
				throw new ArgumentException();
			_buffer = new byte[initBufferSize];
			_initBufferSize = initBufferSize;
		}

		/// <summary>
		/// 构造器
		/// </summary>
		/// <param name="data">初始数据</param>
		/// <param name="littleEndian">是否LITTLE_ENDIAN字节顺序，默认为false</param>
		public DataBuffer(byte[] data, bool littleEndian = false)
			: base(littleEndian)
		{
			int len = data.Length;
			_buffer = new byte[len];
			_initBufferSize = len;
			Append(data, 0, len);
		}

		/// <summary>
		/// 构造器
		/// </summary>
		/// <param name="data">初始数据缓冲区</param>
		/// <param name="offset">数据起始位置</param>
		/// <param name="len">数据长度</param>
		/// <param name="littleEndian">是否LITTLE_ENDIAN字节顺序，默认为false</param>
		public DataBuffer(byte[] data, int offset, int len, bool littleEndian = false)
			: base(littleEndian)
		{
			_buffer = new byte[len];
			_initBufferSize = len;
			Append(data, offset, len);
		}

		public override int capacity
		{
			get { return _buffer.Length; }
		}
		
		public override int length
		{
			get { return _length; }
		}

		public override void Remove(int len)
		{
			if (len < 0)
				throw new ArgumentException();
			if (len > _length)
				throw new IndexOutOfRangeException();

			_firstOffset += len;
			_length -= len;
			CheckFreeBuffer();
		}

		public override void Truncate(int len)
		{
			if (len < 0)
				throw new ArgumentException();
			if (len > _length)
				throw new IndexOutOfRangeException();

			_length -= len;
			CheckFreeBuffer();
		}

		public override void TrimBuffer()
		{
			Resize(length);
		}

		public override void Clear()
		{
			_length = 0;
			_firstOffset = 0;
			CheckFreeBuffer();
		}

		/// <summary>
		/// 重新设置缓冲区大小
		/// </summary>
		/// <param name="size"></param>
		public void Resize(int size)
		{
			if (size < 0)
				throw new ArgumentException();

			byte[] nbuf = new byte[size];

			if (_length > 0) // 原来有数据
			{
				if (size < _length)
					_length = size;
				Array.Copy(_buffer, _firstOffset, nbuf, 0, _length); // 拷贝数据
			}

			_firstOffset = 0;
			_buffer = nbuf;
		}

		/// <summary>
		/// 移动数据到缓冲区最前端
		/// </summary>
		public void MigrateData()
		{
			if (_firstOffset > 0) // 数据不在最前端
			{
				if (_length > 0) // 有数据
					Array.Copy(_buffer, _firstOffset, _buffer, 0, _length); // 拷贝数据到最前端
				_firstOffset = 0;
			}
		}

		/// <summary>
		/// 检查前后空闲空间并调整数据位置
		/// </summary>
		private void CheckFreeBuffer()
		{
			int newCap = _length * 2 + _initBufferSize;
			int cap = capacity;
			if (newCap * 2 < cap // 新容量小于当前容量的1/2
					&& cap - newCap >= _initBufferSize * 10) // 可释放大于初始缓冲区大小的10倍空间
				Resize(newCap);

			if (_firstOffset > _length * 2) // 前端空闲值大于实际数据值2倍时执行数据前移
				MigrateData();
		}

		/// <summary>
		/// 扩充缓冲区
		/// </summary>
		/// <param name="need">最小需扩充的大小</param>
		public void ExtendBuffer(int need)
		{
			if (need > 0)
			{
				int newSize = (capacity + need) * 3 / 2
						+ Math.Max(need * 2, _initBufferSize);
				Resize(newSize);
			}
		}

		public override void Append(byte[] data, int offset, int len)
		{
			if (len < 0)
				throw new ArgumentException();
			if (offset < 0 || offset + len > data.Length)
				throw new IndexOutOfRangeException();

			int need = len - (capacity - _length); // 缺少的大小
			if (need > 0) // 剩下空间不够
				ExtendBuffer(need);

			Set(_length, data, offset, len);
			_length += len;
		}

		public override void Append(Buffer bf)
		{
			int len = bf.length;
			int need = len - (capacity - _length); // 缺少的大小
			if (need > 0) // 剩下空间不够
				ExtendBuffer(need);

			Set(_length, bf);
			_length += len;
		}

		public override void Pop(byte[] buf, int offset, int len)
		{
			Get(0, buf, offset, len); // 拷贝数据

			_firstOffset += len;
			_length -= len; // 剩下的数据长度

			CheckFreeBuffer(); // 检查释放
		}

		public override void Set(int pos, byte[] data, int offset, int len)
		{
			if (len < 0)
				throw new ArgumentException();
			if (pos < 0 || offset < 0 || pos + len > capacity
					|| offset + len > data.Length)
				throw new IndexOutOfRangeException();

			int need = len - (capacity - pos - _firstOffset);
			if (need > 0) // 后面空间不够
				MigrateData();

			Array.Copy(data, offset, _buffer, _firstOffset + pos, len);
		}

		public override void Set(int pos, Buffer bf)
		{
			int len = bf.length;
			if (pos < 0 || pos + len > capacity)
				throw new IndexOutOfRangeException();

			int need = len - (capacity - pos - _firstOffset);
			if (need > 0) // 后面空间不够
				MigrateData();

			bf.Get(0, _buffer, _firstOffset + pos, len);
		}

		public override void Get(int pos, byte[] buf, int offset, int len)
		{
			if (len < 0)
				throw new ArgumentException();
			if (pos < 0 || offset < 0 || offset + len > buf.Length
					|| pos + len > _length)
				throw new IndexOutOfRangeException();

			Array.Copy(_buffer, _firstOffset + pos, buf, offset, len);
		}
	}
}
