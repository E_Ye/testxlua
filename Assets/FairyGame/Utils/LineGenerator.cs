﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace FairyGame
{
	public class LineGenerator
	{
		private int x0, y0, x1, y1;
		private int i;
		private int steep;
		private int sx, sy;
		private int dx, dy;
		private int e;
		private int segSize;

		public LineGenerator() { }

		public void Start(Vector2 origin, Vector2 terminal)
		{
			x0 = (int)origin.x;
			y0 = (int)origin.y;
			x1 = (int)terminal.x;
			y1 = (int)terminal.y;

			dx = Math.Abs(x1 - x0);
			sx = ((x1 - x0) > 0) ? 1 : -1;
			dy = Math.Abs(y1 - y0);
			sy = ((y1 - y0) > 0) ? 1 : -1;
			if (dy > dx)
			{
				steep = 0;
				int tmpswap;
				tmpswap = x0; x0 = y0; y0 = tmpswap;
				tmpswap = dx; dx = dy; dy = tmpswap;
				tmpswap = sx; sx = sy; sy = tmpswap;
			}
			else
				steep = 1;
			e = (dy << 1) - dx;
			i = 0;
		}

		public void Reset()
		{
			dx = -1;
		}

		public int Count
		{
			get { return dx + 1; }
		}

		public bool HasNext
		{
			get { return i <= dx; }
		}

		public Vector2 Next()
		{
			Vector2 pt = new Vector2();
			if (steep != 0)
			{
				pt.x = x0;
				pt.y = y0;
			}
			else
			{
				pt.x = y0;
				pt.y = x0;
			}

			if (i == dx)
				i++;
			else
			{
				while (e >= 0)
				{
					y0 += sy;
					e -= (dx << 1);
				}
				x0 += sx;
				e += (dy << 1);
				i++;
			}
			return pt;
		}

		public void Skip(int cnt)
		{
			for (int j = 0; j < cnt && i < dx; i++, j++)
			{
				while (e >= 0)
				{
					y0 += sy;
					e -= (dx << 1);
				}
				x0 += sx;
				e += (dy << 1);
			}
		}
	}
}
