﻿/********************************************************************
	filename: 	WebClient.cs
	created:	2014-07-21 23:56:19
	author:		Moncat
*********************************************************************/
using System;
using System.IO;
using System.Text;
using System.Collections;
using System.Net;
using System.Threading;

namespace FairyGame
{
	public enum WebClientState
	{
		Idle = 0,
		Sending,
		Reciving,
		Finished,
		Error
	}

	/// <summary>
	/// web客户端
	/// </summary>
	public class WebClient : IDisposable
	{
		private const int DEFAULT_BUF_SIZE = 1024 * 4; //4k

		#region Fields

		private string _url;
		private WebRequest _req;
		private WebResponse _resp;

		protected long _size = -1;
		protected long _recived = 0;
		protected string _error = null;
		protected WebClientState _state = WebClientState.Idle;
		protected byte[] _buf = new byte[DEFAULT_BUF_SIZE];

		private Stream _stream; //reponse stream
		private int _outType = 0; // 0-bytes, 1-stream, 2-file
		private Stream _saveStream; //output stream
		private string _saveFile;
		private bool _isDisposed = false;

		#endregion

		#region Properties

		/// <summary>
		/// 下载的url
		/// </summary>
		public string url { get { return _url; } }

		/// <summary>
		/// 内部WebRequest对象
		/// </summary>
		public WebRequest request { get { return _req; } }

		/// <summary>
		/// 内容WebResponse对象
		/// </summary>
		public WebResponse response { get { return _resp; } }

		/// <summary>
		/// 缓冲区大小
		/// </summary>
		public int bufSize
		{
			get { return _buf.Length; }
			set
			{
				int newSize = value;
				if (newSize <= 0)
					throw new ArgumentException("Buffer size must be positive!");
				_buf = new byte[newSize];
			}
		}

		/// <summary>
		/// 发送或下载内容总字节大小
		/// </summary>
		public long size { get { return _size; } }

		/// <summary>
		/// 已发送或下载数据字节大小
		/// </summary>
		public long recived { get { return _recived; } }

		/// <summary>
		/// 下载进度（0.0~1.0）
		/// </summary>
		public float progress
		{
			get
			{
				if (_size <= 0)
					return 0f;
				return (float)_recived / (float)_size;
			}
		}

		/// <summary>
		/// 输出类型，0-自由获取数据，1-输出到数据流，2-输出到文件
		/// </summary>
		public int outType { get { return _outType; } }

		/// <summary>
		/// 当前状态
		/// </summary>
		public WebClientState state { get { return _state; } }

		/// <summary>
		/// 指示是否已结束下载（一般用于异步模式）
		/// </summary>
		public bool isDone { get { return (_state == WebClientState.Finished || _state == WebClientState.Error); } }

		/// <summary>
		/// 不为空表示出现错误
		/// </summary>
		public string error { get { return _error; } }

		/// <summary>
		/// 下载的数据（一般用于异步模式）
		/// </summary>
		public byte[] bytes
		{
			get
			{
				if (_outType == 0 && !_isDisposed)
					return ((MemoryStream)_saveStream).ToArray();
				return null;
			}
		}

		/// <summary>
		/// 下载的文本内容（一般用于异步模式）
		/// </summary>
		public string text { get { return GetText(); } }

		/// <summary>
		/// 获得下载的文本内容（一般用于异步模式）
		/// </summary>
		public virtual string GetText(string encoding = null)
		{
			if (_outType != 0 || _isDisposed)
				return null;

			Encoding enc = string.IsNullOrEmpty(encoding) ? Encoding.ASCII : Encoding.GetEncoding(encoding);
			return enc.GetString(bytes);
		}

		#endregion

		#region Constructors

		/// <summary>
		/// 构造器
		/// </summary>
		/// <param name="url">请求url</param>
		/// <param name="headers">请求header数据</param>
		public WebClient(string url, IDictionary headers = null)
		{
			SetUrl(url);
			if (headers != null)
				SetHeaders(headers);
		}

		#endregion

		#region Async Interfaces

		private void StartGetResponse()
		{
			_req.BeginGetResponse(OnAsyncGetResponse, this); //网络连不上时，为什么此方法会阻塞？
		}

		/// <summary>
		/// 异步获取数据
		/// </summary>
		public void AsyncGet()
		{
			_outType = 0;
			_state = WebClientState.Sending;
			_saveStream = new MemoryStream();
            //_req.BeginGetResponse(OnAsyncGetResponse, this);
#if !UNITY_WSA || UNITY_EDITOR
            new Thread(StartGetResponse).Start();
#endif
        }

		/// <summary>
		/// 异步请求资源并保存到流
		/// </summary>
		/// <param name="saveStream">输出流</param>
		public void AsyncToStream(Stream saveStream)
		{
			_outType = 1;
			_state = WebClientState.Sending;
			_saveStream = saveStream;
            //_req.BeginGetResponse(OnAsyncGetResponse, this);
#if !UNITY_WSA || UNITY_EDITOR
            new Thread(StartGetResponse).Start();
#endif
        }

        /// <summary>
        /// 异步请求资源并保存到文件
        /// </summary>
        /// <param name="saveFile">输出文件名</param>
        public void AsyncToFile(string saveFile)
		{
			_saveFile = saveFile;
			_outType = 2;
			_state = WebClientState.Sending;
			_saveStream = File.Open(saveFile, FileMode.Create, FileAccess.Write, FileShare.None);
            //_req.BeginGetResponse(OnAsyncGetResponse, this);
#if !UNITY_WSA || UNITY_EDITOR
            new Thread(StartGetResponse).Start();
#endif
        }

        private void StartGetRequestStream(object state)
		{
			_req.BeginGetRequestStream(OnAsyncGetRequestStream, state); //网络连不上时，为什么此方法会阻塞？
		}

		/// <summary>
		/// 异步请求提交数据并获取数据
		/// </summary>
		/// <param name="postData">提交数据</param>
		public void AsyncPost(byte[] postData)
		{
			_outType = 0;
			_state = WebClientState.Sending;
			_saveStream = new MemoryStream();
            //_req.BeginGetRequestStream(OnAsyncGetRequestStream, postData);
#if !UNITY_WSA || UNITY_EDITOR
            new Thread(StartGetRequestStream).Start(postData);
#endif
        }

        /// <summary>
        /// 异步请求提交数据并获取数据
        /// </summary>
        /// <param name="dataStream">输入数据流</param>
        public void AsyncPost(Stream dataStream)
		{
			_outType = 0;
			_state = WebClientState.Sending;
			_saveStream = new MemoryStream();
            //_req.BeginGetRequestStream(OnAsyncGetRequestStream, dataStream);
#if !UNITY_WSA || UNITY_EDITOR
            new Thread(StartGetRequestStream).Start(dataStream);
#endif
        }

            /// <summary>
            /// 异步请求提交数据并保存到流
            /// </summary>
            /// <param name="postData">提交数据</param>
            /// <param name="saveStream">输出流</param>
        public void AsyncPostAndToStream(byte[] postData, Stream saveStream)
		{
			_outType = 1;
			_state = WebClientState.Sending;
			_saveStream = saveStream;
            //_req.BeginGetRequestStream(OnAsyncGetRequestStream, postData);
#if !UNITY_WSA || UNITY_EDITOR
            new Thread(StartGetRequestStream).Start(postData);
#endif
        }

        /// <summary>
        /// 异步请求提交数据并保存到流
        /// </summary>
        /// <param name="dataStream">输入数据流</param>
        /// <param name="saveStream">输出流</param>
        public void AsyncPostAndToStream(Stream dataStream, Stream saveStream)
		{
			_outType = 1;
			_state = WebClientState.Sending;
			_saveStream = saveStream;
            //_req.BeginGetRequestStream(OnAsyncGetRequestStream, dataStream);
#if !UNITY_WSA || UNITY_EDITOR
            new Thread(StartGetRequestStream).Start(dataStream);
#endif
        }

        /// <summary>
        /// 异步请求提交数据并保存到文件
        /// </summary>
        /// <param name="postData">提交数据</param>
        /// <param name="saveFile">输出文件名</param>
        public void AsyncPostAndToFile(byte[] postData, string saveFile)
		{
			_saveFile = saveFile;
			_outType = 2;
			_state = WebClientState.Sending;
			_saveStream = File.Open(saveFile, FileMode.Create, FileAccess.Write, FileShare.None);
            //_req.BeginGetRequestStream(OnAsyncGetRequestStream, postData);
#if !UNITY_WSA || UNITY_EDITOR
            new Thread(StartGetRequestStream).Start(postData);
#endif
        }

        /// <summary>
        /// 异步请求提交数据并保存到文件
        /// </summary>
        /// <param name="dataStream">输入数据流</param>
        /// <param name="saveFile">输出文件名</param>
        public void AsyncPostAndToFile(Stream dataStream, string saveFile)
		{
			_saveFile = saveFile;
			_outType = 2;
			_state = WebClientState.Sending;
			_saveStream = File.Open(saveFile, FileMode.Create, FileAccess.Write, FileShare.None);
            //_req.BeginGetRequestStream(OnAsyncGetRequestStream, dataStream);
#if !UNITY_WSA || UNITY_EDITOR
            new Thread(StartGetRequestStream).Start(dataStream);
#endif
        }

        #endregion

        #region Async Impls

        private void OnAsyncGetRequestStream(IAsyncResult result)
		{
			if (isDone)
				return;

			try
			{
				Stream os = _req.EndGetRequestStream(result);

				object state = result.AsyncState;
				if (state is Stream)
				{
					Stream stream = (Stream)state;
					_size = -1;
					try
					{
						_size = stream.Length;
					}
					catch
					{
						_size = -1;
					}
					_recived = 0;
					do
					{
						int tmp = stream.Read(_buf, 0, _buf.Length);
						if (tmp > 0)
						{
							os.Write(_buf, 0, tmp);
							_recived += tmp;
						}
						else
							break;
					}
					while (true);
				}
				else
				{
					byte[] data = (byte[])state;
					_size = data.Length;
					_recived = 0;
					while (_recived < _size)
					{
						int bs = (int)Math.Min(_buf.Length, _size - _recived);
						os.Write(data, (int)_recived, bs);
						_recived += bs;
					}
				}
			}
			catch (Exception e)
			{
				_error = "Send request data error! [" + e.Message + "]";
				_state = WebClientState.Error;
				return;
			}

			try
			{
				_req.BeginGetResponse(OnAsyncGetResponse, this);
			}
			catch (Exception e)
			{
				_error = "Get response error! [" + e.Message + "]";
				_state = WebClientState.Error;
			}
		}

		private void OnAsyncGetResponse(IAsyncResult result)
		{
			if (isDone)
				return;

			try
			{
				_resp = _req.EndGetResponse(result);
				_size = _resp.ContentLength;
				_recived = 0;
				_state = WebClientState.Reciving;
			}
			catch (Exception e)
			{
				_error = "Get response error! [" + e.Message + "]";
				_state = WebClientState.Error;
				return;
			}

			OnAsyncGetResponse();
		}

		protected virtual void OnAsyncGetResponse()
		{
			try
			{
				_stream = _resp.GetResponseStream();

                _stream.BeginRead(_buf, 0, _buf.Length, OnAsyncReciveData, this);
			}
			catch (Exception e)
			{
				_error = "Get response input stream error! [" + e.Message + "]";
				_state = WebClientState.Error;
			}
		}

		private void OnAsyncReciveData(IAsyncResult result)
		{
			if (isDone)
				return;

			try
			{
				int len = _stream.EndRead(result);
				if (len > 0)
				{
					_recived += len;

					try
					{
						_saveStream.Write(_buf, 0, len);
					}
					catch (Exception e)
					{
						_error = "Write data error! [" + e.Message + "]";
						_state = WebClientState.Error;
						return;
					}
#if !UNITY_WSA || UNITY_EDITOR

                    _stream.BeginRead(_buf, 0, _buf.Length, OnAsyncReciveData, this);
#endif
                }
				else
				{
#if !UNITY_WSA || UNITY_EDITOR

                    _stream.Close();
#endif
                    _saveStream.Flush();
					if (_outType != 1)
						_saveStream.Close();
					_state = WebClientState.Finished;
				}
			}
			catch (Exception e)
			{
				_error = "Recive data error! [" + e.Message + "]";
				_state = WebClientState.Error;
			}
		}

#endregion

#region Sync Interfaces

		/// <summary>
		/// 请求获得WebResponse对象
		/// </summary>
		/// <returns></returns>
		public WebResponse GetResponse()
		{
			_size = 0;
			_recived = 0;
			_state = WebClientState.Sending;
			_resp = _req.GetResponse();

			_size = _resp.ContentLength;
			_state = WebClientState.Reciving;
			return _resp;
		}

		/// <summary>
		/// 同步请求Post提交数据
		/// </summary>
		/// <param name="postData">提交数据</param>
		public virtual void PostData(byte[] postData)
		{
			_size = postData.Length;
			_recived = 0;
			_state = WebClientState.Sending;
			Stream os = _req.GetRequestStream();
			while (_recived < _size)
			{
				int bs = (int)Math.Min(_buf.Length, _size - _recived);
				os.Write(postData, (int)_recived, bs);
				_recived += bs;
			}
		}

		/// <summary>
		/// 同步请求Post提交数据
		/// </summary>
		/// <param name="dataStream">输入数据流</param>
		public virtual void PostStream(Stream dataStream)
		{
			Stream os = _req.GetRequestStream();
			_size = -1;
			try
			{
				_size = dataStream.Length;
			}
			catch
			{
				_size = -1;
			}
			_recived = 0;
			do
			{
				int tmp = dataStream.Read(_buf, 0, _buf.Length);
				if (tmp > 0)
				{
					os.Write(_buf, 0, tmp);
					_recived += tmp;
				}
				else
					break;
			}
			while (true);
		}

		protected void CopyStream(Stream from, Stream to)
		{
			do
			{
				int tmp = from.Read(_buf, 0, _buf.Length);
				if (tmp > 0)
				{
					to.Write(_buf, 0, tmp);
					_recived += tmp;
				}
				else
					break;
			}
			while (true);
			to.Flush();
		}

		/// <summary>
		/// 同步获取数据
		/// </summary>
		public virtual byte[] GetBytes()
		{
			_outType = 0;
			_saveStream = new MemoryStream();

			if (_resp == null)
				GetResponse();

			_stream = _resp.GetResponseStream();
			CopyStream(_stream, _saveStream);
			_saveStream.Close();
			_state = WebClientState.Finished;
			return bytes;
		}

		/// <summary>
		/// 同步获取字符串数据
		/// </summary>
		public string GetString(string encoding = null)
		{
			GetBytes();
			return GetText(encoding);
		}

		/// <summary>
		/// 同步保存数据到流
		/// </summary>
		public virtual void ToStream(Stream saveStream)
		{
			_outType = 1;
			_saveStream = saveStream;

			if (_resp == null)
				GetResponse();

			_stream = _resp.GetResponseStream();
			CopyStream(_stream, _saveStream);
			_state = WebClientState.Finished;
		}

		/// <summary>
		/// 同步保存数据到文件
		/// </summary>
		public virtual void ToFile(string saveFile)
		{
			_saveFile = saveFile;
			_outType = 2;
			_saveStream = File.Open(saveFile, FileMode.Create, FileAccess.Write, FileShare.None);

			if (_resp == null)
				GetResponse();

			_stream = _resp.GetResponseStream();
			CopyStream(_stream, _saveStream);
			_saveStream.Close();
			_state = WebClientState.Finished;
		}

#endregion

		protected void SetUrl(string url)
		{
			_url = url;
			_req = WebRequest.Create(url);
		}

		/// <summary>
		/// 设置请求header数据
		/// </summary>
		/// <param name="headers">请求header数据</param>
		public void SetHeaders(IDictionary headers)
		{
			SetHeaders(_req, headers);
		}

		/// <summary>
		/// 关闭对象，释放资源
		/// </summary>
		public virtual void Dispose()
		{
			if (_isDisposed)
				return;

			if (_req != null)
			{
				try
				{
					_req.Abort();
				}
				catch
				{ }
			}

			if (_stream != null)
				_stream.Close();

			if (_resp != null)
			{
				try
				{
					_resp.Close();
				}
				catch
				{ }
			}

			if (_outType == 2)
			{
				_saveStream.Close();
				if (!string.IsNullOrEmpty(_error)) // error
				{
					try
					{
						File.Delete(_saveFile);
					}
					catch
					{ }
				}
			}

			_state = WebClientState.Finished;
			_error = "Closed";

			_req = null;
			_resp = null;
			_stream = null;
			_saveStream = null;
			_buf = null;
			_isDisposed = true;
		}

		// inner methods

		// static method

		protected static void SetHeaders(WebRequest req, IDictionary headers)
		{
			foreach (DictionaryEntry de in headers)
				req.Headers.Add("" + de.Key, "" + de.Value);
		}

	}
}
