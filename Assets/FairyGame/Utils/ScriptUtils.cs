﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using FairyGUI;
using FairyGUI.Utils;

namespace FairyGame
{
	public static class ScriptUtils
	{
		public static int Int(object o)
		{
			return Convert.ToInt32(o);
		}

		public static float Float(object o)
		{
			return (float)Math.Round(Convert.ToSingle(o), 2);
		}

		public static long Long(object o)
		{
			return Convert.ToInt64(o);
		}

		public static int RandomInt(int min, int max)
		{
			return UnityEngine.Random.Range(min, max);
		}

		public static float RandomFloat(float min, float max)
		{
			return UnityEngine.Random.Range(min, max);
		}

		public static Vector2 RandomInsideUnitCircle()
		{
			return UnityEngine.Random.insideUnitCircle;
		}

		public static bool BitAnd(int s, int b)
		{
			return (s & b) != 0;
		}

		public static long GetTime()
		{
			TimeSpan ts = new TimeSpan(DateTime.UtcNow.Ticks - new DateTime(1970, 1, 1, 0, 0, 0).Ticks);
			return (long)ts.TotalMilliseconds;
		}

		public static String GetUtf8String(byte[] data)
		{
			return encodingUTF8.GetString(data);
		}

		/// <summary>
		/// Base64编码
		/// </summary>
		public static string Encode(string message)
		{
			byte[] bytes = Encoding.GetEncoding("utf-8").GetBytes(message);
			return Convert.ToBase64String(bytes);
		}

		/// <summary>
		/// Base64解码
		/// </summary>
		public static string Decode(string message)
		{
			byte[] bytes = Convert.FromBase64String(message);
			return Encoding.GetEncoding("utf-8").GetString(bytes);
		}

		/// <summary>
		/// 判断数字
		/// </summary>
		public static bool IsNumeric(string str)
		{
			if (str == null || str.Length == 0) return false;
			for (int i = 0; i < str.Length; i++)
			{
				if (!Char.IsNumber(str[i])) { return false; }
			}
			return true;
		}

		/// <summary>
		/// HashToMD5Hex
		/// </summary>
		public static string HashToMD5Hex(string sourceStr)
		{
			byte[] Bytes = encodingUTF8.GetBytes(sourceStr);
			using (MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider())
			{
				byte[] result = md5.ComputeHash(Bytes);
				StringBuilder builder = new StringBuilder();
				for (int i = 0; i < result.Length; i++)
					builder.Append(result[i].ToString("x2"));
				return builder.ToString();
			}
		}

		static UTF8Encoding encodingUTF8 = new UTF8Encoding();
		public static string md5(string source)
		{
			MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
			byte[] data = encodingUTF8.GetBytes(source);
			byte[] md5Data = md5.ComputeHash(data, 0, data.Length);
			md5.Clear();

			string destString = "";
			for (int i = 0; i < md5Data.Length; i++)
			{
				destString += System.Convert.ToString(md5Data[i], 16).PadLeft(2, '0');
			}
			destString = destString.PadLeft(32, '0');
			return destString;
		}

		/// <summary>
		/// 计算文件的MD5值
		/// </summary>
		public static string md5file(string file)
		{
			try
			{
                byte[] retVal;
                using (FileStream fs = new FileStream(file, FileMode.Open))
                {
                    System.Security.Cryptography.MD5 md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
                    retVal = md5.ComputeHash(fs);
                }

				StringBuilder sb = new StringBuilder();
				for (int i = 0; i < retVal.Length; i++)
				{
					sb.Append(retVal[i].ToString("x2"));
				}
				return sb.ToString();
			}
			catch (Exception ex)
			{
				throw new Exception("md5file() fail, error:" + ex.Message);
			}
		}

		/// <summary>
		/// 清理内存
		/// </summary>
		public static void ClearMemory()
		{
			GC.Collect();
			Resources.UnloadUnusedAssets();
		}

		/// <summary>
		/// 是否为数字
		/// </summary>
		public static bool IsNumber(string strNumber)
		{
			Regex regex = new Regex("[^0-9]");
			return !regex.IsMatch(strNumber);
		}

		/// <summary>
		/// 网络可用
		/// </summary>
		public static bool NetAvailable
		{
			get
			{
				return Application.internetReachability != NetworkReachability.NotReachable;
			}
		}

		/// <summary>
		/// 是否是无线
		/// </summary>
		public static bool IsWifi
		{
			get
			{
				return Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork;
			}
		}

		public static byte[] Decompress(byte[] data)
		{
			ByteArray ba = new ByteArray(data);
			ba.Decompress();
			return ba.data;
		}

		public static string DecryptString(string source)
		{
			try
			{
				return MiscUtil.DecryptValue(source);
			}
			catch (Exception)
			{
				return null;
			}
		}

		public static string EncryptString(string source)
		{
			return MiscUtil.EncryptValue(source);
		}

		public static string ParseUBB(string source)
		{
			return UBBParser.inst.Parse(source);
		}

		public static byte[] LoadResource(string path)
		{
			return Resources.Load<TextAsset>(path).bytes;
		}

		public static GLoader GetHtmlImage(GRichTextField textField, int index)
		{
			return ((HtmlImage)textField.richTextField.GetHtmlElementAt(index).htmlObject).loader;
		}
	}
}
