﻿/********************************************************************
	filename: 	Buffer.cs
	created:	2014/06/17 15:28
	author:		Moncat
*********************************************************************/

using System;

namespace FairyGame
{
	/// <summary>
	/// 数据缓冲区
	/// 1. 可以获取当前可用缓冲区大小和当前实际数据大小，可以截断后面或移除前面指定长度数据，可以清空数据。
	/// 2. 可以追加数据到后面，支持Buffer,byte[],byte,bool,short,ushort,int,uint,long,ulong,float,double。
	/// 3. 可以设置数据到可用缓冲区内的指定位置，支持Buffer,byte[],byte,bool,short,ushort,int,uint,long,ulong,float,double。
	/// 4. 可以弹出前面的数据，同时支持Buffer,byte[],byte,bool,short,ushort,int,uint,long,ulong,float,double,line。
	/// 5. 可以获取有效数据范围内的指定位置的数据，同时支持Buffer,byte[],byte,bool,short,ushort,int,uint,long,ulong,float,double,line。
	/// 6. 自动按需扩展缓冲区和自动释放冗余缓冲区。
	/// </summary>
	public abstract class Buffer
	{
		private const byte NEW_LINE_BYTE = (byte)'\n';

		private byte[] _byteBuf = new byte[8];
		private bool _littleEndian;
		private bool _isSystemEndian;

		public Buffer(bool littleEndian = true)
		{
			_littleEndian = littleEndian;
			_isSystemEndian = (BitConverter.IsLittleEndian == _littleEndian);
		}

		/// <summary>
		/// 是否LITTLE_ENDIAN字节顺序
		/// </summary>
		public bool isLittleEndian
		{
			get { return _littleEndian; }
			set
			{
				_littleEndian = value;
				_isSystemEndian = (BitConverter.IsLittleEndian == _littleEndian);
			}
		}

		/// <summary>
		/// 是否与本地主机字节相符
		/// </summary>
		public bool isSystemEndian { get { return _isSystemEndian; } }

		/// <summary>
		/// 当前是否为网络字节顺序
		/// </summary>
		public bool isNetOrder
		{
			get { return !_littleEndian; }
		}

		/// <summary>
		/// 当前缓冲区大小
		/// </summary>
		public abstract int capacity { get; }

		/// <summary>
		/// 当前数据大小
		/// </summary>
		public abstract int length { get; }

		/// <summary>
		/// 移除前面指定长度数据
		/// </summary>
		/// <param name="len"></param>
		public abstract void Remove(int len);

		/// <summary>
		/// 截断后面指定长度数据
		/// </summary>
		/// <param name="len"></param>
		public abstract void Truncate(int len);

		/// <summary>
		/// 整理缓冲区，是缓冲区大小与数据大小相同
		/// </summary>
		public abstract void TrimBuffer();

		/// <summary>
		/// 清空数据
		/// </summary>
		public abstract void Clear();

		/// <summary>
		/// 设置一个字节到缓冲区指定位置
		/// </summary>
		/// <param name="pos">位置</param>
		/// <param name="value">字节值</param>
		public void SetByte(int pos, byte value)
		{
			_byteBuf[0] = value;
			Set(pos, _byteBuf, 0, 1);
		}

		/// <summary>
		/// 设置一个逻辑值到缓冲区指定位置
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="value"></param>
		public void SetBool(int pos, bool value)
		{
			Set(pos, BitConverter.GetBytes(value));
		}

		/// <summary>
		/// 设置一个字符到缓冲区指定位置
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="value"></param>
		public void SetChar(int pos, char value)
		{
			byte[] bb = BitConverter.GetBytes(value);
			if (!_isSystemEndian)
				Array.Reverse(bb);
			Set(pos, bb, 0, bb.Length);
		}

		/// <summary>
		/// 设置一个短整数到缓冲区指定位置
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="value"></param>
		public void SetShort(int pos, short value)
		{
			byte[] bb = BitConverter.GetBytes(value);
			if (!_isSystemEndian)
				Array.Reverse(bb);
			Set(pos, bb, 0, bb.Length);
		}

		/// <summary>
		/// 设置一个整数到缓冲区指定位置
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="value"></param>
		public void SetInt(int pos, int value)
		{
			byte[] bb = BitConverter.GetBytes(value);
			if (!_isSystemEndian)
				Array.Reverse(bb);
			Set(pos, bb, 0, bb.Length);
		}

		/// <summary>
		/// 设置一个长整数到缓冲区指定位置
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="value"></param>
		public void SetLong(int pos, long value)
		{
			byte[] bb = BitConverter.GetBytes(value);
			if (!_isSystemEndian)
				Array.Reverse(bb);
			Set(pos, bb, 0, bb.Length);
		}

		/// <summary>
		/// 设置一个无符号短整数到缓冲区指定位置
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="value"></param>
		public void SetUShort(int pos, ushort value)
		{
			byte[] bb = BitConverter.GetBytes(value);
			if (!_isSystemEndian)
				Array.Reverse(bb);
			Set(pos, bb, 0, bb.Length);
		}

		/// <summary>
		/// 设置一个无符号整数到缓冲区指定位置
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="value"></param>
		public void SetUInt(int pos, uint value)
		{
			byte[] bb = BitConverter.GetBytes(value);
			if (!_isSystemEndian)
				Array.Reverse(bb);
			Set(pos, bb, 0, bb.Length);
		}

		/// <summary>
		/// 设置一个无符号长整数到缓冲区指定位置
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="value"></param>
		public void SetULong(int pos, ulong value)
		{
			byte[] bb = BitConverter.GetBytes(value);
			if (!_isSystemEndian)
				Array.Reverse(bb);
			Set(pos, bb, 0, bb.Length);
		}

		/// <summary>
		/// 设置一个浮点数到缓冲区指定位置
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="value"></param>
		public void SetFloat(int pos, float value)
		{
			byte[] bb = BitConverter.GetBytes(value);
			if (!_isSystemEndian)
				Array.Reverse(bb);
			Set(pos, bb, 0, bb.Length);
		}

		/// <summary>
		/// 设置一个双精浮点数到缓冲区指定位置
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="value"></param>
		public void SetDouble(int pos, double value)
		{
			byte[] bb = BitConverter.GetBytes(value);
			if (!_isSystemEndian)
				Array.Reverse(bb);
			Set(pos, bb, 0, bb.Length);
		}

		/// <summary>
		/// 设置数据到缓冲区指定位置
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="data"></param>
		public void Set(int pos, byte[] data)
		{
			Set(pos, data, 0, data.Length);
		}

		/// <summary>
		/// 设置数据到缓冲区指定位置
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="data"></param>
		/// <param name="offset"></param>
		/// <param name="len"></param>
		public abstract void Set(int pos, byte[] data, int offset, int len);

		/// <summary>
		/// 设置数据到缓冲区指定位置
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="bf"></param>
		public abstract void Set(int pos, Buffer bf);

		/// <summary>
		/// 从缓冲区指定位置获得一个字节
		/// </summary>
		/// <param name="pos"></param>
		/// <returns></returns>
		public byte GetByte(int pos)
		{
			Get(pos, _byteBuf, 0, 1);
			return _byteBuf[0];
		}

		/// <summary>
		/// 从缓冲区指定位置获得一个逻辑值
		/// </summary>
		/// <param name="pos"></param>
		/// <returns></returns>
		public bool GetBool(int pos)
		{
			Get(pos, _byteBuf, 0, 1);
			return BitConverter.ToBoolean(_byteBuf, 0);
		}

		/// <summary>
		/// 从缓冲区指定位置获得一个字符
		/// </summary>
		/// <param name="pos"></param>
		/// <returns></returns>
		public char GetChar(int pos)
		{
			Get(pos, _byteBuf, 0, 2);
			if (!_isSystemEndian)
				Array.Reverse(_byteBuf, 0, 2);
			return BitConverter.ToChar(_byteBuf, 0);
		}

		/// <summary>
		/// 从缓冲区指定位置获得一个短整数
		/// </summary>
		/// <param name="pos"></param>
		/// <returns></returns>
		public short GetShort(int pos)
		{
			Get(pos, _byteBuf, 0, 2);
			if (!_isSystemEndian)
				Array.Reverse(_byteBuf, 0, 2);
			return BitConverter.ToInt16(_byteBuf, 0);
		}

		/// <summary>
		/// 从缓冲区指定位置获得一个整数
		/// </summary>
		/// <param name="pos"></param>
		/// <returns></returns>
		public int GetInt(int pos)
		{
			Get(pos, _byteBuf, 0, 4);
			if (!_isSystemEndian)
				Array.Reverse(_byteBuf, 0, 4);
			return BitConverter.ToInt32(_byteBuf, 0);
		}

		/// <summary>
		/// 从缓冲区指定位置获得一个长整数
		/// </summary>
		/// <param name="pos"></param>
		/// <returns></returns>
		public long GetLong(int pos)
		{
			Get(pos, _byteBuf, 0, 8);
			if (!_isSystemEndian)
				Array.Reverse(_byteBuf, 0, 8);
			return BitConverter.ToInt64(_byteBuf, 0);
		}

		/// <summary>
		/// 从缓冲区指定位置获得一个短整数
		/// </summary>
		/// <param name="pos"></param>
		/// <returns></returns>
		public ushort GetUShort(int pos)
		{
			Get(pos, _byteBuf, 0, 2);
			if (!_isSystemEndian)
				Array.Reverse(_byteBuf, 0, 2);
			return BitConverter.ToUInt16(_byteBuf, 0);
		}

		/// <summary>
		/// 从缓冲区指定位置获得一个整数
		/// </summary>
		/// <param name="pos"></param>
		/// <returns></returns>
		public uint GetUInt(int pos)
		{
			Get(pos, _byteBuf, 0, 4);
			if (!_isSystemEndian)
				Array.Reverse(_byteBuf, 0, 4);
			return BitConverter.ToUInt32(_byteBuf, 0);
		}

		/// <summary>
		/// 从缓冲区指定位置获得一个长整数
		/// </summary>
		/// <param name="pos"></param>
		/// <returns></returns>
		public ulong GetULong(int pos)
		{
			Get(pos, _byteBuf, 0, 8);
			if (!_isSystemEndian)
				Array.Reverse(_byteBuf, 0, 8);
			return BitConverter.ToUInt64(_byteBuf, 0);
		}

		/// <summary>
		/// 从缓冲区指定位置获得一个浮点数
		/// </summary>
		/// <param name="pos"></param>
		/// <returns></returns>
		public float GetFloat(int pos)
		{
			Get(pos, _byteBuf, 0, 4);
			if (!_isSystemEndian)
				Array.Reverse(_byteBuf, 0, 4);
			return BitConverter.ToSingle(_byteBuf, 0);
		}

		/// <summary>
		/// 从缓冲区指定位置获得一个双精浮点数
		/// </summary>
		/// <param name="pos"></param>
		/// <returns></returns>
		public double GetDouble(int pos)
		{
			Get(pos, _byteBuf, 0, 8);
			if (!_isSystemEndian)
				Array.Reverse(_byteBuf, 0, 8);
			return BitConverter.ToDouble(_byteBuf, 0);
		}

		/// <summary>
		/// 从缓冲区获得所有数据
		/// </summary>
		/// <returns></returns>
		public byte[] GetAll()
		{
			return Get(0, length);
		}

		/// <summary>
		/// 从缓冲区指定位置获得指定长度的数据
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="len"></param>
		/// <returns></returns>
		public byte[] Get(int pos, int len)
		{
			if (len < 0)
				throw new ArgumentException();
			if (pos < 0 || pos + len > length)
				throw new IndexOutOfRangeException();
			byte[] buf = new byte[len];
			Get(pos, buf);
			return buf;
		}

		/// <summary>
		/// 从缓冲区指定位置获得数据
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="buf"></param>
		public void Get(int pos, byte[] buf)
		{
			Get(pos, buf, 0, buf.Length);
		}

		/// <summary>
		/// 从缓冲区指定位置获得数据
		/// </summary>
		/// <param name="pos"></param>
		/// <param name="buf"></param>
		/// <param name="offset"></param>
		/// <param name="len"></param>
		public abstract void Get(int pos, byte[] buf, int offset, int len);

		/// <summary>
		/// 向缓冲区追加一个字节
		/// </summary>
		/// <param name="value"></param>
		public void AppendByte(byte value)
		{
			_byteBuf[0] = value;
			Append(_byteBuf, 0, 1);
		}

		/// <summary>
		/// 向缓冲区追加一个逻辑值
		/// </summary>
		/// <param name="value"></param>
		public void AppendBool(bool value)
		{
			Append(BitConverter.GetBytes(value));
		}

		/// <summary>
		/// 向缓冲区追加一个字符
		/// </summary>
		/// <param name="value"></param>
		public void AppendChar(char value)
		{
			byte[] bb = BitConverter.GetBytes(value);
			if (!_isSystemEndian)
				Array.Reverse(bb);
			Append(bb, 0, bb.Length);
		}

		/// <summary>
		/// 向缓冲区追加一个短整数
		/// </summary>
		/// <param name="value"></param>
		public void AppendShort(short value)
		{
			byte[] bb = BitConverter.GetBytes(value);
			if (!_isSystemEndian)
				Array.Reverse(bb);
			Append(bb, 0, bb.Length);
		}

		/// <summary>
		/// 向缓冲区追加一个整数
		/// </summary>
		/// <param name="value"></param>
		public void AppendInt(int value)
		{
			byte[] bb = BitConverter.GetBytes(value);
			if (!_isSystemEndian)
				Array.Reverse(bb);
			Append(bb, 0, bb.Length);
		}

		/// <summary>
		/// 向缓冲区追加一个长整数
		/// </summary>
		/// <param name="value"></param>
		public void AppendLong(long value)
		{
			byte[] bb = BitConverter.GetBytes(value);
			if (!_isSystemEndian)
				Array.Reverse(bb);
			Append(bb, 0, bb.Length);
		}

		/// <summary>
		/// 向缓冲区追加一个无符号短整数
		/// </summary>
		/// <param name="value"></param>
		public void AppendUShort(ushort value)
		{
			byte[] bb = BitConverter.GetBytes(value);
			if (!_isSystemEndian)
				Array.Reverse(bb);
			Append(bb, 0, bb.Length);
		}

		/// <summary>
		/// 向缓冲区追加一个无符号整数
		/// </summary>
		/// <param name="value"></param>
		public void AppendUInt(uint value)
		{
			byte[] bb = BitConverter.GetBytes(value);
			if (!_isSystemEndian)
				Array.Reverse(bb);
			Append(bb, 0, bb.Length);
		}

		/// <summary>
		/// 向缓冲区追加一个无符号长整数
		/// </summary>
		/// <param name="value"></param>
		public void AppendULong(ulong value)
		{
			byte[] bb = BitConverter.GetBytes(value);
			if (!_isSystemEndian)
				Array.Reverse(bb);
			Append(bb, 0, bb.Length);
		}

		/// <summary>
		/// 向缓冲区追加一个浮点数
		/// </summary>
		/// <param name="value"></param>
		public void AppendFloat(float value)
		{
			byte[] bb = BitConverter.GetBytes(value);
			if (!_isSystemEndian)
				Array.Reverse(bb);
			Append(bb, 0, bb.Length);
		}

		/// <summary>
		/// 向缓冲区追加一个双精浮点数
		/// </summary>
		/// <param name="value"></param>
		public void AppendDouble(double value)
		{
			byte[] bb = BitConverter.GetBytes(value);
			if (!_isSystemEndian)
				Array.Reverse(bb);
			Append(bb, 0, bb.Length);
		}

		/// <summary>
		/// 向缓冲区追加数据
		/// </summary>
		/// <param name="data"></param>
		public void Append(byte[] data)
		{
			Append(data, 0, data.Length);
		}

		/// <summary>
		/// 向缓冲区追加数据. 缓冲区不足时，自动扩充。
		/// </summary>
		/// <param name="data">要加入缓冲区的数据</param>
		/// <param name="offset">data的数据位置</param>
		/// <param name="len">添加的数据长度</param>
		public abstract void Append(byte[] data, int offset, int len);

		/// <summary>
		/// 向缓冲区追加数据. 缓冲区不足时，自动扩充。
		/// </summary>
		/// <param name="bf"></param>
		public abstract void Append(Buffer bf);

		/// <summary>
		/// 从缓冲区前面弹出一个字节
		/// </summary>
		/// <returns></returns>
		public byte PopByte()
		{
			Pop(_byteBuf, 0, 1);
			return _byteBuf[0];
		}

		/// <summary>
		/// 从缓冲区前面弹出一个逻辑值
		/// </summary>
		/// <returns></returns>
		public bool PopBool()
		{
			Pop(_byteBuf, 0, 1);
			return BitConverter.ToBoolean(_byteBuf, 0);
		}

		/// <summary>
		/// 从缓冲区前面弹出一个字符
		/// </summary>
		/// <returns></returns>
		public char PopChar()
		{
			Pop(_byteBuf, 0, 2);
			if (!_isSystemEndian)
				Array.Reverse(_byteBuf, 0, 2);
			return BitConverter.ToChar(_byteBuf, 0);
		}

		/// <summary>
		/// 从缓冲区前面弹出一个短整数
		/// </summary>
		/// <returns></returns>
		public short PopShort()
		{
			Pop(_byteBuf, 0, 2);
			if (!_isSystemEndian)
				Array.Reverse(_byteBuf, 0, 2);
			return BitConverter.ToInt16(_byteBuf, 0);
		}

		/// <summary>
		/// 从缓冲区前面弹出一个整数
		/// </summary>
		/// <returns></returns>
		public int PopInt()
		{
			Pop(_byteBuf, 0, 4);
			if (!_isSystemEndian)
				Array.Reverse(_byteBuf, 0, 4);
			return BitConverter.ToInt32(_byteBuf, 0);
		}

		/// <summary>
		/// 从缓冲区前面弹出一个长整数
		/// </summary>
		/// <returns></returns>
		public long PopLong()
		{
			Pop(_byteBuf, 0, 8);
			if (!_isSystemEndian)
				Array.Reverse(_byteBuf, 0, 8);
			return BitConverter.ToInt64(_byteBuf, 0);
		}

		/// <summary>
		/// 从缓冲区前面弹出一个无符号短整数
		/// </summary>
		/// <returns></returns>
		public ushort PopUShort()
		{
			Pop(_byteBuf, 0, 2);
			if (!_isSystemEndian)
				Array.Reverse(_byteBuf, 0, 2);
			return BitConverter.ToUInt16(_byteBuf, 0);
		}

		/// <summary>
		/// 从缓冲区前面弹出一个无符号整数
		/// </summary>
		/// <returns></returns>
		public uint PopUInt()
		{
			Pop(_byteBuf, 0, 4);
			if (!_isSystemEndian)
				Array.Reverse(_byteBuf, 0, 4);
			return BitConverter.ToUInt32(_byteBuf, 0);
		}

		/// <summary>
		/// 从缓冲区前面弹出一个无符号长整数
		/// </summary>
		/// <returns></returns>
		public ulong PopULong()
		{
			Pop(_byteBuf, 0, 8);
			if (!_isSystemEndian)
				Array.Reverse(_byteBuf, 0, 8);
			return BitConverter.ToUInt64(_byteBuf, 0);
		}

		/// <summary>
		/// 从缓冲区前面弹出一个浮点数
		/// </summary>
		/// <returns></returns>
		public float PopFloat()
		{
			Pop(_byteBuf, 0, 4);
			if (!_isSystemEndian)
				Array.Reverse(_byteBuf, 0, 4);
			return BitConverter.ToSingle(_byteBuf, 0);
		}

		/// <summary>
		/// 从缓冲区前面弹出一个双精浮点数
		/// </summary>
		/// <returns></returns>
		public double PopDouble()
		{
			Pop(_byteBuf, 0, 8);
			if (!_isSystemEndian)
				Array.Reverse(_byteBuf, 0, 8);
			return BitConverter.ToDouble(_byteBuf, 0);
		}

		/// <summary>
		/// 从缓冲区前面弹出所有数据
		/// </summary>
		/// <returns></returns>
		public byte[] PopAll()
		{
			return Pop(length);
		}

		/// <summary>
		/// 从缓冲区前面弹出指定长度的数据
		/// </summary>
		/// <param name="len"></param>
		/// <returns></returns>
		public byte[] Pop(int len)
		{
			if (len < 0)
				throw new ArgumentException();
			if (len > length)
				throw new IndexOutOfRangeException();
			byte[] buf = new byte[len];
			Pop(buf);
			return buf;
		}

		/// <summary>
		/// 从缓冲区前面弹出数据
		/// </summary>
		/// <param name="buf"></param>
		public void Pop(byte[] buf)
		{
			Pop(buf, 0, buf.Length);
		}

		/// <summary>
		/// 从缓冲区前面弹出数据. 缓冲区冗余时，自动释放。
		/// </summary>
		/// <param name="buf">数据缓冲区</param>
		/// <param name="offset">数据缓冲存放位置</param>
		/// <param name="len">要弹出的数据长度</param>
		public abstract void Pop(byte[] buf, int offset, int len);

		/// <summary>
		/// 从缓冲区前面弹出一行数据，返回数据中包含回车换行符
		/// </summary>
		/// <returns>足够则返回一行，不足则返回全部</returns>
		public byte[] PopLine()
		{
			return PopLine(true);
		}

		/// <summary>
		/// 从缓冲区前面弹出一行数据，返回数据中包含回车换行符
		/// </summary>
		/// <param name="forAll">为true时，不足一行截取全部数据；为false时，不足一行返回null</param>
		/// <returns>根据forAll的值返回一行数据</returns>
		public byte[] PopLine(bool forAll)
		{
			if (length == 0)
			{
				if (forAll)
					return new byte[0];
				else
					return null;
			}

			int i;
			for (i = 0; i < length; i++)
			{
				if (GetByte(i) == NEW_LINE_BYTE)
					break;
			}

			if (i >= length && !forAll)
				return null;
			byte[] data = new byte[i + 1];
			Pop(data);
			return data;
		}
	}
}
