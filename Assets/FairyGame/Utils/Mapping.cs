﻿/********************************************************************
	filename: 	Mapping.cs
	created:	2014/05/17 16:08
	author:		Moncat
*********************************************************************/

using System;
using System.Collections;
using System.Text.RegularExpressions;

namespace FairyGame
{
	/// <summary>
	/// map对象，Hastable的便捷扩展
	/// </summary>
	public class Mapping : Hashtable
	{
		public Mapping() : base()
		{
		}

		public Mapping(int capacity) : base(capacity)
		{
		}

		public Mapping(IDictionary map) : base(map)
		{
		}

		public Mapping(int capacity, float loadFactor) : base(capacity, loadFactor)
		{
		}

		public Mapping(IDictionary map, float loadFactor) : base(map, loadFactor)
		{
		}

		/// <summary>
		/// 设置值
		/// </summary>
		/// <param name="prop"></param>
		/// <param name="data"></param>
		public void Set(object prop, object data)
		{
			this[prop] = data;
		}

		/// <summary>
		/// 获得对象，为空则返回默认之dv
		/// </summary>
		/// <param name="prop"></param>
		/// <param name="dv"></param>
		/// <returns></returns>
		public object Get(object prop, object dv = null)
		{
			object o = this[prop];
			if (o != null)
				return o;
			return dv;
		}

		/// <summary>
		/// 获得逻辑值，为空或非字符串和逻辑类型则返回默认值dv
		/// </summary>
		/// <param name="prop"></param>
		/// <param name="dv"></param>
		/// <returns></returns>
		public bool GetBool(object prop, bool dv = false)
		{
			return TypeUtil.GetBool(Get(prop), dv);
		}

		/// <summary>
		/// 获得单字节整数值，为空或非数值类型则返回默认值dv
		/// </summary>
		/// <param name="prop"></param>
		/// <param name="dv"></param>
		/// <returns>返回byte值</returns>
		public byte GetByte(object prop, byte dv = (byte)0)
		{
			return TypeUtil.GetByte(Get(prop), dv);
		}

		/// <summary>
		/// 获得字符，为空或非数值类型则返回默认值dv
		/// </summary>
		/// <param name="prop"></param>
		/// <param name="dv"></param>
		/// <returns>返回short值</returns>
		public char GetChar(object prop, char dv = (char)0)
		{
			return TypeUtil.GetChar(Get(prop), dv);
		}

		/// <summary>
		/// 获得短整数值，为空或非数值类型则返回默认值dv
		/// </summary>
		/// <param name="prop"></param>
		/// <param name="dv"></param>
		/// <returns>返回short值</returns>
		public short GetShort(object prop, short dv = (short)0)
		{
			return TypeUtil.GetShort(Get(prop), dv);
		}

		/// <summary>
		/// 获得整数值，为空或非数值类型则返回默认值dv
		/// </summary>
		/// <param name="prop"></param>
		/// <param name="dv"></param>
		/// <returns>返回int值</returns>
		public int GetInt(object prop, int dv = 0)
		{
			return TypeUtil.GetInt(Get(prop), dv);
		}

		/// <summary>
		/// 获得长整数值，为空或非数值类型则返回默认值dv
		/// </summary>
		/// <param name="prop"></param>
		/// <param name="dv"></param>
		/// <returns>返回long值</returns>
		public long GetLong(object prop, long dv = 0)
		{
			return TypeUtil.GetLong(Get(prop), dv);
		}

		/// <summary>
		/// 获得浮点数值，为空或非数值类型则返回默认值dv
		/// </summary>
		/// <param name="prop"></param>
		/// <param name="dv"></param>
		/// <returns>返回float值</returns>
		public float GetFloat(object prop, float dv = 0)
		{
			return TypeUtil.GetFloat(Get(prop), dv);
		}

		/// <summary>
		/// 获得双精浮点数值，为空或非数值类型则返回默认值dv
		/// </summary>
		/// <param name="prop"></param>
		/// <param name="dv"></param>
		/// <returns>返回double值</returns>
		public double GetDouble(object prop, double dv = 0)
		{
			return TypeUtil.GetDouble(Get(prop), dv);
		}

		/// <summary>
		/// 获得字符串，为空则返回默认值dv
		/// </summary>
		/// <param name="prop"></param>
		/// <param name="dv"></param>
		/// <returns>返回字符串</returns>
		public String GetString(object prop, String dv = "")
		{
			return TypeUtil.GetString(Get(prop), dv);
		}

		/// <summary>
		/// 获取时间
		/// </summary>
		/// <param name="prop"></param>
		/// <param name="dv"></param>
		/// <returns></returns>
		public DateTime GetDateTime(object prop, DateTime dv)
		{
			object o = Get(prop);
			if (o is DateTime)
				return (DateTime) o;
			else
				return dv;
		}

		/// <summary>
		/// 获得列表，为空或非List类型则返回默认值dv
		/// </summary>
		/// <param name="prop"></param>
		/// <param name="dv"></param>
		/// <returns>返回列表</returns>
		public IList GetList(object prop, IList dv = null)
		{
			object o = Get(prop);
			if (TypeUtil.IsList(o))
				return (IList)o;
			return dv;
		}

		/// <summary>
		/// 获得影射，为空或非Map类型则返回默认值dv
		/// </summary>
		/// <param name="prop"></param>
		/// <param name="dv"></param>
		/// <returns>返回影射</returns>
		public IDictionary GetMap(object prop, IDictionary dv = null)
		{
			object o = Get(prop);
			if (TypeUtil.IsMap(o))
				return (IDictionary)o;
			return dv;
		}

		//add methods

		/// <summary>
		/// 增加短整数值
		/// </summary>
		/// <param name="prop"></param>
		/// <param name="data"></param>
		/// <returns>返回增加后的short值</returns>
		public short AddShort(object prop, short data)
		{
			data += GetShort(prop);
			this[prop] = data;
			return data;
		}

		/// <summary>
		/// 增加整数值
		/// </summary>
		/// <param name="prop"></param>
		/// <param name="data"></param>
		/// <returns>返回增加后的整数值</returns>
		public int AddInt(object prop, int data)
		{
			data += GetInt(prop);
			this[prop] = data;
			return data;
		}

		/// <summary>
		/// 增加长整数值
		/// </summary>
		/// <param name="prop"></param>
		/// <param name="data"></param>
		/// <returns>返回增加后的长整数值</returns>
		public long AddLong(object prop, long data)
		{
			data += GetLong(prop);
			this[prop] = data;
			return data;
		}

		/// <summary>
		/// 增加浮点数值
		/// </summary>
		/// <param name="prop"></param>
		/// <param name="data"></param>
		/// <returns>返回增加后的float值</returns>
		public float AddFloat(object prop, float data)
		{
			data += GetFloat(prop);
			this[prop] = data;
			return data;
		}

		/// <summary>
		/// 增加双精浮点数值
		/// </summary>
		/// <param name="prop"></param>
		/// <param name="data"></param>
		/// <returns>返回增加后的浮点值</returns>
		public double AddDouble(object prop, double data)
		{
			data += GetDouble(prop);
			this[prop] = data;
			return data;
		}

		/// <summary>
		/// 增加字符串值
		/// </summary>
		/// <param name="prop"></param>
		/// <param name="data"></param>
		/// <returns>返回增加后的字符串</returns>
		public string AddString(object prop, string data)
		{
			data += GetString(prop);
			this[prop] = data;
			return data;
		}

		/// <summary>
		/// 批量删除指定模式的值（限字符串key）
		/// </summary>
		/// <param name="regex"></param>
		public virtual void RemovePattern(string regex)
		{
			Regex reg = new Regex(regex);
			ArrayList ls = new ArrayList();
			foreach(DictionaryEntry de in this)
			{
				Object key = de.Key;
				if (TypeUtil.IsString(key) && reg.IsMatch((String)key))
					ls.Add(key);
			}
			for (int i = 0; i < ls.Count; i++)
				Remove(ls[i]);
		}

		public virtual void AddAll(IDictionary map)
		{
			foreach (DictionaryEntry de in map)
				this[de.Key] = de.Value;
		}
	}
}
