﻿using System;
using System.Collections;
using System.Collections.Generic;
using FairyGUI;
using UnityEngine;

namespace FairyGame
{
	public class FrameData
	{
		public DirData owner;
		public int index; //帧索引，基于0
		
		public Rect rect; //包围
		public Vector2 sheetOffset; //如果使用SpriteSheet，表示在SpriteSheet上的偏移
		public bool rotated;
		public Rect uvRect;

		public FrameData(DirData owner, int index) {
			this.owner = owner;
			this.index = index;
			
			rect = new Rect();
			sheetOffset = new Vector2();
		}

		public FrameData Clone()
		{
			FrameData ret = new FrameData(this.owner, this.index);
			ret.rect = this.rect;
			ret.sheetOffset = this.sheetOffset;
			ret.uvRect = this.uvRect;
			ret.rotated = this.rotated;
			return ret;
		}
	}
}
