﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace FairyGame
{
	public class ActionData
	{
		public AniDef owner;
		public int index; //动作索引

		public Rect boundsRect; //所有方向的最大矩形
		public float interval; //帧间隔时间
		public int fireFrame; //发射动作的帧编号
		public float repeatDelay; //每次循环之间的等待
		public bool swing; //摆动播放
		public int effectPosition; //特效播放位置 0-脚部，1-中心，2-头顶，3-底座，4-正面脚部，5-正面中心，6-正面头顶，7-背面脚部，8-背面中心，9-背面头顶, 10-坐标底座
		public float[] frameDelays; //每帧之间的延迟
		public int frameCount; //帧数
		public int suggestedDir;
		public int originDirCount; //原始方向数量
		public int ridingPoint; //骑乘点

		public DirData[] dirList; //8个方向

		private static Dictionary<int, int[]> dirCompletionScheme;

		public ActionData(AniDef owner, int index)
		{
			this.owner = owner;
			this.index = index;

			boundsRect = new Rect();
			dirList = new DirData[9];
			for (int i = 1; i <= 8; i++)
				dirList[i] = new DirData(this, i);
		}

		public void Read(ByteArray ba, int dataLen)
		{
			frameCount = ba.ReadByte();//frame count

			boundsRect.x = ba.ReadShort();
			boundsRect.y = ba.ReadShort();
			boundsRect.width = ba.ReadShort();
			boundsRect.height = ba.ReadShort();

			ridingPoint = ba.ReadShort();
			interval = ba.ReadByte() / 24f;
			fireFrame = ba.ReadByte();
			repeatDelay = ba.ReadByte() / 24f;
			swing = ba.ReadByte() == 1 ? true : false;
			effectPosition = ba.ReadByte();

			frameDelays = new float[frameCount];
			for (int kk = 0; kk < frameCount; kk++)
				frameDelays[kk] = ba.ReadByte() / 24f;

			originDirCount = ba.ReadByte();
			for (int di = 0; di < originDirCount; di++)
			{
				int dir = ba.ReadByte(); //dir
				DirData dirData = dirList[dir];

				dirData.boundsRect.x = ba.ReadShort();
				dirData.boundsRect.y = ba.ReadShort();
				dirData.boundsRect.width = ba.ReadShort();
				dirData.boundsRect.height = ba.ReadShort();

				dirData.firePoint.x = ba.ReadShort();
				dirData.firePoint.y = ba.ReadShort();

				dirData.hitTestRect.x = ba.ReadShort();
				dirData.hitTestRect.y = ba.ReadShort();
				dirData.hitTestRect.width = ba.ReadShort();
				dirData.hitTestRect.height = ba.ReadShort();

				dirData.frameList = new FrameData[frameCount];
				for (int i = 0; i < frameCount; i++)
				{
					FrameData frame = new FrameData(dirData, i);
					dirData.frameList[i] = frame;

					frame.rect.x = ba.ReadShort();
					frame.rect.y = ba.ReadShort();
					frame.rect.width = ba.ReadShort();
					frame.rect.height = ba.ReadShort();

					frame.sheetOffset.x = ba.ReadShort();
					frame.sheetOffset.y = ba.ReadShort();
					frame.rotated = ba.ReadByte() == 1 ? true : false;
				}
			}
		}

		public ActionData Clone()
		{
			ActionData ad = new ActionData(owner, index);
			ad.boundsRect = boundsRect;
			ad.interval = interval;
			ad.fireFrame = fireFrame;
			ad.repeatDelay = repeatDelay;
			ad.swing = swing;
			ad.effectPosition = effectPosition;
			ad.frameDelays = (float[])frameDelays.Clone();
			ad.frameCount = frameCount;
			ad.suggestedDir = suggestedDir;
			ad.originDirCount = originDirCount;
			for (int i = 1; i <= 8; i++)
			{
				ad.dirList[i].CopyFrom(dirList[i]);
			}

			return ad;
		}

		public void RemoveFrames(int beginIndex, int endIndex)
		{
			for (int i = 1; i <= 8; i++)
			{
				dirList[i].RemoveFrames(beginIndex, endIndex);
			}
			Array.Copy(frameDelays, endIndex + 1, frameDelays, beginIndex, frameCount - endIndex - 1);
			frameCount = frameCount - (endIndex - beginIndex + 1);
			Array.Resize<float>(ref frameDelays, frameCount);
		}

		public void CompleteDirs()
		{
			if (dirCompletionScheme == null)
				InitCompletionScheme();

			int i, k;
			DirData dirData = null, tmp = null;
			int[] arr;
			int dirFillFlags = 0; //使用一个整数表达8个方向的填充情况

			for (i = 1; i <= 8; i++)
			{
				dirData = dirList[i];
				if (!dirData.empty)
					dirFillFlags |= (1 << i);
			}

			if (dirCompletionScheme.TryGetValue(dirFillFlags, out arr))
			{
				for (i = 1; i <= 8; i++)
				{
					dirData = dirList[i];
					k = arr[i - 1];
					if (i == k)
						continue;

					if (k > 0)
						dirData.CopyFrom(dirList[k]);
					else
						dirData.FlipFrom(dirList[-k]);
				}
			}
			else
			{
				//先完成对称转换
				for (i = 1; i <= 8; i++)
				{
					if (i == 1 || i == 5)
						continue;
					dirData = dirList[i];
					if (dirData.empty)
					{
						tmp = dirList[10 - i];
						if (tmp != null && !tmp.empty)
						{
							dirData.FlipFrom(tmp);
						}
					}
				}

				for (i = 1; i <= 8; i++)
				{
					dirData = dirList[i];
					if (dirData.empty)
					{
						for (int j = 1; j < 5; j++)
						{
							k = i + j;
							if (k > 8)
								k = 1;
							tmp = dirList[k];
							if (!tmp.empty)
								break;

							k = i - j;
							if (k < 1)
								k = 8;
							tmp = dirList[k];
							if (!tmp.empty)
								break;
						}
						dirData.CopyFrom(tmp);
					}
				}
			}

			suggestedDir = 0;
			if (!dirList[4].empty && (dirFillFlags & 16) != 0)
				suggestedDir = 4;
			else if (!dirList[5].empty && (dirFillFlags & 32) != 0)
				suggestedDir = 5;
			else
			{
				int ret2 = 0;
				for (i = 8; i >= 1; i--)
				{
					if (!dirList[i].empty)
					{
						if ((dirFillFlags & (1 << i)) != 0)
							ret2 = i;
						suggestedDir = i;
					}
				}
				if (ret2 != 0)
					suggestedDir = ret2;
			}
			if (suggestedDir == 0)
				suggestedDir = 1;
		}

		private static void InitCompletionScheme()
		{
			dirCompletionScheme = new Dictionary<int, int[]>();
			//减号代表镜像
			/*2*/
			dirCompletionScheme[4] = new int[] { 2, 2, 2, 2, -2, -2, -2, -2 };
			/*3*/
			dirCompletionScheme[8] = new int[] { 3, 3, 3, 3, -3, -3, -3, -3 };
			/*4*/
			dirCompletionScheme[16] = new int[] { 4, 4, 4, 4, -4, -4, -4, -4 };
			/*6*/
			dirCompletionScheme[64] = new int[] { -6, -6, -6, -6, 6, 6, 6, 6 };
			/*7*/
			dirCompletionScheme[128] = new int[] { -7, -7, -7, -7, 7, 7, 7, 7 };
			/*8*/
			dirCompletionScheme[256] = new int[] { -8, -8, -8, -8, 8, 8, 8, 8 };
			/*2+4*/
			dirCompletionScheme[20] = new int[] { 2, 2, 2, 4, 4, -4, -4, -2 };
			/*6+8*/
			dirCompletionScheme[320] = new int[] { 8, -8, -8, -6, 6, 6, 6, 8 };
			/*1+5*/
			dirCompletionScheme[34] = new int[] { 1, 1, 1, 5, 5, 5, 5, 1 };
			/*3+7*/
			dirCompletionScheme[136] = new int[] { 3, 3, 3, 3, 7, 7, 7, 7 };
			/*2+6*/
			dirCompletionScheme[68] = new int[] { 2, 2, 2, -6, 6, 6, 6, -2 };
			/*4+8*/
			dirCompletionScheme[272] = new int[] { 8, -8, -8, 4, 4, -4, -4, 8 };
			/*1+2+3+4+5*/
			dirCompletionScheme[62] = new int[] { 1, 2, 3, 4, 5, -4, -3, -2 };
			/*1+5+6+7+8*/
			dirCompletionScheme[482] = new int[] { 1, -8, -7, -6, 5, 6, 7, 8 };
		}
	}
}
