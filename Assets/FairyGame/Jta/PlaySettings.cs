﻿using System;
using System.Collections;
using System.Collections.Generic;
using FairyGUI;
using UnityEngine;

namespace FairyGame
{
	public class PlaySettings
	{
		public int curFrame; //当前帧
		public float curFrameDelay; //当前帧延迟
		public int direction; //方向
		public float speedRatio; //速度比率
		public int action; //动作
		/// <summary>
		/// 是否忽略TimeScale的影响，即在TimeScale改变后依然保持原有的播放速度
		/// </summary>
		public bool ignoreTimeScale;
		public bool enterFrame;

		//以下值不要直接修改
		public int repeatedCount; //重复次数
		public bool reachEnding; //是否已播放到结尾
		public bool reversed; //是否已反向播放

		int _lastUpdateFrameId;

		public PlaySettings()
		{
			direction = 1;
			speedRatio = 1;
			enterFrame = true;
		}

		public bool NextFrame(AniDef ani)
		{
			if(!ani.ready || action > ani.actionList.Length-1)
				return false;

			ActionData ad = ani.actionList[action];
			if (ad == null)
			{
				reachEnding = true;
				return false;
			}

			float elapsed;
			int frameId = Time.frameCount;
			if (frameId - _lastUpdateFrameId != 1)
				//1、如果>1，表示不是连续帧了，说明刚启动（或者停止过），这里不能用流逝的时间了，不然会跳过很多帧
				//2、如果==0，表示在本帧已经处理过了，这通常是因为一个PlayState用于多个MovieClip共享，目的是多个MovieClip同步播放
				elapsed = 0;
			else if (ignoreTimeScale)
				elapsed = Time.unscaledDeltaTime;
			else
				elapsed = Time.deltaTime;
			_lastUpdateFrameId = frameId;

			reachEnding = false;
			curFrameDelay += elapsed;
			float interval = ad.interval + ad.frameDelays[curFrame] + ((curFrame == 0 && repeatedCount > 0) ? ad.repeatDelay : 0);
			interval = interval / speedRatio;
			if (curFrameDelay < interval)
			{
				enterFrame = false;
				return false;
			}

			enterFrame = true;
			curFrameDelay -= interval;
			if (curFrameDelay > ad.interval)
				curFrameDelay = ad.interval;

			if (ad.swing)
			{
				if (reversed)
				{
					curFrame--;
					if (curFrame <= 0)
					{
						repeatedCount++;
						reversed = !reversed;
					}
				}
				else
				{
					curFrame++;
					if (curFrame > ad.frameCount - 1)
					{
						curFrame = Mathf.Max(0, ad.frameCount - 2);
						repeatedCount++;
						reachEnding = true;
						reversed = !reversed;
					}
				}
			}
			else
			{
				curFrame++;
				if (curFrame > ad.frameCount - 1)
				{
					curFrame = 0;
					repeatedCount++;
					reachEnding = true;
				}
			}

			return true;
		}

		public float EvaluteTime(AniDef ani, int fromFrame = 0, int toFrame = -1)
		{
			ActionData ad = ani.actionList[action];
			if (ad == null || curFrame == toFrame)
				return 0;

			float ret = 0;
			if (toFrame == -1 || toFrame >= ad.frameCount)
				toFrame = ad.frameCount - 1;
			int i;
			for (i = fromFrame; i < toFrame; i++)
				ret += ad.interval + ad.frameDelays[i];
			return ret;
		}

		public void Rewind()
		{
			curFrame = 0;
			curFrameDelay = 0;
			reversed = false;
			reachEnding = false;
			enterFrame = true;
		}

		public void Reset()
		{
			curFrame = 0;
			curFrameDelay = 0;
			speedRatio = 1;
			repeatedCount = 0;
			reachEnding = false;
			action = 0;
			reversed = false;
			enterFrame = true;
		}

		public void Copy(PlaySettings src)
		{
			curFrame = src.curFrame;
			curFrameDelay = src.curFrameDelay;
			direction = src.direction;
			speedRatio = src.speedRatio;
			repeatedCount = src.repeatedCount;
			reachEnding = src.reachEnding;
			action = src.action;
			reversed = src.reversed;
		}
	}
}
