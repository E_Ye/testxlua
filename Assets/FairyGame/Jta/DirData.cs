﻿using System;
using System.Collections;
using System.Collections.Generic;
using FairyGUI;
using UnityEngine;

namespace FairyGame
{
	public class DirData
	{
		public ActionData owner;
		public int index; //方向索引，1-8

		public Rect boundsRect; //所有帧的最大矩形
		public Vector2 firePoint; //击发点
		public Rect hitTestRect;

		public FrameData[] frameList; //所有帧

		public DirData(ActionData owner, int index)
		{
			this.owner = owner;
			this.index = index;

			boundsRect = new Rect();
			firePoint = new Vector2();
			hitTestRect = new Rect();
		}

		public void CopyFrom(DirData source)
		{
			boundsRect = source.boundsRect;
			hitTestRect = source.hitTestRect;
			firePoint = source.firePoint;
			frameList = new FrameData[source.frameList.Length];
			for (int i = 0; i < frameList.Length; i++)
			{
				FrameData frame = source.frameList[i].Clone();
				frame.owner = this;
				frameList[i] = frame;
			}
		}

		public void FlipFrom(DirData source)
		{
			CopyFrom(source);

			boundsRect.x = -boundsRect.xMax;
			firePoint.x = -firePoint.x;
			hitTestRect.x = -hitTestRect.xMax;
			for (int i = 0; i < frameList.Length; i++)
			{
				FrameData frame = frameList[i];
				frame.rect.x = -frame.rect.xMax;

				float tmp = frame.uvRect.x;
				frame.uvRect.x = frame.uvRect.xMax;
				frame.uvRect.xMax = tmp;
			}
		}

		public bool empty
		{
			get { return frameList == null; }
		}

		public void RemoveFrames(int beginIndex, int endIndex)
		{
			Array.Copy(frameList, endIndex + 1, frameList, beginIndex, frameList.Length - endIndex - 1);
			int cnt = frameList.Length - (endIndex - beginIndex + 1);
			Array.Resize<FrameData>(ref frameList, cnt);

			for (int i = 0; i < frameList.Length; i++)
			{
				frameList[i].index = i;
			}
		}
	}
}
