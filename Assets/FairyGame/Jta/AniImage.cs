﻿using System;
using System.Collections;
using System.Collections.Generic;
using FairyGUI;
using UnityEngine;

namespace FairyGame
{
	public class AniImage : Image
	{
		protected AniDef _def;
		protected PlaySettings _settings;
		protected DirData _drawingDir;
		protected FrameData _drawingFrame;

		private bool _autoPlay;
		private TimerCallback _playDelegate;

		public AniImage()
		{
			_settings = new PlaySettings();

			_playDelegate = __play;
			onAddedToStage.Add(__addedToStage);
			onRemovedFromStage.Add(__removeFromStage);
		}

		public AniImage(PlaySettings settings)
		{
			_settings = settings;
		}

		public AniDef def
		{
			get { return _def; }
			set
			{
				if (value != _def)
				{
					this.texture = null;
					_drawingDir = null;
					_drawingFrame = null;
					_def = value;
					graphics.ClearMesh();
				}
			}
		}

		public void Load()
		{
			if (_def == null)
				return;

			AnimationManager.inst.LoadAnimation(_def);
		}

		public void Unload()
		{
			this.def = null;
			graphics.texture = null;
			graphics.ClearMesh();
		}

		public void Clear()
		{
			graphics.ClearMesh();
		}

		public void SetSource(string filePath)
		{
			int pos = filePath.IndexOf("/");
			this.def = AnimationManager.inst.GetAnimation(filePath.Substring(0, pos), filePath.Substring(pos + 1));
		}

		public ActionData GetAction(int action)
		{
			if (_def != null && _def.ready)
			{
				if (action >= 0 && action < _def.actionList.Length)
					return _def.actionList[action];
			}

			return null;
		}

		public ActionData currentAction
		{
			get
			{
				if (_def != null && _def.ready)
				{
					if (_settings.action >= 0 && _settings.action < _def.actionList.Length)
						return _def.actionList[_settings.action];
				}
				return null;
			}
		}

		public FrameData currentFrame
		{
			get
			{
				if (_def != null && _def.ready)
				{
					if (_settings.action >= 0 && _settings.action < _def.actionList.Length)
					{
						ActionData ad = _def.actionList[_settings.action];
						if (ad != null)
						{
							DirData dirData = ad.dirList[_settings.direction];
							FrameData f = dirData.frameList[_settings.curFrame];
							return f;
						}
					}
				}
				return null;
			}
		}

		public FrameData GetFrame(int action, int dir, int index)
		{
			if (_def != null && _def.ready)
			{
				if (_settings.action >= 0 && _settings.action < _def.actionList.Length)
				{
					ActionData ad = _def.actionList[action];
					if (ad != null)
					{
						DirData dirData = ad.dirList[dir];
						FrameData f = dirData.frameList[index];
						return f;
					}
				}
			}
			return null;
		}

		public DirData drawingDir
		{
			get { return _drawingDir; }
		}

		public FrameData drawingFrame
		{
			get { return _drawingFrame; }
		}

		public PlaySettings settings
		{
			get { return _settings; }
			set { _settings = value; }
		}

		public bool defined
		{
			get { return _def != null; }
		}

		public bool autoPlay
		{
			get { return _autoPlay; }
			set
			{
				if (_autoPlay != value)
				{
					_autoPlay = value;
					if (_autoPlay && this.stage != null)
						Timers.inst.AddUpdate(_playDelegate);
				}
			}
		}

		protected override void UpdateTexture(NTexture value)
		{
			//nothing
		}

		public void NextFrame()
		{
			if (_def != null)
				_settings.NextFrame(_def);
		}

		public void Draw(bool forceDraw = false)
		{
			if (_def == null || !_def.ready)
				return;

			if (graphics.texture == null)
			{
				graphics.texture = def.texture;
				_contentRect = def.unionRect;
				base.OnSizeChanged(true, true);
			}

			if (_settings.enterFrame || forceDraw)
			{
				_drawingDir = null;
				_drawingFrame = null;
				ActionData ad = _settings.action < _def.actionList.Length ? _def.actionList[_settings.action] : null;
				if (ad == null)
					return;

				int index;
				if (_settings.reversed)
					index = ad.frameCount - _settings.curFrame - 1;
				else
					index = _settings.curFrame;
				_drawingDir = ad.dirList[_settings.direction];
				if (index >= _drawingDir.frameList.Length)
					index = 0;
				_drawingFrame = _drawingDir.frameList[index];
				graphics.SetOneQuadMesh(_drawingFrame.rect, _drawingFrame.uvRect, _color, null, _drawingFrame.rotated);
			}
		}

		protected override void Rebuild()
		{
			_requireUpdateMesh = false;
			//nothing more
		}

		void __play(object param)
		{
			Draw();
			NextFrame();
		}

		void __addedToStage()
		{
			if (_autoPlay)
				Timers.inst.AddUpdate(_playDelegate);
		}

		void __removeFromStage()
		{
			Timers.inst.Remove(_playDelegate);
		}
	}
}
