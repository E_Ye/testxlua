﻿using System;
using FairyGUI;
using UnityEngine;

using FairyGUI.Utils;

namespace FairyGame
{
	public class AniDef
	{
		public string assetPath;
		public string fileName;
		public Rect unionRect; //所有动作的包围
		public bool loading;
		public bool ready;

		public ActionData[] actionList; //所有动作对象
		public NTexture texture;

		public AniDef(string assetPath, string fileName)
		{
			this.assetPath = assetPath;
			this.fileName = fileName;
			this.actionList = new ActionData[10];
		}

		public void Create(ByteArray ba, NTexture texture)
		{
			ba.position = 0;
			string mark = ba.ReadLongString();
			if (mark != "yytou")
				throw new Exception(this.fileName + ": wrong jta format");

			ba.ReadShort(); //version
			int textureWidth = ba.ReadShort();
			int textureHeight = ba.ReadShort();
			int actionCount = ba.ReadByte();
			ActionData ad;
			while (actionCount-- > 0)
			{
				int action = ba.ReadByte();
				int actionDataLen = ba.ReadInt();

				ad = new ActionData(this, action);
				actionList[action] = ad;
				ad.Read(ba, actionDataLen);
			}

			this.texture = texture;

			for (int ai = 0; ai < 10; ai++)
			{
				ad = actionList[ai];
				if (ad == null)
					continue;

				for (int di = 1; di <= 8; di++)
				{
					DirData dd = ad.dirList[di];
					if (dd.frameList == null)
						continue;

					int cnt = dd.frameList.Length;
					for (int fi = 0; fi < cnt; fi++)
					{
						FrameData frame = dd.frameList[fi];
						Rect uvRect;
						if (frame.rotated)
						{
							uvRect = new Rect(frame.sheetOffset.x / textureWidth, 1 - (frame.sheetOffset.y + frame.rect.width) / textureHeight,
								frame.rect.height / textureWidth, frame.rect.width / textureHeight);
							float tmp = uvRect.width;
							uvRect.width = uvRect.height;
							uvRect.height = tmp;
						}
						else
						{
							uvRect = new Rect(frame.sheetOffset.x / textureWidth, 1 - (frame.sheetOffset.y + frame.rect.height) / textureHeight,
								frame.rect.width / textureWidth, frame.rect.height / textureHeight);
						}
						frame.uvRect = uvRect;
					}
				}

				ad.CompleteDirs();
				unionRect = ToolSet.Union(ref unionRect, ref ad.boundsRect);
			}

			if (actionList.Length > 3)
			{
				ad = actionList[3]; //受攻击动作只有一帧时，给他加个延迟
				if (ad != null && ad.frameCount == 1)
					ad.frameDelays[0] = 0.25f;
			}

			if (actionList.Length > 2 && assetPath == "user")
			{
				ad = actionList[2]; //将攻击动作拆分出一个暴击专用
				if (ad != null && ad.frameCount > 5)
				{
					ActionData ad2 = ad.Clone();
					ad2.index = 9;
					if (ad2.frameCount == 8)
						ad2.fireFrame = 6;
					else
						ad2.fireFrame = 7;
					actionList[9] = ad2;

					ad.RemoveFrames(5, ad.frameCount - 1);
					ad.fireFrame = 4;
				}
			}
		}
	}
}
