using FairyGUI;
using KBEngine;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using UnityEngine;

namespace FairyGame
{
    public class SocketProcessor
    {
        enum SocketState
        {
            None,
            Disconnect,
            Connecting,
            Success,
            Connected
        }

        public delegate void OnConnectedDelegate();
        public delegate void OnDisconnectedDelegate(string exception);
        public delegate void OnPacketDelegate(ByteArray pack);

        public OnConnectedDelegate onConnected;
        public OnDisconnectedDelegate onDisconnected;
        public OnPacketDelegate onPacket;

        private Socket _clientSocket;
        private SocketAsyncEventArgs _clientSocketEventArg;
        private float _timeStamp;
        private SocketState _state;
        private string _reason;
        private byte[] _socketBuffer;
        private DataBuffer _sendBuffer;
        private DataBuffer _receiveBuffer;

        private List<ByteArray> _packs;
        private bool _isPause;
        private readonly object _lock = new object();

        const int BUFFER_SIZE = 1024 * 16;
        const bool USE_NAGLE = false;//指定流 System.Net.Sockets.Socket 是否正在使用 Nagle 算法
        const int CONNECTION_TIME_OUT = 5000;//Socket连接超时时间

        public SocketProcessor()
        {
            this._state = SocketState.None;
            _packs = new List<ByteArray>();

            _socketBuffer = new byte[BUFFER_SIZE];
            _sendBuffer = new DataBuffer(BUFFER_SIZE, false);
            _sendBuffer.AppendShort(0); //reserve for len
            _sendBuffer.AppendShort(0); //reserve for cmd

            _receiveBuffer = new DataBuffer(BUFFER_SIZE, false);
        }

        public bool connected
        {
            get { return this._state == SocketState.Connected; }
        }

        public void Connect(string host, int port)
        {

            if (this._state != SocketState.Disconnect && this._state != SocketState.None)
                return;
            this._clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            this._clientSocketEventArg = new SocketAsyncEventArgs();
            IPEndPoint ipEndpoint;
            if (Char.IsLetter(host[0]))
            {
                IPHostEntry ipHost = Dns.GetHostEntry(host);
                ipEndpoint = new IPEndPoint(ipHost.AddressList[0], port);
            }
            else
            {
                IPAddress ipAddress = IPAddress.Parse(host);
                ipEndpoint = new IPEndPoint(ipAddress, port);
            }
            _clientSocketEventArg.Completed += _clientSocketEventArg_Completed;
            _clientSocketEventArg.RemoteEndPoint = ipEndpoint;
            _clientSocketEventArg.UserToken = _clientSocket;

            this.Connect(ipEndpoint);
        }

        private void _clientSocketEventArg_Completed(object sender, SocketAsyncEventArgs e)
        {
            if (e.SocketError != SocketError.Success)
                return;

            Socket socket = sender as Socket;
            if (socket.Connected)
            {
                this._state = SocketState.Success;
                try
                {
                    var receiveSaea = new SocketAsyncEventArgs();
                    var receiveBuffer = new byte[1024 * 4];
                    receiveSaea.SetBuffer(receiveBuffer, 0, receiveBuffer.Length);       //设置消息的缓冲区大小
                    receiveSaea.Completed += ReceiveSaea_Completed;         //绑定回调事件
                    receiveSaea.RemoteEndPoint = e.RemoteEndPoint;
                    _clientSocket.ReceiveAsync(receiveSaea);

                }
                catch (Exception exc)
                {
                    this.Dispose(exc.Message);
                }
            }
            else
            {
                this.Dispose("无法连接服务器");
            }
        }

        private void ReceiveSaea_Completed(object sender, SocketAsyncEventArgs e)
        {
            if (e.SocketError == SocketError.OperationAborted) return;

            Socket client = sender as Socket;
            int revCount = e.BytesTransferred;
            if (e.SocketError == SocketError.Success && revCount > 0)
            {
                Debug.Log(">>>>>>>>count:" + revCount);
                 _receiveBuffer.Append(e.Buffer, 0, revCount);
                this.Unpack();
                try
                {
                    client.ReceiveAsync(e);
                }
                catch (Exception exc)
                {
                    this.Dispose(exc.Message);
                }
            }
            else
            {
                this.Dispose("与服务器断开连接");
            }

        }

        void Connect(IPEndPoint ipEndPoint)
        {
            //this._clientSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.SendTimeout, CONNECTION_TIME_OUT);
            //this._clientSocket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReceiveTimeout, CONNECTION_TIME_OUT);
            this._clientSocket.NoDelay = USE_NAGLE;

            try
            {
                //this._clientSocket.BeginConnect(ipEndPoint, this.ConnectCallback, this._clientSocket);
                this._clientSocket.ConnectAsync(_clientSocketEventArg);
                this._state = SocketState.Connecting;
                this._timeStamp = Time.realtimeSinceStartup;
                Timers.inst.AddUpdate(this.Processing);
            }
            catch (Exception e)
            {
                this.Dispose(e.Message);
            }
        }

        private void Unpack()
        {
            while (_receiveBuffer.length >= 4)
            {
                ushort len = _receiveBuffer.GetUShort(0);
                if (_receiveBuffer.length < len)
                    break;

                ushort cmd = _receiveBuffer.GetUShort(2);
                _receiveBuffer.Pop(4);

                ByteArray byteArray = new ByteArray(_receiveBuffer.Pop(len - 4));
                byteArray.cmd = cmd;

                lock (this._lock)
                {
                    _packs.Add(byteArray);
                }
            }
        }

        public void Send(ushort cmd)
        {
            if (this._clientSocket != null)
            {
                ushort len = (ushort)_sendBuffer.length;
                _sendBuffer.SetUShort(0, len);
                _sendBuffer.SetUShort(2, cmd);

                try
                {
                    byte[] sendBuffer = _sendBuffer.Pop(len);
                    ResetSendBuffer();
                    var sendSaea = new SocketAsyncEventArgs();
                    sendSaea.SetBuffer(sendBuffer, 0, _sendBuffer.length);       //设置消息的缓冲区大小
                    sendSaea.Completed += SendSaea_Completed; ;         //绑定回调事件
                    sendSaea.RemoteEndPoint = this._clientSocket.RemoteEndPoint;
                    _clientSocket.SendAsync(sendSaea);
                }
                catch (Exception e)
                {
                    this.Dispose(e.Message);
                }
            }
        }

        private void SendSaea_Completed(object sender, SocketAsyncEventArgs e)
        {
            if (e.SocketError != SocketError.Success) return;
            var socket = sender as Socket;
            byte[] sendBuffer = e.Buffer;
            
            //if (OnSendCompletedEvent != null) OnSendCompletedEvent(messageFormat);

        }

        //主动关闭，在主线程中调用
        public void Close()
        {
            this._isPause = false;
            lock (this._lock)
            {
                this._packs.Clear();
            }

            Dispose(string.Empty);
            Timers.inst.Remove(this.Processing);
            this._state = SocketState.None; //onDisconnect不会被调用
        }

        //被动关闭，可能在其他线程
        bool _inDispose;
        private void Dispose(string reason)
        {
            if (_inDispose || _clientSocket == null)
                return;

            _inDispose = true;
            _reason = reason;

            if (this._clientSocket != null)
            {
                if (this._clientSocket.Connected)
                {
                    try
                    {
                        this._clientSocket.Shutdown(SocketShutdown.Both);
                        this._clientSocket.Close();
                    }
                    catch (Exception ex)
                    {
                        Debug.LogError(ex.Message);
                    }
                }
                this._clientSocket = null;
            }

            _receiveBuffer.Clear();
            _sendBuffer.Clear();
            _sendBuffer.AppendShort(0); //reserve for len
            _sendBuffer.AppendShort(0); //reserve for cmd

            this._state = SocketState.Disconnect;
            this._isPause = false;
            _inDispose = false;
        }

        private void Processing(object param)
        {
            switch (this._state)
            {
                case SocketState.Connecting:
                    float ct = Time.realtimeSinceStartup;
                    float dt = (ct - this._timeStamp) * 1000.0f;
                    if (dt >= CONNECTION_TIME_OUT)
                    {
                        _reason = "Socket connection timeout";
                        this._state = SocketState.Disconnect;
                    }
                    break;

                case SocketState.Success:
                    this._state = SocketState.Connected;
                    if (onConnected != null)
                        onConnected();
                    break;
            }

            if (this._state == SocketState.Disconnect)
            {
                this._state = SocketState.None;
                Timers.inst.Remove(this.Processing);
                if (onDisconnected != null)
                    onDisconnected(_reason);
            }

            ByteArray pack;
            while (!this._isPause)
            {
                lock (this._lock)
                {
                    if (this._packs.Count == 0)
                        return;

                    pack = this._packs[0];
                    this._packs.RemoveAt(0);
                }

                if (onPacket != null)
                    onPacket(pack);
            }
        }

        public void ResetSendBuffer()
        {
            this._sendBuffer.Clear();
            this._sendBuffer.AppendShort(0); //reserve for len
            this._sendBuffer.AppendShort(0); //reserve for cmd
        }

        public void Pause()
        {
            this._isPause = true;
        }

        public void Resume()
        {
            this._isPause = false;
        }

        public void WriteByte(byte value)
        {
            this._sendBuffer.AppendByte(value);
        }

        public void WriteShort(short value)
        {
            this._sendBuffer.AppendShort(value);
        }

        public void WriteUShort(ushort value)
        {
            this._sendBuffer.AppendUShort(value);
        }

        public void WriteInt(int value)
        {
            this._sendBuffer.AppendInt(value);
        }

        public void WriteLong(long value)
        {
            this._sendBuffer.AppendLong(value);
        }

        public void WriteFloat(float value)
        {
            this._sendBuffer.AppendFloat(value);
        }

        public void WriteBool(bool value)
        {
            this._sendBuffer.AppendByte(value ? (byte)1 : (byte)0);
        }

        static UTF8Encoding encodingUTF8 = new UTF8Encoding();
        public void WriteLongString(string value)
        {
            if (value.Length > 0)
            {
                byte[] toWrite = encodingUTF8.GetBytes(value);
                this._sendBuffer.AppendShort((short)toWrite.Length);
                this._sendBuffer.Append(toWrite);
            }
            else
                this._sendBuffer.AppendShort((short)0);
        }

        public void WriteString(string value)
        {
            if (value.Length > 0)
            {
                byte[] toWrite = encodingUTF8.GetBytes(value);
                this._sendBuffer.AppendByte((byte)toWrite.Length);
                this._sendBuffer.Append(toWrite);
            }
            else
                this._sendBuffer.AppendByte((byte)0);
        }

        public void WriteJSON(string value)
        {
            byte compress = 0;
            byte[] data = Encoding.UTF8.GetBytes(value);
            if (data.Length > 500)
            {
                data = Compression.Compress(data);
                compress = 1;
            }
            this._sendBuffer.AppendByte((byte)compress);
            this._sendBuffer.Append(data);
        }

    }
}                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   