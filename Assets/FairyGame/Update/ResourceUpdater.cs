﻿using ICSharpCode.SharpZipLib.Zip;
using System;
using System.Collections;
using System.IO;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;

namespace FairyGame
{
	public class ResourceUpdater
	{
		public delegate void CompleteCallback();
		public delegate void ProgressCallback(int total, int progress);
		public delegate void ErrorCallback(string error);

		public bool firstRun { get; private set; }
		public bool newVersion { get; private set; }
		public string releaseNotes { get; private set; }

		IniConfig _vers;
		Hashtable _resp;

		public ResourceUpdater()
		{
			_vers = new IniConfig();
		}

		public IEnumerator CheckUpdate(CompleteCallback onComplete, ErrorCallback onError, float connectTimeout)
		{
			_vers.Clear();
			newVersion = false;
			releaseNotes = string.Empty;

			string verUrl = GameConfig.localResURL + "ver.o";
			WWW wwwVer = new WWW(verUrl);
			yield return wwwVer;

			if (!string.IsNullOrEmpty(wwwVer.error))
			{
				//firstRun = true;
			}
			else
			{
				string txt = Encoding.UTF8.GetString(wwwVer.bytes);
				_vers.Decode(txt);
			}
			wwwVer.Dispose();
			int localVer = _vers.GetInt("res");
			if (localVer < GameConfig.packageResVer)
				localVer = GameConfig.packageResVer;

			WWWForm form = new WWWForm();
			form.AddField("q", GameConfig.partnerId);
			form.AddField("i", GameConfig.terminalId);

            string account = PlayerPrefs.GetString("account", string.Empty);
            if (account.Length > 0)
			{
                //删除加密
				//try
				//{
				//	account = MiscUtil.DecryptValue(account);
				//}
				//catch (Exception e)
				//{
				//	account = string.Empty;
				//	Debug.LogWarning("decrypt account: " + e);
				//}
			}
			form.AddField("u", account);
			form.AddField("p", SystemInfo.operatingSystem);
			form.AddField("m", SystemInfo.deviceName);
			form.AddField("d", SystemInfo.deviceModel);
			form.AddField("w", Screen.width);
			form.AddField("h", Screen.height);
			form.AddField("app", GameConfig.appVersion);
			form.AddField("res", localVer);

			WWW www = new WWW(GameConfig.versionCheckURL, form);
			float startTime = Time.realtimeSinceStartup;
			bool timeout = false;
			while (!www.isDone)
			{
				float elapsed = Time.realtimeSinceStartup - startTime;
				if (connectTimeout > 0 && elapsed > connectTimeout)
				{
					timeout = true;
					break;
				}
				yield return null;
			}

			if (timeout)
			{
				Debug.LogWarning("Ver check error! timeout");
				onError("timeout");
			}
			else if (!string.IsNullOrEmpty(www.error))
			{
				Debug.LogWarning("Ver check error! [" + www.error + "]");
				onError(www.error);
			}
			else
			{
				string txt = Encoding.UTF8.GetString(www.bytes);
				Debug.Log(txt);
				_resp = (Hashtable)MiniJSON.JsonDecode(txt);
				if (_resp == null)
					onError("unknow response - " + txt.Substring(0, 20));
				else if (_resp.ContainsKey("code") && Convert.ToInt32(_resp["code"]) != 0)
					onError((string)_resp["msg"]);
				else
				{
					Hashtable info = (Hashtable)_resp["res"];
					int serverVer = TypeUtil.GetInt(info["ver"]);
					string resUrl = TypeUtil.GetString(info["url"]);
					newVersion = serverVer > localVer && !string.IsNullOrEmpty(resUrl);
					ArrayList dscList = (ArrayList)info["dsc"];
					if (dscList != null)
					{
						StringBuilder sb = new StringBuilder();
						int cnt = dscList.Count;
						for (int i = cnt - 1; i >= 0; i--)
						{
							sb.Append(dscList[i]);
							sb.Append("\n\n");
						}
						releaseNotes = sb.ToString();
					}
					if (string.IsNullOrEmpty(releaseNotes))
					{
						releaseNotes = "更新至版本: " + serverVer;
					}
					onComplete();
				}
			}
			if (!timeout)
				www.Dispose();
		}

		public IEnumerator UpdateRes(CompleteCallback onComplete, ErrorCallback onError, ProgressCallback onProgress = null)
		{
			yield return null; //让界面有机会先更新

			int recved = 0;
			bool isError = false;

			Hashtable info = (Hashtable)_resp["res"];;
			int total = TypeUtil.GetInt(info["size"], 0);
			string url = TypeUtil.GetString(info["url"]);
			string name = TypeUtil.GetString(info["name"]);
			//string md5 = TypeUtil.GetString(info["md5"]);

			string file = GameConfig.tempPath + name;
			info["file"] = file;

			Debug.Log("start download " + url + "->" + file);
			WWWEx www = new WWWEx(url, file, null);

			while (!www.isDone)
			{
				if (onProgress != null)
					onProgress(total, recved + (int)www.recived);
				yield return null;
			}
			if (onProgress != null)
				onProgress(total, recved + (int)www.recived);

			if (!string.IsNullOrEmpty(www.error))
			{
				Debug.Log("download error: " + www.error);
				isError = true;
				if (onError != null)
					onError(www.error);
				www.Dispose();
			}
			else
			{
				Debug.Log("end download");

				recved += (int)www.recived;
				www.Dispose();

				//校验
				/*FileInfo fi = new FileInfo(file);
				if (fi.Length != total)
				{
					isError = true;
					Debug.LogError("File size error! [" + fi.Length + "!=" + total + "]");
					if (onError != null)
						onError("File size error! [" + fi.Length + "!=" + total + "]");
				}

				if (md5.Length > 0 && MD5Util.GetMD5HexDigest(fi).ToLower() != md5.ToLower())
				{
					isError = true;
					Debug.LogError("File content error!");
					if (onError != null)
						onError("File content error!");
				}*/
			}

			if (!isError && onComplete != null)
				onComplete();
		}

		public IEnumerator UnzipRes(CompleteCallback onComplete, ErrorCallback onError, ProgressCallback onProgress = null)
		{
			yield return null; //让界面有机会先更新

			UnzipParam param = new UnzipParam();
			param.caller = this;
            this.DoDoUnzipResAsync(param);

			float lastProgress = 0f;

			while (!param.done)
			{
				if (param.progress != lastProgress)
				{
					lastProgress = param.progress;
					if (onProgress != null)
						onProgress(param.total, param.progress);
				}
				yield return null;
			}

			if (param.error != null)
				onError(param.error);
			else
				onComplete();
		}

		void SaveResVers()
		{
			string verFile = GameConfig.localResPath + "ver.o";
			string txt = _vers.Encode();
			File.WriteAllText(verFile, txt, Encoding.UTF8);
		}

		class UnzipParam
		{
			public ResourceUpdater caller;
			public bool done;
			public string error;
			public int total;
			public int progress;
		}

		void _DoUnzipRes(UnzipParam param)
		{
			Hashtable info = (Hashtable)_resp["res"];
			if (info.ContainsKey("file"))
			{
				try
				{
					//解压
					ZipFile zipFile = new ZipFile((string)info["file"]);
					foreach (ZipEntry ze in zipFile)
					{
						if (ze.IsFile)
							param.total += (int)ze.Size;
					}

					foreach (ZipEntry ze in zipFile)
					{
                        if (ze.IsFile)
                        {
                            Stream s = zipFile.GetInputStream(ze);
                            string file = GameConfig.localResPath + ze.Name;
                            string path = Path.GetDirectoryName(file);
                            if (!Directory.Exists(path) && Directory.CreateDirectory(path) == null)
                            {
                                param.error = "Cannot create directory, " + path;
                                return;
                            }
                            using (Stream os = new FileStream(file, FileMode.Create, FileAccess.Write, FileShare.ReadWrite))
                            {
                                try
                                {
                                    Stdio.CopyStream(s, os);
                                }
                                catch
                                {
                                    File.Delete(file);
                                    throw;
                                }
                            }

							param.progress += (int)ze.Size;
						}
					}

					zipFile.Close();
					try
					{
						File.Delete((string)info["file"]);
					}
					catch { }

					//更新版本号
					int ver = TypeUtil.GetInt(info["ver"], 0);
					_vers["res"] = ver;
					SaveResVers();
				}
				catch (Exception e)
				{
					Debug.LogError("Extract package error! " + e.ToString());
					param.error = e.Message;
				}
			}
		}
        private  async void DoDoUnzipResAsync(object param)
        {
            await DoDoUnzipResTask(param);
        }

        private Task DoDoUnzipResTask(object param)
        {
            return Task.Run(() => { this.__DoUnzipRes(param); });
        }

        private  void __DoUnzipRes(object param)
		{
			UnzipParam up = (UnzipParam)param;
			up.caller._DoUnzipRes(up);
			up.done = true;
		}
	}
}
