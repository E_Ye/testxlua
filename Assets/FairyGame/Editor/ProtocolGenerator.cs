﻿/********************************************************************
	filename: 	ProtocolPacker.cs
	created:	2015/10/30 16:50
	author:		Moncat
*********************************************************************/

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using FairyGame;
using UnityEngine;
using UnityEditor;
using FairyGUI.Utils;

public class ProtocolPacker
{
	static int varCounter;
	static string eol = "\n";

	[MenuItem("FairyGame/生成通讯协议Lua文件")]
	public static void BuildBundles()
	{
		string sourceDir = Application.dataPath + "/../Lua/";

		StringBuilder output = new StringBuilder();
		StringBuilder strFunctions = new StringBuilder();
		XML xml;
		XMLList.Enumerator et;

		string qcmdFile = sourceDir + "Net/Protocol_qcmd.lua";
		xml = new XML(File.ReadAllText(sourceDir + "/Net/cts.xml", Encoding.UTF8));

		output.Append("--[[  这是自动生成的文件，请勿修改  ]]").Append(eol);
		output.Append("--cts.xml ver ").Append(xml.GetAttribute("ver")).Append(eol);
		output.Append(eol);

		strFunctions.Append("local names = NetKeys.QCMD_NAMES").Append(eol);
		strFunctions.Append("local handlers = NetKeys.PackHandlers").Append(eol);
		strFunctions.Append("local json = require 'cjson'").Append(eol).Append(eol);

		et = xml.GetEnumerator("packet");
		while (et.MoveNext())
		{
			XML packet = et.Current;
			string key = packet.GetAttribute("id");
			string code = packet.GetAttribute("value");
			string type = packet.GetAttribute("type");

			output.Append("NetKeys.").Append(key).Append(" = ").Append(code).Append(eol);
			strFunctions.Append("names[").Append(code).Append("] = '").Append(key).Append("'").Append(eol);

			if (type == "json")
			{
				strFunctions.Append("handlers[").Append(code).Append("] = function(sp, arg0)").Append(eol);
				strFunctions.Append("\tsp:WriteJSON(json.encode(arg0))").Append(eol);
				strFunctions.Append("end").Append(eol);
			}
			else
			{
				XMLList col = packet.Elements();
				if (col.Count > 0)
				{
					strFunctions.Append("handlers[").Append(code).Append("] = function(sp");
					for (int i = 0; i < col.Count; i++)
						strFunctions.Append(", arg" + i);
					strFunctions.Append(")").Append(eol);
					for (int i = 0; i < col.Count; i++)
					{
						XML field = col[i];
						string fieldType = field.GetAttribute("type");

						if (field.name == "field")
						{
							strFunctions.Append("\t").Append(WriteField(field, "arg" + i)).Append(eol);
						}
						else if (fieldType == "list")
						{
							XML subField = field.Elements()[0];

							strFunctions.Append("\tsp:Write").Append(field.GetAttributeInt("lenBytes", 2) == 1 ? "Byte" : "UShort");
							strFunctions.Append("(").Append("#arg").Append(i).Append(")").Append(eol);

							strFunctions.Append("\tfor _,v in ipairs(arg").Append(i).Append(") do").Append(eol);
							strFunctions.Append("\t\t").Append(WriteField(subField, "v")).Append(eol);
							strFunctions.Append("\tend").Append(eol);
						}
						else if (fieldType == "map")
						{
							XML subField = field.Elements()[1];

							strFunctions.Append("\tsp:Write").Append(field.GetAttributeInt("lenBytes", 2) == 1 ? "Byte" : "UShort");
							strFunctions.Append("(").Append("#arg").Append(i).Append(")").Append(eol);

							strFunctions.Append("\tfor k,v in pairs(arg").Append(i).Append(") do").Append(eol);
							strFunctions.Append("\t\tsp:WriteString(k)").Append(eol);
							strFunctions.Append("\t\t").Append(WriteField(subField, "v")).Append(eol);
							strFunctions.Append("\tend").Append(eol);
						}
					}
					strFunctions.Append("end").Append(eol);
				}
			}
			strFunctions.Append(eol);
		}

		output.Append(eol);
		output.Append(strFunctions.ToString());
		File.WriteAllText(qcmdFile, output.ToString());

		output.Length = 0;
		strFunctions.Length = 0;

		//===============================================
		string acmdFile = sourceDir + "Net/Protocol_acmd.lua";
		xml = new XML(File.ReadAllText(sourceDir + "/Net/stc.xml", Encoding.UTF8));

		output.Append("--[[  这是自动生成的文件，请勿修改  ]]").Append(eol);
		output.Append("--stc.xml ver ").Append(xml.GetAttribute("ver")).Append(eol);
		output.Append(eol);

		strFunctions.Append("local names = NetKeys.ACMD_NAMES").Append(eol);
		strFunctions.Append("local handlers = NetKeys.UnpackHandlers").Append(eol);
		strFunctions.Append("local json = require 'cjson'").Append(eol);
		strFunctions.Append("local UnpackJSONPacket = function(packet)").Append(eol);
		strFunctions.Append("\treturn json.decode(packet:ReadJSONString())").Append(eol);
		strFunctions.Append("end").Append(eol).Append(eol);

		varCounter = 0;

		et = xml.GetEnumerator("packet");
		while (et.MoveNext())
		{
			XML packet = et.Current;
			string key = packet.GetAttribute("id");
			string code = packet.GetAttribute("value");
			string type = packet.GetAttribute("type");
			varCounter = 0;
			XMLList col = packet.Elements();

			output.Append("NetKeys.").Append(key).Append(" = ").Append(code).Append(eol);
			strFunctions.Append("names[").Append(code).Append("] = '").Append(key).Append("'").Append(eol);
			strFunctions.Append("handlers[").Append(code).Append("] = ");
			if (type == "json")
				strFunctions.Append("UnpackJSONPacket");
			else
			{
				strFunctions.Append("function(packet)").Append(eol);
				PacketField subField = DecodeField(col.Count == 1 ? col[0] : packet);
				foreach (string line in subField.lines)
					strFunctions.Append("\t").Append(line).Append(eol);
				strFunctions.Append("\treturn ").Append(subField.rightValue).Append(eol);
				strFunctions.Append("end");
			}

			strFunctions.Append(eol);
			strFunctions.Append(eol);
		}

		output.Append(eol);
		output.Append(strFunctions.ToString());
		File.WriteAllText(acmdFile, output.ToString());

		Debug.Log("Protocol generated.");
	}

	static private string WriteField(XML field, string arg)
	{
		string fieldType = field.GetAttribute("type");
		if (fieldType == "long")
			return string.Format("sp:WriteLong({0})", arg);
		else if (fieldType == "boolean")
			return string.Format("sp:WriteBool({0})", arg);
		else if (fieldType == "string" && field.GetAttributeInt("lenBytes") == 2)
			return string.Format("sp:WriteLongString({0})", arg);
		else if (fieldType == "int" && field.GetAttribute("name") == "x,y坐标")
			return string.Format("sp:WriteShort({0}.x) sp:WriteShort({0}.y)", arg, eol);
		else
			return string.Format("sp:Write" + char.ToUpper(fieldType[0]) + fieldType.Substring(1) + "({0})", arg);
	}

	class PacketField
	{
		public string key;
		public string rightValue;
		public List<string> lines;

		public PacketField()
		{
			lines = new List<string>();
		}
	}

	static private PacketField DecodeField(XML field)
	{
		PacketField pf = new PacketField();
		pf.key = field.GetAttribute("id");

		string fieldType = field.GetAttribute("type");
		int lenBytes = field.GetAttributeInt("lenbytes", 0);
		string condType = null;
		string condField = null;
		string condValue = null;

		if (field.name == "field")
		{
			if (fieldType == "long")
				pf.rightValue = "int64.tonum2(packet:ReadLong())";
			else if (fieldType == "boolean")
				pf.rightValue = "packet:ReadBool()";
			else if (fieldType == "string" && lenBytes == 2)
				pf.rightValue = "packet:ReadLongString()";
			else if (fieldType == "int" && field.GetAttribute("name") == "x,y坐标")
				pf.rightValue = "Vector2.New(packet:ReadUShort(),packet:ReadUShort())";
			else
				pf.rightValue = "packet:Read" + char.ToUpper(fieldType[0]) + fieldType.Substring(1) + "()";
		}
		else if (field.name == "packet" || field.name == "fieldset")
		{
			int localVarCounter = varCounter++;
			int localVarCounter2;
			XMLList col = field.Elements();
			PacketField subField;
			pf.rightValue = "m" + localVarCounter;
			switch (fieldType)
			{
				case "list":
					pf.lines.Add(string.Format("local m{0} = {{}}", localVarCounter));
					pf.lines.Add(string.Format("local cnt{0} = packet:Read{1}()", localVarCounter, lenBytes == 1 ? "Byte" : "UShort"));
					pf.lines.Add(string.Format("for i=1,cnt{0} do", localVarCounter));
					subField = DecodeField(col[0]);
					foreach (string line in subField.lines)
						pf.lines.Add("\t" + line);
					pf.lines.Add(string.Format("\ttable.insert(m{0}, {1})", localVarCounter, subField.rightValue));
					pf.lines.Add("end");
					break;

				case "map":
					pf.lines.Add(string.Format("local m{0} = {{}}", localVarCounter));
					pf.lines.Add(string.Format("local cnt{0} = packet:Read{1}()", localVarCounter, lenBytes == 1 ? "Byte" : "UShort"));
					pf.lines.Add(string.Format("local key{0}", localVarCounter));
					pf.lines.Add(string.Format("for i=1,cnt{0} do", localVarCounter));
					subField = DecodeField(col[0]);
					pf.lines.Add(string.Format("\tkey{0} = {1}", localVarCounter, subField.rightValue));
					subField = DecodeField(col[1]);
					foreach (string line in subField.lines)
						pf.lines.Add("\t" + line);
					pf.lines.Add(string.Format("\tm{0}[key{0}] = {1}", localVarCounter, subField.rightValue));
					pf.lines.Add("end");
					break;

				case "alist":
					pf.lines.Add(string.Format("local m{0} = {{}}", localVarCounter));
					pf.lines.Add(string.Format("local cnt{0} = packet:Read{1}()", localVarCounter, lenBytes == 1 ? "Byte" : "UShort"));
					pf.lines.Add(string.Format("for i=1,cnt{0} do", localVarCounter));
					localVarCounter2 = varCounter++;
					pf.lines.Add(string.Format("\tlocal m{0} = {{}}", localVarCounter2));
					for (int i = 0; i < col.Count; i++)
					{
						subField = DecodeField(col[i]);
						foreach (string line in subField.lines)
							pf.lines.Add(line);
						pf.lines.Add(string.Format("\tm{0}.{1} = {2}", localVarCounter2, col[i].GetAttribute("id"), subField.rightValue));
					}
					pf.lines.Add(string.Format("\ttable.insert(m{0}, m{1})", localVarCounter, localVarCounter2));
					pf.lines.Add("end");
					break;

				case "amap":
					pf.lines.Add(string.Format("local m{0} = {{}}", localVarCounter));
					pf.lines.Add(string.Format("local cnt{0} = packet:Read{1}()", localVarCounter, lenBytes == 1 ? "Byte" : "UShort"));
					pf.lines.Add(string.Format("for i=1,cnt{0} do", localVarCounter));
					localVarCounter2 = varCounter++;
					pf.lines.Add(string.Format("\tlocal m{0} = {{}}", localVarCounter2));
					for (int i = 0; i < col.Count; i++)
					{
						subField = DecodeField(col[i]);
						foreach (string line in subField.lines)
							pf.lines.Add(line);
						pf.lines.Add(string.Format("\tm{0}.{1} = {2}", localVarCounter2, col[i].GetAttribute("id"), subField.rightValue));
					}
					pf.lines.Add(string.Format("\tm{0}[m{1}[{2}]] = m{1}", localVarCounter, localVarCounter2, pf.key));
					pf.lines.Add("end");
					break;

				default:// "attr" "attrs":
					pf.lines.Add(string.Format("local m{0} = {{}}", localVarCounter));
					for (int i = 0; i < col.Count; i++)
					{
						subField = DecodeField(col[i]);
						condType = null;

						string cond = col[i].GetAttribute("cond", "");
						if (cond.Length > 0)
						{
							int pos;
							if ((pos = cond.IndexOf(">=")) != -1)
							{
								condType = ">=";
								condField = cond.Substring(0, pos).Trim();
								condValue = cond.Substring(pos + 2).Trim();
							}
							else if ((pos = cond.IndexOf("<=")) != -1)
							{
								condType = "<=";
								condField = cond.Substring(0, pos).Trim();
								condValue = cond.Substring(pos + 2).Trim();
							}
							else if ((pos = cond.IndexOf("!=")) != -1 || (pos = cond.IndexOf("<>")) != -1)
							{
								condType = "~=";
								condField = cond.Substring(0, pos).Trim();
								condValue = cond.Substring(pos + 2).Trim();
							}
							else if ((pos = cond.IndexOf("==")) != -1)
							{
								condType = "==";
								condField = cond.Substring(0, pos).Trim();
								condValue = cond.Substring(pos + 2).Trim();
							}
							else if ((pos = cond.IndexOf(">")) != -1)
							{
								condType = ">";
								condField = cond.Substring(0, pos).Trim();
								condValue = cond.Substring(pos + 1).Trim();
							}
							else if ((pos = cond.IndexOf("<")) != -1)
							{
								condType = "<";
								condField = cond.Substring(0, pos).Trim();
								condValue = cond.Substring(pos + 1).Trim();
							}
							else if ((pos = cond.IndexOf("=")) != -1)
							{
								condType = "==";
								condField = cond.Substring(0, pos).Trim();
								condValue = cond.Substring(pos + 1).Trim();
							}
						}

						if (condType != null)
						{
							pf.lines.Add(string.Format("if m{0}.{1}{2}{3} then", localVarCounter, condField, condType, condValue));

							foreach (string line in subField.lines)
								pf.lines.Add("\t" + line);
							pf.lines.Add(string.Format("\tm{0}.{1} = {2}", localVarCounter, col[i].GetAttribute("id"), subField.rightValue));

							pf.lines.Add("end");
						}
						else
						{
							foreach (string line in subField.lines)
								pf.lines.Add(line);
							pf.lines.Add(string.Format("m{0}.{1} = {2}", localVarCounter, col[i].GetAttribute("id"), subField.rightValue));
						}
					}
					break;
			}
		}

		return pf;
	}
}
