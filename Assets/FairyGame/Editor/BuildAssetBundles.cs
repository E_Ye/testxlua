﻿using FairyGame;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;

public class BuildAssetBundles
{
	public static string ASSET_PATH = "Assets/ABResources/";
	public static string[] ASSET_DIRS = new string[] { "user", "npc", "mount", "pet", "magic", "effect", "sound", "icon", "ui" };

	[MenuItem("FairyGame/打包资源/Android %L", false, 0)]
	public static void BuildBundlesAndroid()
	{
        Debug.Log(Application.streamingAssetsPath + "/Res");
		BuildPipeline.BuildAssetBundles(Application.streamingAssetsPath + "/Res",
			BuildAssetBundleOptions.None, BuildTarget.Android);
	}

    [MenuItem("FairyGame/打包资源/IOS %L", false, 0)]
    public static void BuildBundlesIOS()
    {
        Debug.Log(Application.streamingAssetsPath + "/Res");
        BuildPipeline.BuildAssetBundles(Application.streamingAssetsPath + "/Res",
            BuildAssetBundleOptions.None, BuildTarget.iOS);
    }
    [MenuItem("FairyGame/打包资源/UWP %L", false, 0)]
    public static void BuildBundlesUWP()
    {
        Debug.Log(Application.streamingAssetsPath + "/Res");
        BuildPipeline.BuildAssetBundles(Application.streamingAssetsPath + "/Res",
            BuildAssetBundleOptions.None, BuildTarget.WSAPlayer);
    }

    [MenuItem("FairyGame/设置选定资源的AssetBundle名称", false, 1)]
	public static void SetAssetBundleNames()
	{
		Object[] selection = Selection.GetFiltered(typeof(Object), SelectionMode.DeepAssets);
		foreach (Object go in selection)
		{
			if (go is DefaultAsset) //directory
				continue;

			string assetPath = AssetDatabase.GetAssetPath(go);
			if (!assetPath.StartsWith(ASSET_PATH))
				continue;

			AssetImporter importer = AssetImporter.GetAtPath(assetPath);
			string subName = RemoveFileExt(assetPath.Substring(ASSET_PATH.Length));
			if (subName.Length == 0)
				continue;

			string assetType = subName.Substring(0, subName.IndexOf("/"));
			switch (assetType)
			{
				case "user":
				case "npc":
				case "effect":
				case "mount":
				case "magic":
				case "pet":
					{
						int k = subName.IndexOf("!");
						if (k == -1)
							importer.assetBundleName = subName;
						else
							importer.assetBundleName = subName.Substring(0, k);
						if (importer is TextureImporter)
						{
							TextureImporter ti = (TextureImporter)importer;
							ti.maxTextureSize = 2048;
							ti.mipmapEnabled = false;
							ti.wrapMode = TextureWrapMode.Clamp;
							ti.filterMode = FilterMode.Bilinear;
							ti.textureShape = TextureImporterShape.Texture2D;
#if UNITY_5_5_OR_NEWER
							ti.textureCompression = TextureImporterCompression.Compressed;
#else
							ti.textureFormat = TextureImporterFormat.AutomaticCompressed;
#endif
							importer.SaveAndReimport();
						}
						break;
					}

				case "map":
					{
						int k = subName.LastIndexOf("/");
						importer.assetBundleName = subName.Substring(0, k);
						if (importer is TextureImporter)
						{
							TextureImporter ti = (TextureImporter)importer;
							ti.maxTextureSize = 2048;
							ti.mipmapEnabled = false;
							ti.wrapMode = TextureWrapMode.Clamp;
							if (go.name == "thumb")
							{
								ti.filterMode = FilterMode.Bilinear;
								ti.npotScale = TextureImporterNPOTScale.None;
							}
							else
								ti.filterMode = FilterMode.Point;
#if UNITY_5_5_OR_NEWER
							ti.textureCompression = TextureImporterCompression.Compressed;
#else
							ti.textureFormat = TextureImporterFormat.AutomaticCompressed;
#endif
							ti.SaveAndReimport();
						}
						break;
					}

				case "ui":
                case "ui1":
					{
						int k = subName.IndexOf("@");
						if (k == -1)
							importer.assetBundleName = subName;
						else
							importer.assetBundleName = subName.Substring(0, k) + "_res";
					}
					break;

				case "icon":
					{
						importer.assetBundleName = subName;
						if (importer is TextureImporter)
						{
							TextureImporter ti = (TextureImporter)importer;
							ti.maxTextureSize = 256;
							ti.mipmapEnabled = false;
							ti.wrapMode = TextureWrapMode.Clamp;
							ti.filterMode = FilterMode.Bilinear;
#if UNITY_5_5_OR_NEWER
							ti.textureCompression = TextureImporterCompression.CompressedHQ;
#else
							ti.textureFormat = TextureImporterFormat.RGBA16;
#endif
							ti.npotScale = TextureImporterNPOTScale.None;
							ti.SaveAndReimport();
						}
					}
					break;

				default:
					importer.assetBundleName = subName;
					break;
			}
		}

		AssetDatabase.Refresh();
	}

	public static string RemoveFileExt(string source)
	{
		int i = source.LastIndexOf(".");
		if (i != -1)
			return source.Substring(0, i);
		else
			return source;
	}
    
}