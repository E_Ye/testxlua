﻿
namespace FairyGame
{
	public class GroundTest : IPathValueTest
	{
		public static GroundTest inst = new GroundTest();

		public bool IsAlpha(PathValue pv)
		{
			return pv.value == 1;
		}

		public bool IsBlock(PathValue pv)
		{
			return pv.value >= 2;
		}

		public bool IsSkillThrough(PathValue pv)
		{
			return pv.value <= 2 || pv.value == 7;
		}

		public bool IsBlockForPathFinder(PathValue pv)
		{
			if (pv.tag == 20)
				return false;
			else
				return pv.value >= 2;
		}

		public bool IsSameArea(PathValue pv1, PathValue pv2)
		{
			return pv1.area == pv2.area;
		}

		public bool IsSameAreaForPathFinder(PathValue pv1, PathValue pv2)
		{
			return pv1.realArea == pv2.realArea;
		}
	}
}
