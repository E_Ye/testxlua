﻿
namespace FairyGame
{
	public interface IPathValueTest
	{
		bool IsAlpha(PathValue pv);
		bool IsSkillThrough(PathValue pv);
		bool IsBlock(PathValue pv);
		bool IsBlockForPathFinder(PathValue pv);
		bool IsSameArea(PathValue pv1, PathValue pv2);
		bool IsSameAreaForPathFinder(PathValue pv1, PathValue pv2);
	}
}
