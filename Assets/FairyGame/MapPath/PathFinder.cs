﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FairyGame
{
	public class PathFinder
	{
		private const int HV_COST = 10;// "Movement cost" for horizontal/vertical moves
		private const int D_COST = 10;// "Movement cost" for diagonal moves

		private PathInfo _pathInfo;
		private List<Vector2> _openList;
		private List<List<PathValue>> _pathData;

		private uint _version;

		public PathFinder(PathInfo pathInfo)
		{
			_openList = new List<Vector2>();
			_pathInfo = pathInfo;
		}

		public bool FindPath(int startX, int startY, int endX, int endY, ArrayList returnPath, IPathValueTest test)
		{
			_pathData = _pathInfo.pathData;
			int pathH = _pathInfo.pathHeight;
			int pathW = _pathInfo.pathWidth;

			_version++;
			_openList.Clear();

			_openList.Add(new Vector2(startX, startY));

			while (_openList.Count > 0 && !IsClosed(endY, endX))
			{
				Vector2 now = _openList[0];
				int nowX = (int)now.x;
				int nowY = (int)now.y;

				// Closes current square as it has done its purpose...
				PathValue nms = _pathData[nowY][nowX];
				nms.ver = _version;
				nms.openIndex = -1;
				ShiftOpenList();

				// Opens all nearby squares, ONLY if:
				for (int j = nowY - 1; j < nowY + 2; j++)
				{
					for (int k = nowX - 1; k < nowX + 2; k++)
					{
						if (j >= 0
							&& j < pathH
							&& k >= 0
							&& k < pathW
							&& !(j == nowY && k == nowX)
							&& (j == nowY || k == nowX || !test.IsBlockForPathFinder(_pathData[j][nowX]) && !test.IsBlockForPathFinder(_pathData[nowY][k]))
						)
						{
							// If not outside the boundaries or at the same point or a diagonal (if disabled) or a diagonal (with a wall next to it)...
							if (!test.IsBlockForPathFinder(_pathData[j][k]))
							{
								// And if not a wall...
								if (!IsClosed(j, k))
								{
									int dir = k == nowX ? 0 : (j == nowY ? 2 : 1);
									int G = nms.G + (dir == 2 ? HV_COST : D_COST);
									// And if not closed... THEN open.
									PathValue ms = _pathData[j][k];
									if (IsOpen(j, k))
									{
										// Already opened: check if it's ok to re-open (cheaper)
										if (G < ms.G)
										{
											// Cheaper: simply replaces with new cost and parent.
											ms.parent = now;
											ms.F = G + ms.H;
											ms.G = G;

											ResortOpenList(ms.openIndex + 1);
										}
									}
									else
									{
										// Empty: open.
										int H = (Math.Abs(j - endY) + Math.Abs(k - endX)) * 10;

										_openList.Add(new Vector2(k, j));
										ms.parent = now;
										ms.G = G;
										ms.H = H;
										ms.F = G + H;
										ms.openIndex = _openList.Count - 1;
										ms.ver = _version;

										ResortOpenList(_openList.Count);
									}
								}

							}
							else
							{
								// Already closed, ignore.
							}
						}
						else
						{
							// Wall, ignore.
						}
					}
				}
			}
			bool pFound = IsClosed(endY, endX);

			if (pFound)
			{
				returnPath.Clear();
				Vector2 pt = new Vector2(endX, endY);
				while (pt.y != startY || pt.x != startX)
				{
					returnPath.Add(pt);
					pt = _pathData[(int)pt.y][(int)pt.x].parent;
				}
				returnPath.Reverse();
				return true;

			}
			else
			{
				return false;
			}
		}    
		private bool IsOpen(int y, int x)
		{
			PathValue pv = _pathData[y][x];
			return pv.ver == _version && pv.openIndex != -1;
		}

		private bool IsClosed(int y, int x)
		{
			PathValue pv = _pathData[y][x];
			return pv.ver == _version && pv.openIndex == -1;
		}

		private void ShiftOpenList()
		{
			if(_openList.Count>1)
			{
				int last = _openList.Count - 1;
				_openList[0] = _openList[last];
				_openList.RemoveAt(last);
			}
			_pathData[(int)_openList[0].y][(int)_openList[0].x].openIndex = 0;

			int checkIndex = 1;
			int tmp;
			while (true)
			{
				tmp = checkIndex;
				if (2 * tmp <= _openList.Count)
				{
					if (_pathData[(int)_openList[checkIndex - 1].y][(int)_openList[checkIndex - 1].x].F > _pathData[(int)_openList[2 * tmp - 1].y][(int)_openList[2 * tmp - 1].x].F)
					{
						checkIndex = 2 * tmp;
					}
					if (2 * tmp + 1 <= _openList.Count)
					{
						if (_pathData[(int)_openList[checkIndex - 1].y][(int)_openList[checkIndex - 1].x].F > _pathData[(int)_openList[2 * tmp].y][(int)_openList[2 * tmp].x].F)
						{
							checkIndex = 2 * tmp + 1;
						}
					}
				}
				if (tmp == checkIndex)
				{
					break;
				}
				else
				{
					Vector2 d1 = _openList[tmp - 1];
					Vector2 d2 = _openList[checkIndex - 1];
					_openList[tmp - 1] = d2;
					_openList[checkIndex - 1] = d1;
					_pathData[(int)d1.y][(int)d1.x].openIndex = checkIndex - 1;
					_pathData[(int)d2.y][(int)d2.x].openIndex = tmp - 1;
				}
			}
		}

		private void ResortOpenList(int p_index)
		{
			int father;
			while (p_index > 1)
			{
				father = Mathf.FloorToInt((float)p_index / 2);
				Vector2 d1 = _openList[p_index - 1];
				Vector2 d2 = _openList[father - 1];
				PathValue status1 = _pathData[(int)d1.y][(int)d1.x];
				PathValue status2 = _pathData[(int)d2.y][(int)d2.x];
				if (status1.F < status2.F)
				{
					_openList[p_index - 1] = d2;
					_openList[father - 1] = d1;
					status1.openIndex = father - 1;
					status2.openIndex = p_index - 1;
					p_index = father;
				}
				else
				{
					break;
				}
			}
		}
	}
}
