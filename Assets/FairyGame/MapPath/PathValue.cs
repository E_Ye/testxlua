﻿using UnityEngine;
using System.Collections;

namespace FairyGame
{
	public class PathValue
	{
		public int area;
		public int realArea; //用tag20联通的区域被合并后的新编号
		public int tag;
		public int value;//0-通行，1-透明，2-低障碍，3-高障碍
		public int value2; //保存用途
		public int env; //0-主场景，>0子场景

		public int projectionTag; //投影值
		public int projectionValue; //投影值

		//用于寻径的值
		public Vector2 parent;
		public int G;
		public int H;
		public int F;
		public int openIndex;
		public uint ver;

		public PathValue()
		{
		}

		public int tagAndValue
		{
			get { return (tag << 3) + value; }
			set
			{
				if (value == 255)
				{
					tag = 31;
					value = 7;
				}
				else
				{
					tag = (value >> 3) & 0x1F;
					value = value & 0x7;
				}
			}
		}

		public int projectionTagAndValue
		{
			get { return (projectionTag << 3) + projectionValue; }
			set
			{
				if (value == 255)
				{
					projectionTag = 31;
					projectionValue = 7;
				}
				else
				{
					projectionTag = (value >> 3) & 0x1F;
					projectionValue = value & 0x7;
				}
			}
		}
	}
}