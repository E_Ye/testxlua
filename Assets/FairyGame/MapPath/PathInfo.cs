﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FairyGUI.Utils;
using FairyGUI;

namespace FairyGame
{
	public class PathInfo
	{
		public int mapWidth;
		public int mapHeight;
		public int pathWidth;
		public int pathHeight;
		public Vector2 basePoint;
		public List<List<PathValue>> pathData;
		public float[][] cellXYWeight;//表示在8个方向上，物理坐标的XY分量
		public IPathValueTest testProvider;

		private int _version;
		private int _connectedY;
		private float _scale;
		private int _cellHeight;
		private int _cellWidth;
		private float _cellDLen;
		private float _diamondAngle;
		private float _const1;
		private float _const2;
		private bool _diamond;

		private Rect _projectionRect;
		private PathFinder _pathFinder;
		private LineGenerator _lineGenerator;

		private int _decodeStep;
		private int _decodeI;
		private int _decodeJ;
		private ByteArray _decodeData;

		public const int CUR_VERSION = 3;

		public static int cfg_cellWidth = 40;
		public static int cfg_cellHeight = 20;
		public static bool cfg_cellDiamond = true;

		public static float cfg_decodeBatchTime = 0.05f;

		public PathInfo()
		{
			_pathFinder = new PathFinder(this);
			_lineGenerator = new LineGenerator();
			_scale = 1;
			_projectionRect = new Rect();
			basePoint = new Vector2();
			pathData = new List<List<PathValue>>();
			testProvider = GroundTest.inst;
			ConfigCell(cfg_cellWidth, cfg_cellHeight, cfg_cellDiamond);
		}

		public void Clear()
		{
			mapWidth = 0;
			mapHeight = 0;
			pathWidth = 0;
			pathHeight = 0;
		}

		public void ConfigCell(int cellWidth, int cellHeight, bool diamond = true)
		{
			_cellHeight = cellHeight;
			_cellWidth = cellWidth;
			_diamond = diamond;
			_cellDLen = Mathf.Sqrt(Mathf.Pow(_cellWidth, 2) + Mathf.Pow(_cellHeight, 2)) / 2;
			_diamondAngle = Mathf.Atan((float)_cellWidth / _cellHeight);
			float cos = Mathf.Cos(_diamondAngle);
			float sin = Mathf.Sin(_diamondAngle);
			cellXYWeight = new float[][] { new float[] {0,0},
				new float[] {0, -1}, new float[] {sin, -cos}, new float[] {1, 0}, new float[] {sin, cos},
				new float[] {0, 1}, new float[] {-sin, cos}, new float[] {-1, 0}, new float[] {-sin, -cos}};
			_const1 = Mathf.Pow((float)_cellWidth / 2 / _cellDLen, 2);
			_const2 = Mathf.Pow((float)_cellHeight / 2 / _cellDLen, 2);
		}

		public Vector2 GetRealPosition(Vector2 pos)
		{
			if (_diamond)
			{
				return new Vector2((int)((pos.x - (pos.y - _connectedY)) * _cellWidth / 2 * _scale),
				 (int)((pos.x + (pos.y - _connectedY)) * _cellHeight / 2 * _scale));
			}
			else
			{
				return new Vector2((int)((pos.x * _cellWidth + _cellWidth / 2) * _scale),
				(int)(pos.y * _cellHeight + _cellHeight / 2 * _scale));
			}
		}

		public Vector2 GetPathPosition(Vector2 pos)
		{
			if (_diamond)
			{
				return new Vector2(Mathf.Ceil(pos.x / _cellWidth - 0.5f + pos.y / _cellHeight),
				_connectedY - Mathf.Ceil(pos.x / _cellWidth - 0.5f - pos.y / _cellHeight));
			}
			else
			{
				return new Vector2((int)(pos.x / _cellWidth), (int)(pos.y / _cellHeight));
			}
		}

		public int GetPathDistance(Vector2 pt1, Vector2 pt2)
		{
			float dx = pt1.x - pt2.x;
			float dy = pt1.y - pt2.y;
			return (int)Mathf.Sqrt(_const1 * Mathf.Pow(dx - dy, 2) + _const2 * Mathf.Pow(dx + dy, 2));
		}

		public bool TestOutside(Vector2 pos)
		{
			if (_diamond)
			{
				int rx, ry;
				rx = (int)((pos.x - (pos.y - _connectedY)) * _cellWidth / 2);
				ry = (int)((pos.x + (pos.y - _connectedY)) * _cellHeight / 2);
				if (rx - _cellWidth / 2 < 0 || rx + _cellWidth / 2 > mapWidth
					|| ry - _cellHeight / 2 < 0 || ry + _cellHeight / 2 > mapHeight)
				{
					return true;
				}
				else
					return false;
			}
			else
			{
				return pos.x < 0 || pos.x >= pathWidth - 1 || pos.y < 0 || pos.y >= pathHeight - 1;
			}
		}

		public int CheckDistance(Vector2 pt1, Vector2 pt2, int dist)
		{
			int dist1 = GetPathDistance(pt1, pt2);
			if (dist1 == dist)
				return 0;
			else if (dist1 < dist)
				return -1;
			else
				return 1;
		}

		public bool IsSkillReachable(Vector2 src, Vector2 target, int dist)
		{
			int dist1 = GetPathDistance(src, target);
			if (dist1 <= dist)
				return IsSkillThroughLine(src, target);
			else
				return false;
		}

		public bool IsSkillThroughLine(Vector2 src, Vector2 target)
		{
			_lineGenerator.Start(src, target);
			while (_lineGenerator.HasNext)
			{
				Vector2 pt = _lineGenerator.Next();
				if (!IsSkillThrough(pt))
				{
					return false;
				}
			}
			return true;
		}

		public bool IsReachable(Vector2 src, Vector2 target)
		{
			_lineGenerator.Start(src, target);
			while (_lineGenerator.HasNext)
			{
				Vector2 pt = _lineGenerator.Next();
				if (IsBlock(pt))
				{
					return false;
				}
			}
			return true;
		}

		public PathValue GetPathValue(Vector2 pos)
		{
			if (pos.x >= 0 && pos.y >= 0 && pos.x < pathWidth && pos.y < pathHeight)
			{
				PathValue pv = pathData[(int)pos.y][(int)pos.x];
				return pv;
			}
			else
				return null;
		}

		public bool IsBlock(Vector2 pos)
		{
			PathValue pv = GetPathValue(pos);
			if (pv != null)
				return testProvider.IsBlock(pv);
			else
				return true;
		}

		public bool IsSkillThrough(Vector2 pos)
		{
			PathValue pv = GetPathValue(pos);
			if (pv != null)
				return testProvider.IsSkillThrough(pv);
			else
				return true;
		}

		public bool IsAlpha(Vector2 pos)
		{
			PathValue pv = GetPathValue(pos);
			if (pv != null)
				return testProvider.IsAlpha(pv);
			else
				return false;
		}

		public bool IsSameArea(Vector2 pos1, Vector2 pos2)
		{
			PathValue pv1 = GetPathValue(pos1);
			PathValue pv2 = GetPathValue(pos2);
			if (pv1 == null || pv2 == null)
				return false;

			return testProvider.IsSameArea(pv1, pv2);
		}

		public bool FindPath(Vector2 origin, Vector2 terminal, ArrayList returnPath)
		{
			PathValue pv1 = GetPathValue(origin);
			if (pv1 == null || testProvider.IsBlock(pv1))
				return false;

			Vector2 t = terminal;
			PathValue pv2 = GetPathValue(terminal);
			List<Vector2> path = null;
			if (pv2 == null || testProvider.IsBlockForPathFinder(pv2) || !testProvider.IsSameAreaForPathFinder(pv1, pv2))
			{
				path = new List<Vector2>();
				_lineGenerator.Start(terminal, origin);
				PathValue pvCur;

				while (_lineGenerator.HasNext)
				{
					Vector2 pt = _lineGenerator.Next();
					pvCur = GetPathValue(pt);
					if (pvCur != null
						&& !testProvider.IsBlock(pvCur)
						&& testProvider.IsSameAreaForPathFinder(pv1, pvCur))
					{
						t = pt;
						break;
					}

					path.Add(pt);
				}
			}

			if (origin.x == t.x && origin.y == t.y)
			{
				if (path != null && path.Count > 0)
				{
					for (int i = path.Count - 1; i >= 0; i--)
						returnPath.Add(path[i]);
					return true;
				}
				else
					return false;
			}

			bool ret = _pathFinder.FindPath((int)origin.x, (int)origin.y, (int)t.x, (int)t.y, returnPath, testProvider);
			if (ret)
			{
				if (path != null)
				{
					for (int i = path.Count - 1; i >= 0; i--)
						returnPath.Add(path[i]);
				}
				return true;
			}
			else
				return false;
		}

		public Vector2 GetNearest(Vector2 origin, Vector2 terminal)
		{
			PathValue pvOrigin = GetPathValue(origin);
			if (pvOrigin == null)
				return origin;

			_lineGenerator.Start(terminal, origin);

			PathValue pvCur;
			while (_lineGenerator.HasNext)
			{
				Vector2 pt = _lineGenerator.Next();
				pvCur = GetPathValue(pt);
				if (pvCur != null
					&& !testProvider.IsBlock(pvCur)
					&& testProvider.IsSameAreaForPathFinder(pvOrigin, pvCur))
					return pt;
			}

			return origin;
		}

		public Vector2? FindReachable(Vector2 pos)
		{
			int i, j;
			List<PathValue> a;
			if (pos.x >= pathWidth)
				pos.x = pathWidth - 1;
			if (pos.y >= pathHeight)
				pos.y = pathHeight - 1;
			PathValue pvOrigin = GetPathValue(pos);
			PathValue pvCur;
			for (i = (int)pos.y; i >= 0; i--)
			{
				a = pathData[i];
				for (j = (int)pos.x; j >= 0; j--)
				{
					pvCur = a[j];
					if (!testProvider.IsBlock(pvCur)
						&& testProvider.IsSameAreaForPathFinder(pvOrigin, pvCur))
						return new Vector2(j, i);
				}
				for (j = (int)pos.x + 1; j < pathWidth; j++)
				{
					pvCur = a[j];
					if (!testProvider.IsBlock(pvCur)
						&& testProvider.IsSameAreaForPathFinder(pvOrigin, pvCur))
						return new Vector2(j, i);
				}
			}

			for (i = (int)pos.y + 1; i < pathHeight; i++)
			{
				a = pathData[i];
				for (j = (int)pos.x; j >= 0; j--)
				{
					pvCur = a[j];
					if (!testProvider.IsBlock(pvCur)
						&& testProvider.IsSameAreaForPathFinder(pvOrigin, pvCur))
						return new Vector2(j, i);
				}
				for (j = (int)pos.x + 1; j < pathWidth; j++)
				{
					pvCur = a[j];
					if (!testProvider.IsBlock(pvCur)
						&& testProvider.IsSameAreaForPathFinder(pvOrigin, pvCur))
						return new Vector2(j, i);
				}
			}

			return null;
		}

		//找出从origin到terminal的连线中，找出距离terminal小于distance的最近的点，不理会是否有障碍
		public Vector2 FindPoint(Vector2 origin, Vector2 terminal, int distance)
		{
			float totalDistance = GetPathDistance(origin, terminal);
			_lineGenerator.Start(terminal, origin);
			distance = (int)((float)distance / totalDistance * _lineGenerator.Count);
			_lineGenerator.Skip(distance - 1);
			return _lineGenerator.Next();
		}

		private bool iClockWise;
		public Vector2 GetForwardPoint(Vector2 current, int dir)
		{
			Vector2 pt = GetNearbyCell(current, dir, 1);
			if (IsBlock(pt))
			{
				int dir2;
				if (iClockWise)
				{
					dir2 = UtilsDir.GetNextDirection(dir);
					pt = GetNearbyCell(current, dir2, 1);
					if (IsBlock(pt))
					{
						dir2 = UtilsDir.GetNextDirection(dir2);
						pt = GetNearbyCell(current, dir2, 1);
						if (IsBlock(pt))
						{
							dir2 = UtilsDir.GetPrevDirection(dir);
							pt = GetNearbyCell(current, dir2, 1);
							if (IsBlock(pt))
							{
								dir2 = UtilsDir.GetPrevDirection(dir2);
								pt = GetNearbyCell(current, dir2, 1);
								if (IsBlock(pt))
									return current;

								iClockWise = false;
							}
							else
								iClockWise = false;
						}
					}
				}
				else
				{
					dir2 = UtilsDir.GetPrevDirection(dir);
					pt = GetNearbyCell(current, dir2, 1);
					if (IsBlock(pt))
					{
						dir2 = UtilsDir.GetPrevDirection(dir2);
						pt = GetNearbyCell(current, dir2, 1);
						if (IsBlock(pt))
						{
							dir2 = UtilsDir.GetNextDirection(dir);
							pt = GetNearbyCell(current, dir2, 1);
							if (IsBlock(pt))
							{
								dir2 = UtilsDir.GetNextDirection(dir2);
								pt = GetNearbyCell(current, dir2, 1);
								if (IsBlock(pt))
									return current;

								iClockWise = true;
							}
							else
								iClockWise = true;
						}
					}
				}
			}
			return pt;
		}

		public void MergePath(PathInfo source, int sourceEnv, Rect sourceRect, Vector2 destOffset)
		{
			int ox = (int)destOffset.x;
			int oy = (int)destOffset.y;

			Rect tmpRect = new Rect();
			tmpRect.width = source.pathWidth;
			tmpRect.height = source.pathHeight;
			Rect rect = ToolSet.Intersection(ref tmpRect, ref sourceRect);
			rect.x += ox;
			rect.y += oy;
			tmpRect.width = pathWidth;
			tmpRect.height = pathHeight;
			rect = ToolSet.Intersection(ref rect, ref tmpRect);

			int lx = (int)rect.x;
			int ly = (int)rect.y;
			int rx = (int)rect.xMax;
			int ry = (int)rect.yMax;
			int i, j;
			PathValue pvSource, pvMe; //pv1 source的值，pv2 本地图的值
			int vSrc, vMe;
			List<PathValue> a, b;

			for (i = ly; i < ry; i++)
			{
				a = source.pathData[i - oy];
				b = pathData[i];

				for (j = lx; j < rx; j++)
				{
					pvSource = a[j - ox];
					pvMe = b[j];
					vSrc = pvSource.value;
					vMe = pvMe.value;

					if (vSrc == 7 || vMe == 7) //有一方超出边界
						continue;

					if (pvMe.env != 0 && pvMe.env != sourceEnv)
					{//已经被其他子场景占有
						if (vMe == 0 || vMe == 1 && vSrc > 1)
						{ //可通行或障碍
						  //先恢复
							vMe = pvMe.value2;
							pvMe.value = vMe;
							pvMe.env = 0;
						}
						else
							continue;
					}

					//保存旧值
					pvMe.value2 = vMe;
					pvMe.env = sourceEnv;

					//合并障碍					
					if (vMe == 2)
					{ //本地图是低障碍
						if (pvSource.projectionValue == 3) //投影层是更高的障碍
							pvMe.value = 3;
					}
					else if (vMe == 1)
					{ //本地图是透明区域
						if (pvSource.projectionValue != 0)
							pvMe.value = pvSource.projectionValue;
					}
					else if (vMe != 3)
					{ //本地图是通行区域
						pvMe.value = pvSource.projectionValue;
					}
				}
			}
		}

		public void RecoverPath(int sourceEnv, Rect? bounds)
		{
			Rect tmpRect = new Rect(0, 0, pathWidth, pathHeight);
			Rect rect;
			if (bounds == null)
				rect = tmpRect;
			else
			{
				rect = (Rect)bounds;
				rect = ToolSet.Intersection(ref tmpRect, ref rect);
			}

			int lx = (int)rect.x;
			int ly = (int)rect.y;
			int rx = (int)rect.xMax;
			int ry = (int)rect.yMax;
			int i, j;
			PathValue pvMe;
			List<PathValue> a;
			for (i = ly; i < ry; i++)
			{
				a = pathData[i];
				for (j = lx; j < rx; j++)
				{
					pvMe = a[j];
					if (sourceEnv == 0)
					{
						if (pvMe.env != 0)
						{
							pvMe.value = pvMe.value2;
							pvMe.env = 0;
						}
					}
					else if (pvMe.env == sourceEnv)
					{ //是sourceEnv改的才恢复
						pvMe.value = pvMe.value2;
						pvMe.env = 0;
					}
				}
			}
		}

		public void CopySelfToProjection()
		{
			int i, j;
			List<PathValue> array;
			PathValue pv;
			for (i = 0; i < pathHeight; i++)
			{
				array = pathData[i];
				for (j = 0; j < pathWidth; j++)
				{
					pv = array[j];
					if (pv != null)
					{
						pv.projectionTag = pv.tag;
						pv.projectionValue = pv.value;
					}
				}
			}
		}

		public void CreateByRef(PathInfo source)
		{
			_version = source._version;
			ConfigCell(source._cellWidth, source._cellHeight);
			mapWidth = source.mapWidth;
			mapHeight = source.mapHeight;
			pathWidth = source.pathWidth;
			pathHeight = source.pathHeight;
			_connectedY = source._connectedY;
			basePoint.x = source.basePoint.x;
			basePoint.y = source.basePoint.y;
			pathData = source.pathData;
		}

		public void AllocMemory(int requestWidth, int requestHeight)
		{
			if (pathData.Count < requestHeight)
			{
				if (pathData.Capacity < requestHeight)
					pathData.Capacity = requestHeight;
				for (int j = pathData.Count; j < requestHeight; j++)
					pathData.Add(null);
			}

			List<PathValue> array;
			for (int i = 0; i < requestHeight; i++)
			{
				array = pathData[i];
				if (array == null)
				{
					array = new List<PathValue>(requestWidth);
					pathData[i] = array;
				}
				else if (array.Capacity < requestWidth)
				{
					array.Capacity = requestWidth;
				}
				for (int j = array.Count; j < requestWidth; j++)
					array.Add(null);
			}
		}

		public void Resize(int mapWidth, int mapHeight, bool zeroData = false)
		{
			this.mapWidth = mapWidth;
			this.mapHeight = mapHeight;
			if (_diamond)
			{
				int ch1 = (int)Mathf.Ceil((float)this.mapWidth / (2 * Mathf.Sin(_diamondAngle)) / _cellDLen);
				int ch2 = (int)Mathf.Ceil((float)this.mapHeight / (2 * Mathf.Cos(_diamondAngle)) / _cellDLen);
				pathWidth = ch1 + ch2 + 1;
				pathHeight = pathWidth;
				_connectedY = ch1;
			}
			else
			{
				pathWidth = (int)Mathf.Ceil((float)this.mapWidth / _cellWidth);
				pathHeight = (int)Mathf.Ceil((float)this.mapHeight / _cellHeight);
				_connectedY = 0;
			}
			AllocMemory(pathWidth, pathHeight);

			int i, j;
			List<PathValue> array;
			PathValue pv;
			for (i = 0; i < pathHeight; i++)
			{
				array = pathData[i];
				for (j = 0; j < pathWidth; j++)
				{
					pv = array[j];
					if (pv == null)
					{
						pv = new PathValue();
						array[j] = pv;
					}

					if (TestOutside(new Vector2(j, i)))
					{
						pv.tag = 31;
						pv.value = 7;
						pv.projectionTag = 31;
						pv.projectionValue = 7;
						pv.area = 0;
						pv.env = 0;
						pv.realArea = 0;
					}
					else
					{
						if (zeroData)
						{
							pv.tag = 0;
							pv.value = 0;
							pv.projectionTag = 0;
							pv.projectionValue = 0;
							pv.area = 0;
							pv.env = 0;
							pv.realArea = 0;
						}
						else if (pv.value == 7)
						{
							pv.tag = 0;
							pv.value = 0;
							pv.projectionTag = 0;
							pv.projectionValue = 0;
						}
					}
				}
			}
		}

		public void CutHalf()
		{
			int i, j;
			List<PathValue> array;
			List<PathValue> array2;
			PathValue pv;
			PathValue pv2;
			for (i = 0; i < pathHeight; i++)
			{
				array = pathData[i];
				j = 0;
				while (j < array.Count - 1)
				{
					pv = array[j];
					pv2 = array[j + 1];
					if (pv2.value < 2)
						pv.value = pv2.value;
					if (pv2.tag != 31)
						pv.tag = pv2.tag;
					array.RemoveAt(j + 1);
					j++;
				}
			}
			pathWidth /= 2;

			i = 0;
			while (i < pathData.Count - 1)
			{
				array = pathData[i];
				array2 = pathData[i + 1];
				for (j = 0; j < pathWidth; j++)
				{
					pv = array[j];
					pv2 = array2[j];
					if (pv2.value < 2)
						pv.value = pv2.value;
					if (pv2.tag != 31)
						pv.tag = pv2.tag;
					pathData.RemoveAt(i + 1);
					i++;
				}
			}
			pathHeight /= 2;

			this.Resize(Mathf.RoundToInt((float)mapWidth / 2), Mathf.RoundToInt((float)mapHeight / 2));
		}

		public int GetDirectionOfPath(Vector2 pt1, Vector2 pt2, bool nearby = false)
		{
			int ret;
			if (!nearby)
			{
				float x = pt2.x - pt1.x;
				float y = pt1.y - pt2.y;
				int deg = 90 - (int)(Mathf.Atan2(y, x) * UtilsDir.CONST_RAD_TO_DEG);
				if (deg > 180)
					deg -= 360;
				ret = UtilsDir.GetDirOfDeg(deg);
			}
			else
			{
				if (pt2.x == pt1.x && pt2.y < pt1.y)
					ret = 1;
				else if (pt2.x > pt1.x && pt2.y < pt1.y)
					ret = 2;
				else if (pt2.x > pt1.x && pt2.y == pt1.y)
					ret = 3;
				else if (pt2.x > pt1.x && pt2.y > pt1.y)
					ret = 4;
				else if (pt2.x == pt1.x && pt2.y > pt1.y)
					ret = 5;
				else if (pt2.x < pt1.x && pt2.y > pt1.y)
					ret = 6;
				else if (pt2.x < pt1.x && pt2.y == pt1.y)
					ret = 7;
				else if (pt2.x < pt1.x && pt2.y < pt1.y)
					ret = 8;
				else
					ret = 1;
			}

			if (_diamond)
				ret++;
			if (ret > 8)
				ret = 1;
			return ret;
		}

		public Vector2 GetNearbyCell(Vector2 pt, int dir, int distance = 1)
		{
			Vector2 ret = new Vector2();
			if (_diamond)
			{
				dir--;
				if (dir <= 0)
					dir = 8;
			}
			switch (dir)
			{
				case 1:
					ret.x = pt.x;
					ret.y = pt.y - distance;
					break;
				case 2:
					ret.x = pt.x + distance;
					ret.y = pt.y - distance;
					break;
				case 3:
					ret.x = pt.x + distance;
					ret.y = pt.y;
					break;
				case 4:
					ret.x = pt.x + distance;
					ret.y = pt.y + distance;
					break;
				case 5:
					ret.x = pt.x;
					ret.y = pt.y + distance;
					break;
				case 6:
					ret.x = pt.x - distance;
					ret.y = pt.y + distance;
					break;
				case 7:
					ret.x = pt.x - distance;
					ret.y = pt.y;
					break;
				case 8:
					ret.x = pt.x - distance;
					ret.y = pt.y - distance;
					break;
			}
			if (ret.x < 0)
				ret.x = 0;
			if (ret.y < 0)
				ret.y = 0;
			return ret;
		}

		public void DecodeAsync(ByteArray ba)
		{
			_decodeData = ba;
			_decodeStep = 0;

			Timers.inst.Add(0.05f, 0, _decodeAsync);
		}

		public void CancelAsyncDecode()
		{
			_decodeData = null;
			Timers.inst.Remove(_decodeAsync);
		}

		public bool asyncDecoding
		{
			get { return _decodeData != null; }
		}

		private void _decodeAsync(object param)
		{
			if (_decode(_decodeData, true))
			{
				Timers.inst.Remove(_decodeAsync);
				_decodeData = null;
			}
		}

		public void Decode(ByteArray ba)
		{
			_decode(ba);
		}

		private bool _decode(ByteArray ba, bool async = false)
		{
			List<PathValue> array;
			int c;
			PathValue pv;
			float startTime = 0;
			if (!async)
				_decodeStep = 0;
			else
				startTime = Time.time;

			if (_decodeStep == 0)
			{
				ba.position = 0;
				pathWidth = ba.ReadInt();
				pathHeight = ba.ReadInt();
				mapWidth = ba.ReadInt();
				mapHeight = ba.ReadInt();
				_connectedY = ba.ReadInt();
				_version = ba.ReadInt();
				basePoint.x = ba.ReadShort();
				basePoint.y = ba.ReadShort();
				int cw = ba.ReadByte();
				int ch = ba.ReadByte();
				int nd = ba.ReadByte();
				if (cw != 0 && ch != 0)
					ConfigCell(cw, ch, nd != 0 ? false : true);
				else
					ConfigCell(cfg_cellWidth, cfg_cellHeight, cfg_cellDiamond);
				for (int i = 0; i < 29; i++) //reserved
					ba.ReadByte();
				_projectionRect.Set(0, 0, 0, 0);

				AllocMemory(pathWidth, pathHeight);
				_decodeStep++;
				_decodeI = 0;
				_decodeJ = 0;
			}

			if (_decodeStep == 1)
			{
				for (; _decodeI < pathHeight; _decodeI++)
				{
					if (async && (Time.time - startTime > cfg_decodeBatchTime))
					{
						//Debug.Log(Time.time-startTime);
						return false;
					}

					array = pathData[_decodeI];
					for (; _decodeJ < pathWidth; _decodeJ++)
					{
						pv = array[_decodeJ];
						if (pv == null)
						{
							pv = new PathValue();
							array[_decodeJ] = pv;
						}

						pv.env = 0;
						if (_version == 0)
						{
							c = ba.ReadByte();
							pv.area = (c & 0xF0) >> 4;
							pv.realArea = pv.area;
							c = c & 0x0F;
							if (c == 15)
							{
								pv.tag = 31;
								pv.value = 7;
							}
							else
							{
								if (c == 14)
									c = 3;
								pv.tag = 0;
								pv.value = c;
							}
						}
						else
						{
							pv.area = ba.ReadByte();
							c = ba.ReadByte();
							if (c == 255)
							{
								pv.tag = 31;
								pv.value = 7;
							}
							else
							{
								pv.tag = (c >> 3) & 0x1F;
								pv.value = c & 0x7;
							}
						}
					}
					_decodeJ = 0;
				}
				_decodeStep++;
				_decodeI = 0;
				_decodeJ = 0;
			}

			if (_decodeStep == 2 && ba.bytesAvailable > 0)
			{
				if (_version > 0)
				{
					//int xmin, xmax, ymin, ymax;
					for (; _decodeI < pathHeight; _decodeI++)
					{
						if (async && (Time.time - startTime > cfg_decodeBatchTime))
						{
							//Debug.Log(Time.time-startTime);
							return false;
						}

						array = pathData[_decodeI];
						for (; _decodeJ < pathWidth; _decodeJ++)
						{
							pv = array[_decodeJ];
							if (_version > 1)
								pv.realArea = ba.ReadByte();
							else
							{
								pv.realArea = pv.area;
								ba.ReadByte(); //ignore
							}
							c = ba.ReadByte();
							/*if(c!=255 && c!=0) {
							if(j<xmin)
							xmin = j;
							if(j>xmax)
							xmax = j;
							if(i<ymin)
							ymin = i;
							if(i>ymax)
							ymax = i;
							}*/
							if (c == 255)
							{
								pv.projectionTag = 31;
								pv.projectionValue = 7;
							}
							else
							{
								pv.projectionTag = (c >> 3) & 0x1F;
								pv.projectionValue = c & 0x7;
							}
						}

						_decodeJ = 0;
					}
					/*iProjectionRect.x = xmin;
					iProjectionRect.y = ymin;
					iProjectionRect.right = xmax;
					iProjectionRect.bottom = ymax;*/
				}
			}

			return true;
		}
	}
}
