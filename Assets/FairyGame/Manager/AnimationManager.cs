﻿using System.Collections.Generic;
using FairyGUI;
using UnityEngine;

namespace FairyGame
{
	public class AnimationManager
	{
		public static AnimationManager inst { get; private set; }
		public static AnimationManager Instantiate()
		{
			if (inst == null)
				inst = new AnimationManager();
			return inst;
		}

		Dictionary<string, Dictionary<string, AniDef>> _pool = new Dictionary<string, Dictionary<string, AniDef>>();
		ByteArray _nullNPCData;
		NTexture _nullNPCTexture;

		public AniDef GetAnimation(string assetPath, string fileName)
		{
			AniDef def;
			Dictionary<string, AniDef> subPool;
			if (!_pool.TryGetValue(assetPath, out subPool))
			{
				subPool = new Dictionary<string, AniDef>();
				_pool[assetPath] = subPool;
			}

			if (subPool.TryGetValue(fileName, out def))
				return def;

			def = new AniDef(assetPath, fileName);
			subPool[fileName] = def;
			return def;
		}

		public void LoadAnimation(AniDef def)
		{
			if (def.loading || def.ready)
				return;

			def.loading = true;
			AssetManager.inst.LoadAsset(def.assetPath, def.fileName, OnLoadCompleted);
		}

		public void LoadAnimation(string assetPath, string fileName)
		{
			AniDef def = GetAnimation(assetPath, fileName);
			LoadAnimation(def);
		}

		public void Cleanup()
		{

		}

		void OnLoadCompleted(string assetPath, string fileName, object data)
		{
			AniDef def = GetAnimation(assetPath, fileName);
			AssetBundle bundle = (AssetBundle)data;
			if (bundle != null)
			{
				Texture2D[] texs = bundle.LoadAllAssets<Texture2D>();
				if (texs == null || texs.Length < 2)
				{
					Debug.LogWarning("load failed " + def.fileName);
				}
				else
				{
					ByteArray desc = new ByteArray(bundle.LoadAllAssets<TextAsset>()[0].bytes);
					if (texs[0].name.IndexOf("!a") != -1)
					{
						NTexture texture = new NTexture(texs[1]);
						texture.alphaTexture = new NTexture(texs[0]);
						def.Create(desc, texture);
					}
					else
					{
						NTexture texture = new NTexture(texs[0]);
						texture.alphaTexture = new NTexture(texs[1]);
						def.Create(desc, texture);
					}
					def.ready = true;
				}

				bundle.Unload(false);
			}
			else
			{
				//Debug.LogWarning(www.error);
				if (def.assetPath == "npc")
				{
					if (_nullNPCData == null)
					{
						_nullNPCData = new ByteArray(Resources.Load<TextAsset>("npc_null").bytes);
						_nullNPCTexture = new NTexture(Resources.Load<Texture2D>("npc_null!tex"));
						_nullNPCTexture.alphaTexture = new NTexture(Resources.Load<Texture2D>("npc_null!tex!a"));
					}
					def.Create(_nullNPCData, _nullNPCTexture);
					def.ready = true;
				}
			}
		}
	}
}
