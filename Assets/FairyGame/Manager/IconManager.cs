﻿using FairyGUI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace FairyGame
{
	public class IconManager
	{
		public static IconManager inst { get; private set; }
		public static IconManager Instantiate()
		{
			if (inst == null)
				inst = new IconManager();
			return inst;
		}

		Dictionary<string, NTexture> poolBig;  //大图缓存
		Dictionary<string, NTexture> poolSmall;  //图标缓存
		HashSet<string> failedList;

		public IconManager()
		{
			poolBig = new Dictionary<string, NTexture>();
			poolSmall = new Dictionary<string, NTexture>();
			failedList = new HashSet<string>();
		}

		public void GetIcon(string src, LoadAssetCallback callback)
		{
			if (failedList.Contains(src))
			{
				callback("icon", src, null);
				return;
			}
			bool isBig = src.EndsWith("_b"); //大图
			if (isBig)
			{
				if (poolBig.ContainsKey(src))
					callback("icon", src, poolBig[src]);
			}
			else
			{
				if (poolSmall.ContainsKey(src))
					callback("icon", src, poolSmall[src]);
			}

			AssetManager.inst.LoadAsset("icon", src, (string assetPath, string fileName, object data) =>
			{
				if (data != null)
				{
					bool isBig2 = fileName.EndsWith("_b"); //大图
					if (isBig2)
						poolBig[fileName] = (NTexture)data;
					else
						poolSmall[fileName] = (NTexture)data;
				}
				else
					failedList.Add(src);

				callback(assetPath, fileName, data);
			});
		}

		public void Cleanup()
		{
			//图标缓存就一直缓存好了，这里清大图
			if (poolBig.Count > 8)
			{
				ArrayList toRemove = null;
				Dictionary<string, NTexture>.Enumerator iter = poolBig.GetEnumerator();
				while (iter.MoveNext())
				{
					string key = iter.Current.Key;
					NTexture texture = iter.Current.Value;
					if (texture.refCount == 0 && !key.StartsWith("u"))
					{
						if (toRemove == null)
							toRemove = new ArrayList();
						toRemove.Add(key);
						texture.Dispose();

						//Debug.Log("free icon " + de.Key);
					}
				}
				iter.Dispose();
				if (toRemove != null)
				{
					foreach (string key in toRemove)
						poolBig.Remove(key);
				}
			}
		}
	}
}
