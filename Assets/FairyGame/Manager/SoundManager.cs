﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace FairyGame
{
	public class SoundManager : MonoBehaviour
	{
		public static SoundManager inst { get; private set; }
		public static SoundManager Instantiate()
		{
			if (inst == null)
			{
				GameObject go = new GameObject("SoundManager");
				DontDestroyOnLoad(go);
				inst = go.AddComponent<SoundManager>();
				inst._audioSource = go.AddComponent<AudioSource>();
			}
			return inst;
		}

		private AudioClip _bgSound;
		private string _bgSoundSrc;
		//private int _repeatDelay;
		private AudioSource _audioSource;

		private bool _bgSoundOn;
		private bool _soundOn;
		private bool _masterSwitch;

		private Dictionary<string, AudioClip> _soundPool;

		public SoundManager()
		{
			_soundPool = new Dictionary<string, AudioClip>();
			_bgSoundOn = true;
			_soundOn = true;
			_masterSwitch = true;
		}

		public bool bgSoundOn
		{
			get { return _bgSoundOn; }
			set
			{
				if (_bgSoundOn != value)
				{
					_bgSoundOn = value;
					if (!value)
						StopBgSound();
					else if (!string.IsNullOrEmpty(_bgSoundSrc) && _masterSwitch)
						PlayBgSound();
				}
			}
		}

		public bool soundOn
		{
			get
			{
				return _soundOn;
			}
			set
			{
				_soundOn = value;
			}
		}

		public bool masterSwitch
		{
			get
			{
				return _masterSwitch;
			}
			set
			{
				if (_masterSwitch != value)
				{
					_masterSwitch = value;
					if (!value)
						StopBgSound();
					else if (!string.IsNullOrEmpty(_bgSoundSrc) && _masterSwitch && _bgSoundOn)
						PlayBgSound();
				}
			}
		}

		public void Play(string src)
		{
			if (!_soundOn || !_masterSwitch)
				return;

			AudioClip clip;
			if (_soundPool.TryGetValue(src, out clip))
				_audioSource.PlayOneShot(clip);
			else
				AssetManager.inst.LoadAsset("sound", src, OnLoadCompleted);
		}

		public void SetEnvSound(string src)
		{
			SetEnvSound(src, 0);
		}

		public void SetEnvSound(string src, int repeateDelay)
		{
			//_repeatDelay = repeateDelay;
			if (_bgSoundSrc == src)
				return;

			StopBgSound();
			_bgSound = null;

			_bgSoundSrc = src;
			if (!string.IsNullOrEmpty(_bgSoundSrc) && _bgSoundOn && _masterSwitch)
				PlayBgSound();
		}

		public void Cleanup()
		{

		}

		void PlayBgSound()
		{
			StopBgSound();

			if (_bgSound == null)
			{
				if (!_soundPool.TryGetValue(_bgSoundSrc, out _bgSound))
				{
					AssetManager.inst.LoadAsset("sound", _bgSoundSrc, OnLoadCompleted);
					return;
				}
			}

			_audioSource.clip = _bgSound;
			_audioSource.loop = true;
			_audioSource.Play();
		}

		void StopBgSound()
		{
			if (_bgSound != null)
			{
				_audioSource.loop = false;
				_audioSource.Stop();
			}
		}

		void OnLoadCompleted(string assetPath, string fileName, object data)
		{
			_soundPool[fileName] = (AudioClip)data;
			if (fileName == _bgSoundSrc)
			{
				if (_bgSoundOn && _masterSwitch)
					PlayBgSound();
			}
		}
	}
}
