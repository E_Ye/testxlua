﻿//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using System.IO;
//using LuaInterface;

//namespace FairyGame
//{
//	/// <summary>
//	/// 集成自LuaFileUtils，重写里面的ReadFile，
//	/// </summary>
//	public class LuaLoader : LuaFileUtils
//	{
//		public static LuaLoader Instantiate()
//		{
//			return new LuaLoader();
//		}

//		// Use this for initialization
//		public LuaLoader()
//		{
//			LuaConst.luaDir = LuaConst.toluaDir = Application.dataPath + "/LuaFramework/ToLua/Lua";
//		}

//		ByteArray bundle;
//		Dictionary<string, int> entries;

//		ByteArray baseBundle;
//		Dictionary<string, int> baseEntries;

//		public void AddBundle(byte[] bytes, bool isbase)
//		{
//#if UNITY_IPHONE
//			string s1 = "oXlL9C+Odq1vLvc6iQ7TJw";
//			string s2 = "rFtHHECPCUFOZDBCfKVeyA";
//			string s3 = "IlRE+o78GytNe8st1pOnXQ";
//			string c = Convert.ToChar(61).ToString();
//			s1 += c + c;
//			s2 += c + c;
//			s3 += c + c;
//			byte[] key1 = Convert.FromBase64String(s1);
//			byte[] key2 = Convert.FromBase64String(s2);
//			byte[] key3 = Convert.FromBase64String(s3);
//			byte[] key = new byte[8];
//			byte[] iv = new byte[8];
//			for (int i = 0; i < 8; i++)
//			{
//				int j = i * 2;
//				int v1 = key1[j];
//				int v2 = key2[j];
//				int v = v1 << 4 + v2 >> 4;
//				key[i] = (byte)v;
//			}
//			for (int i = 0; i < 8; i++)
//			{
//				int j = i * 2;
//				int v1 = key3[j];
//				int v2 = key2[j];
//				int v = v2 << 4 + v1 >> 4;
//				iv[i] = (byte)v;
//			}

//			bytes = DesUtil.DesDecrypt(bytes, key, iv);
//#endif

//			ByteArray ba = new ByteArray(bytes);
//			Dictionary<string, int> entries = new Dictionary<string, int>();

//			if (isbase)
//			{
//				this.baseBundle = ba;
//				this.baseEntries = entries;
//			}
//			else
//			{
//				this.bundle = ba;
//				this.entries = entries;
//			}

//			int len;
//			string fileName;
//			while (ba.bytesAvailable > 0)
//			{
//				fileName = ba.ReadLongString();
//				entries.Add(fileName, ba.position);

//				len = ba.ReadInt();
//				ba.SkipBytes(len);
//			}
//		}

//		/// <summary>
//		/// 当LuaVM加载Lua文件的时候，这里就会被调用，
//		/// 用户可以自定义加载行为，只要返回byte[]即可。
//		/// </summary>
//		/// <param name="fileName"></param>
//		/// <returns></returns>
//		public override byte[] ReadFile(string fileName)
//		{
//			if (!fileName.EndsWith(".lua"))
//			{
//				fileName += ".lua";
//			}
//			if (entries != null)
//			{
//				int pos;
//				if (entries.TryGetValue(fileName, out pos))
//				{
//					bundle.position = pos;
//					int len = bundle.ReadInt();
//					byte[] buf = new byte[len];
//					bundle.ReadBytes(ref buf, 0, len);
//					return buf;
//				}
//			}
//			if (baseEntries != null)
//			{
//				int pos;
//				if (baseEntries.TryGetValue("ToLua/" + fileName, out pos))
//				{
//					baseBundle.position = pos;
//					int len = baseBundle.ReadInt();
//					byte[] buf = new byte[len];
//					baseBundle.ReadBytes(ref buf, 0, len);
//					return buf;
//				}
//			}

//			return base.ReadFile(fileName);
//		}
//	}
//}