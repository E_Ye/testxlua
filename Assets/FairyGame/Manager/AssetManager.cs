﻿using FairyGUI;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;

namespace FairyGame
{
	public delegate void LoadAssetCallback(string assetPath, string fileName, object data);

	public class AssetManager : MonoBehaviour
	{
		public static AssetManager inst { get; private set; }
		public static AssetManager Instantiate()
		{
			if (inst == null)
			{
				GameObject go = new GameObject("AssetManager");
				DontDestroyOnLoad(go);
				inst = go.AddComponent<AssetManager>();
			}
			return inst;
		}

		List<LoadItem> _items;
		bool _started;

		private AssetManager()
		{
			_items = new List<LoadItem>();
		}

		public bool isDone
		{
			get { return !_started; }
		}

		public string GetAssetPath(string path, string fileName)
		{
			path = path + "/" + fileName + GameConfig.bundleNameSubfix;
			string assetPath;
			if (GameConfig.downloadResource)
			{
				assetPath = Path.Combine(GameConfig.localResPath, path);
				if (!File.Exists(assetPath))
					assetPath = Path.Combine(GameConfig.staticResPath, path);
			}
			else
				assetPath = Path.Combine(GameConfig.staticResPath, path);

			return assetPath;
		}

		public string LoadText(string path, string fileName)
		{
			return Encoding.UTF8.GetString(LoadBytes(path, fileName));
		}

		public byte[] LoadBytes(string path, string fileName)
		{
			AssetBundle bundle = AssetBundle.LoadFromFile(GetAssetPath(path, fileName));
			byte[] bytes = ((TextAsset)bundle.LoadAsset(bundle.GetAllAssetNames()[0])).bytes;
			bundle.Unload(true);
			return bytes;
		}

		public void LoadAsset(string path, string fileName)
		{
			LoadAsset(path, fileName, null);
		}

		public void LoadAsset(string path, string fileName, LoadAssetCallback callback)
		{
			LoadItem item = new LoadItem();
			item.path = path;
			item.fileName = fileName;
			item.callback = callback;
			_items.Add(item);

			if (!_started)
			{
				_started = true;
				StartCoroutine(Run());
			}
		}

		public void Cleanup()
		{

		}

		IEnumerator Run()
		{
			while (_items.Count > 0)
			{
				LoadItem item = _items[0];
				_items.RemoveAt(0);

				item.startTime = Time.realtimeSinceStartup;

				AssetBundleCreateRequest req = AssetBundle.LoadFromFileAsync(GetAssetPath(item.path, item.fileName));
				yield return req;

				AssetBundle bundle = req.assetBundle;
				object callbackData = null;
				if (bundle == null)
					Debug.LogWarning("load failed " + item.path + "/" + item.fileName);
				else
				{
					switch (item.path)
					{
						case "ui":
                        case "ui1":
							{
								req = AssetBundle.LoadFromFileAsync(GetAssetPath(item.path, item.fileName + "_res"));
								yield return req;

								AssetBundle resBundle = req.assetBundle;
								UIPackage pkg = UIPackage.AddPackage(bundle, resBundle);
								pkg.customId = item.fileName;
								callbackData = pkg;
								break;
							}

						case "map":
							{
								Hashtable ms = new Hashtable();
								ByteArray ba = new ByteArray(bundle.LoadAsset<TextAsset>("path").bytes);
								ba.Decompress();
								ms["pathData"] = ba;
								Texture2D[] texs = bundle.LoadAllAssets<Texture2D>();
								int cnt = texs.Length;
								NTexture[] pieces;
								if (bundle.Contains("thumb"))
									pieces = new NTexture[cnt - 1];
								else
									pieces = new NTexture[cnt];
								for (int i = 0; i < cnt; i++)
								{
									Texture2D tex = texs[i];
									if (tex.name == "thumb")
										ms["thumb"] = new NTexture(tex);
									else
									{
										pieces[int.Parse(tex.name)] = new NTexture(tex);
									}
								}
								ms["pieces"] = pieces;
								bundle.Unload(false);

								callbackData = ms;
							}
							break;

						case "sound":
							{
								callbackData = bundle.LoadAsset<AudioClip>(bundle.GetAllAssetNames()[0]);
								bundle.Unload(false);
								break;
							}

						case "icon":
							{
								callbackData = new NTexture(bundle.LoadAsset<Texture2D>(bundle.GetAllAssetNames()[0]));
								bundle.Unload(false);
								break;
							}

						default:
							callbackData = bundle;
							break;
					}
				}

				if (item.callback != null)
					item.callback(item.path, item.fileName, callbackData);

				{ //检查队列中是否有相同的请求
					int i = 0;
					int cnt = _items.Count;
					while (i < cnt)
					{
						LoadItem item2 = _items[i];
						if (item2.path == item.path && item2.fileName == item.fileName)
						{
							_items.RemoveAt(i);

							if (item2.callback != null)
								item2.callback(item2.path, item2.fileName, callbackData);
							cnt = _items.Count;
						}
						else
							i++;
					}
				}
			}

			_started = false;
		}
	}

	class LoadItem
	{
		public string path;
		public string fileName;
		public float startTime;
		public LoadAssetCallback callback;
	}
}
