﻿//using UnityEngine;
//using System.Collections;
//using LuaInterface;

//namespace FairyGame
//{
//	public class LuaManager : MonoBehaviour
//	{
//		public static LuaManager inst { get; private set; }
//		public static LuaManager Instantiate()
//		{
//			if (inst == null)
//			{
//				GameObject go = new GameObject("LuaManager");
//				DontDestroyOnLoad(go);
//				inst = go.AddComponent<LuaManager>();
//			}
//			return inst;
//		}

//		protected LuaState luaState = null;
//		protected LuaLooper loop = null;

//		// Use this for initialization
//		void Awake()
//		{
//			Init();
//		}

//		protected void Init()
//		{
//			luaState = new LuaState();
//			OpenLibs();
//			luaState.LuaSetTop(0);
//			Bind();

//			 luaState.Start();
//			StartLooper();
//			StartMain();
//		}

//		protected void StartLooper()
//		{
//			loop = gameObject.AddComponent<LuaLooper>();
//			loop.luaState = luaState;
//		}

//		protected virtual void Bind()
//		{
//			LuaBinder.Bind(luaState);
//			LuaCoroutine.Register(luaState, this);
//		}

//		public LuaLooper GetLooper()
//		{
//			return loop;
//		}

//		/// <summary>
//		/// 初始化加载第三方库
//		/// </summary>
//		void OpenLibs()
//		{
//			//luaState.OpenLibs(LuaDLL.luaopen_pb);
//			luaState.OpenLibs(LuaDLL.luaopen_struct);
//			//luaState.OpenLibs(LuaDLL.luaopen_lpeg);
//#if UNITY_STANDALONE_OSX || UNITY_EDITOR_OSX
//			luaState.OpenLibs(LuaDLL.luaopen_bit);
//#endif
//			OpenCJson();
//		}

//		//cjson 比较特殊，只new了一个table，没有注册库，这里注册一下
//		protected void OpenCJson()
//		{
//			luaState.LuaGetField(LuaIndexes.LUA_REGISTRYINDEX, "_LOADED");
//			luaState.OpenLibs(LuaDLL.luaopen_cjson);
//			luaState.LuaSetField(-2, "cjson");

//			luaState.OpenLibs(LuaDLL.luaopen_cjson_safe);
//			luaState.LuaSetField(-2, "cjson.safe");
//		}

//		protected void CallMain()
//		{
//			LuaFunction main = luaState.GetFunction("Main");
//			main.Call();
//			main.Dispose();
//			main = null;
//		}

//		protected void StartMain()
//		{
//			luaState.DoFile("Main.lua");
//			CallMain();
//		}

//		protected void Destroy()
//		{
//			if (luaState != null)
//			{
//				LuaState state = luaState;
//				luaState = null;

//				if (loop != null)
//				{
//					loop.Destroy();
//					loop = null;
//				}

//				state.Dispose();
//			}
//		}

//		protected void OnDestroy()
//		{
//			Destroy();
//		}

//		protected void OnApplicationQuit()
//		{
//			Destroy();
//		}
//	}
//}