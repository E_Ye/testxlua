﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FairyGame;

public class TestSocket : MonoBehaviour {

	// Use this for initialization
	void Start () {
        SocketProcessor sp = new SocketProcessor();
        sp.Connect("127.0.0.1", 61234);
        sp.onConnected = () =>
        {
            sp.Send(1002);
        };
        sp.onDisconnected = (exc) => { Debug.Log(">>>>:" + exc); };
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
