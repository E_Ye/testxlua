local LoginScene = class("LoginScene", BaseScene) 

function LoginScene:ctor()
  log("LoginScene:ctor")
--  加载包资源
  local uipackList = {"Common","DengLu","Main"}
  LoginScene.super.ctor(self, uipackList)
  KBEngineLua.InitEngine()
end

function LoginScene:OnEnter()
  log("LoginScene:OnEnter:"..#PageList)
  --  init view
  UIMgr.Init(PageList, WindowsList)
  --  init crlt
  CtrlManager.Init(DengLuCtrlList)
  CtrlManager.GetCtrl(DengLuCtrlList.DenLuCtrl).Start()
end


return LoginScene