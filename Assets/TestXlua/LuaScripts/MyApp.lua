

class("MyApp")
--- @class MyApp
--- @field public curScene BaseScene
function MyApp:ctor()
  self.curScene = nil;
  Event.AddListener("ontest",MyApp.t)
end

function MyApp:run()
  GRoot.inst:SetContentScaleFactor(1136, 640);
  -- Event.Brocast("ontest",self,1,2)
  -- log(">>>>shift："..tonumber(0x87654321>>0x12))
  self:enterScene("LoginScene")
end

function MyApp:enterScene(SceneName)
  if self.curScene then
  	self.curScene:OnExit()
  end
  self.curScene = require("scenes/"..SceneName).new()
  loadMgr:ReplaceScene(self.curScene.ablist, MyApp.OnLoadFinish,self)
 
end

function MyApp:OnLoadFinish()
	self.curScene:OnEnter()
end

function MyApp:t(a,b)
	log(">>>>>>>>>>TTTTTTTTTTTTTT"..a+b)
end

return MyApp
