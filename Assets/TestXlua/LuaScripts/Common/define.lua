UnityEngine     = CS.UnityEngine
WWW             = UnityEngine.WWW
GameObject      = UnityEngine.GameObject
Vector2         = UnityEngine.Vector2
Vector3         = UnityEngine.Vector3

loadMgr         = CS.LoadMgr.Instance
SocketProcessor = CS.FairyGame.SocketProcessor
GameConfig      = CS.FairyGame.GameConfig
TweenUtils      = CS.FairyGame.TweenUtils
UIPackage       = CS.FairyGUI.UIPackage
LuaWindow       = CS.FairyGUI.LuaWindow
UIObjectFactory = CS.FairyGUI.UIObjectFactory
Ease            = CS.DG.Tweening.Ease

--kbe
KBEngin         = CS.KBEngine
MemoryStream    = KBEngin.MemoryStream
NetworkInterface= KBEngin.NetworkInterface

CtrlNames = {
	Prompt = "PromptCtrl",
	Message = "MessageCtrl"
}

PanelNames = {
  "PromptPanel",	
  "MessagePanel",
  "DenLu",
  "Main"
}

-- UIPath = {page="View/Pages/",wind="View/Windows/",commonWind="View/Windows/Common"}
WindowType = {normal=1,common=2}


PageList = {
  DengLuPanel = "DengLuPanel"
  -- "Main"
}

WindowsList = {
  AlertWin    = {name="AlertWin", type=WindowType.normal, parentPage = PageList.DengLuPanel },
}

DengLuCtrlList = {
  DenLuCtrl = "DenLuCtrl"
}

MainPanelList = {
  MainPanel = "MainPanel"
}

MainCtrlList = {
  MainCtrl = "MainCtrl"
}





--协议类型--
--ProtocalType = {
--	BINARY = 0,
--	PB_LUA = 1,
--	PBC = 2,
--	SPROTO = 3,
--}
--当前使用的协议类型--
--TestProtoType = ProtocalType.BINARY;

--Util = LuaFramework.Util;
--AppConst = LuaFramework.AppConst;
--LuaHelper = LuaFramework.LuaHelper;
--ByteBuffer = LuaFramework.ByteBuffer;

--resMgr = LuaHelper.GetResManager();
--panelMgr = LuaHelper.GetPanelManager();
--soundMgr = LuaHelper.GetSoundManager();
--networkMgr = LuaHelper.GetNetManager();



