local print_r = require "3rd/sproto/print_r"

function secondsToStr(time, short)
	local s = math.floor( time / (60 * 60) )
    local f = math.floor( (time - s * 60 * 60) / 60)
    local m = math.floor(  time - s * 60 * 60 - f * 60 )
	
    local s1
    local s2
	local s3
	if s<=9 then
        s1 = "0"..s 
    else
        s1 = ""..s
    end
    if f<=9 then
        s2 = "0"..f 
    else
        s2 = ""..f
    end
    if m<=9 then
        s3 = "0"..m 
    else
        s3 = ""..m
    end
	
    if short then
       return string.format("%s:%s", s2, s3)
    else
	   return string.format("%s:%s:%s", s1, s2, s3)
    end
end

function GetIdFromRid( rid )
    if rid==nil then
        return ""
    end
    
    local pos = string.find(rid, "@")
    if pos==nil then
        return rid
    else
        return string.sub(rid, 1, pos-1 )
    end
end

function TickToSeconds( tick )
    return math.floor(tick * 20 * 0.001)
end

function EncodeHTML(str)
    if str==nil then
        return ''
    else
        str = string.gsub(str, '&', "&amp;")
        str = string.gsub(str, '<', "&lt;")
        str = string.gsub(str, '>', "&gt;")
        str = string.gsub(str, "'", "&apos;")
        return str
    end
end

function string.isNilOrEmpty(str)
    return str == nil or str == ''
end
 
-- 返回由指定分隔符分割的数组
function string.split(str, delimiter)
    if string.isNilOrEmpty(str) or string.isNilOrEmpty(delimiter) then
        return nil
    end
 
    local result = {}
    for match in (str..delimiter):gmatch("(.-)"..delimiter) do
        table.insert(result, match)
    end
    return result
end

function string.startswith(str, substr)
    if str == nil or substr == nil then
        return nil, "the string or the sub-stirng parameter is nil"
    end
    if string.find(str, substr) ~= 1 then
        return false
    else
        return true
    end
end

function string.endswith(str, substr)  
    if str == nil or substr == nil then  
        return nil, "the string or the sub-string parameter is nil"  
    end  
    str_tmp = string.reverse(str)  
    substr_tmp = string.reverse(substr)  
    if string.find(str_tmp, substr_tmp) ~= 1 then  
        return false  
    else  
        return true  
    end
end

function string.trim(str)
    return string.match(string.match(str, "%S.*"), ".*%S")
end

function GetBetweenBrakets(source)
    local i1 = string.find(source, "%(")
    local i2 = string.find(source, "%)")
    if i1 and i2 then
        return string.sub(source, i1+1, i2-1)
    else
        return source
    end
end

function table.indexOf( t, value, begin )
    for i = begin or 1, #t do
        if t[i] == value then
            return i
        end
    end
    return nil
end

function CreateRandomPassword(hashLen, includeLowercase, includeNumbers , includeUppercase)
    local strHash = ""
    if includeLowercase or includeLowercase==nil then strHash = strHash..'abchefghjkmnpqrstuvwxyz' end
    if includeNumbers or includeNumbers==nil then strHash = strHash..'0123456789' end
    if includeUppercase then strHash = strHash..'ABCDEFGHIJKLMNOPQRSTUVWXYZ' end
    local maskPick
    local passwordStr = 'xy'
    local maskLen = string.len(strHash)
    for i = 1,hashLen do
        maskPick = ScriptUtils.RandomInt(1, maskLen)
        passwordStr = passwordStr..string.sub(strHash, maskPick, maskPick)
    end
    return passwordStr
end

function string.formatWithWan(value)
    local tv = tonumber(value)
    if tv > 10000 then
        tv = (tv / 10000).."万"
    end
    return tv
end

function string.eval(str, ...)
    if type(str) == "string" then
        str = string.format(str, ...)
        return loadstring("return " .. str)()
    else
        return nil
    end
end

function table.dump(t)
    print_r(t)
end