
require "Common/MyUtils"
require "Common/define"
require "Common/FairyGUI"
require "Common/functions"


require "framework/Net/NetClient"
require "framework/class_function"
require "framework/BaseScene"

require "framework/ui/BasePage"
require "framework/ui/BaseWindow"
require "framework/ui/UIMgr"

require "framework/CtrlManager"

require "framework/kbe/Dbg"
require "framework/kbe/KBEngineLua"

-- require "framework/kbe/Kbe/Account"
-- require "framework/Kbe/Avatar"
-- require "framework/Kbe/Gate"
-- require "framework/Kbe/Monster"
-- require "framework/Kbe/NPC"

Event = require "framework/kbe/events"
