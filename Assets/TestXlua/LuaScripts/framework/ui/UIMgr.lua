require "framework/ui/WindowBase"

local M = { }

local print_r = require "3rd/sproto/print_r"
UIMgr = M

M.page = nil   --固定的页面，如开始登陆、主UI等非窗口类
M.windows = nil --窗口

function M.Init(pageList, windowsList)
  M.pages = {}
  M.windows = {}
  local keyname
  for k,v in pairs(pageList) do
    keyname = string.format("View/Pages/%s", v)
    M.pages[v] = require(keyname).new()
  end

  for k,v in pairs(windowsList) do
    if v.type == WindowType.normal and v.parentPage then
      keyname = string.format("View/Windows/%s", v.name)
      M.windows[v.parentPage] = M.windows[v.parentPage] or {}
      M.windows[v.parentPage][v.name] = require(keyname).new()

    elseif v.type == WindowType.common then
      keyname = string.format("View/Windows/Common/%s", v.name)
      M.windows["common"] = M.windows["common"] or {}
      M.windows["common"][v.name] = require(keyname).new()
    end
    
  end
end

function M.ShowPage(pageName)
  local curPage = M.pages[pageName]
  curPage:Show()
  return curPage
end

function M.HidePage(pageName)
  local curPage = M.pages[pageName]
  curPage:Hide()
end

function M.ShowWindow(wind, data, callback)
  local curWind 
  if wind.type == WindowType.normal and wind.parentPage then
    curWind = M.windows[wind.parentPage][wind.name]
  elseif wind.type == WindowType.common then
    curWind = M.windows["common"][wind.name]
  end

  if not curWind then return end 
  if curWind.isShowing then return end 
  
  curWind:Show()

  return curWind
end

function M.HidePage(pageName)
  local curPage = M.page.obj
  curPage:Hide()
end

function M.ClearScene()
  if page.isShowing then page:Hide() end 

  for k,wind in pairs(M.windows) do
    page:Hide()
    
    local mResident = page.mResident
    if mResident ~= false then
      page:Destroy()
    end
  
  end
end

return M