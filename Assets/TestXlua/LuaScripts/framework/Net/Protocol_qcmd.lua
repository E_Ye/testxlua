--[[  这是自动生成的文件，请勿修改  ]]
--cts.xml ver 1.0

NetKeys.QCMD_ACTIVE = 0
NetKeys.QCMD_WELCOME = 1
NetKeys.QCMD_LINES = 2
NetKeys.QCMD_CREATE_ROLE = 3
NetKeys.QCMD_REMOVE_ROLE = 4
NetKeys.QCMD_RANDOM_NAME = 5
NetKeys.QCMD_CHANGE_NAME = 6
NetKeys.QCMD_CHECK_ROLE_NAME = 7
NetKeys.QCMD_LOGIN = 8
NetKeys.QCMD_LOGOUT = 9
NetKeys.QCMD_READY = 10
NetKeys.QCMD_SWITCH_LINE = 11
NetKeys.QCMD_DEBUG = 12
NetKeys.QCMD_ENTER = 13
NetKeys.QCMD_MOVE = 101
NetKeys.QCMD_GO = 102
NetKeys.QCMD_FLY = 103
NetKeys.QCMD_RELIVE = 104
NetKeys.QCMD_ENTER_CLAN_SCENE = 105
NetKeys.QCMD_QUERY_ATTRS = 201
NetKeys.QCMD_PRIVATE_SETUP = 202
NetKeys.QCMD_SET_PK = 203
NetKeys.QCMD_SET_ROLE_LOCK = 204
NetKeys.QCMD_SET_USER_TITLE = 205
NetKeys.QCMD_FETCH_GIFTS = 206
NetKeys.QCMD_MAIN_LIST_OP = 207
NetKeys.QCMD_MAIN_LIST_VIEW = 208
NetKeys.QCMD_VIEW_USER = 251
NetKeys.QCMD_SET_EN_LINGLI = 252
NetKeys.QCMD_XIULIAN_JINGMAI = 253
NetKeys.QCMD_ADD_LINGLI = 254
NetKeys.QCMD_ADD_ATTRS = 255
NetKeys.QCMD_VIEW_ERXX = 256
NetKeys.QCMD_APPLY_ERXX = 257
NetKeys.QCMD_ALLOW_ERXX = 258
NetKeys.QCMD_GX_ERXX_EXP = 259
NetKeys.QCMD_JF_ERXX = 260
NetKeys.QCMD_FETCH_SX = 261
NetKeys.QCMD_ERXX_LEARN_SKILL = 262
NetKeys.QCMD_QUIT_ERXX = 263
NetKeys.QCMD_CLICK_HEAD = 264
NetKeys.QCMD_MY_ITEMS = 301
NetKeys.QCMD_USE_ITEM = 302
NetKeys.QCMD_HANG_ON_ITEM = 303
NetKeys.QCMD_PICK_ITEM = 304
NetKeys.QCMD_ADD_BURTHEN = 305
NetKeys.QCMD_VIEW_ITEM = 306
NetKeys.QCMD_DROP_ITEM = 307
NetKeys.QCMD_SALE_ITEM_TO_NPC = 308
NetKeys.QCMD_TAKE_OFF_EQUIP2 = 309
NetKeys.QCMD_LIST_USER_ITEMS = 310
NetKeys.QCMD_HANG_OFF_ITEM = 311
NetKeys.QCMD_BUY_ITEM_FROM_USER = 314
NetKeys.QCMD_GIVE_ITEM = 312
NetKeys.QCMD_GIVE_MONEY = 313
NetKeys.QCMD_VIEW_MAGIC = 352
NetKeys.QCMD_MAGIC_CALL_OUT = 353
NetKeys.QCMD_MAGIC_CALL_BACK = 354
NetKeys.QCMD_SKILL_LVLUP = 355
NetKeys.QCMD_MAGIC_LIANHUA_VIEW = 356
NetKeys.QCMD_MAGIC_LIANHUA = 357
NetKeys.QCMD_MY_PETS = 401
NetKeys.QCMD_VIEW_PET = 402
NetKeys.QCMD_CHUCK = 403
NetKeys.QCMD_CALL_OUT = 404
NetKeys.QCMD_CALL_BACK = 405
NetKeys.QCMD_ADD_PET_ZZ = 406
NetKeys.QCMD_LEARN_SKILL = 407
NetKeys.QCMD_TUJIANS = 408
NetKeys.QCMD_TUJIAN_VIEW = 409
NetKeys.QCMD_TUJIAN_FUHUA = 410
NetKeys.QCMD_HECHENG_PET_VIEW = 411
NetKeys.QCMD_HECHENG_PET = 412
NetKeys.QCMD_HECHENG_PETS = 413
NetKeys.QCMD_GET_LIANYAOHU_GEZI = 414
NetKeys.QCMD_LIANYAOHU_GO = 415
NetKeys.QCMD_VIEW_MOUNT = 452
NetKeys.QCMD_RIDE_ON = 453
NetKeys.QCMD_JUMP_DOWN = 454
NetKeys.QCMD_DUANLIANLU_VIEW = 455
NetKeys.QCMD_BUILD_MOUNTS = 456
NetKeys.QCMD_DUANLIAN_MOUNTS = 457
NetKeys.QCMD_ATTACK = 501
NetKeys.QCMD_TASKS = 601
NetKeys.QCMD_VIEW_NPC = 651
NetKeys.QCMD_TOUCH_NPC = 652
NetKeys.QCMD_BUY_ITEM_FROM_NPC = 653
NetKeys.QCMD_FRIENDS = 701
NetKeys.QCMD_FRIEND_ADD = 702
NetKeys.QCMD_FRIEND_REMOVE = 703
NetKeys.QCMD_FIND_USER = 704
NetKeys.QCMD_CHAT_MSG = 711
NetKeys.QCMD_MAILS = 801
NetKeys.QCMD_MAIL_SEND = 802
NetKeys.QCMD_MAIL_SET_STATUS = 803
NetKeys.QCMD_MAIL_RECV_EXT = 804
NetKeys.QCMD_TEAM_VIEW = 901
NetKeys.QCMD_TEAM_CREATE = 902
NetKeys.QCMD_TEAM_JOIN = 903
NetKeys.QCMD_TEAM_OPERATE = 904
NetKeys.QCMD_TEAM_INVITE = 905
NetKeys.QCMD_TEAM_KICKOUT = 906
NetKeys.QCMD_TEAM_QUIT = 907
NetKeys.QCMD_TEAM_OVER = 908
NetKeys.QCMD_TEAM_SHIFT = 909
NetKeys.QCMD_TEAM_SETUP = 910
NetKeys.QCMD_SORTBOARD = 1001
NetKeys.QCMD_MALL_OPEN = 1101
NetKeys.QCMD_MALL_BUY = 1102
NetKeys.QCMD_HD_INFO = 1201
NetKeys.QCMD_SIGN_INFO = 1202
NetKeys.QCMD_DO_SIGN = 1203
NetKeys.QCMD_FILL_AMOUNT = 1204
NetKeys.QCMD_QITIAN_VIEW = 1210
NetKeys.QCMD_QITIAN_LINQU = 1211
NetKeys.QCMD_QUERY_HANGING_SETUP = 1300
NetKeys.QCMD_HANGING_SETUP = 1301
NetKeys.QCMD_HANGING_START = 1302
NetKeys.QCMD_HANGING_STOP = 1303
NetKeys.QCMD_HANGING_GJK = 1304
NetKeys.QCMD_EMBED_VIEW = 1350
NetKeys.QCMD_EMBED_ON = 1351
NetKeys.QCMD_EMBED_OFF = 1352
NetKeys.QCMD_UPGRADE_WEAPONS = 1353
NetKeys.QCMD_UPGRADE_WEAPON_VIEW = 1354
NetKeys.QCMD_UPGRADE_WEAPON = 1355
NetKeys.QCMD_UPGRADE_SHENBING = 1356
NetKeys.QCMD_BUILD_ITEMS = 1357
NetKeys.QCMD_BUILD_ITEM_VIEW = 1358
NetKeys.QCMD_BUILD_ITEM = 1359
NetKeys.QCMD_RONGLIAN_ITEMS = 1360
NetKeys.QCMD_RONGLIAN_ITEM_VIEW = 1361
NetKeys.QCMD_RONGLIAN_ITEM = 1362
NetKeys.QCMD_XILIAN_RONGLIAN_VIEW = 1363
NetKeys.QCMD_XILIAN_RONGLIAN = 1364
NetKeys.QCMD_XILIANCHI_XILIAN_VIEW = 1365
NetKeys.QCMD_XILIANCHI_XILIAN_ITEM_VIEW = 1366
NetKeys.QCMD_XILIANCHI_XILIAN_ITEM = 1367
NetKeys.QCMD_XILIANCHI_KAIGUANG = 1368
NetKeys.QCMD_XILIANCHI_KAIGUANG_XILIAN = 1369
NetKeys.QCMD_XILIANCHI_XILIAN_LOCK = 1370
NetKeys.QCMD_XILIANCHI_XILIAN_UNLOCK = 1371
NetKeys.QCMD_XILIANCHI_JINGLIAN_VIEW = 1372
NetKeys.QCMD_XILIANCHI_JINGLIAN_ITEM_VIEW = 1373
NetKeys.QCMD_JINGLIAN_EQUIP = 1374
NetKeys.QCMD_LIST_CLAN = 1400
NetKeys.QCMD_CREATE_CLAN = 1401
NetKeys.QCMD_VIEW_CLAN = 1402
NetKeys.QCMD_LIST_CLAN_MEMBER = 1403
NetKeys.QCMD_LIST_JOIN_CLAN_REQS = 1404
NetKeys.QCMD_JOIN_CLAN = 1405
NetKeys.QCMD_ALLOW_JOIN_CLAN = 1406
NetKeys.QCMD_REJECT_JOIN_CLAN = 1407
NetKeys.QCMD_AUTOJOIN_CLAN = 1408
NetKeys.QCMD_DONATE_CLAN_MONEY = 1409
NetKeys.QCMD_DONATE_CLAN_EXP = 1410
NetKeys.QCMD_QUIT_CLAN = 1411
NetKeys.QCMD_ALLOT_CLAN_MONEY = 1412
NetKeys.QCMD_ADD_CLAN_BRANCH = 1413
NetKeys.QCMD_MODIFY_CLAN = 1414
NetKeys.QCMD_RECALL_CLAN_DUTY = 1415
NetKeys.QCMD_APPOINT_CLAN_DUTY = 1416
NetKeys.QCMD_DEPLOY_CLAN_MEMBER = 1417
NetKeys.QCMD_KICKOUT_CLAN_MEMBER = 1418
NetKeys.QCMD_REMOVE_CLAN_BRANCH = 1419
NetKeys.QCMD_MODIFY_CLAN_BRANCH = 1420
NetKeys.QCMD_VIEW_CLAN_LOGS = 1421
NetKeys.QCMD_VIEW_CLAN_ITEM_SEIZERS = 1423
NetKeys.QCMD_VIEW_CLAN_ITEM_FLAG = 1424
NetKeys.QCMD_UPGRADE_CLAN_ITEM_FLAG = 1425
NetKeys.QCMD_SEIZE_CLAN_ITEM = 1426
NetKeys.QCMD_VIEW_CLAN_ITEM_STATUS = 1427
NetKeys.QCMD_ENTER_CLAN_SCENE = 1428
NetKeys.QCMD_ENTER_CLAN_WAR_LINE = 1429
NetKeys.QCMD_CLAN_ITEMS = 1430
NetKeys.QCMD_DIAOYU_VIEW = 1450
NetKeys.QCMD_DO_DIAOYU = 1451
NetKeys.QCMD_TOUHU_VIEW = 1452
NetKeys.QCMD_TOUHU = 1453
NetKeys.QCMD_TOUHU_RESET = 1454

local names = NetKeys.QCMD_NAMES
local handlers = NetKeys.PackHandlers
-- local json = require 'cjson'

names[0] = 'QCMD_ACTIVE'
handlers[0] = function(sp, arg0)
	sp:WriteLong(arg0)
end

names[1] = 'QCMD_WELCOME'
handlers[1] = function(sp, arg0, arg1)
	sp:WriteShort(arg0)
	sp:WriteString(arg1)
end

names[2] = 'QCMD_LINES'

names[3] = 'QCMD_CREATE_ROLE'
handlers[3] = function(sp, arg0, arg1, arg2)
	sp:WriteByte(arg0)
	sp:WriteByte(arg1)
	sp:WriteString(arg2)
end

names[4] = 'QCMD_REMOVE_ROLE'
handlers[4] = function(sp, arg0, arg1)
	sp:WriteString(arg0)
	sp:WriteString(arg1)
end

names[5] = 'QCMD_RANDOM_NAME'
handlers[5] = function(sp, arg0)
	sp:WriteByte(arg0)
end

names[6] = 'QCMD_CHANGE_NAME'
handlers[6] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[7] = 'QCMD_CHECK_ROLE_NAME'
handlers[7] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[8] = 'QCMD_LOGIN'
handlers[8] = function(sp, arg0, arg1, arg2)
	sp:WriteString(arg0)
	sp:WriteString(arg1)
	sp:WriteString(arg2)
end

names[9] = 'QCMD_LOGOUT'
handlers[9] = function(sp, arg0, arg1)
	sp:WriteString(arg0)
	sp:WriteString(arg1)
end

names[10] = 'QCMD_READY'

names[11] = 'QCMD_SWITCH_LINE'
handlers[11] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[12] = 'QCMD_DEBUG'
handlers[12] = function(sp, arg0, arg1, arg2, arg3, arg4)
	sp:WriteInt(arg0)
	sp:WriteInt(arg1)
	sp:WriteString(arg2)
	sp:WriteInt(arg3)
	sp:WriteString(arg4)
end

names[13] = 'QCMD_ENTER'
handlers[13] = function(sp, arg0, arg1, arg2, arg3)
	sp:WriteString(arg0)
	sp:WriteInt(arg1)
	sp:WriteString(arg2)
	sp:WriteString(arg3)
end

names[101] = 'QCMD_MOVE'
handlers[101] = function(sp, arg0)
	sp:WriteUShort(#arg0)
	for _,v in ipairs(arg0) do
		sp:WriteShort(v.x) sp:WriteShort(v.y)
	end
end

names[102] = 'QCMD_GO'
handlers[102] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[103] = 'QCMD_FLY'
handlers[103] = function(sp, arg0)
	sp:WriteByte(arg0)
end

names[104] = 'QCMD_RELIVE'
handlers[104] = function(sp, arg0)
	sp:WriteByte(arg0)
end

names[105] = 'QCMD_ENTER_CLAN_SCENE'
handlers[105] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[201] = 'QCMD_QUERY_ATTRS'
handlers[201] = function(sp, arg0)
	sp:WriteUShort(#arg0)
	for _,v in ipairs(arg0) do
		sp:WriteString(v)
	end
end

names[202] = 'QCMD_PRIVATE_SETUP'
handlers[202] = function(sp, arg0)
	sp:WriteJSON(json.encode(arg0))
end

names[203] = 'QCMD_SET_PK'
handlers[203] = function(sp, arg0)
	sp:WriteByte(arg0)
end

names[204] = 'QCMD_SET_ROLE_LOCK'
handlers[204] = function(sp, arg0, arg1)
	sp:WriteString(arg0)
	sp:WriteString(arg1)
end

names[205] = 'QCMD_SET_USER_TITLE'
handlers[205] = function(sp, arg0)
	sp:WriteByte(arg0)
end

names[206] = 'QCMD_FETCH_GIFTS'
handlers[206] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[207] = 'QCMD_MAIN_LIST_OP'
handlers[207] = function(sp, arg0, arg1)
	sp:WriteShort(arg0)
	sp:WriteString(arg1)
end

names[208] = 'QCMD_MAIN_LIST_VIEW'

names[251] = 'QCMD_VIEW_USER'
handlers[251] = function(sp, arg0, arg1)
	sp:WriteString(arg0)
	sp:WriteShort(arg1)
end

names[252] = 'QCMD_SET_EN_LINGLI'
handlers[252] = function(sp, arg0, arg1)
	sp:WriteShort(arg0)
	sp:WriteString(arg1)
end

names[253] = 'QCMD_XIULIAN_JINGMAI'
handlers[253] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[254] = 'QCMD_ADD_LINGLI'

names[255] = 'QCMD_ADD_ATTRS'
handlers[255] = function(sp, arg0)
	sp:WriteJSON(json.encode(arg0))
end

names[256] = 'QCMD_VIEW_ERXX'
handlers[256] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[257] = 'QCMD_APPLY_ERXX'
handlers[257] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[258] = 'QCMD_ALLOW_ERXX'
handlers[258] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[259] = 'QCMD_GX_ERXX_EXP'
handlers[259] = function(sp, arg0)
	sp:WriteLong(arg0)
end

names[260] = 'QCMD_JF_ERXX'

names[261] = 'QCMD_FETCH_SX'

names[262] = 'QCMD_ERXX_LEARN_SKILL'
handlers[262] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[263] = 'QCMD_QUIT_ERXX'

names[264] = 'QCMD_CLICK_HEAD'
handlers[264] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[301] = 'QCMD_MY_ITEMS'
handlers[301] = function(sp, arg0, arg1, arg2)
	sp:WriteByte(arg0)
	sp:WriteInt(arg1)
	sp:WriteInt(arg2)
end

names[302] = 'QCMD_USE_ITEM'
handlers[302] = function(sp, arg0, arg1, arg2, arg3)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
	sp:WriteString(arg2)
	sp:WriteByte(arg3)
end

names[303] = 'QCMD_HANG_ON_ITEM'
handlers[303] = function(sp, arg0, arg1, arg2)
	sp:WriteString(arg0)
	sp:WriteLong(arg1)
	sp:WriteInt(arg2)
end

names[304] = 'QCMD_PICK_ITEM'
handlers[304] = function(sp, arg0, arg1)
	sp:WriteString(arg0)
	sp:WriteString(arg1)
end

names[305] = 'QCMD_ADD_BURTHEN'

names[306] = 'QCMD_VIEW_ITEM'
handlers[306] = function(sp, arg0, arg1, arg2, arg3, arg4)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
	sp:WriteString(arg2)
	sp:WriteString(arg3)
	sp:WriteByte(arg4)
end

names[307] = 'QCMD_DROP_ITEM'
handlers[307] = function(sp, arg0, arg1)
	sp:WriteString(arg0)
	sp:WriteInt(arg1)
end

names[308] = 'QCMD_SALE_ITEM_TO_NPC'
handlers[308] = function(sp, arg0, arg1, arg2)
	sp:WriteString(arg0)
	sp:WriteString(arg1)
	sp:WriteInt(arg2)
end

names[309] = 'QCMD_TAKE_OFF_EQUIP2'
handlers[309] = function(sp, arg0, arg1, arg2)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
	sp:WriteString(arg2)
end

names[310] = 'QCMD_LIST_USER_ITEMS'
handlers[310] = function(sp, arg0, arg1)
	sp:WriteString(arg0)
	sp:WriteByte(arg1)
end

names[311] = 'QCMD_HANG_OFF_ITEM'
handlers[311] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[314] = 'QCMD_BUY_ITEM_FROM_USER'
handlers[314] = function(sp, arg0, arg1, arg2, arg3)
	sp:WriteString(arg0)
	sp:WriteString(arg1)
	sp:WriteInt(arg2)
	sp:WriteLong(arg3)
end

names[312] = 'QCMD_GIVE_ITEM'
handlers[312] = function(sp, arg0, arg1, arg2)
	sp:WriteString(arg0)
	sp:WriteString(arg1)
	sp:WriteInt(arg2)
end

names[313] = 'QCMD_GIVE_MONEY'
handlers[313] = function(sp, arg0, arg1)
	sp:WriteString(arg0)
	sp:WriteInt(arg1)
end

names[352] = 'QCMD_VIEW_MAGIC'
handlers[352] = function(sp, arg0, arg1)
	sp:WriteString(arg0)
	sp:WriteString(arg1)
end

names[353] = 'QCMD_MAGIC_CALL_OUT'
handlers[353] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[354] = 'QCMD_MAGIC_CALL_BACK'

names[355] = 'QCMD_SKILL_LVLUP'
handlers[355] = function(sp, arg0, arg1)
	sp:WriteString(arg0)
	sp:WriteString(arg1)
end

names[356] = 'QCMD_MAGIC_LIANHUA_VIEW'
handlers[356] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[357] = 'QCMD_MAGIC_LIANHUA'
handlers[357] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[401] = 'QCMD_MY_PETS'
handlers[401] = function(sp, arg0)
	sp:WriteInt(arg0)
end

names[402] = 'QCMD_VIEW_PET'
handlers[402] = function(sp, arg0, arg1)
	sp:WriteString(arg0)
	sp:WriteString(arg1)
end

names[403] = 'QCMD_CHUCK'
handlers[403] = function(sp, arg0, arg1)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
end

names[404] = 'QCMD_CALL_OUT'
handlers[404] = function(sp, arg0, arg1)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
end

names[405] = 'QCMD_CALL_BACK'

names[406] = 'QCMD_ADD_PET_ZZ'
handlers[406] = function(sp, arg0, arg1)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
end

names[407] = 'QCMD_LEARN_SKILL'
handlers[407] = function(sp, arg0, arg1, arg2)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
	sp:WriteString(arg2)
end

names[408] = 'QCMD_TUJIANS'
handlers[408] = function(sp, arg0)
	sp:WriteByte(arg0)
end

names[409] = 'QCMD_TUJIAN_VIEW'
handlers[409] = function(sp, arg0, arg1)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
end

names[410] = 'QCMD_TUJIAN_FUHUA'
handlers[410] = function(sp, arg0, arg1)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
end

names[411] = 'QCMD_HECHENG_PET_VIEW'
handlers[411] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[412] = 'QCMD_HECHENG_PET'
handlers[412] = function(sp, arg0, arg1)
	sp:WriteString(arg0)
	sp:WriteString(arg1)
end

names[413] = 'QCMD_HECHENG_PETS'
handlers[413] = function(sp, arg0, arg1)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
end

names[414] = 'QCMD_GET_LIANYAOHU_GEZI'
handlers[414] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[415] = 'QCMD_LIANYAOHU_GO'
handlers[415] = function(sp, arg0, arg1)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
end

names[452] = 'QCMD_VIEW_MOUNT'
handlers[452] = function(sp, arg0, arg1)
	sp:WriteString(arg0)
	sp:WriteString(arg1)
end

names[453] = 'QCMD_RIDE_ON'
handlers[453] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[454] = 'QCMD_JUMP_DOWN'

names[455] = 'QCMD_DUANLIANLU_VIEW'
handlers[455] = function(sp, arg0)
	sp:WriteByte(arg0)
end

names[456] = 'QCMD_BUILD_MOUNTS'
handlers[456] = function(sp, arg0, arg1, arg2)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
	sp:WriteString(arg2)
end

names[457] = 'QCMD_DUANLIAN_MOUNTS'
handlers[457] = function(sp, arg0, arg1)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
end

names[501] = 'QCMD_ATTACK'
handlers[501] = function(sp, arg0, arg1, arg2, arg3)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
	sp:WriteString(arg2)
	sp:WriteByte(arg3)
end

names[601] = 'QCMD_TASKS'

names[651] = 'QCMD_VIEW_NPC'
handlers[651] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[652] = 'QCMD_TOUCH_NPC'
handlers[652] = function(sp, arg0, arg1)
	sp:WriteString(arg0)
	sp:WriteString(arg1)
end

names[653] = 'QCMD_BUY_ITEM_FROM_NPC'
handlers[653] = function(sp, arg0, arg1, arg2)
	sp:WriteString(arg0)
	sp:WriteString(arg1)
	sp:WriteInt(arg2)
end

names[701] = 'QCMD_FRIENDS'
handlers[701] = function(sp, arg0)
	sp:WriteByte(arg0)
end

names[702] = 'QCMD_FRIEND_ADD'
handlers[702] = function(sp, arg0, arg1)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
end

names[703] = 'QCMD_FRIEND_REMOVE'
handlers[703] = function(sp, arg0, arg1)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
end

names[704] = 'QCMD_FIND_USER'
handlers[704] = function(sp, arg0, arg1)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
end

names[711] = 'QCMD_CHAT_MSG'
handlers[711] = function(sp, arg0, arg1, arg2)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
	sp:WriteString(arg2)
end

names[801] = 'QCMD_MAILS'
handlers[801] = function(sp, arg0, arg1, arg2)
	sp:WriteByte(arg0)
	sp:WriteInt(arg1)
	sp:WriteByte(arg2)
end

names[802] = 'QCMD_MAIL_SEND'
handlers[802] = function(sp, arg0, arg1, arg2)
	sp:WriteString(arg0)
	sp:WriteString(arg1)
	sp:WriteString(arg2)
end

names[803] = 'QCMD_MAIL_SET_STATUS'
handlers[803] = function(sp, arg0, arg1)
	sp:WriteByte(arg0)
	sp:WriteUShort(#arg1)
	for _,v in ipairs(arg1) do
		sp:WriteInt(v)
	end
end

names[804] = 'QCMD_MAIL_RECV_EXT'
handlers[804] = function(sp, arg0)
	sp:WriteInt(arg0)
end

names[901] = 'QCMD_TEAM_VIEW'
handlers[901] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[902] = 'QCMD_TEAM_CREATE'
handlers[902] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[903] = 'QCMD_TEAM_JOIN'
handlers[903] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[904] = 'QCMD_TEAM_OPERATE'
handlers[904] = function(sp, arg0, arg1)
	sp:WriteString(arg0)
	sp:WriteByte(arg1)
end

names[905] = 'QCMD_TEAM_INVITE'
handlers[905] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[906] = 'QCMD_TEAM_KICKOUT'
handlers[906] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[907] = 'QCMD_TEAM_QUIT'

names[908] = 'QCMD_TEAM_OVER'

names[909] = 'QCMD_TEAM_SHIFT'
handlers[909] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[910] = 'QCMD_TEAM_SETUP'
handlers[910] = function(sp, arg0, arg1)
	sp:WriteString(arg0)
	sp:WriteByte(arg1)
end

names[1001] = 'QCMD_SORTBOARD'
handlers[1001] = function(sp, arg0, arg1, arg2)
	sp:WriteByte(arg0)
	sp:WriteByte(arg1)
	sp:WriteByte(arg2)
end

names[1101] = 'QCMD_MALL_OPEN'
handlers[1101] = function(sp, arg0)
	sp:WriteByte(arg0)
end

names[1102] = 'QCMD_MALL_BUY'
handlers[1102] = function(sp, arg0, arg1)
	sp:WriteString(arg0)
	sp:WriteInt(arg1)
end

names[1201] = 'QCMD_HD_INFO'

names[1202] = 'QCMD_SIGN_INFO'

names[1203] = 'QCMD_DO_SIGN'

names[1204] = 'QCMD_FILL_AMOUNT'
handlers[1204] = function(sp, arg0, arg1, arg2)
	sp:WriteString(arg0)
	sp:WriteString(arg1)
	sp:WriteString(arg2)
end

names[1210] = 'QCMD_QITIAN_VIEW'
handlers[1210] = function(sp, arg0)
	sp:WriteShort(arg0)
end

names[1211] = 'QCMD_QITIAN_LINQU'
handlers[1211] = function(sp, arg0)
	sp:WriteShort(arg0)
end

names[1300] = 'QCMD_QUERY_HANGING_SETUP'

names[1301] = 'QCMD_HANGING_SETUP'
handlers[1301] = function(sp, arg0)
	sp:WriteJSON(json.encode(arg0))
end

names[1302] = 'QCMD_HANGING_START'
handlers[1302] = function(sp, arg0, arg1)
	sp:WriteByte(arg0)
	sp:WriteByte(arg1)
end

names[1303] = 'QCMD_HANGING_STOP'

names[1304] = 'QCMD_HANGING_GJK'
handlers[1304] = function(sp, arg0)
	sp:WriteByte(arg0)
end

names[1350] = 'QCMD_EMBED_VIEW'
handlers[1350] = function(sp, arg0, arg1, arg2)
	sp:WriteString(arg0)
	sp:WriteString(arg1)
	sp:WriteString(arg2)
end

names[1351] = 'QCMD_EMBED_ON'
handlers[1351] = function(sp, arg0, arg1, arg2, arg3)
	sp:WriteString(arg0)
	sp:WriteString(arg1)
	sp:WriteString(arg2)
	sp:WriteString(arg3)
end

names[1352] = 'QCMD_EMBED_OFF'
handlers[1352] = function(sp, arg0, arg1, arg2, arg3)
	sp:WriteString(arg0)
	sp:WriteString(arg1)
	sp:WriteString(arg2)
	sp:WriteString(arg3)
end

names[1353] = 'QCMD_UPGRADE_WEAPONS'
handlers[1353] = function(sp, arg0, arg1)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
end

names[1354] = 'QCMD_UPGRADE_WEAPON_VIEW'
handlers[1354] = function(sp, arg0, arg1)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
end

names[1355] = 'QCMD_UPGRADE_WEAPON'
handlers[1355] = function(sp, arg0, arg1)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
end

names[1356] = 'QCMD_UPGRADE_SHENBING'
handlers[1356] = function(sp, arg0, arg1, arg2)
	sp:WriteString(arg0)
	sp:WriteString(arg1)
	sp:WriteString(arg2)
end

names[1357] = 'QCMD_BUILD_ITEMS'
handlers[1357] = function(sp, arg0)
	sp:WriteByte(arg0)
end

names[1358] = 'QCMD_BUILD_ITEM_VIEW'
handlers[1358] = function(sp, arg0, arg1)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
end

names[1359] = 'QCMD_BUILD_ITEM'
handlers[1359] = function(sp, arg0, arg1, arg2)
	sp:WriteString(arg0)
	sp:WriteByte(arg1)
	sp:WriteByte(arg2)
end

names[1360] = 'QCMD_RONGLIAN_ITEMS'

names[1361] = 'QCMD_RONGLIAN_ITEM_VIEW'
handlers[1361] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[1362] = 'QCMD_RONGLIAN_ITEM'
handlers[1362] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[1363] = 'QCMD_XILIAN_RONGLIAN_VIEW'
handlers[1363] = function(sp, arg0)
	sp:WriteByte(arg0)
end

names[1364] = 'QCMD_XILIAN_RONGLIAN'
handlers[1364] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[1365] = 'QCMD_XILIANCHI_XILIAN_VIEW'

names[1366] = 'QCMD_XILIANCHI_XILIAN_ITEM_VIEW'
handlers[1366] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[1367] = 'QCMD_XILIANCHI_XILIAN_ITEM'
handlers[1367] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[1368] = 'QCMD_XILIANCHI_KAIGUANG'
handlers[1368] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[1369] = 'QCMD_XILIANCHI_KAIGUANG_XILIAN'
handlers[1369] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[1370] = 'QCMD_XILIANCHI_XILIAN_LOCK'
handlers[1370] = function(sp, arg0, arg1)
	sp:WriteString(arg0)
	sp:WriteInt(arg1)
end

names[1371] = 'QCMD_XILIANCHI_XILIAN_UNLOCK'
handlers[1371] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[1372] = 'QCMD_XILIANCHI_JINGLIAN_VIEW'

names[1373] = 'QCMD_XILIANCHI_JINGLIAN_ITEM_VIEW'
handlers[1373] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[1374] = 'QCMD_JINGLIAN_EQUIP'
handlers[1374] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[1400] = 'QCMD_LIST_CLAN'
handlers[1400] = function(sp, arg0, arg1, arg2)
	sp:WriteShort(arg0)
	sp:WriteShort(arg1)
	sp:WriteString(arg2)
end

names[1401] = 'QCMD_CREATE_CLAN'
handlers[1401] = function(sp, arg0, arg1, arg2)
	sp:WriteString(arg0)
	sp:WriteString(arg1)
	sp:WriteString(arg2)
end

names[1402] = 'QCMD_VIEW_CLAN'
handlers[1402] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[1403] = 'QCMD_LIST_CLAN_MEMBER'
handlers[1403] = function(sp, arg0, arg1, arg2, arg3)
	sp:WriteString(arg0)
	sp:WriteByte(arg1)
	sp:WriteShort(arg2)
	sp:WriteShort(arg3)
end

names[1404] = 'QCMD_LIST_JOIN_CLAN_REQS'
handlers[1404] = function(sp, arg0, arg1)
	sp:WriteShort(arg0)
	sp:WriteShort(arg1)
end

names[1405] = 'QCMD_JOIN_CLAN'
handlers[1405] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[1406] = 'QCMD_ALLOW_JOIN_CLAN'
handlers[1406] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[1407] = 'QCMD_REJECT_JOIN_CLAN'
handlers[1407] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[1408] = 'QCMD_AUTOJOIN_CLAN'
handlers[1408] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[1409] = 'QCMD_DONATE_CLAN_MONEY'
handlers[1409] = function(sp, arg0)
	sp:WriteLong(arg0)
end

names[1410] = 'QCMD_DONATE_CLAN_EXP'

names[1411] = 'QCMD_QUIT_CLAN'

names[1412] = 'QCMD_ALLOT_CLAN_MONEY'
handlers[1412] = function(sp, arg0, arg1)
	sp:WriteType(arg0)
	sp:WriteLong(arg1)
end

names[1413] = 'QCMD_ADD_CLAN_BRANCH'
handlers[1413] = function(sp, arg0, arg1, arg2)
	sp:WriteString(arg0)
	sp:WriteString(arg1)
	sp:WriteShort(arg2)
end

names[1414] = 'QCMD_MODIFY_CLAN'
handlers[1414] = function(sp, arg0, arg1, arg2)
	sp:WriteString(arg0)
	sp:WriteString(arg1)
	sp:WriteString(arg2)
end

names[1415] = 'QCMD_RECALL_CLAN_DUTY'
handlers[1415] = function(sp, arg0, arg1)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
end

names[1416] = 'QCMD_APPOINT_CLAN_DUTY'
handlers[1416] = function(sp, arg0, arg1, arg2)
	sp:WriteByte(arg0)
	sp:WriteByte(arg1)
	sp:WriteString(arg2)
end

names[1417] = 'QCMD_DEPLOY_CLAN_MEMBER'
handlers[1417] = function(sp, arg0, arg1)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
end

names[1418] = 'QCMD_KICKOUT_CLAN_MEMBER'
handlers[1418] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[1419] = 'QCMD_REMOVE_CLAN_BRANCH'
handlers[1419] = function(sp, arg0)
	sp:WriteByte(arg0)
end

names[1420] = 'QCMD_MODIFY_CLAN_BRANCH'
handlers[1420] = function(sp, arg0, arg1, arg2, arg3)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
	sp:WriteString(arg2)
	sp:WriteShort(arg3)
end

names[1421] = 'QCMD_VIEW_CLAN_LOGS'
handlers[1421] = function(sp, arg0, arg1)
	sp:WriteByte(arg0)
	sp:WriteByte(arg1)
end

names[1423] = 'QCMD_VIEW_CLAN_ITEM_SEIZERS'
handlers[1423] = function(sp, arg0, arg1, arg2, arg3)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
	sp:WriteString(arg2)
	sp:WriteShort(arg3)
end

names[1424] = 'QCMD_VIEW_CLAN_ITEM_FLAG'
handlers[1424] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[1425] = 'QCMD_UPGRADE_CLAN_ITEM_FLAG'
handlers[1425] = function(sp, arg0, arg1, arg2)
	sp:WriteByte(arg0)
	sp:WriteLong(arg1)
	sp:WriteString(arg2)
end

names[1426] = 'QCMD_SEIZE_CLAN_ITEM'
handlers[1426] = function(sp, arg0, arg1)
	sp:WriteByte(arg0)
	sp:WriteString(arg1)
end

names[1427] = 'QCMD_VIEW_CLAN_ITEM_STATUS'
handlers[1427] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[1428] = 'QCMD_ENTER_CLAN_SCENE'
handlers[1428] = function(sp, arg0)
	sp:WriteString(arg0)
end

names[1429] = 'QCMD_ENTER_CLAN_WAR_LINE'

names[1430] = 'QCMD_CLAN_ITEMS'

names[1450] = 'QCMD_DIAOYU_VIEW'
handlers[1450] = function(sp, arg0)
	sp:WriteByte(arg0)
end

names[1451] = 'QCMD_DO_DIAOYU'
handlers[1451] = function(sp, arg0)
	sp:WriteByte(arg0)
end

names[1452] = 'QCMD_TOUHU_VIEW'

names[1453] = 'QCMD_TOUHU'
handlers[1453] = function(sp, arg0)
	sp:WriteByte(arg0)
end

names[1454] = 'QCMD_TOUHU_RESET'

