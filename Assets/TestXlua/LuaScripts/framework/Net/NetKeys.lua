NetKeys = {}
NetKeys.QCMD_NAMES = {}
NetKeys.ACMD_NAMES = {}
NetKeys.PackHandlers = {}
NetKeys.UnpackHandlers = {}

function NetKeys.GetCmdName(cmd, isqcmd)
	local ret
	if isqcmd then
		ret = NetKeys.QCMD_NAMES[cmd]
	else
		ret = NetKeys.ACMD_NAMES[cmd]
	end
	if ret==nil then ret = ''..cmd end
	return ret
end

require "framework/Net/Protocol_qcmd"
require "framework/Net/Protocol_acmd"

--特殊处理
NetKeys.PackHandlers[NetKeys.QCMD_MOVE] = function(sp, arg0)
	sp:WriteUShort(arg0.length)
	for i=1,arg0.length do
		sp:WriteShort(arg0[i].x)
		sp:WriteShort(arg0[i].y)
	end
end
