require "framework/Net/NetKeys"
require "Common/event"

-- require "3rd/cjson"

--LineInfo = {}

NetClient = {
    --private
    _OnQCMD = {},
    _OnACMD = {},

    connected = false, --物理连接已建立
    roleLogin = false, --角色已进入游戏
    traceEnabled = true
}

NetClient.VER = 256

NetClient.USER_KEY = ""
NetClient.HOST = ""
NetClient.PORT = 0
NetClient.USER_ID = ""
NetClient.AREA_ID = ""
NetClient.AREA_NAME  = ""
NetClient.CKey = ""

local MAX_CONNECT_RETRY = 5

local _socket
local _lineSocket
local _lineData

local _retryConnect = 0
local _reconnectPrompt

function NetClient.Init()
    _socket = SocketProcessor()--SocketProcessor.New()
    _socket.onConnected = NetClient.OnConnected
    _socket.onDisconnected = NetClient.OnDisconnected
    _socket.onPacket = NetClient.OnPacket

    _lineSocket = SocketProcessor()

    NetClient.traceEnabled = GameConfig.netTraceEnabled

    NetClient.ListenRet(NetKeys.QCMD_WELCOME, NetClient.OnWelcomeError)
    NetClient.Listen(NetKeys.ACMD_THINGS, NetClient.HandleSceneInfo)
    NetClient.Listen(NetKeys.ACMD_REDIRECT, NetClient.OnRedirect)
    NetClient.Listen(NetKeys.ACMD_OPEN_WIN, NetClient.OnOpenWin)
    NetClient.Listen(NetKeys.ACMD_WELCOME, NetClient.OnWeleome)

    _reconnectPrompt = UIPackage.CreateObject("Common", "ModalWaiting")
    _reconnectPrompt:SetSize(GRoot.inst.width, GRoot.inst.height)
    _reconnectPrompt.sortingOrder = SortingOrder.Modal
end

function NetClient.Pause()
    _socket:Pause()
end

function NetClient.Resume()
    _socket:Resume()
end

function NetClient.ListenRet(cmd, func, selfp)
    assert(cmd and func)

    local r = NetClient._OnQCMD[cmd]
    if r==nil then
        r = event('OnQCMD'..cmd)
        NetClient._OnQCMD[cmd] = r
    end
    r:Add(func, selfp)
end

function NetClient.UnlistenRet(cmd, func, selfp)
    assert(cmd and func)

    local r = NetClient._OnQCMD[cmd]
    if r==nil then
        r = event('OnQCMD'..cmd)
        NetClient._OnQCMD[cmd] = r
    end
    r:Remove(func, selfp)
end

function NetClient.Listen(cmd, func, selfp)
    assert(cmd and func)

    local r = NetClient._OnACMD[cmd]
    if r==nil then
        r = event('OnACMD'..cmd)
        NetClient._OnACMD[cmd] = r
    end
    r:Add(func, selfp)
end

function NetClient.Unlisten(cmd, func, selfp)
    assert(cmd and func)

    local r = NetClient._OnACMD[cmd]
    if r==nil then
        r = event('OnACMD'..cmd)
        NetClient._OnACMD[cmd] = r
    end
    r:Remove(func, selfp)
end

function NetClient.ConnectAndLogin()
    _retryConnect = 0
    print(string.format("connect %s:%s" , NetClient.HOST , NetClient.PORT))
    _socket:Connect(NetClient.HOST, NetClient.PORT)
end

function NetClient.Close()
    NetClient.connected = false
    NetClient.roleLogin = false

    fgui.remove_timer(NetClient.Reconnect)
    fgui.remove_timer(NetClient.KeepActive)
    _socket:Close()
    if _reconnectPrompt.parent~=nil then
        GRoot.inst:RemoveChild(_reconnectPrompt)
    end
end

function NetClient.Send(cmd, ...)
    assert(cmd)

    if NetClient.traceEnabled then
        if cmd~=NetKeys.QCMD_MOVE and cmd~= NetKeys.QCMD_ACTIVE then
            print("send.."..NetKeys.GetCmdName(cmd, true)) 
        end
    end
    local packHandler = NetKeys.PackHandlers[cmd]
    if packHandler~=nil then
        packHandler(_socket, ...)
    end
    _socket:Send(cmd)
end

---private start------
--由C#调用，通知连接已建立
function NetClient.OnConnected()
    print "server connected"

    NetClient.connected = true
    NetClient.deletingRole = false

    --需要显示ping值把时间改为30s
    fgui.add_timer(30, 0, NetClient.KeepActive)
    NetClient.Send(NetKeys.QCMD_WELCOME, NetClient.VER, NetClient.USER_KEY)

    if _retryConnect>0 and _lineData~=nil then
        NetClient.Send(NetKeys.QCMD_LOGIN, _lineData.id, "", "")
    end

    _retryConnect = 0
end

function NetClient.KeepActive()
    NetClient.Send(NetKeys.QCMD_ACTIVE, Time.realtimeSinceStartup)
end

--由C#调用，通知连接已断开
function NetClient.OnDisconnected(reason)
    print "server disconnected"

    NetClient.connected = false
    NetClient.roleLogin = false
    fgui.remove_timer(NetClient.Reconnect)
    fgui.remove_timer(NetClient.KeepActive)

    if NetClient.deletingRole then
        NetClient.deletingRole = false
        World.Close()
        RoleUI.Show()
        return
    end

    if _reconnectPrompt.parent~=nil then
        GRoot.inst:RemoveChild(_reconnectPrompt)
    end

    if LoginUI.visible then
        AlertWin.Open(reason)
    elseif _retryConnect<MAX_CONNECT_RETRY then
        GRoot.inst:AddChild(_reconnectPrompt)
        if _retryConnect>0 then
            fgui.add_timer(5, 1, NetClient.Reconnect)
        else
            NetClient.Reconnect()
        end
    else
        AlertWin.Open("无法连接服务器，请检查网络", function()
            World.Close()
            RoleUI.Show()
        end)
    end
end

function NetClient.Reconnect()
    _retryConnect = _retryConnect+1

    print(string.format("reconnect %s:%s %d" , NetClient.HOST , NetClient.PORT, _retryConnect))
    _socket:Connect(NetClient.HOST, NetClient.PORT)
end

--由C#调用，通知连收到数据包
function NetClient.OnPacket(packet)
    local cmd = packet.cmd

    local unpackHandler = NetKeys.UnpackHandlers[cmd]
    local data
    if unpackHandler~=nil then
        if NetClient.traceEnabled then
            if cmd~=NetKeys.ACMD_RET and cmd~=NetKeys.ACMD_RET_EXT and cmd~=NetKeys.ACMD_MOVE_USER and cmd~=NetKeys.ACMD_MOVE_NPC then
                 print(string.format("received %s", NetKeys.GetCmdName(cmd)))
            end
        end
        data = unpackHandler(packet)
    else
        print(string.format("no handler for %s", NetKeys.GetCmdName(cmd)))
        return
    end

    local m
    if cmd==NetKeys.ACMD_RET or cmd==NetKeys.ACMD_RET_EXT then
        m = NetClient._OnQCMD[data.rcmd]

        if data.code~=0 then
            if NetClient.traceEnabled then
                print(string.format("%s err=%d %s", NetKeys.GetCmdName(data.rcmd, true), data.code, data.msg))
            end
            if m==nil then
                AlertWin.Open(data.msg)
            end
        else
            if m==nil and data.msg ~= nil and data.msg ~= "" then
                --AlertWin.Open(data.msg)
                ChatPanel.AddAlertMsg(data.msg)
                --在具体接口里处理，不能一概而论用弹窗，影响体验
            --[[elseif data.msg ~= nil and data.msg ~= "" then
                AlertWin.Open(data.msg)]]
            end
        end
    else
        m = NetClient._OnACMD[cmd]
    end

    if m ~= nil then
        m(cmd, data)
    end
end

function NetClient.OnRedirect(cmd, data)
    _lineData = data
    if _lineData.host=="192.168.1.243" then
        _lineData.host = 'juntai.yytou.com'
    end
    if data.ckey ~= nil and data.ckey ~= "" then
        NetClient.CKey = data.ckey
    end
    print(string.format("connect %s:%s", _lineData.host, _lineData.port))

    _lineSocket.onConnected = NetClient.OnLineConnected
    _lineSocket.onDisconnected = NetClient.OnLineDisconnected
    _lineSocket.onPacket = NetClient.OnLinePacket
    _lineSocket:Connect(_lineData.host, _lineData.port)
end

function NetClient.OnOpenWin(cmd, data)
    if data.win == "WeaponUpgradeShenBingWin" then
        G_WeaponUpgradeShenBingWin:Open(data.op, data.rid)
    elseif data.win == "EquipBuildWin0" then
        G_EquipBuildWin:Open(0)
    elseif data.win == "EquipBuildWin1" then
        G_EquipBuildWin:Open(1)
    elseif data.win == "EquipRongLianWin" then
        G_EquipRongLianWin:Open()
    elseif data.win == "DiaoYuWin" then
        G_DiaoYuWin:Show()
    elseif data.win == "TouHuWin" then
        G_TouHuWin:Show()
    end
end

function NetClient.OnLineConnected()
    print("line connected")

    local packHandler = NetKeys.PackHandlers[NetKeys.QCMD_ENTER]
    packHandler(_lineSocket, _lineData.id, _lineData.time, _lineData.ckey, _lineData.param)
    _lineSocket:Send(NetKeys.QCMD_ENTER)
end

function NetClient.OnLineDisconnected(reason)
    AlertWin.Open(reason)
end

function NetClient.OnLinePacket(packet)
    local cmd = packet.cmd
    local unpackHandler = NetKeys.UnpackHandlers[cmd]
    local data = unpackHandler(packet)

    print(string.format("received %s", NetKeys.GetCmdName(cmd)))
    if cmd==NetKeys.ACMD_RET then
        if data.code~=0 then
            AlertWin.Open(data.msg)
            _lineSocket:Close()
        end
    elseif cmd==NetKeys.ACMD_THINGS then
        _socket:Close()
        local tmp = _socket
        _socket = _lineSocket
        _lineSocket = tmp

        _socket.onConnected = NetClient.OnConnected
        _socket.onDisconnected = NetClient.OnDisconnected
        _socket.onPacket = NetClient.OnPacket

        NetClient.HandleSceneInfo(cmd, data)
    end
end

function NetClient.OnWelcomeError(cmd, data)
    if _reconnectPrompt.parent~=nil then
        GRoot.inst:RemoveChild(_reconnectPrompt)
    end

    NetClient.Close()
    AlertWin.Open(data.msg, LoginUI.Show)
end

function NetClient.HandleSceneInfo(cmd, data)
    if not NetClient.roleLogin then
        NetClient.roleLogin = true
        print(string.format("%s login", _lineData.id))
    end

    if _reconnectPrompt.parent~=nil then
        GRoot.inst:RemoveChild(_reconnectPrompt)
    end

    World.EnterScene(data)
end

function NetClient.OnWeleome(cmd, data)
--    LineInfo = {}
--    local lineInfo = data.ls
--    if lineInfo ~= nil then
--        for _i, _line in ipairs(lineInfo) do
--            if _line ~= nil and _line.uid ~= nil then
--                LineInfo[_line.uid] = {}
--                LineInfo[_line.uid].id = _line.id
--                LineInfo[_line.uid].na = _line.na
--            end
--        end
--    end
end

