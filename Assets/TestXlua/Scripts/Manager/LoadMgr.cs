﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using FairyGUI;
using System.Linq;
using FairyGame;
/// <summary>
/// 场景加载前的资源加载
/// </summary>
public class LoadMgr : UnitySingleton<LoadMgr>
{
    bool startReplace = false;
    public string[] fairyGUIPackList = null;

    void Awake() { 
		this.enabled = false; 
	}

    public void ReplaceScene(string uiPacList, Action<object> func = null, object self=null )
    {
        this.requestQueue = new Queue<Callback>();
        this.allIndex = 0;
        this.curIndex = 0;
        this.finish_func = func;
        this._self = self;
        this.timer = 1f;

        if (fairyGUIPackList != null) {
            Debug.Log(" 释放之前场景的 UI Fairy包 "); 
            for (int i = 0; i < fairyGUIPackList.Length; i++)
            {
                string curPackName = fairyGUIPackList[i];
                requestQueue.Enqueue(delegate() { UIPackage.RemovePackage("UI/" + curPackName); });
                this.allIndex++;
            }
        }

        this.fairyGUIPackList = uiPacList.Split(":"[0]).Where(t => { return !string.IsNullOrEmpty(t); }).ToArray();
        Debug.Log(" 加载当前的UI Fairy包" );
       
        for (int i = 0; i < fairyGUIPackList.Length; i++)
        {
            string curPackName = fairyGUIPackList[i];
            Debug.Log(" fairyGUIPackList[" + i + "] " + curPackName);
            AssetManager.inst.LoadAsset("ui1", curPackName,
                (string assetPath, string fileName, object data) => { requestQueue.Enqueue(delegate () 
                {
                    Debug.Log(">>>:" + fileName);
                    //UIPackage.AddPackage(assetPath);
                }); });

            //var packagePath = FairyGame.AssetManager.inst.GetAssetPath("ui", curPackName);
            //requestQueue.Enqueue(delegate() { UIPackage.AddPackage(packagePath); });
            this.allIndex++;
        }

        this.enabled = true;
    }

    public Queue<Callback> requestQueue;
    int allIndex;
    int curIndex;
    public Callback curRequest;


    float timer = 1f;
    private bool flag = false;
    void Update() 
    {
        if (flag)
            return;
        ///强制性加点延迟
        if ((timer -= Time.deltaTime) < 0f)
        {
            timer = 1f;

            if (curIndex < allIndex)
            {
                this.curRequest = requestQueue.Dequeue();
                this.curRequest();
                this.curIndex++;
                //UpdateProgree();
            }
            else
            {
                OnProgressFinish();
                //flag = true;
            }
        }
    }

    ///进度条显示当前的情况
    void OnGUI()
    {
        GUI.color = Color.red;
        GUI.Label(new Rect(10, 10, 1500, 100), "当前比例 " + (float)curIndex / (float)allIndex + "");
    }
    
    Action<object> finish_func;
    object _self;
    void OnProgressFinish() 
    {
        Debug.Log(" OnProgressFinish ");
		
        this.enabled = false;
        
        finish_func(_self);
        finish_func = null;
    }

    void OnDestroy()
    {
        if (finish_func != null) finish_func = null;
    }
}

public class TestA : UnitySingleton<TestA>
{
    public void Test()
    {
        Debug.Log(">>>>>>>>TestA : UnitySingleton<TestA>");
    }
}