﻿using FairyGUI;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Runtime.InteropServices;
using System.Text;
using UnityEngine;
using XLua;

public class LuaManager : UnitySingleton<LuaManager>
{
    public LuaEnv luaenv;
    private void Awake()
    {
        Debug.Log(">>>>LuaManager awake..");

        luaenv = new LuaEnv();

        InitLuaPath();
        AddLuaLoader();

    }
    public void Start() 
    {
          StartLuaLogic();
    }


    public void Release() 
    {

        luaenv.Dispose();
        Debug.Log(">>>>LuaManager release..");

    }
    public object[] CallFunction(string module, string function, params object[] args)
    {
        LuaManager.Instance.luaenv.DoString(string.Format("require '{0}'", module));
        LuaFunction func = luaenv.Global.Get<LuaFunction>(function);
        if (func != null)
        {
            return func.Call(args);
        }
        return null;
    }
    #region Init
    void AddLuaLoader()
    {
        luaenv.AddLoader(LuaFileUtils.Instance.ReadFile);
    }
     void InitLuaPath()
     {
         if (AppConst.DebugMode)
         {
             //string rootPath = AppConst.FrameworkRoot;
             AddSearchPath(AppConst.luaDir);
         }
         //else
         //{
         //    AddSearchPath(Util.DataPath + "lua");
         //}
     }
	#endregion

    public void Update() { }
     
    //Application.dataPth + "/Lua"
    public void AddSearchPath(string fullPath)
    {
        if (!Path.IsPathRooted(fullPath))
        {
            throw new LuaException(fullPath + " is not a full path");
        }


        fullPath = ToPackagePath(fullPath);
        LuaFileUtils.Instance.AddSearchPath(fullPath);
    }

    //Application.dataPth + "/Lua" + "/?.lua"
    string ToPackagePath(string path)
    {
        //StringBuilder sb = StringBuilderCache.Acquire();
        StringBuilder sb = new StringBuilder();
        sb.Append(path);
        sb.Replace('\\', '/');

        if (sb.Length > 0 && sb[sb.Length - 1] != '/')
        {
            sb.Append('/');
        }

        sb.Append("?.lua");
        //return StringBuilderCache.GetStringAndRelease(sb);
        return sb.ToString();
    }

    void InitUISetting()
    {
#if UNITY_WEBPLAYER || UNITY_WEBGL || UNITY_STANDALONE_WIN || UNITY_STANDALONE_OSX || UNITY_EDITOR
        CopyPastePatch.Apply();
#endif
        UIConfig.defaultFont = "afont";
        GRoot.inst.SetContentScaleFactor(1152, 648, UIContentScaler.ScreenMatchMode.MatchHeight);
    }

    void StartLuaLogic()
    {
        UnityEngine.Debug.Log(" Start Lua : Logic");
        LuaManager.Instance.luaenv.DoString("require 'Main'");
        Action main = LuaManager.Instance.luaenv.Global.Get<Action>("Main");
        main();
    }
}

